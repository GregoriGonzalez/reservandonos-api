## Creating Template AWS
- aws ses create-template --cli-input-json file://path/to/update_template.json

## update Template AWS
- aws ses update-template --cli-input-json file://path/to/update_template.json
- aws ses update-template --cli-input-json file://reservation-offline-confirmed-en.json
- aws ses create-template --cli-input-json file://reservation-offline-pending-en.json
- aws ses delete-template --template-name ReservationOfflinePendingEN
- aws ses create-template --cli-input-json file://reservation-online-cancelled-en.json

## Standar
- Todas las funciones deben ser de preferencia asincronas
- Usar for-of en lugar de foreach
- los controladores deben ser declarados asincronos
- los controladores deben usar try catch de forma global
- Usar el manejador HandleError del helper error en la clausula catch global para los controladores
- Todas las llamadas a funciones deben tener await
- Usar sintaxis async/await en lugar de then/catch
- Los modulos solo deben exportar las funciones que son necesarias
- Los modulos deben estar definidos como: ejemplo.module.js y exportado como EjemploModule
- Evitar practicas de hardCode, en su lugar definir constantes en el archivo constant.helper.js
- Para declarar funciones preferiblemente usar el formanto de funciones arrow. Example: const suma = (a, b) => { };
#### Definición de Controladores
- Las funciones van declaradas despues de las dependencias
- Las rutas van definidad despues de las funciones
- Las rutas deben estar agrupadas por su verbo, GET, POST, PUT, DELETE y ordenadas de mayor a menor cantidad de secciones en su url ejemplo "/casa/carro" despues "/casa"
- si varias rutas tienen definidas las mismas secciones las estaticas deben ir antes de las dinamicas ejemplo "/casa/carro" despues /:idCasa/carro para evitar el solapamiento de rutas.

#### Extensiones de archivos
- Los archivos creados deben poseer extension que corresponda a su capa de dominio ejemplo: ejemplo.helper.js, main.controller.js, modelo.model.js

#### Rutas para balance de carga
- Todos endpoint usados para activar procesos manuales deberán estar en en la ruta raiz '/manual-processes' controlador manualProcesses.js
- Todas los pdfs construidos a nivel de servidor deberan estar en la raiz '/pdf'
