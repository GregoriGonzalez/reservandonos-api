const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const helmet = require('helmet');
const path = require("path");
const niv = require('node-input-validator');

const app = express();

const auth = require('./middlewares/auth');
const decodeJson = require('./middlewares/decodeJson');
const authentication = require('./controllers/auth');
const email = require('./controllers/email');
const users = require('./controllers/users');
const plans = require('./controllers/plans');
const places = require('./controllers/places');
const zones = require('./controllers/zones');
const layouts = require('./controllers/layouts');
const services = require('./controllers/services');
const costumers = require('./controllers/costumers');
const settings = require('./controllers/settings');
const reservations = require('./controllers/reservations');
const partners = require('./controllers/partners');
const payments = require('./controllers/payments');
const reviews = require('./controllers/reviews');
const giftCards = require('./controllers/gift-cards');
const googleReserve = require('./controllers/google-reserves');
const testing = require('./controllers/testing');
const notifications = require('./controllers/notifications');
const twilio = require('./controllers/twilio');
const conekta = require('./controllers/conekta');
const shorter = require('./controllers/shorter.js');
const groupsBusiness = require('./controllers/groups-business.js');
const countries = require('./controllers/country.js');
const placeLock = require('./controllers/place-locks.js');
const billings = require('./controllers/billings.js');
const sellers = require('./controllers/sellers');
const home = require('./controllers/home');
const registeredCustomer = require('./controllers/registeredCustomer');
const login = require('./controllers/login');
const zoho = require('./controllers/zoho-integration');
const sponsors = require('./controllers/sponsor');
const reports = require('./controllers/reports');
const webHook = require('./controllers/webHook');
const applicationSettings = require('./controllers/applicationSettings');
const promotions = require('./controllers/promotions');
const coupons = require('./controllers/coupons');
const pdf = require('./controllers/pdf');
const manualProcesses = require('./controllers/manualProcesses');
const sponsorManual = require('./controllers/sponsorManual');

const createLocaleMiddleware = require('express-locale');
const trans = require('./helpers/translate');


niv.extendMessages({
  required: 'El(La) :attribute no debe estar vacio.',
  email: 'E-mail debe ser una direccion valida.',
  alpha: 'El valor de :attribute debe contener solo letras.',
  phoneNumber: 'El número de teléfono no es valido.'
}, 'es');

niv.extendMessages({
  required: 'The :attribute is required',
  email: 'E-mail must be a valid email.',
  alpha: 'The :attribute must contain only letters.',
  phoneNumber: 'The phone number is invalid.'
}, 'en');

app.all('/*', function(req, res, next) {
  console.log('Proccess: ', process.pid, ' Request: ', req.url, 'Method: ', req.method);
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Authorization, Cache-Control, Pragma"); //  x-xsrf-token
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH");
  if(req.method === 'OPTIONS') {
    return res.end();
  }
  next();
});
app.use(express.static(path.join(__dirname , '/public')));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(multer().single('file'));
app.use(decodeJson.decode);
app.use(createLocaleMiddleware({
  "priority": ["accept-language", "default"],
  "default": "en_US"
}));
app.use(trans.startPolyglot);

app.get('/', async (req, res) => {
  return res.json('API AVAILABLE');
});
app.use('/registeredCustomer', registeredCustomer);
app.use('/login', login);
app.use('/auth', authentication);
app.use('/v3', auth.isAuthenticatedGoogle, googleReserve);
app.use('/manual-processes', manualProcesses);
app.use('/v1', webHook);
app.use(auth.decode);
// app.use(auth.isAuthenticated);
app.use('/unsubscribe', email)
app.use('/users', users);
app.use('/plans', plans);
app.use('/places', places);
app.use('/zones', auth.hasRole('ADMIN', 'MANAGER', 'HOSTESS'), zones);
app.use('/layouts', auth.hasRole('ADMIN', 'MANAGER'), layouts);
app.use('/services', /* auth.hasRole('ADMIN', 'MANAGER'), */ services);
app.use('/costumers', auth.hasRole('ADMIN', 'MANAGER', 'WAITLIST', 'HOSTESS', 'RECEPCIONIST', 'FASTFOOD'), costumers);
app.use('/settings', /*auth.hasRole('ADMIN', 'MANAGER'),*/ settings);
app.use('/reservations', reservations);
app.use('/partners', partners);
app.use('/payments', payments);
app.use('/reviews', reviews);
app.use('/gift-cards', giftCards);
app.use('/testing', testing);
app.use('/notifications', notifications);
app.use('/twilio', twilio);
app.use('/conekta', conekta);
app.use('/s', shorter);
app.use('/groups', groupsBusiness);
app.use('/countries', countries);
app.use('/placeLock', placeLock);
app.use('/billings', billings);
app.use('/sellers', sellers);
app.use('/home', home);
app.use('/zoho', zoho);
app.use('/sponsors', [auth.hasRole('ADMIN')], sponsors);
app.use('/sponsor-manual', sponsorManual);
app.use('/reports', reports);
app.use('/application-settings', applicationSettings);
app.use('/promotions', promotions);
app.use('/coupons', coupons);
app.use('/pdf', pdf);

app.use(function(err, req, res, next) {
  console.log('Middleware ERROR: ' + err);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json(res.locals.error);
});

module.exports = app;
