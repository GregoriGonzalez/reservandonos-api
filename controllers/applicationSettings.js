const express = require('express');
const router = express.Router();
const models = require('../models');
const environment = require('../config/env');
const {HandleError} = require("../helpers/error.helper");

router.get('/contact-mobile', async (req, res) => {
  try {
    return res.json({error: false, data: {whatsappContact: 'https://wa.me/' + environment.contact.whatsapp}})
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/', async (req, res) => {
  try {
    const {placeId} = req.query;
    if (placeId) {
      const place = await models.Place.scope('withStateAndCountry').findByPk(placeId);
      if (place) {
        const applicationSettings = await models.ApplicationSettings.findOne({
          where: {
            countryId: place.states?.country?.id
          }
        });
        return res.json(applicationSettings);
      } else {
        return res.json({error: true, message: 'Place not found'});
      }
    } else {
      return res.json({error: true, message: 'placeId is required'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
