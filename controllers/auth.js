const express = require('express');
const router = express.Router();
const AuthMiddleware = require('../middlewares/auth');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const models = require('../models');
const Op = models.Sequelize.Op;
const sequelize = models.sequelize;
const env = require('../config/env');
const Sanitize = require("express-validator");
const {Validator} = require("node-input-validator");
const Redis = require("../redis");
const {emailModule} = require('../helpers/email.helper');
const {sendSMS} = require('../helpers/sms');
const encryption = require('../helpers/encryption');
const cutsModule = require('../helpers/cuts.helper');
const {ROLES, ERROR_CODE_APP} = require("../helpers/constants.helper");
const {HandleError} = require("../helpers/error.helper");
const {generateRandomNumber, onlyMessage} = require("../helpers/utils.helper");

const generateToken = async (user, rememberme, type = 'booker', placeId = null) => {
  const payload = {
    id: user.id,
    email: user.email,
    fullname: user.fullname,
    profileImage: user.profileImage,
    genre: user.genre,
    placeLogo: user.logoImage ? user.logoImage : user.partner ? user.partner.logoImage : null,
    placeId: placeId ? placeId : null,
    partnerId: user.partnerId,
    role: user.role,
    unreadedNotificationsCount: user.unreadedNotificationsCount, // TODO ¿esto realmente tiene que ir aquí?
    darkMode: user.darkMode,
    theme: user.place ? user.place.theme : null,
    accountType: type,
    adminPartner: user.adminPartner
  };
  return jwt.sign(payload, env.jwtSecret, {
    expiresIn: rememberme ? '4w' : '4w'
  });
};

router.get('/users-notification', async (req, res) => {
  try {
    const placeUsers = await models.User.findAll({
      attributes: ['fullname', 'email', 'phone'],
      include: [
        {
          model: models.Place,
          as: 'places',
          attributes: ['id'],
          required: true,
          through: {
            where: {
              placeId: 1187
            }
          }
        }]
    });
    return res.json(placeUsers);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/lock', async (req, res) => {
  try {
    const bloked = await models.PlaceLock.find({
      where: {
        placeId: req.query.placeId,
        [Op.or]: [
          {date: req.query.date},
          {
            [Op.and]: [
              {date: {[Op.lte]: req.query.date}},
              {dateEnd: {[Op.gte]: req.query.date}}
            ]
          }
        ],
        [Op.and]: [
          {start: {[Op.lte]: req.query.time}},
          {end: {[Op.gte]: req.query.time}}
        ]
      }
    });
    return res.json(bloked);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/cuts', async (req, res) => {
  try {
    await cutsModule.updateRsvCutsLates();
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/email-exists/:email', async (req, res, next) => {
  try {
    const query = req.query;
    if (query.settings) {
      const placeId = query.placeId;
      const body = {userExists: false};
      const user = await models.User.getByEmailAndPlaceId(req.params.email, placeId, {
        attributes: ['id', 'fullname', 'address', 'phone', 'genre', 'profileImage', 'role'],
      });
      if (user) {
        body.userExists = true;
        if (placeId) {
          if ([ROLES.MANAGER, ROLES.HOSTESS, ROLES.RECEPCIONIST, ROLES.CASHIER, ROLES.FASTFOOD].includes(user.dataValues.role)) {
            body.user = await user.populateNotification();
            body.existsInPlace = user.usersPlaces.length;
          } else {
            body.restricted = true;
          }
        }
      }
      return res.json(body);
    }
    const count = await models.User.count({
      where: {email: req.params.email},
    });
    return res.json({exists: count > 0});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/register/settings', async (req, res, next) => {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const placeId = req.body.placeId;
    let place = await models.Place.findByPk(placeId);
    await place.update({billingFrequencyId: req.body.billigFrequencyId}, {transaction});
    for (const user of req.body.users) {
      if (!user.id) {
        const hash = await bcrypt.hash(user.authUser ? user.password : 'abcd4321', 10);
        const userNew = await models.User.create({
          email: user.email,
          password: hash,
          fullname: `${user.fullname}`,
          phone: `${user.phone}`,
          role: ROLE.MANAGER,
          verified: 0,
          acceptTerms: 1, // TODO poner un check en la pantalla de registrar usuario
          placeId: null,
          darkMode: 0,
          langCode: 'es',
        }, {transaction});
        await models.UsersPlaces.create({
          userId: userNew.id,
          placeId,
          notificationEmail: user.notificationEmail,
          notificationWhatsApp: user.notificationWhatsApp
        }, {transaction})
      } else {
        const User = await models.User.findOne({where: {[Op.and]: [{id: user.id}, {email: user.email}]}});
        if (User) {
          if (user.authUser === false) {
            delete user.password;
            delete user.authUser;
          }
          await User.update({...user}, {transaction});
          const UsersPlaces = await models.UsersPlaces.findOne({where: {placeId, userId: user.id}});
          if (UsersPlaces) {
            await UsersPlaces.update({
              notificationEmail: user.notificationEmail,
              notificationWhatsApp: user.notificationWhatsApp
            }, {transaction})
          } else {
            await models.UsersPlaces.create({
              placeId,
              userId: user.id,
              notificationEmail: user.notificationEmail,
              notificationWhatsApp: user.notificationWhatsApp
            }, {transaction});
          }
        }
      }
    }
    await transaction.commit();
    return res.status(200).json({code: 200, message: 'Proceso exitoso'});
  } catch (e) {
    if (transaction) {
      await transaction.rollback();
    }
    HandleError(req, res, e);
  }
});

router.post('/register', async (req, res, next) => {
  let transaction;
  try {
    const existEmail = await models.User.findOne({where: {email: req.body.email}});
    if (existEmail) {
      return res.status(401).json({message: 'email ya registrado'});
    }

    let place;
    if (!req.body.placeId) {
      place = await models.Place.create({
        name: req.body.restaurant,
        timezone: "America/Mexico_City",
        monthlyPlanPrice: 0,
        sentPersonPrice: 0,
        advancePaymentPolicyEnable: 0,
        cancellationPolicyEnable: 0,
        priorityPaymentWidget: '',
        isActive: 0,
        enableGoogleReserve: 0,
        methodReserve: '',
        ownerId: 1,
        planId: 1,
        dueDate: moment().format()
      });
    }

    let validationRegister;
    if (req.body['placeId']) {
      const encrypt = encryption.encrypt(`${req.body['placeId']}/${req.body['restaurant']}/${req.body['email']}`);
      validationRegister = req.body['key'] === encrypt;
      if (!validationRegister) {
        return res.status(401).json({message: 'La información no ha sido verificada.'});
      }
    }

    if ((req.body['placeId']) || (place && place['id'])) {
      let id;
      if (req.body.reservandonosUser) {
        id = null;
      } else {
        id = req.body['placeId'] ? req.body['placeId'] : place['id'];
      }

      transaction = await sequelize.transaction();
      const hash = await bcrypt.hash(req.body.password, 10);
      const userCreated = await models.User.create({
        email: req.body.email,
        password: hash,
        fullname: `${req.body.firstname} ${req.body.lastname}`,
        phone: req.body.phone,
        role: ROLES.MANAGER,
        verified: 0,
        acceptTerms: 1,
        placeId: null,
        darkMode: 0,
        langCode: 'es'
      }, {transaction});
      if (!id) {
        let relationPlaceId = req.body['placeId'] ? req.body['placeId'] : place && place['id'] ? place['id'] : 0;
        if (relationPlaceId !== 0) {
          await models.UsersPlaces.create({
            userId: userCreated.id,
            placeId: relationPlaceId,
            notificationEmail: req.body.notificationEmail || false,
            notificationWhatsApp: req.body.notificationWhatsApp || false,
          }, {transaction});
        }
      }
      await transaction.commit();
      return res.json({success: true});
    } else {
      return res.status(401).json({message: 'placeId is required'});
    }
  } catch (e) {
    if (transaction) {
      await transaction.rollback();
    }
    HandleError(req, res, e);
  }
});

router.post('/signup', async (req, res, next) => {
  try {
    req.body.password = await bcrypt.hash(req.body.password, 10);
    const user = await models.User.create(req.body);
    const token = await generateToken(user, true);
    return res.json({access_token: token, langCode: user.langCode});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/login', async (req, res) => {
  try {
    let type = req.body['type'] ? req.body['type'] : 'booker';
    const user = await models.User.findOne({
      where: {
        [Op.or]: [
          {email: req.body.email},
          {email: req.body.username}
        ]
      },
      attributes: ['id', 'email', 'password', 'fullname', 'profileImage', 'partnerId', 'role', 'adminPartner', 'unreadedNotificationsCount', 'genre', 'darkMode', 'langCode'], // placeId
      include: [
        {model: models.Place, as: 'places'},
        {model: models.Partner, as: 'partner'}
      ]
    });
    if (user && await bcrypt.compare(req.body.password, user.password)) {
      if (user.places && user.places.length > 0) {
        let placeType = user.places.filter(place => place.typeAccount == type && place.isActive);
        if (placeType.length > 0) {
          const place = placeType.find(place => place.isActive);
          if (place) {
            user.logoImage = place.logoImage;
            const token = await generateToken(user, req.body.rememberme, req.body['type'], place.id);
            return res.json({
              access_token: token,
              paymetGatewayPublicKey: place.paymentGatewayPublicKey,
              langCode: user.langCode
            });
          } else {
            return res.status(403).json({error: 'payment'});
          }
        } else {
          return res.status(402).json({error: 'permision'});
        }
      } else {
        if ([ROLES.ADMIN, ROLES.TELEMARKETING, ROLES.SPONSOR].includes(user.role)) {

          if (ROLES.SPONSOR === user.role) {
            user.logoImage = '/assets/images/logo-rsv-blanco.png'
          }
          const token = await generateToken(user, req.body.rememberme, req.body['type']);
          return res.json({access_token: token, langCode: user.langCode});
        } else {
          return res.status(400).json({error: 'places'});
        }
      }
    } else {
      return res.status(401).json({error: 'credentials'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/change-place', async (req, res) => {
  try {
    if (!req.body.user) res.status(401);
    const user = req.body.user;
    if (user.placeId !== null) {
      const place = await models.Place.findByPk(user.placeId, {
        attributes: ['paymentGatewayPublicKey', 'logoImage', 'primaryColor', 'secondaryColor']
      });
      user.logoImage = place.logoImage;
      const token = await generateToken(user, true, user.accountType ? user.accountType : 'booker', user.placeId);
      return res.json({
        access_token: token,
        paymentGatewayPublicKey: place.paymentGatewayPublicKey,
        langCode: user.langCode
      });
    } else {
      return res.json({access_token: token, langCode: user.langCode});
    }
  } catch (err) {
    HandleError(req, res, err);
  }
});

router.post('/password-reset', async (req, res, next) => {
  try {
    const user = await models.User.findOne({
      where: {
        email: req.body.email
      }
    });
    if (user) {
      await emailModule.passwordReset(user);
      return res.json({success: true});
    } else {
      return res.sendStatus(404);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/password-reset/token-validity/:token', async (req, res, next) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    const valid = token && moment().isBefore(token.dueDate);
    return res.json({valid});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/password-reset/new-password/:token', async (req, res, next) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    if (token && moment().isBefore(token.dueDate)) {
      const password = await bcrypt.hash(req.body.password, 10);
      const user = await models.User.findByPk(token.userId);
      user.password = password;
      await user.save();
      return res.json({success: true});
    } else {
      return res.sendStatus();
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/login-app',
  Sanitize.body(['code', 'step']).isNumeric().toInt(),
  async (req, res) => {
    try {
      const PHONE_DEMO = '+5212345678';
      const CODE_DEMO = 12345;
      const {step, phone, code} = req.body;

      const user = await models.User.findOne({
        where: {
          phone,
          role: {
            [Op.in]: [ROLES.MANAGER, ROLES.HOSTESS]
          }
        },
        attributes: [
          'id', 'email', 'password', 'fullname', 'address', 'phone', 'genre',
          'profileImage', 'role', 'adminPartner', 'langCode'
        ],
      });
      if (step === 1) {
        if (user) {
          let temporalCode = CODE_DEMO;
          if (phone !== PHONE_DEMO) {
            temporalCode = generateRandomNumber(10000, 99999);
            sendSMS(phone, `Reservándonos Partners, código de autenticación: ${temporalCode}`);
          }
          await Redis.setRegister({temporal: temporalCode}, `login-app-${phone}`, 180);
          return res.json({error: false, message: "Code sms sent successful"});
        }
        return res.status(400).json({error: true, message: "Phone number not found"});
      } else {
        const {temporal} = await Redis.getRegister(`login-app-${phone}`);
        if (code === temporal && user) {
          const places = await user.getPlaces({
            attributes: [
              'id', 'name'
            ],
            where: {
              typeAccount: 'rsv',
              isActive: true
            },
            raw: true
          });
          if (places.length) {
            const placesList = places.map(item => ({id: item.id, name: item.name}));
            const token = await generateToken(user, true, 'rsv', placesList[0].id);
            const body = {
              session: {...user.dataValues, token},
              place: {
                current: placesList[0],
                list: placesList
              }
            };
            return res.json(body);
          }
          return res.status(400).json({
            error: true,
            code: ERROR_CODE_APP.USER_WITH_NO_ASSOCIATED_PLACES,
            message: 'This user is not associate with any places'
          });
        }
        return res.status(400).json({
          error: true,
          code: ERROR_CODE_APP.ACCESS_CODE_INVALID,
          message: 'Code invalid'
        });
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.post('/change-place-app',
  AuthMiddleware.decode,
  AuthMiddleware.hasRole(ROLES.MANAGER, ROLES.HOSTESS),
  async (req, res, next) => {
    const rules = {
      nextPlaceId: 'required|numeric'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const {nextPlaceId} = req.body;
      const user = await models.User.findByPk(req.user.id, {
        attributes: [
          'id', 'email', 'password', 'fullname', 'address', 'phone', 'genre',
          'profileImage', 'role', 'adminPartner', 'langCode'
        ],
        include: [{
          model: models.Place,
          as: 'places',
          attributes: [
            'id', 'name', 'logoImage'
          ],
          where: {
            id: nextPlaceId,
            typeAccount: 'rsv',
            isActive: true
          }
        }]
      });
      if (user.places?.length) {
        const token = await generateToken(user, true, 'rsv', nextPlaceId);
        const body = {
          session: {...user.dataValues, token},
        };
        return res.json(body);
      }
      return res.status(400).json({error: true, message: 'User not registered in the place'});
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

module.exports = router;
module.exports.generateToken = generateToken;
