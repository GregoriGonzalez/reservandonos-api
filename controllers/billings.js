const express = require('express');
const router = express.Router();
const moment = require('moment');
const models = require('../models');
const Op = models.Sequelize.Op;
const {typeChargesToApplyInRangeDate} = require("../helpers/query.helper");
const cutsModule = require('../helpers/cuts.helper');
const {sum, divide, multiply, getCommissionByChargeOfReservation} = require('../helpers/calculate.helper');
const {RESERVATION_STATUS, BILLINGS_LABELS, BILLINGS_STATUS} = require("../helpers/constants.helper");
const {rangeByMonthsAndYear} = require("../helpers/utils.helper");
const {HandleError} = require("../helpers/error.helper");

router.get('/generateAdvertency', async (req, res) => {
  try {
    // let response = await cutsModule.generateAdvertency();
    await cutsModule.generateCuts('2021-11-16');
    await cutsModule.generateCuts('2021-12-01');
    return res.json();
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/:placeId', async (req, res) => {
  try {
    const cuts = await models.Billings.findAll({
      attributes: ['id', 'dateStart', 'dateEnd', 'status', 'placeId', 'paymentUrl', 'appliedTax'],
      include: ['payments'],
      where: {
        placeId: req.params.placeId
      },
      order: [['id', 'DESC']],
    });
    return res.json(cuts);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservation/cuts', async (req, res) => {
  try {
    let order;
    let where = {
      partnerId: 1
    };
    if (req.query.sortDir) {
      switch (req.query.sortBy) {
        case 'place':
          order = [['place', 'name', req.query.sortDir]];
          break;
        case 'partner':
          order = [['partner', 'name', req.query.sortDir]];
          break;
        case 'creator':
          order = [['creator', 'fullname', req.query.sortDir]];
          break;
        case 'fullname':
          order = [['costumer', 'fullname', req.query.sortDir]];
          break;
        case 'phone':
          order = [['costumer', 'phone', req.query.sortDir]];
          break;
        case 'email':
          order = [['costumer', 'email', req.query.sortDir]];
          break;
        case 'date':
          order = [['date', req.query.sortDir], ['start', req.query.sortDir]];
          break;
        case 'origin':
          order = [['origin', req.query.sortDir]];
          break;
        default:
          order = [[req.query.sortBy, req.query.sortDir]];
          break;
      }
    }
    if (req.query.placeId) {
      where.placeId = req.query.placeId;
    }
    if (req.query.dateStart && req.query.dateEnd) {
      where.date = {
        [Op.gte]: moment(req.query.dateStart).startOf('day'),
        [Op.lte]: moment(req.query.dateEnd).endOf('day')
      };
    }
    if (req.query.status) {
      where.status = req.query.status;
    }

    const reservations = await models.Reservation.findAndCountAll({
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name']
        },
        {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['fullname', 'countryCode', 'phone', 'email', 'validEmail']
        },
        {
          model: models.User,
          as: 'creator',
          attributes: ['fullname']
        },
        {
          model: models.Partner,
          as: 'partner',
          attributes: ['name', 'sentPersonFee']
        },
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor'
        },
        {
          model: models.Billings,
          as: 'billings',
          attributes: ['id', 'status']
        }
      ],
      where: where,
      order: order,
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    reservations['place'] = await models.Place.findOne({
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          required: false,
          ...typeChargesToApplyInRangeDate(req.query.dateStart, req.query.dateEnd)
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          required: false,
          include: [{
            model: models.Sponsor,
            as: 'sponsor'
          }]
        }
      ],
      where: {id: req.query.placeId}
    });
    return res.json(reservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/profit/cuts/:billingId', async (req, res) => {
  try {
    const {billingId} = req.params;
    const billing = await models.Billings.findByPk(billingId);
    if (billing) {
      const place = await billing.getPlace({
        attributes: ['id', 'name', 'monthlyPlanPrice', 'sentPersonPrice', 'fixedCharge']
      });
      const typeCharges = await place.getTypeCharges({
        ...typeChargesToApplyInRangeDate
      });
      const reservations = await billing.getReservation({
        attributes: ['id', 'date', 'invoiceAmount', 'peopleCount', 'peopleSeating', 'status', 'origin', 'partnerId'],
        include: [
          {
            model: models.ReservationSponsors,
            as: 'reservationSponsor',
            include: [{
              model: models.Sponsor,
              as: 'sponsor'
            }]
          }
        ]
      });
      const defaultValue = {
        iva: 0,
        profitByTypeCharge: 0,
        profitBySponsor: 0,
        profitTotal: 0,
        dinersAttended: 0,
        totalReservation: 0,
        fixedChargeApplied: 0
      };
      const dataByRanges = rangeByMonthsAndYear(
        moment(billing.dateEnd).format('YYYY-MM'),
        moment(billing.dateStart).format('YYYY-MM')
      ).map(item => ({period: item, data: {...defaultValue, status: {}, sponsors: []}}));

      for (const reservation of reservations) {
        const {data} = dataByRanges.find(item => item.period === moment(reservation.date).format('YYYY-MM'));
        if (data.status[reservation.status]) {
          data.status[reservation.status]++;
        } else {
          data.status[reservation.status] = 1;
        }
        data.totalReservation++;
        if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status)) {
          const profitReservation = await getCommissionByChargeOfReservation(reservation, typeCharges, place.fixedCharge);
          data.profitByTypeCharge = await sum(2, data.profitByTypeCharge, profitReservation);

          const dinnersAttended = reservation.peopleSeating > 0 ? reservation.peopleSeating : reservation.peopleCount;
          data.dinersAttended = await sum(2, data.dinersAttended, dinnersAttended);

          for (const rs of reservation.reservationSponsor) {
            data.profitBySponsor = await sum(2, data.profitBySponsor, rs.totalCommission);
            const sponsor = data.sponsors.find(value => value.sponsorId === rs.sponsorId);
            if (sponsor === undefined) {
              data.sponsors.push({
                sponsorId: rs.sponsorId,
                quantity: rs.quantity,
                commission: rs.totalCommission,
                name: rs.sponsor.name,
                price: (rs.quantity !== 0) ? await divide(2, rs.totalCommission, rs.quantity) : 0
              });
            } else {
              sponsor.quantity = await sum(2, sponsor.quantity, rs.quantity);
              sponsor.commission = await sum(2, sponsor.commission, rs.totalCommission);
              sponsor.price = (sponsor.price !== 0) ? sponsor.price : ((rs.quantity !== 0) ? await divide(2, rs.totalCommission, rs.quantity) : 0)
            }
          }
        }
      }

      const general = {...defaultValue, status: {}, sponsors: []};
      for (const {data} of dataByRanges) {
        data.fixedChargeApplied = billing.fixedChargeApplied;
        data.profitTotal = await sum(2, data.profitByTypeCharge, data.fixedChargeApplied, data.profitBySponsor);
        data.iva = await multiply(2, data.profitTotal, await sum(2, 1, billing.appliedTax));

        general.iva = await sum(2, general.iva, data.iva);
        general.profitByTypeCharge = await sum(2, general.profitByTypeCharge, data.profitByTypeCharge);
        general.dinersAttended = await sum(2, general.dinersAttended, data.dinersAttended);
        general.profitBySponsor = await sum(2, general.profitBySponsor, data.profitBySponsor);
        general.profitTotal = await sum(2, general.profitTotal, data.profitTotal);
        general.totalReservation = await sum(2, general.totalReservation, data.totalReservation);
        general.sponsors = await sum (2, general.sponsors, data.sponsors.length > 0);
        general.fixedChargeApplied = await sum(2, general.fixedChargeApplied, data.fixedChargeApplied);
        for (const status of Object.keys(data.status)) {
          if (general.status[status]) {
            general.status[status] = await sum(2, general.status[status], data.status[status]);
          } else {
            general.status[status] = data.status[status];
          }
        }
      }
      return res.json({general, details: dataByRanges});
    } else {
      return res.status(400).json({error: true, message: 'Billing Not Found'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:id/status/', async (req, res) => {
  try {
    const id = req.params.id;
    const status = req.body.status;
    const billing = await models.Billings.findByPk(id);
    if (billing) {
      billing.status = status;
      await billing.save();
      return res.json(billing);
    } else {
      return res.status(400).json({error: true, message: "Billing not found"});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:id/with/label/:label', async (req, res) => {
  try {
    const {id, label} = req.params;
    const billing = await models.Billings.findByPk(id);
    if (billing) {
      if (BILLINGS_LABELS[label]) {
        billing.label = label;
        await billing.save();
        return res.json({error: false, billing});
      } else {
        return res.status(400).json({error: true, message: "Label Invalid"});
      }
    } else {
      return res.status(400).json({error: true, message: "Billing not found"});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/mix-cuts', async (req, res) => {
  try {
    const {cutsId = []} = req.body;
    if (cutsId.length >= 2) {
      const cuts = await models.Billings.findAll({
        where: {
          id: {
            [Op.in]: cutsId
          }
        },
        order: [['dateStart', 'asc']]
      });
      if (cuts.length === cutsId.length) {
        const firstCut = cuts[0];
        const lastCut = cuts[cutsId.length - 1];
        if (cuts.every(cut => cut.placeId === firstCut.placeId)) {
          let canMix = true;
          for (let i = 1; i < cuts.length; i++) {
            const dayFinalCut = moment(cuts[i - 1].dateEnd);
            const dayStartNextCut = moment(cuts[i].dateStart);
            if (!dayFinalCut.isSame(dayStartNextCut.subtract(1, 'days'))) {
              canMix = false;
              break;
            }
          }
          if (canMix) {
            const cutCreated = await models.Billings.create({
              dateStart: firstCut.dateStart,
              dateEnd: lastCut.dateEnd,
              status: BILLINGS_STATUS.PENDING,
              label: null,
              placeId: firstCut.placeId,
              appliedTax: firstCut.appliedTax,
              paymentUrl: null
            });
            if (cutCreated) {
              for (const c of cuts) {
                const reservations = await c.getReservation();
                for (const reservation of reservations) {
                  await reservation.update({billingId: cutCreated.id});
                }
                await c.destroy({force: true});
              }
              return res.json({error: false, cutCreated});
            } else {
              return res.status(400).json({error: true, message: 'Error to the create cut'});
            }
          } else {
            return res.status(400).json({error: true, message: 'The cuts must be consecutive'});
          }
        } else {
          return res.status(400).json({error: true, message: 'All cuts must be of the same place'});
        }
      } else {
        return res.status(400).json({error: true, message: 'At least one cut not found'});
      }
    } else {
      return res.status(400).json({error: true, message: 'At least two cuts are required'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
