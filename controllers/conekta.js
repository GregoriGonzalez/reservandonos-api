const express = require('express');
const router = express.Router();
const {emailModule: email} = require('../helpers/email.helper');
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");

async function saveTransaction(paymentCustomerId, cnktTransId) {
  const place = await models.Place.findOne({where: {paymentCustomerId}, attributes: ['id', 'monthlyPlanPrice']});
  if (place) {
    await models.PlacePayment.create({value: place.monthlyPlanPrice, cnktTransId, placeId: place.id});
    // para activar en el primer pago
    await models.Place.update({isActive: true}, {where: {id: place.id}});
  }
}

router.post('/', async (req, res) => {
  try {
    const data = req.body;
    switch (data.type) {
      case 'subscription.paid':
        await saveTransaction(data.data.object.customer_id, data.data.object.charge_id);
        await email.subscriptionOp(data.data.object.customer_id, false);
        break;
      case 'subscription.payment_failed':
        await email.subscriptionOp(data.data.object.customer_id, true);
        break;
      default:
        break;
    }
    return res.sendStatus(200);
  } catch (e) {
    HandleError(req, res, e);
  }
})

module.exports = router;
