const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const csv = require('csvtojson');
const paymentMiddleware = require('../middlewares/paymentGateway');
const payment = require('../helpers/conekta');
const {HandleError} = require("../helpers/error.helper");

const attributesList = ['id', 'fullname', 'countryCode', 'phone', 'email', 'validEmail', 'emailComplaints', 'subscribedEmails', 'notes', 'tags', 'language', 'paymentCustomerId'];

router.get('/', async (req, res) => {
  try {
    let whereCondition;
    if (req.query.term) {
      whereCondition = {
        placeId: req.user.placeId,
        [Op.or]: [
          {fullname: {[Op.like]: `%${req.query.term}%`}},
          {email: {[Op.like]: `%${req.query.term}%`}},
          {phone: {[Op.like]: `%${req.query.term}%`}},
          {tags: {[Op.like]: `%${req.query.term}%`}},
        ]
      }
    } else {
      whereCondition = {
        placeId: req.user.placeId,
        fullname: {[Op.ne]: ''}
      }
    }
    const costumers = await models.Costumer.findAndCountAll({
      attributes: attributesList,
      where: whereCondition,
      order: [
        [req.query.sortBy, req.query.sortDir]
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    })
    return res.json(costumers);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-name/:name', async (req, res) => {
  try {
    const customers = await models.Costumer.findAll({
      attributes: attributesList,
      include: [{
        model: models.Reservation,
        as: 'reservations',
        attributes: ['date', 'status']
      }],
      where: {
        placeId: req.user.placeId,
        fullname: {[Op.like]: `%${req.params.name}%`},
        [Op.or]: [{email: {[Op.ne]: null}}, {phone: {[Op.ne]: null}}]
      },
      order: [['fullname', 'asc']]
    });
    return res.json(customers);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/by-phone/:phone', async (req, res) => {
  try {
    const costumers = await models.Costumer.findAll({
      attributes: attributesList,
      include: [{
        model: models.Reservation,
        as: 'reservations',
        attributes: ['date', 'status']
      }],
      where: {
        placeId: req.user.placeId,
        phone: {[Op.like]: `%${req.params.phone}%`}
      },
      order: [['phone', 'asc']]
    });
    return res.json(costumers);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-email/:email', async (req, res) => {
  try {
    const costumers = await models.Costumer.findAll({
      attributes: attributesList,
      include: [{
        model: models.Reservation,
        as: 'reservations',
        attributes: ['date', 'status']
      }],
      where: {
        placeId: req.user.placeId,
        email: {[Op.like]: `%${req.params.email}%`}
      },
      order: [['email', 'asc']]
    });
    return res.json(costumers);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const costumer = await models.Costumer.findOne({
      where: {
        id: req.params.id,
        placeId: req.user.placeId
      },
    })
    return res.json(costumer);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/cards/:paymentCustomerId', paymentMiddleware.privateKey, async (req, res) => {
  try {
    const customer = await payment.customer.get(req.params.paymentCustomerId, req.paymentGateway.privateKey);
    return res.json(customer.payment_sources.toObject());
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res) => {
  try {
    req.body.placeId = req.user.placeId;
    req.body.subscribedEmails = true;
    const customerFind = await models.Costumer.findOne({
      where: {
        email: req.body.email,
        placeId: req.body.placeId,
        phone: req.body.phone
      },
      raw: true
    });
    if (!customerFind) {
      const costumer = await models.Costumer.create(req.body);
      return res.json(costumer);
    } else {
      return res.json({
        error: true,
        code: 'duplicateCustomer',
        msg: `Ya existe un cliente con el email ${req.body.email}`
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/csv', async (req, res) => {
  try {
    const file = req.file;
    const costumers = [];
    csv().fromString(file.buffer.toString())
      .subscribe(
        (costumer) => {
          costumer.placeId = req.user.placeId;
          if (costumer.fullname && costumer.email && costumer.phone && costumer.countryCode) {
            costumer.notes = costumer.notes !== 'null' ? costumer.notes : '';
            costumer.tags = costumer.tags !== 'null' ? costumer.tags : '';
            costumers.push(costumer);
          }
        },
        (err) => console.error(err),
        async () => {
          const c = await models.Costumer.bulkCreate(costumers);
          return res.json(c);
        }
      );
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    let costumer = await models.Costumer.findOne({
      where: {
        id: req.body.id,
        placeId: req.user.placeId
      },
    });
    const all = await models.Costumer.findAll({
      where: {
        id: {[Op.ne]: req.body.id},
        placeId: req.user.placeId,
        email: req.body.email,
        phone: req.body.phone
      },
      raw: true
    });
    if (all.length > 0) {
      return res.json({
        error: true,
        code: 'duplicateCustomer',
        msg: `Ya existe un cliente con el email ${req.body.email}`
      });
    } else {
      if (costumer.email != req.body.email) {
        req.body.validEmail = true;
        req.body.emailComplaints = 0;
      }
      costumer = Object.assign(costumer, req.body);
      await costumer.save();
      return res.json(costumer);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
