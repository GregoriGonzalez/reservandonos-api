const express = require('express');
const router = express.Router();
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");
const Redis = require('../redis');

router.get('/', async (req, res) => {
  try {
    const countries = await models.Country.findAll();
    return res.json(countries);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:countryId/states', async (req, res) => { //142 son los estados de mexico
  try {
    const data = await Redis.getRegister(`states-${req.params.countryId}`);
    if (data) {
      console.log('REDIS-GetStatesByCountryId');
      return res.json(data);
    }
    const states = await models.State.findAll({
      where: {countryId: req.params.countryId}
    });

    if (states) {
      Redis.setRegister( states, `states-${req.params.countryId}`);
    }
    return res.json(states);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/states/:stateId/cities', async (req, res) => {
  try {
    const {stateId} = req.params;
    const data = await Redis.getRegister(`cities-${stateId}`);
    if (data) {
      console.log('REDIS-getCitiesByStateId');
      return res.json(data);
    }
    const cities = await models.City.findAll({
      where: {stateId: stateId}
    });

    if (cities) {
      Redis.setRegister(cities, `cities-${stateId}`);
    }

    return res.json(cities);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/city/:cityId/colonies', async (req, res) => {
  try {
    const {cityId} = req.params;
    const data = await Redis.getRegister(`colonies-${cityId}`);
    if (data) {
      console.log('REDIS-getColoniesByCityId');
      return res.json(data);
    }

    const colonies = await models.Colony.findAll({
      attributes: ['id', 'name'],
      where: {cityId: cityId},
      order: [
        ['name', 'ASC']
      ]
    });

    if (colonies) {
      Redis.setRegister(colonies, `colonies-${cityId}`);
    }

    return res.json(colonies);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:countryId/states-with-cities', async (req, res) => {
  try {
    const {countryId} = req.params;
    const result = await models.State.findAll({
      attributes: ['id', 'name'],
      include: [{
        model: models.City,
        as: 'Cities',
        attributes: ['stateId', 'id', 'name'],
      }],
      where: {countryId},
      order: [
        ['name', 'ASC'],
        [{model: models.City, as: 'Cities'}, 'name', 'ASC']
      ]
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
