const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const {Validator} = require('node-input-validator');
const {onlyMessage} = require("../helpers/utils.helper");
const moment = require("moment-timezone");
const {ROLES, TYPE_NOTIFICATION_BY_COUPON} = require("../helpers/constants.helper");
const {moduleWhatsApp} = require('../helpers/toolsSendWhatsApp');
const PDF = require("../helpers/pdf.helper");
const {HandleError, InternalProcessError} = require("../helpers/error.helper");
const Notifications = require("../helpers/notifications.helper");
const {emailModule: Emails} = require("../helpers/email.helper");


router.post('/customer', async (req, res) => {
  try {
    req.body.subscribedEmails = true;
    const customerFind = await models.Costumer.findOne({
      where: {
        email: req.body.email,
        placeId: req.body.placeId,
        phone: req.body.phone,
        countryCode: req.body.countryCode
      },
      raw: true
    });

    if (!customerFind) {
      const customer = await models.Costumer.create(req.body);
      return res.json({customer, error: false});
    } else {
      return res.json({customer: customerFind, error: false});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('', async (req, res) => {
  try {
    let code, newCode;
    let customer;
    do {
      let chars = [..."ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
      code = [...Array(10)].map(i => chars[Math.random() * chars.length | 0]).join``;
      newCode = await models.Coupons.findOne({
        where: {
          code: code
        }
      });
    } while (newCode !== null);

    let coupon = {
      code: code,
      promotionId: req.body.promotionId
    }

    if (req.body.costumerId) {
      coupon.costumerId = req.body.costumerId
      customer = await models.Costumer.findByPk(req.body.costumerId);
    }

    if (req.body.registeredCustomerId) {
      coupon.registeredCustomerId = req.body.registeredCustomerId;
      customer = await models.RegisteredCustomer.findByPk(req.body.registeredCustomerId);
      customer.countryCode = customer.lada;
    }

    const promotion = await models.Promotions.findOne({
      where: {
        id: req.body.promotionId
      },
      include: [{
        as: 'place',
        model: models.Place,
        required: true
      }]
    });

    const newCoupon = await models.Coupons.create(coupon);

    const dataWhatsappMessage = {
      fullname: `${customer.fullname}`,
      coupon: `${code}`,
      promotionName: `${promotion.title}`,
      placeName: `${promotion.place.name}`,
      start: `${moment(promotion.start).locale('es').format('DD MMMM YYYY')}`,
      end: `${moment(promotion.end).locale('es').format('DD MMMM YYYY')}`
    };
    moduleWhatsApp.sendCoupon([`${customer.countryCode}${customer.phone}`], dataWhatsappMessage)
      .catch(e => HandleError(res, res, new InternalProcessError(e, dataWhatsappMessage)));;

    Emails.sendCoupon(newCoupon.id)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {code, customer: customer.dataValues})));

    Notifications.dispatchCoupon(TYPE_NOTIFICATION_BY_COUPON.NEW_COUPON, newCoupon.id)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {coupon: newCoupon.dataValues})));

    return res.json({coupon: newCoupon, error: false});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Check if a coupon is valid and belongs to a place
 */
router.get(
  '/validate',
  async (req, res, next) => {
    const validator = new Validator(req.query, {
      code: 'required|string',
      placeId: 'required|numeric'
    });
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const {code, placeId} = req.query;
      const coupon = await models.Coupons.findOne({
        where: {code},
        include: [{
          as: 'promotion',
          model: models.Promotions,
          required: true,
          where: {placeId}
        },
          'costumer',
          'registeredCustomer'
        ]
      });
      return res.json({error: false, coupon});
    } catch (e) {
      HandleError(req, res, e);
    }
  },
);

/**
 * Mark a coupon as redeemed
 */
router.put(
  '/claim',
  async (req, res, next) => {
    const validator = new Validator(req.body, {
      code: 'required|string',
      placeId: 'required|numeric'
    });
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const {code, placeId} = req.body;
      const place = await models.Place.findByPk(placeId);
      if (place) {
        const coupon = await models.Coupons.findOne({
          where: {code},
          include: [{
            as: 'promotion',
            model: models.Promotions,
            required: true,
            where: {placeId}
          },
            'costumer',
            'registeredCustomer'
          ]
        });
        if (coupon) {
          if (coupon.redeemed) {
            return res.status(400).json({error: true, message: 'This coupon is redeemed'})
          }

          await coupon.update({
            redeemed: true,
            dateRedeemed: moment().tz(place.timezone).format('YYYY-MM-DD hh:mm:ss')
          });

          Notifications.dispatchCoupon(TYPE_NOTIFICATION_BY_COUPON.REDEEMED_COUPON, coupon.id)
            .catch(e => HandleError(req, res, new InternalProcessError(e, {coupon: coupon.dataValues})));

          return res.json({error: false, coupon});
        } else {
          return res.status(400).json({error: true, message: 'Coupon not found'});
        }
      } else {
        return res.status(400).json({error: true, message: 'Place not found'});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  },
);

/**
 * Get coupons list of a place
 */
router.get(
  '',
  async (req, res, next) => {
    if (req.user.role !== ROLES.ADMIN) {
      const validator = new Validator(req.query, {
        placeId: 'required|numeric'
      });
      if (await validator.check()) {
        next();
      } else {
        return res.status(422).send(onlyMessage(validator.errors));
      }
    } else {
      next();
    }
  },
  async (req, res) => {
    try {
      const {
        placeId,
        term,
        redeemed,
        pageIndex = 0,
        pageSize = 10,
        sortBy = 'createdAt',
        sortDir = 'desc'
      } = req.query;
      let wherePlace = req.user.role === ROLES.ADMIN && placeId === '' ? {} : {placeId};
      let whereCouponCostumer = {};
      let whereCouponRegisteredCustomer = {};
      let order = [];

      if (term) {
        whereCouponCostumer = {
          [Op.or]: [
            {code: {[Op.like]: `%${term}%`}},
            models.Sequelize.where(
              models.Sequelize.col('costumer.fullname'),
              {[Op.like]: `%${term}%`}
            )
          ]
        };
        whereCouponRegisteredCustomer = {
          [Op.or]: [
            {code: {[Op.like]: `%${term}%`}},
            models.Sequelize.where(
              models.Sequelize.fn('CONCAT', models.Sequelize.col('registeredCustomer.name'), ' ', models.Sequelize.col('registeredCustomer.lastname')),
              {[Op.like]: `%${term}%`}
            )
          ]
        }
      }
      if (redeemed !== 'ALL') {
        whereCouponCostumer.redeemed = redeemed === 'true';
        whereCouponRegisteredCustomer.redeemed = redeemed === 'true';
      }
      switch (sortBy) {
        case 'place':
          order.push(['promotion', 'place', 'name', sortDir !== '' ? sortDir : 'asc']);
          break;
        case 'promotionTitle':
          order.push(['promotion', 'title', sortDir !== '' ? sortDir : 'asc']);
          break;
        case 'couponCode':
          order.push(['code', sortDir !== '' ? sortDir : 'asc']);
          break;
        case 'dateRedeemed':
          order.push(['dateRedeemed', sortDir !== '' ? sortDir : 'asc']);
          break;
        case 'createdAt':
          order.push(['createdAt', sortDir !== '' ? sortDir : 'asc']);
          break;
      }

      const couponsWithCustomer = await models.Coupons.findAll({
        attributes: ['id'],
        include: [
          {
            as: 'promotion',
            model: models.Promotions,
            attributes: [],
            required: true,
            where: wherePlace
          },
          {
            as: 'costumer',
            model: models.Costumer,
            attributes: [],
            required: true
          }
        ],
        where: whereCouponCostumer
      });
      const couponsWithRegisterCustomer = await models.Coupons.findAll({
        attributes: ['id'],
        include: [
          {
            as: 'promotion',
            model: models.Promotions,
            attributes: [],
            required: true,
            where: wherePlace
          },
          {
            as: 'registeredCustomer',
            model: models.RegisteredCustomer,
            attributes: [],
            required: true
          }
        ],
        where: whereCouponRegisteredCustomer
      });
      const coupons = await models.Coupons.findAndCountAll({
        include: [
          {
            as: 'promotion',
            model: models.Promotions,
            attributes: ['id', 'title', 'imageUrl'],
            required: true,
            include: [{
              as: 'place',
              model: models.Place,
              attributes: ['id', 'name']
            }]
          },
          {
            as: 'costumer',
            model: models.Costumer,
            attributes: ['id', 'fullname'],
            required: false
          },
          {
            as: 'registeredCustomer',
            model: models.RegisteredCustomer,
            attributes: ['id', 'name', 'lastname'],
            required: false
          }
        ],
        where: {
          id: {
            [Op.in]: [
              ...couponsWithCustomer.map(value => value.id),
              ...couponsWithRegisterCustomer.map(value => value.id)
            ]
          }
        },
        order: order,
        offset: (+pageIndex * +pageSize) || 0,
        limit: +pageSize || 10
      });
      return res.json({
        error: false,
        count: coupons.count,
        coupons: coupons.rows
      });
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * Get coupons list of a place
 */
router.get(
  '/resume',
  async (req, res, next) => {
    if (req.user.role !== ROLES.ADMIN) {
      const validator = new Validator(req.query, {
        placeId: 'required|numeric'
      });
      if (await validator.check()) {
        next();
      } else {
        return res.status(422).send(onlyMessage(validator.errors));
      }
    } else {
      next();
    }
  },
  async (req, res) => {
    try {
      const {
        placeId
      } = req.query;
      let wherePlace = req.user.role === ROLES.ADMIN && placeId === '' ? {} : {placeId};
      const resume = {
        totalPromotions: 0,
        totalCoupons: 0,
        totalCouponsRedeemed: 0,
        totalCouponsNotRedeemed: 0
      };
      const promotions = await models.Promotions.findAll({
        where: wherePlace,
        include: ['coupons']
      });
      for (const promotion of promotions) {
        resume.totalPromotions++;
        for (const coupon of promotion.coupons) {
          resume.totalCoupons++;
          if (coupon.redeemed) {
            resume.totalCouponsRedeemed++;
          } else {
            resume.totalCouponsNotRedeemed++;
          }
        }
      }

      return res.json({
        error: false,
        resume
      });
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

module.exports = router;
