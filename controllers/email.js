const express = require('express');
const router = express.Router();
const models = require('../models');
const jwt = require('jsonwebtoken');
const {HandleError} = require("../helpers/error.helper");

router.put('/toggleEmail', async (req, res) => {
  try {
    const coded = req.body.coded;
    const newValue = req.body.newValue;
    const decoded = jwt.decode(coded);
    if (decoded) {
      if (decoded.reservationId) {
        await models.Reservation.update(
          {notificationEmail: newValue},
          {where: {id: decoded.reservationId}}
        );
        return res.json('ok');
      } else if (decoded.customerId) {
        await models.Costumer.update(
          {subscribedEmails: newValue},
          {where: {id: decoded.customerId}}
        );
        return res.json('ok')
      } else {
        // TODO return error
      }
    }
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
