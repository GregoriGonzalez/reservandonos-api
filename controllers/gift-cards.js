const express = require('express');
const router = express.Router();
const moment = require('moment');
const models = require('../models');
const Op = models.Sequelize.Op;
const payment = require('../helpers/conekta');
const {emailModule} = require('../helpers/email.helper');
const paymentMiddleware = require('../middlewares/paymentGateway');
const {generateRandomCode} = require('../helpers/util');
const notifications = require('../helpers/notifications.helper');
const io = require('../socket');
const {HandleError, InternalProcessError} = require("../helpers/error.helper");
const Notifications = require("../helpers/notifications.helper");
const {TYPE_NOTIFICATION_BY_GIFT} = require("../helpers/constants.helper");

router.get('/', async (req, res) => {
  try {
    const giftCards = await models.GiftCard.findAndCountAll({
      where: {
        placeId: req.user.placeId,
        redeemed: req.query.redeemed === 'true',
        [Op.or]: [
          {beneficiaryName: {[Op.like]: `%${req.query.term}%`}},
          {email: {[Op.like]: `%${req.query.term}%`}},
          {token: {[Op.like]: `%${req.query.term}%`}}
        ]
      },
      order: [[req.query.sortBy, req.query.sortDir]],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(giftCards);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const giftCard = await models.GiftCard.findOne({
      where: {
        placeId: req.user.placeId,
        id: req.params.id
      }
    });
    return res.json(giftCard);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', paymentMiddleware.privateKey, async (req, res, next) => {
  try {
    const order = await payment.createUniqueCharge(req.paymentGateway.privateKey, req.body.paymentTokenId, req.body.amount, req.body.beneficiaryName, req.body.email, `${req.body.countryCode}${req.body.phone}`);
    req.body.paymentOrderId = order.id;
    req.body.placeId = req.user.placeId;
    req.body.redeemed = false;
    while (true) {
      const token = await generateRandomCode();
      const giftCard = await models.GiftCard.findOne({where: {token: token}});
      if (!giftCard) {
        req.body.token = token;
        break;
      }
    }
    const giftCard = await models.GiftCard.create(req.body);
    // if (process.env.NODE_ENV === 'production') {
    await emailModule.giftCardEmail(giftCard.id);
    // }

    Notifications.dispatchGiftCard(TYPE_NOTIFICATION_BY_GIFT.NEW_GIFT, giftCard.id)
      .catch(e => HandleError(null, null, new InternalProcessError(e)));

    return res.json(giftCard);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:id/redeem', async (req, res) => {
  try {
    const giftCard = await models.GiftCard.findOne({
      where: {
        placeId: req.user.placeId,
        id: req.params.id
      }
    });
    giftCard.redeemed = true;
    giftCard.redeemedAt = moment().utc().format();
    await giftCard.save();
    return res.json(giftCard);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
