const express = require('express');
const router = express.Router();
const models  = require('../models');
const moment = require('moment');
const helpGoogle = require('../helpers/googleReserve')
const Op = models.Sequelize.Op;
const bcrypt = require('bcrypt');
const notificationsHelper = require('../helpers/notifications.helper');
const {emailModule} = require('../helpers/email.helper');
const { moduleWhatsApp } = require('../helpers/toolsSendWhatsApp');
const { moduleBitacora } = require('../helpers/bitacora');
const { RESERVATION_STATUS, ACCOUNT_TYPE, TYPE_NOTIFICATION_BY_RESERVATION} = require('../helpers/constants.helper');
const { ModuleZoho } = require("../helpers/zoho.helper");
const io = require('../socket');

const path = require('path');
const fs = require('fs');
const { google } = require('googleapis');
const {HandleError, InternalProcessError} = require("../helpers/error.helper");
const Notifications = require("../helpers/notifications.helper");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const placeIsBlocked = (place, date, start) => {
  if (place.isReservationsBlocked) {
    if (date >= place.blockedDateStart && date <= place.blockedDateEnd) {
      const time = start;
      return time >= place.blockedHourStart && time <= place.blockedHourEnd;
    } else {
      return false;
    }
  }
  return false;
}

const getLocks = async (placeId) => {
  return models.PlaceLock.findAll({
    where: {
      placeId: placeId,
      [Op.or]: [
        { date: { [Op.gte]: moment().format('YYYY-MM-DD') } },
        {
          [Op.and]: [
            { date: { [Op.lte]: moment().format('YYYY-MM-DD') } },
            { dateEnd: { [Op.gte]: moment().format('YYYY-MM-DD') } }
          ]
        }
      ]
    }
  });
}

const  isBlock = (slotTimeElement, place, locks) => {
  const datetime = moment.unix(slotTimeElement.start_sec).tz(place.timezone);
  const day = datetime.format('YYYY-MM-DD');
  const minuteOfDay = datetime.hour() * 60 +  datetime.minutes();
  let blockesPlacesLock = placeIsBlocked(place, datetime.format('YYYY-MM-DD'), minuteOfDay);

  if (!blockesPlacesLock) {
    for (const lock of locks) {
      if (lock.isInterval &&  moment(day).diff(moment(lock.date)) >= 0 && moment(day).diff(moment(lock.dateEnd)) <= 0) {
        if (minuteOfDay >= lock.start && lock.end >= minuteOfDay) {
          return true;
        }
      }

      if (!lock.isInterval && moment(day).diff(moment(lock.date)) == 0) {
        if (minuteOfDay >= lock.start && lock.end >= minuteOfDay) {
          return true;
        }
      }
    }
  } else {
    return true;
  }
  return false;
}

const IsBlockesPlacesLock = async (place, date, start) => {
  const bloked = await models.PlaceLock.findOne({
    where: {
      placeId: place,
      [Op.or]: [
        { date: date },
        {
          [Op.and]: [
            { date: { [Op.lte]: date } },
            { dateEnd: { [Op.gte]: date } }
          ]
        }
      ],
      [Op.and]: [
        { start: { [Op.lte]: start} },
        { end: { [Op.gte]: start }}
      ]
    }
  });
  return bloked ? true : false;
}

const responseAvailabilityLookup = (available, value, index, array_slot_primary) => {
  return {
    "slot_time": {
      "service_id": array_slot_primary[index].service_id,
      "start_sec": array_slot_primary[index].start_sec,
      "duration_sec": array_slot_primary[index].duration_sec,
      "resource_ids": array_slot_primary[index].resource_ids,
    },
    "available": available
  };
}

const getReserve = (id, res, timezone, update = false, time) => {
  try {
    models.Reservation.findOne({
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: ['fullname', 'email','countryCode', 'phone', 'email', 'userGoogleReserve'],
        paranoid: false
      }],
      where : {
        id: id,
      },
      raw: true
    }).then( async (data) => {
      return res.json({
        booking: await objectResponseReservation(data, id, timezone, update, time)
      });
    });
  } catch (e) {
    console.error({error: e});
    return res.status(500).json({error: true, message: e.message});
  }
}

const objectResponseReservation = async (data, id, timezone, update = false, time) => {
  let name = data['costumer.fullname'].split(' ');
  let obj = {
    booking_id: `${id}`,
    status: data.status === RESERVATION_STATUS.CANCELLED ? 'CANCELED' : data.status === RESERVATION_STATUS.PENDING ? 'PENDING_MERCHANT_CONFIRMATION' : data.status,
    user_information: {
      user_id: data['costumer.userGoogleReserve'] ? data['costumer.userGoogleReserve'] : 0,
      given_name: name.length > 0 ? name[0] : data['costumer.fullname'],
      family_name: name.length > 1 ? name[1] : data['costumer.fullname'],
      telephone: `${data['costumer.countryCode']} ${data['costumer.phone']}`,
      email: data['costumer.email']
    },
    payment_information: {
      prepayment_status: 'PREPAYMENT_NOT_PROVIDED',
    },
    slot: {
      merchant_id: `merch${data.placeId}`,
      service_id: `${data.placeId}-dining`,
      resources: {
        party_size: data.peopleCount
      }
    }
  };
  if (data.status !== RESERVATION_STATUS.CANCELLED) {
    let timeStamp = moment.tz(moment(data.datetime).utc().format('YYYY-MM-DD H:mm:ss'), timezone).utc().unix();
    if (update) {
      obj.slot['start_sec'] = `${Number(time)}`;
    } else {
      obj.slot['start_sec'] = `${Number(timeStamp)}`;
    }
    // obj.slot['duration_sec'] = typeof(data.duration) === 'number' ? `${(data.duration * 60)}` : 1800;
    obj.slot['duration_sec'] = `900`;
  }
  return obj;
}

// Funcion que crea una nueva reserva
const createReserve = (info, date, customer, res) => {
  let infoPlace;
  try {
    models.Place.findOne({
      where: {
        id: info.slot.merchant_id.replace('merch','')
      },
      include: ['cities','states', 'colony', 'typeFood'],
      raw: true
    }).then( place => {
      infoPlace = place;
      date = moment(date).tz(place.timezone);
      models.Reservation.findOne({
        where: {
          placeId: info.slot.merchant_id.replace('merch',''),
          idempotencyToken: info.idempotency_token,
          costumerId: customer.id
        },
        raw: true
      }).then( data => {
        info.slot['confirmation_mode'] = info.slot['confirmation_mode'] ? info.slot['confirmation_mode'] : 'CONFIRMATION_MODE_ASYNCHRONOUS';
        let statusResponseGoogle = info.slot['confirmation_mode'] == 'CONFIRMATION_MODE_ASYNCHRONOUS' ? 'PENDING_MERCHANT_CONFIRMATION' : 'CONFIRMED';
        if (data !== null) {
          return res.json({
            booking: {
              booking_id: `${data.id}`,
              slot: info.slot,
              user_information: info.user_information,
              status: statusResponseGoogle,
              payment_information: info.payment_information
            }
          });
        } else {
          let statusResponse =  info.slot['confirmation_mode'] == 'CONFIRMATION_MODE_ASYNCHRONOUS' ? 'PENDING' : 'CONFIRMED';
          let reservationObject = {
            datetime: date.format('YYYY-MM-DD HH:mm:ss'),
            date: date.format('YYYY-MM-DD'),
            start: moment.duration(date.format('H:mm')).asMinutes(),
            end: moment.duration(date.format('H:mm')).asMinutes() + (Number(info.slot.duration_sec) / 10),
            peopleCount: info.slot.resources['party_size'] ? info.slot.resources.party_size : 1,
            duration: (Number(info.slot.duration_sec) / 10),
            status: statusResponse,
            costumerId: customer.id,
            type: 'FREE',
            placeId: info.slot.merchant_id.replace('merch',''),
            idempotencyToken: info.idempotency_token,
            origin: 'GOOGLE',
            partnerId: 1,
            canceledBy: 'NONE'
          };

          models.Reservation.create(reservationObject).then( async (data) => {
            let reserve = data.get({plain:true});
            moduleBitacora.createBitacora('CREATE', `[NEW] - Nueva Reservación desde Google`, 'Reservations', data.id, null, data.placeId);

            Notifications.dispatchReservation(TYPE_NOTIFICATION_BY_RESERVATION.NEW_RES, data.id)
              .catch(e => HandleError(null, null, new InternalProcessError(e)));

            emailModule.reservationEmail(reserve.id, false, false);
            emailModule.newReservationEmail(reserve.id); // envio de email a hola@reservandonos.
            try {
                ModuleZoho.registerCustomerB2C(customer, {
                  cities: {
                    name: infoPlace['cities.name']
                  },
                  states: {
                    name: infoPlace['states.name']
                  },
                  TypeFoods: {
                    name: infoPlace['typeFood.name']
                  },
                  colony: {
                    name: infoPlace['colony.name']
                  },
                  ...infoPlace
                },
                  'GOOGLE'
                );
            } catch (e) {
              console.log(e)
            }

            if (infoPlace['typeAccount'] === ACCOUNT_TYPE.PARTNER) {
              moduleWhatsApp.sendWhatsAppNewReservation(infoPlace, data, customer, emailModule);
            }

            if (info.slot['confirmation_mode'] == 'CONFIRMATION_MODE_SYNCHRONOUS') {
              reservationObject['id'] = reserve.id;
              await assignTable(info.slot.start_sec, info.slot.duration_sec, info.slot.resources['party_size'] ? info.slot.resources.party_size : 2, reservationObject, place);
            }

            return res.json({
              booking: {
                booking_id: `${reserve.id}`,
                slot: info.slot,
                user_information: info.user_information,
                status: statusResponseGoogle,
                payment_information: info.payment_information
              }
            });
          });
        }
      });
    });
  } catch (e) {
    console.error({error: e});
    return res.status(500).json({error: true, message: e.message});
  }
}

const assignTable = async (start_sec, duration_sec, persons, reservationObject, place) => {
  try {
    const tablesAndGroupsRows = await models.OpeningDay.getTablesAndGroupsAvailable(place, start_sec, persons, duration_sec);
    const tables = tablesAndGroupsRows.filter(value => value.type === 'table');
    const groups = tablesAndGroupsRows.filter(value => value.type === 'group');
    let selected;
    let isGroup = false;
    if (tables.length > 0) {
      selected = tables.sort((a, b) => a.freeChairs - b.freeChairs)[0];
      reservationObject['tableId'] = selected.id;
    } else {
      selected = groups.sort((a, b) => a.freeChairs - b.freeChairs)[0];
      reservationObject['groupId'] = selected.id;
      isGroup = true;
    }
    reservationObject['serviceId'] = selected.serviceId;
    reservationObject['zoneId'] = selected.zoneId;
    reservationObject['isGroup'] = isGroup;
    return await models.Reservation.update(reservationObject, { where: {id: reservationObject.id}});
  } catch (e) {
    console.error({error: e});
    return null;
  }
}

router.get('/verificationPlaces', async (req, res, next) => {
  let place = 'https://reservandonos.com/wp-json/webinfinitech/v1/get-data-places';
  let meta = 'https://reservandonos.com/wp-json/webinfinitech/v1/get-data-places-metas';

  try {
    let arrayPromise = [
      helpGoogle.preparedInfoBooker(),
      helpGoogle.preparedInfoWordpress(place),
      helpGoogle.preparedInfoWordpress(meta)
    ];

    Promise.all(arrayPromise).then( data => {
      let outputs = helpGoogle.validatePlaces(data[0], data[1], data[2], false);
      return res.json(outputs);
    }, err => {
      console.error({request: req, error: err?.message});
      return res.json(err);
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/load', async (req, res, next) => {
  let place = 'https://reservandonos.com/wp-json/webinfinitech/v1/get-data-places';
  let meta = 'https://reservandonos.com/wp-json/webinfinitech/v1/get-data-places-metas';

  try {
    let arrayPromise = [
      helpGoogle.preparedInfoBooker(),
      helpGoogle.preparedInfoWordpress(place),
      helpGoogle.preparedInfoWordpress(meta)
    ];

    Promise.all(arrayPromise).then( data => {
      let outputs = helpGoogle.validatePlaces(data[0], data[1], data[2], true);
      return res.json(outputs);
    }, err => {
      console.error({request: req, error: err?.message});
      return res.json(err);
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/updateTime', async (req, res, next) => {
  try {
    const response = await helpGoogle.updateReservation(req.body);
    return res.json(response);
  } catch (e) {
    HandleError(req, res, e);
  }
  /*const keyPath = path.join(__dirname, 'booking-google.json');
  let keys = {};
  try {
    if (fs.existsSync(keyPath)) {
      keys = require(keyPath);
      let jwtClient = new google.auth.JWT(
          keys.client_email,
          null,
          keys.private_key,
          [ 'https://www.googleapis.com/auth/mapsbooking' ]
      );

      var oReq = new XMLHttpRequest();
      var url = `https://mapsbooking.googleapis.com/v1alpha/notification/partners/${req.body.partner}/bookings/${req.body.reservationId}?updateMask=status`;
      // var url = `https://partnerdev-mapsbooking.googleapis.com/v1alpha/notification/partners/${req.body.partner}/bookings/${req.body.reservationId}?updateMask=status`
      var params = JSON.stringify({
        name: `partners/${req.body.partner}/bookings/${req.body.reservationId}`,
        status: `${req.body.status}`
      });

      jwtClient.authorize( async (err, tokens) => {
        if (err) {
          console.error({request: req, error: err});
          return;
        } else {
          oReq.open("PATCH", url, true);
          oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
          oReq.setRequestHeader('Authorization', `Bearer ${tokens.access_token}`);
          oReq.addEventListener("load", async () => {
            if (oReq.readyState != 4 || oReq.status != 200) {
              return res.json({error: 'Error en el proceso', code: 500});
            } else {
              let response = JSON.parse(oReq.responseText);
              let status = response.status === RESERVATION_STATUS.CONFIRMED ? response.status : RESERVATION_STATUS.CANCELLED;
              await models.Reservation.update({status: status}, {
                  where: {
                    id: response['merchantId'].replace('merch','')
                  }
              });
              return res.json(JSON.parse(oReq.responseText));
            }
          });
          oReq.send(params);
        }
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }*/
});

router.get('/HealthCheck/', async (req, res, next) => {
  return res.json();
});


router.post('/getZoneHour/', async (req, res, next) => {
  let date = moment(new Date(req.body.time * 1000)).tz("America/Mexico_City");
  return res.json({hour: date.utc().format()});
});

router.post('/getUser/', async (req, res, next) => {
  try {
    const user = await models.User.findOne({
      where: {
        [Op.or]: [
          {email: req.body.email},
          {email: req.body.username}
        ]
      },
      attributes: ['id', 'email', 'password', 'fullname', 'profileImage', 'placeId', 'partnerId', 'role', 'adminPartner', 'unreadedNotificationsCount', 'genre'],
      include: ['place', 'partner']
    });

    if(user) {
      bcrypt.compare(req.body.password, user.password).then(match => {
        if(match) {
          return res.json({status_access: true});
        } else {
          return res.status(401).json({ error: 'credentials' });
        }
      });
    } else {
      return res.status(401).json({ error: 'credentials' });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/comparePassword', async (req, res, next) => {
  try {
    bcrypt.compare(req.body.password, req.body.hash).then(match => {
      if(match) {
        return res.json({status_access: true});
      } else {
        return res.status(401).json({
          error: 'credentials'
        });
      }
    });
    bcrypt.hash(req.body.password, 10).then(hash => {
      return res.json({'Update': hash});
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/changePassword/', async (req, res, next) => {
  try {
    const hash = await bcrypt.hash(req.body.password, 10);
    if  (hash) {
      const update = await models.User.update({password: hash}, {
        where: {
          email: req.body.email
        }
      });
      if (update) {
        return res.json({'Update': 'Cambio de contraseña exitoso', value: hash});
      } else {
        return res.status(401).json({'Update': false});
      }
    } else {
      return res.status(401).json({'hash': false});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

// Comprueba la disponibilidad del horario
router.post('/BatchAvailabilityLookup/', async (req, res, next) => {
  try {
    const {body} = req;
    const placeId = body.merchant_id.replace('merch','');
    const slot_time = body.slot_time;
    const arrayPromise = [];
    const place = await  models.Place.findOne({where: {id: placeId}, raw: true});
    if (place) {
      // const datetime = moment.unix(req.body.slot_time[0].start_sec); // .tz(place.timezone);
      // const minuteOfDay = datetime.hour() * 60 +  datetime.minutes();
      // const blockesPlacesLock = await IsBlockesPlacesLock(placeId, datetime.format('YYYY-MM-DD'), minuteOfDay);
      // if (place['onlineSales'] && !placeIsBlocked(place, datetime.format('YYYY-MM-DD'), minuteOfDay) && !blockesPlacesLock) {
      if (place['onlineSales']) {
        if (place.methodReserve === 'CONFIRMATION_MODE_SYNCHRONOUS') {
          if (slot_time.length) {
            slot_time.forEach( (item, i) => {
              arrayPromise.push(
                  models.OpeningDay.getTablesAndGroupsAvailable(place, item.start_sec, item.resource_ids.party_size, item.duration_sec, item?.widget, item?.date || 0, item?.minutes || 0)
              );
            });
            try {
              const results = await Promise.all(arrayPromise);
              const locks = await getLocks(placeId);
              const response = results.map((rows, index) => {
                let available = rows.length > 0;
                if  (available && locks.length > 0) {
                  available = isBlock(slot_time[index], place, locks) ? false : true;
                }
                return responseAvailabilityLookup(available, null, index, slot_time);
              });
              return res.json({
                slot_time_availability: response
              });
            } catch (e) {
              console.log(e.message)
              return res.json({
                slot_time_availability: slot_time.map(responseAvailabilityLookup.bind(null, false))
              });
            }
          }
        } else {
          const locks = await getLocks(placeId);
          const response = [];
          let index = 0;
          for (const slotTimeElement of slot_time) {
            const blockesPlacesLock = isBlock(slotTimeElement, place, locks);
            response.push(responseAvailabilityLookup(!blockesPlacesLock, null, index, slot_time));
            index++;
          }
          return res.json({
            slot_time_availability: response
          });
        }
      }
    }
    return res.json({
      slot_time_availability: slot_time.map(responseAvailabilityLookup.bind(null, false))
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

// Creacion de Reserva
router.post('/CreateBooking/', async (req, res, next) => {
  try {
    let date = moment(new Date(req.body.slot.start_sec * 1000));
    const customer = await models.Costumer.find({
      where: {
        userGoogleReserve: req.body.user_information.user_id
      },
      raw: true
    });
    if (customer === null) {
      let phone = req.body.user_information.telephone.split(' ');
      let customerObj = {
        fullname: `${req.body.user_information.given_name} ${req.body.user_information.family_name}`,
        countryCode: phone.length > 0 ? phone[0] : '',
        phone: phone.length > 1 ? phone.slice(1).join('') : req.body.user_information.telephone,
        email: req.body.user_information.email,
        placeId: req.body.slot.merchant_id.replace('merch',''),
        userGoogleReserve: req.body.user_information.user_id,
        subscribedEmails: 1,
        validEmail: 1,
        emailComplaints: 0
      };
      const customer = await models.Costumer.create(customerObj)
      createReserve(req.body, date, customer.get({plain:true}), res);
    } else {
      createReserve(req.body, date, customer, res);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

// Retorna el estado de la reserva
router.post('/GetBookingStatus/', async (req, res, next) => {
  try {
    const data = await models.Reservation.findOne({
      attributes: ['id', 'status'],
      where: {
        id: req.body.booking_id
      }, raw: true
    });

    if (data) {
      return res.json({
        booking_id: `${data.id}`,
        booking_status: data.status === RESERVATION_STATUS.CANCELLED ? 'CANCELED' : data.status === RESERVATION_STATUS.PENDING ? 'PENDING_MERCHANT_CONFIRMATION' : data.status,
        prepayment_status: "PREPAYMENT_STATUS_UNSPECIFIED"
      });
    } else {
      return res.json({
        booking_id: `${req.body.booking_id}`,
        booking_status: 'BOOKING_STATUS_UNSPECIFIED',
        prepayment_status: "PREPAYMENT_STATUS_UNSPECIFIED"
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

// Actualización de reserva, cancelacion o fecha.
router.post('/UpdateBooking/', async (req, res, next) => {
  try {
    let objUpdate = {};
    let updateTime = false;
    let time = 0;
    models.Reservation.findOne({
      include: [{
        model: models.Place,
        as: 'place',
        attributes: ['methodReserve','timezone'],
        paranoid: false
      }],
      where : {
        id: req.body.booking.booking_id,
      },
      raw: true
    }).then(reservation => {
      if (req.body.booking.status === 'CANCELED') {
        objUpdate['status'] = RESERVATION_STATUS.CANCELLED;
      } else {
        let date = moment.unix(req.body.booking.slot.start_sec).tz(reservation['place.timezone']);
        if (date.isValid()) {
          objUpdate['datetime'] = date.format('YYYY-MM-DD[T]HH:mm:ss.000[Z]');
          objUpdate['date'] = date.format('YYYY-MM-DD');
          objUpdate['duration'] = req.body.booking.slot.duration_sec / 10;
          objUpdate['start'] = moment.duration(date.format('H:mm')).asMinutes();
          objUpdate['end'] = moment.duration(date.format('H:mm')).asMinutes() + (Number(req.body.booking.slot.duration_sec) / 10);
          updateTime = true;
          time = req.body.booking.slot.start_sec;
        }

        objUpdate['status'] = reservation['place.methodReserve'] == 'CONFIRMATION_MODE_SYNCHRONOUS' ? RESERVATION_STATUS.CONFIRMED : RESERVATION_STATUS.PENDING;
        objUpdate['peopleCount'] = req.body.booking.slot['resources'] && req.body.booking.slot.resources.party_size ? req.body.booking.slot.resources.party_size : reservation['peopleCount'];
      }
      models.Reservation.update(objUpdate,
          {
            where: {
              id: req.body.booking.booking_id
            }
          }).then( data => {
        emailModule.newReservationEmail(req.body.booking.booking_id); // envio de email a hola@reservandonos.
        moduleBitacora.createBitacora('UPDATE', `[${objUpdate['status']}] - Reserva de Google Modificada`, 'Reservations', req.body.booking.booking_id, null, reservation.placeId);

        Notifications.dispatchReservation(TYPE_NOTIFICATION_BY_RESERVATION.UPD_RES, reservation.id)
          .catch(e => HandleError(null, null, new InternalProcessError(e)));

        getReserve(req.body.booking.booking_id, res, reservation['place.timezone'], updateTime, time);
      });
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/ListBookings/', async (req, res, next) => {
  try {
    let date = moment().format('YYYY-MM-DD');
    models.Reservation.findAll({
      include: [
        {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['fullname', 'email','countryCode', 'phone', 'email','userGoogleReserve'],
          paranoid: false,
          where: {
            userGoogleReserve: req.body.user_id,
          }
        },
        {
          model: models.Place,
          as: 'place',
          attributes: ['timezone'],
          paranoid: false
        }
      ],
      where : {
        date: {
          [Op.gte]: date
        },
        status: {
          [Op.notIn]: ['CANCELLED']
        }
      },
      raw: true
    }).then( async (data) => {
      let array = [], item;
      for (item of data) {
        array.push(await objectResponseReservation(item, item.id, item['place.timezone'], false, 0));
      }
      let obj = {
        bookings: array
      };
      return res.json(obj);
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
