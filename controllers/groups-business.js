const express = require('express');
const router = express.Router();
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");
const Op = models.Sequelize.Op;

router.get('/', async (req, res) => {
  try {
    const groupsBusiness = await models.GroupsBusiness.findAndCountAll({
      where: {
        name: {
          [Op.like]: `%${req.query.term}%`
        }
      },
      order: [
        [req.query.sortBy, req.query.sortDir]
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(groupsBusiness);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res) => {
  try {
    const groupsBusiness = await models.GroupsBusiness.create(req.body);
    return res.json(groupsBusiness);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    let groupsBusiness = await models.GroupsBusiness.findByPk(req.body.id);
    groupsBusiness = Object.assign(groupsBusiness, req.body);
    await groupsBusiness.save();
    return res.json(groupsBusiness);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/deletePlace/:id', async (req, res) => {
  try {
    const response = await models.GroupsBusinessPlaces.destroy({where: {id: req.params.id}});
    return res.json(response)
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/changeStatus', async (req, res) => {
  try {
    let groupsBusinessPlace = await models.GroupsBusinessPlaces.findByPk(req.body.id);
    groupsBusinessPlace = Object.assign(groupsBusinessPlace, req.body);
    await groupsBusinessPlace.save();
    return res.json(groupsBusinessPlace);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/addPlace', async (req, res) => {
  try {
    const groupsBusinessPlaces = await models.GroupsBusinessPlaces.create(req.body);
    return res.json(groupsBusinessPlaces);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const group = await models.GroupsBusiness.findByPk(req.params.id, {
      attributes: ['id', 'name'],
      include: {
        model: models.GroupsBusinessPlaces,
        as: 'groupsBusinessPlaces',
        attributes: ['id', 'isActive', 'placeId', 'groupsBusinessId'],
        include: {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name']
        }
      }
    });
    return res.json(group);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
