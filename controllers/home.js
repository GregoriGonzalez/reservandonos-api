const express = require('express');
const router = express.Router();
const models = require('../models');
const moment = require('moment');
const Op = models.Sequelize.Op;
const {emailModule} = require('../helpers/email.helper');
const {HandleError} = require("../helpers/error.helper");
const {RESERVATION_STATUS, ROLES} = require("../helpers/constants.helper");

router.get('/place/:id', async (req, res, next) => {
  try {
    const place = await models.Place.findByPk(req.params.id, {
      attributes: ['id', 'name', 'phone', 'email', 'logoImage', 'isActive', 'typeAccount'],
      include: [
        {model: models.TypeCharge, as: 'typeCharges'},
        {model: models.Plan, as: 'plan', attributes: ['id', 'name']},
        {model: models.Seller, as: 'sellers', attributes: ['id', 'name']}
      ]
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/billing', async (req, res) => {
  try {
    let order;
    let where = {};
    if (req.query.sortDir) {
      switch (req.query.sortBy) {
        case 'place':
          order = [['place', 'name', req.query.sortDir]];
          break;
        case 'date':
          order = [['date', req.query.sortDir], ['start', req.query.sortDir]];
          break;
        default:
          order = [[req.query.sortBy, req.query.sortDir]];
          break;
      }
    }

    if (req.query.placeId) {
      where.placeId = req.query.placeId;
    }

    if (req.query.dateStart && req.query.dateEnd) {
      if (req.query.comparative == 'cuts') {
        where.dateStart = {
          [Op.gte]: req.query.dateStart,
          [Op.lte]: req.query.dateEnd
        };
        where.dateEnd = {
          [Op.gte]: req.query.dateStart,
          [Op.lte]: req.query.dateEnd
        };
      } else {
        where.dateStart = {
          [Op.or]: [
            {
              [Op.gte]: moment(req.query.dateStart).startOf('month').format('YYYY-MM-DD'),
              [Op.lte]: moment(req.query.dateStart).endOf('month').format('YYYY-MM-DD')
            },
            {
              [Op.gte]: moment(req.query.dateEnd).startOf('month').format('YYYY-MM-DD'),
              [Op.lte]: moment(req.query.dateEnd).endOf('month').format('YYYY-MM-DD')
            }
          ]
        };
      }
    }

    where.status = RESERVATION_STATUS.CONFIRMED;

    const cuts = await models.Billings.findAndCountAll({
      attributes: ['id', 'dateStart', 'dateEnd', 'status', 'placeId'],
      include: [{
        model: models.Place, as: 'place', attributes: ['name', 'typeAccount']
      }],
      where: where,
      order: order,
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    })
    return res.json(cuts);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/paymentAlert', async (req, res) => {
  try {
    let billing = await models.Billings.findByPk(req.query.billingId, {
      attributes: ['id', 'dateStart', 'dateEnd', 'status', 'placeId', 'createdAt'],
      include: [{
        model: models.Place,
        as: 'place',
        attributes: ['name', 'id'],
        include: [
          {
            model: models.User,
            as: 'usersOfPlace',
            attributes: ['id', 'fullname', 'email'],
            through: {
              where: {
                notificationEmail: true
              }
            },
            required: true,
            where: {
              role: ROLES.MANAGER
            }
          }
        ]
      }]
    });
    for (const user of billing.place.usersOfPlace) {
      emailModule.reservandonosNotificationPendingPayment(user.email, user.fullname, billing.place.name, `${billing.dateStart} al ${billing.dateEnd}`);
    }
    return res.json({success: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/collection', async (req, res) => {
  try {
    let city = '', seller = '', status = '', place = '';
    let where = {};
    where.id = 228
    let whereStatus = {};

    if (req.query.cities) {
      city = (req.query.cities).split(',');
      where.cityId = {[Op.in]: city};
    }

    if (req.query.seller) {
      seller = (req.query.seller).split(',');
      where.sellerId = {[Op.in]: seller};
    }

    if (req.query.place) {
      place = (req.query.place).split(',');
      where.id = {[Op.in]: place};
    }

    if (req.query.status) {
      status = (req.query.status).split(',');
      whereStatus.status = {[Op.in]: status};
    }

    let rsvCuts = await collections(where, req, whereStatus);

    return res.json({
      'status': status,
      'collection': rsvCuts
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/place', async (req, res) => {
  try {
    let where = {id: req.query.placeId}
    let rsvCuts = await collections(where, req);
    return res.json({'billing': rsvCuts});
  } catch (e) {
    HandleError(req, res, e);
  }
});

function collections(where, req, whereBillings = {}) {
  let dateReservation = {};

  let modelPlace = {
    model: models.Place,
    as: 'place',
    attributes: ['id', 'name', 'cityId', 'phone', 'email', 'logoImage', 'isActive', 'fixedCharge', 'typeAccount'],
    include: [
      {model: models.TypeCharge, as: 'typeCharges'},
      {model: models.Plan, as: 'plan', attributes: ['id', 'name']},
      {model: models.Seller, as: 'sellers', attributes: ['id', 'name']},
      {model: models.SponsorPlaces, as: 'sponsorsPlaces'}
    ],
    where: where // seller, city
  };

  if (req.query.comparative == 'rsv' || req.query.comparative == 'day') {

    if (req.query.comparative == 'rsv') {
      dateReservation.date = {
        [Op.gte]: req.query.dateStart,
        [Op.lte]: req.query.dateEnd
      };
    } else { //comparative == 'day'
      dateReservation.date = {
        [Op.or]: [
          {[Op.eq]: req.query.dateStart},
          {[Op.eq]: req.query.dateEnd}
        ]
      };
    }

    if (req.query.status) {
      arrStatus = (req.query.status).split(',');
      status = [];
      for (let iterator of arrStatus) {
        status.push(iterator === RESERVATION_STATUS.CONFIRMED
          ? [RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.CONFIRMED]
          : (iterator === RESERVATION_STATUS.PENDING) ? RESERVATION_STATUS.PENDING : RESERVATION_STATUS.CANCELLED
        );
      }
      dateReservation.status = {[Op.in]: status};
    }

    dateReservation.partnerId = 1;

    return models.Reservation.findAll({
      include: [
        modelPlace,
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor'
        }
      ],
      where: dateReservation, //mes actual, partnerId = 1
    });
  } else {
    if (req.query.comparative == 'month') {
      whereBillings.dateStart = {
        [Op.or]: [
          {
            [Op.gte]: moment(req.query.dateStart).startOf('month').format('YYYY-MM-DD'),
            [Op.lte]: moment(req.query.dateStart).endOf('month').format('YYYY-MM-DD')
          },
          {
            [Op.gte]: moment(req.query.dateEnd).startOf('month').format('YYYY-MM-DD'),
            [Op.lte]: moment(req.query.dateEnd).endOf('month').format('YYYY-MM-DD')
          }
        ]
      };
    } else { //comparative == 'cuts'
      whereBillings.dateStart = {
        [Op.gte]: req.query.dateStart,
        [Op.lte]: req.query.dateEnd
      };
      whereBillings.dateEnd = {
        [Op.gte]: req.query.dateStart,
        [Op.lte]: req.query.dateEnd
      };
    }

    return models.Reservation.findAll({
      // attributes: ['date','invoiceAmount', 'peopleCount', 'peopleSeating', 'billingId', 'status'],
      include: [
        {
          model: models.Billings,
          as: 'billings',
          attributes: ['dateStart', 'dateEnd', 'status', 'placeId'],
          where: whereBillings,  // status y cuts
        },
        modelPlace,
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor'
        }
      ],
      where: {billingId: {[Op.not]: null}}, //tenga cortes(billingId  NOTNULL)
      order: [['billingId', 'asc']],
    });
  }
}

module.exports = router;
