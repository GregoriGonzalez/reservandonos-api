const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const moment = require('moment');
const {RESERVATION_STATUS} = require('../helpers/constants.helper');
const {HandleError} = require("../helpers/error.helper");
const serviceHelper = require("../helpers/services.helper");

router.get('/', async (req, res, next) => {
  try {
    const order =
      req.query.sortBy === 'zone'
        ? [{model: models.Zone, as: 'zone'}, 'name', req.query.sortDir]
        : [req.query.sortBy, req.query.sortDir];

    const result = await models.Layout.findAndCountAll({
      include: [{
        model: models.Zone,
        as: 'zone'
      }],
      distinct: true,
      where: {
        placeId: req.user.placeId,
        name: {
          [Op.like]: `%${req.query.term}%`
        }
      },
      order: [order],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/table-types', async (req, res, next) => {
  try {
    const result = await models.TableType.findAll({
      attributes: ['id', 'name']
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/table-heights', async (req, res, next) => {
  try {
    const result = await models.TableHeight.findAll({
      attributes: ['id', 'name']
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:layoutId/tables', async (req, res, next) => {
  try {
    const result = await models.Table.findAll({
      where: {
        layoutId: req.params.layoutId,
        placeId: req.user.placeId
      },
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:layoutId/available', async (req, res) => {
  try {
    const date = req.query.date;
    const start = req.query.start;
    const end = req.query.end;
    const layoutId = req.params.layoutId;
    const people = req.query.people;
    const allTables = await models.Table.findAll({
      where: {
        layoutId,
        minCapacity: {[Op.lte]: parseInt(people)},
        maxCapacity: {[Op.gte]: parseInt(people)}
      },
      attributes: ['id', 'number', 'minCapacity', 'maxCapacity']
    });
    const allGroups = await models.Group.findAll({
      where: {
        layoutId,
        minCapacity: {[Op.lte]: parseInt(people)},
        maxCapacity: {[Op.gte]: parseInt(people)}
      },
      attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity']
    });

    const tablesIds = Array.from(allTables, table => table.id);
    const groupsIds = Array.from(allGroups, group => group.id);

    const reservations = await models.Reservation.findAll({
      where: {
        [Op.or]: [{tableId: tablesIds}, {groupId: groupsIds}],
        status: [
          RESERVATION_STATUS.CONFIRMED,
          RESERVATION_STATUS.RE_CONFIRMED,
          RESERVATION_STATUS.ARRIVED,
          RESERVATION_STATUS.SEATED
        ],
        date,
        start: {[Op.lt]: end},
        end: {[Op.gt]: start}
      },
      attributes: ['id', 'tableId', 'groupId']
    });

    const tablesWithReservations = Array.from(reservations, r => r.tableId);
    const groupsWithReservations = Array.from(reservations, r => r.groupId);

    const availableTables = allTables.filter(table => !tablesWithReservations.includes(table.id));
    const availableGroups = allGroups.filter(group => !groupsWithReservations.includes(group.id));

    return res.json({availableTables, availableGroups});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:layoutId/groups', async (req, res, next) => {
  try {
    if (req.query.sortBy) {
      const groups = await models.Group.findAndCountAll({
        include: ['tables'],
        distinct: true,
        where: {
          layoutId: req.params.layoutId,
          placeId: req.user.placeId
        },
        order: [
          [req.query.sortBy, req.query.sortDir]
        ],
        offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
        limit: +req.query.pageSize || 10
      });
      return res.json(groups);
    } else {
      const groups = await models.Group.findAll({
        where: {
          layoutId: req.params.layoutId,
          placeId: req.user.placeId
        },
        include: ['tables'],
      });
      return res.json(groups);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const result = await models.Layout.findOne({
      where: {
        id: req.params.id,
        placeId: req.user.placeId
      },
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/tableMinCapAndZone/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const table = await models.Table.findOne({where: {id}, attributes: ['minCapacity', 'layoutId']});
    const layout = await models.Layout.findOne({where: {id: table.layoutId}, attributes: ['zoneId']});
    return res.json({minCapacity: table.minCapacity, zoneId: layout.zoneId});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/:id/clone', async (req, res, next) => {
  try {
    const newName = req.body.newName;
    let ogLayout = await models.Layout.findOne({
      where: {id: req.params.id},
      raw: true
    });
    delete ogLayout.id;
    ogLayout.name = newName;
    let cloneLayout = await models.Layout.create(ogLayout);
    const newId = cloneLayout.dataValues.id;

    let ogTables = await models.Table.findAll({
      where: {layoutId: req.params.id},
      raw: true
    });

    for (const ogTable of ogTables) {
      delete ogTable.id;
      ogTable.layoutId = newId;
      await models.Table.create(ogTable);
    }

    let ogGroups = await models.Group.findAll({
      where: {id: req.params.id},
      raw: true
    });

    for (const ogGroup of ogGroups) {
      delete ogGroup.id;
      ogGroup.layoutId = newId;
      await models.Group.create(ogGroup);
    }

    return res.json({newId});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    req.body.placeId = req.user.placeId;
    const result = await models.Layout.create(req.body);
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/table', async (req, res, next) => {
  try {
    req.body.placeId = req.user.placeId;
    const result = await models.Table.create(req.body);
    await models.Layout.increment({tablesCount: 1}, {where: {id: req.body.layoutId}});
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/:layoutId/groups', async (req, res, next) => {
  try {
    req.body.placeId = req.user.placeId;
    const group = await models.Group.create(req.body);
    await group.setTables(req.body.tableIds);
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(group);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:layoutId/groups/:id', async (req, res, next) => {
  try {
    const group = await models.Group.findOne({
      where: {
        id: req.body.id,
        placeId: req.user.placeId,
        layoutId: req.body.layoutId
      },
    });
    Object.assign(group, req.body);
    group.placeId = req.user.placeId;
    await group.save();
    await group.setTables(req.body.tableIds);
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(group);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res, next) => {
  try {
    let layout = await models.Layout.findOne({
      where: {
        id: req.body.layout.id,
        placeId: req.user.placeId
      },
    });
    layout = Object.assign(layout, req.body.layout);
    await layout.save();

    const tables = await models.Table.findAll({
      where: {
        layoutId: layout.id,
        placeId: req.user.placeId
      }
    });
    let maxCapacity = 0;
    for (let table of tables) {
      maxCapacity += table.maxCapacity;
      const t = req.body.tables.find(t => table.id === t.id);
      if (t) {
        table.coordX = t.coordX;
        table.coordY = t.coordY;
        await table.save();
      }
    }
    layout.tablesCount = tables.length;
    layout.maxCapacity = maxCapacity;
    await layout.save();
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(layout);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:id/update-name/', async (req, res) => {
  try {
    const id = req.params.id;
    const name = req.body.newName;
    const result = await models.Layout.update({name}, {where: {id}});
    if (result) {
      serviceHelper.deleteServiceByPlaceId(req?.user?.placeId);
      return res.json({result});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/table', async (req, res, next) => {
  try {
    let result = await models.Table.findOne({
      where: {
        id: req.body.id,
        placeId: req.user.placeId
      },
    });
    result = Object.assign(result, req.body);
    await result.save();
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/:layoutId/table/:id', async (req, res) => {
  try {
    const tableToDelete = await models.Table.findOne({
      where: {id: req.params.id},
      attributes: ['id'],
      include: [{model: models.Group, as: 'groups', attributes: ['id', 'numbers']}]
    });
    if (tableToDelete.groups.length > 0) {
      return res.status(403).json({error: "La mesa pertenece a grupos", groups: tableToDelete.groups});
    }

    const reservationsOnTableToDelete = await models.Reservation.findAndCountAll({
      where: {
        tableId: req.params.id,
        datetime: {[Op.gt]: moment().startOf('month').subtract(1, 'month')},
        status: {[Op.in]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.SEATED]}
      },
      attributes: ['id', 'date', 'peopleCount', 'status']
    });
    if (reservationsOnTableToDelete.count > 0) {
      return res.status(403).json({
        error: "La mesa tiene reservaciones programadas",
        reservations: reservationsOnTableToDelete.rows
      });
    } else {
      const deleted = await models.Table.destroy({where: {id: req.params.id}});
      await models.Layout.decrement({tablesCount: 1}, {where: {id: req.params.layoutId}});
      serviceHelper.deleteServiceByPlaceId(req.user.placeId);
      return res.json(deleted);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const layoutId = req.params.id
    const layoutTables = await models.Table.findAndCountAll({where: {layoutId}, include: 'groups'});
    if (layoutTables.count > 0) {
      let tables = [];
      let groupsIds = [];
      layoutTables.rows.forEach(table => {
        tables.push(table.id);
        let tableGroups = table.dataValues.groups.map(group => group.dataValues.id);
        groupsIds = groupsIds.concat(tableGroups);
      });
      groupsIds = groupsIds.filter((item, pos) => groupsIds.indexOf(item) === pos); // remove duplicates

      let where = {
        tableId: tables,
        date: {
          [Op.gt]: moment().startOf('month').subtract(1, 'month')
        },
        status: {[Op.in]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.SEATED]}
      };
      if (groupsIds.length > 0) {
        where = {
          date: {
            [Op.gt]: moment().startOf('month').subtract(1, 'month')
          },
          status: {[Op.in]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.SEATED]},
          [Op.or]: [
            {tableId: tables},
            {groupId: {[Op.or]: groupsIds}}
          ]
        }
      }
      const reservationsOnTableToDelete = await models.Reservation.findAndCountAll({where});
      if (reservationsOnTableToDelete.count > 0) {
        return res.status(403).json({
          error: "Mesa(s) en el layout tiene(n) reservacion(es) programada(s)",
          reservations: reservationsOnTableToDelete.rows
        });
      } else {
        serviceHelper.deleteServiceByPlaceId(req.user.placeId);
        const result = await models.Layout.destroy({where: {id: layoutId}});
        return res.json(result);
      }
    } else {
      serviceHelper.deleteServiceByPlaceId(req.user.placeId);
      const result = await models.Layout.destroy({where: {id: layoutId}});
      return res.json(result);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/groups/:id', async (req, res) => {
  try {
    const reservationsOnGroup = await models.Reservation.findAndCountAll({
      where: {
        groupId: req.params.id,
        datetime: {[Op.gt]: moment().startOf('month').subtract(1, 'month')},
        status: {[Op.in]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.SEATED]}
      }
    });
    if (reservationsOnGroup.count > 0) {
      return res.status(403).json({
        error: "El grupo tiene reservaciones programadas",
        reservations: reservationsOnGroup.rows
      });
    } else {
      serviceHelper.deleteServiceByPlaceId(req.user.placeId);
      const deleted = await models.Group.destroy({where: {id: req.params.id}});
      return res.json(deleted);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
