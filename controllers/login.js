const express = require("express");
const router = express.Router();
const {clientRegister} = require("../models/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const env = require('../config/env');
const {HandleError} = require("../helpers/error.helper");

router.get('/', async (req, res) => {
  try {
    let {email, password} = req.body;
    const user = await clientRegister.findOne({
      where: {email: email}
    });
    if (!user) {
      return res.status(404).json({masg: "Email no registrado"});
    } else {
      if (bcrypt.compareSync(password, user.password)) {
        let token = jwt.sign({user: user}, env.jwtSecret, {});
        return res.json({user, token});
      } else {
        return res.status(401).json({msg: "Contraseña invalida"})
      }
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;

