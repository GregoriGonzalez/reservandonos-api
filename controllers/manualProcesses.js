const express = require("express");
const router = express.Router();
const IO = require('../socket');
const Redis = require('../redis');
const models = require('../models');
const Op = models.Sequelize.Op;
const {Validator} = require("node-input-validator");
const {onlyMessage} = require("../helpers/utils.helper");
const CutsModule = require('../helpers/cuts.helper');
const {HandleError} = require("../helpers/error.helper");
const FCM = require('../helpers/pushNotifications.helper');
const {emailModule: Emails} = require('../helpers/email.helper');

router.get('/check-cut-payments', async (req, res) => {
  try {
    await CutsModule.checkCutPayments();
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/redis-delete/:key/:id', async (req, res) => {
  const key = req.params.key;
  const id = req.params?.id;
  let response;
  switch (key) {
    case 'colonies':
      response = await Redis.deleteRow(id ? `${key}-${id}` : key);
      break;
  }
  return res.json({response});
});

router.get('/reset-redis', async (req, res) => {
  const data = await Redis.flushRedis();
  return res.json(data);
});


router.post('/dispatch-event', async (req, res) => {
  try {
    const {nameEvent, channel, data = {}} = req.body;
    const io = IO.getIO();
    if (channel) {
      io.to(channel).emit(nameEvent, data);
    } else {
      io.emit(nameEvent, data);
    }
    return res.json();
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/collect-cut-automatically',
  async (req, res, next) => {
    const rules = {
      placeIds: 'array',
      'placeIds.*': 'required|numeric'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const {placeIds} = req.body;
      await CutsModule.collectCutAutomatically(placeIds);
      return res.json('Ready');
    } catch (e) {
      HandleError(req, res, e);
    }
  });

router.post(
  '/create-cuts',
  async (req, res, next) => {
    const rules = {
      placeIds: 'array',
      'placeIds.*': 'required|numeric',
      date: 'required|date'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const {placeIds, date} = req.body;
      await CutsModule.generateCuts(date, placeIds);
      return res.json({error: false});
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.post('/send-push-notification', async (req, res) => {
  try {
    const {userIds = [], title, body, data = {}} = req.body;
    const users = await models.User.findAll({
      attributes: ['tokenFCM'],
      where: {
        id: {
          [Op.in]: userIds.split(',')
        },
        tokenFCM: {
          [Op.not]: null
        }
      },
      raw: true
    });
    await FCM.send(
      users.map(user => user.tokenFCM),
      title, body, data
    );
    return res.json({error: false});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/send-email-coupon', async (req, res) => {
  try {
    const {couponId} = req.body;
    await Emails.sendCoupon(couponId);
    return res.sendStatus(200);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
