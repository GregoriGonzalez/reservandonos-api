const express = require('express');
const router = express.Router();
const models = require('../models');
const {NOTIFICATION_TYPE} = require("../helpers/constants.helper");
const {HandleError} = require("../helpers/error.helper");

async function getNotifications(req, where) {
  const notiUser = await models.NotiUser.findAll({
    where: where,
    include: {
      model: models.Notification,
      as: 'notification',
      attributes: ['id', 'notiTypeId', 'message', 'createdAt'],
      required: true,
      include: [
        {
          model: models.NotificationType,
          as: 'notificationType',
          required: true,
          attributes: ['type']
        },
        {
          model: models.Reservation,
          as: 'reservation',
          required: false,
          attributes: ['type', 'createdAt', 'peopleCount', 'date', 'id', 'status', 'advancePaymentId', 'placeId'],
          include: [
            {
              model: models.Table,
              as: 'table',
              attributes: ['number']
            },
            {
              model: models.Group,
              as: 'group',
              attributes: ['numbers']
            }
          ],
        },
        {
          model: models.GiftCard,
          as: 'giftCard',
          required: false,
          attributes: ['beneficiaryName', 'amount', 'createdAt', 'id']
        },
        {
          model: models.Billings,
          as: 'billings',
          required: false,
        },
        {
          model: models.Coupons,
          as: 'coupon',
          required: false,
          include: ['promotion']
        },
      ]
    },
    order: [['updatedAt', 'DESC']],
    offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
    limit: +req.query.pageSize || 10
  });
  let nots = [];
  for (const element of notiUser) {
    if (element.notification) {
      const notification = {
        notificationId: element.notification.id,
        createdAt: element.notification.createdAt,
        message: element.notification.message,
        type: element.notification.notiTypeId,
        seen: element.seen,
        userId: element.userId,
        notificationUserId: element.id,
        placeId: element.placeId,
      }
      if (element.notification.giftCard) {
        nots.push({
          ...notification,
          giftCardId: element.notification.giftCard.id,
          beneficiaryName: element.notification.giftCard.beneficiaryName,
          amount: element.notification.giftCard.amount,
          placeId: element.notification.giftCard.placeId,
          type: NOTIFICATION_TYPE.NEW_GIFT
        });
      } else if (element.notification.reservation) {
        nots.push({
          ...notification,
          reservationStatus: element.notification.reservation.status,
          reservationId: element.notification.reservation.id,
          reservationType: element.notification.reservation.type,
          advancePayment: element.notification.reservation.advancePaymentId,
          placeId: element.notification.reservation.placeId,
        });
      } else if (element.notification.billings) {
        nots.push({
          ...notification,
          billingId: element.notification.billings.id,
          billingStatus: element.notification.billings.status,
          billingPlaceId: element.notification.billings.placeId,
          placeId: element.notification.billings.placeId,
        });
      } else if (element.notification.coupon) {
        nots.push({
          ...notification,
          couponId: element.notification.coupon.id,
          code: element.notification.coupon.code,
          placeId: element.notification.coupon.promotion.placeId,
        });
      }
    }
  }
  return {count: nots.length, rows: nots};
}

router.get('/user/:userId/place/:placeId', async (req, res) => {
  try {
    const notifications = await getNotifications(req, {userId: req.params.userId, placeId: req.params.placeId});
    return res.json(notifications);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/user/:userId', async (req, res) => {
  try {
    const notifications = await getNotifications(req, {userId: req.params.userId});
    return res.json(notifications);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/userNotiConfig/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const types = await models.NotificationType.findAll({
      attributes: ['id', 'type'],
    });
    const values = await models.NotiConfig.findAll({where: {userId}, attributes: ['notiType', 'checked']});
    return res.json({types, values});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const notification = await models.Notification.findOne({
      where: {id: req.params.id},
      include: [
        {
          model: models.Reservation,
          as: 'reservation',
          required: false,
          include: [
            {
              model: models.ReservationSponsors,
              as: 'reservationSponsor',
              attributes: ['sponsorId', 'quantity', 'totalCommission']
            },
            {
              model: models.Table,
              as: 'table',
              attributes: ['number']
            },
            {
              model: models.Group,
              as: 'group',
              attributes: ['numbers']
            },
            {
              model: models.Costumer,
              as: 'costumer',
              attributes: ['fullname', 'countryCode', 'phone', 'email', 'notes', 'tags']
            }
          ]
        },
        {
          model: models.GiftCard,
          as: 'giftCard',
          required: false,
          attributes: ['beneficiaryName', 'amount', 'createdAt', 'id', 'email', 'phone', 'redeemed']
        },
        {
          model: models.NotificationType,
          as: 'notificationType',
          required: true,
          attributes: ['type']
        },
        {
          model: models.Billings,
          as: 'billings',
          required: true
        }
      ]
    });
    return res.json(notification);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/userNotiConfig/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const notiType = req.body.notiType;
    const newValue = req.body.newValue;
    const newEntry = {userId, notiType, checked: newValue};
    await models.NotiConfig.create(newEntry);
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/markAsSeen', async (req, res) => {
  try {
    const userId = req.body.userId;
    let where = {where: {userId}};
    if (req.body.placeId) {
      where['placeId'] = req.body.placeId;
    }
    await models.NotiUser.update({seen: true}, where);
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:notificationId/mark-as-read', async (req, res) => {
  try {
    const userId = req.user.id;
    const notificationId = req.params.notificationId;
    await models.NotiUser.update({seen: true}, {
      where: {
        id: notificationId,
        userId
      }
    });
    return res.json({error: false, message: 'Notification Viewed'});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/user/:userId/place/:placeId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const placeId = req.params.placeId;
    await models.NotiUser.destroy({
      where: {userId, seen: true, placeId}
    });
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/user/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    await models.NotiUser.destroy({
      where: {userId, seen: true}
    });
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
