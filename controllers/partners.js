const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const awsModule = require('../helpers/aws');
const env = require('../config/env');
const {HandleError} = require("../helpers/error.helper");
const {ROLES} = require("../helpers/constants.helper");

router.get('/', async (req, res) => {
  try {
    let include = null;
    let where = {name: {[Op.like]: `%${req.query.term}%`}};
    if (req.query.placeId) {
      where['placeId'] = `${req.query.placeId}`;
    } else {
      include = {model: models.Place, as: 'place', attributes: ['name']};
    }

    let order = [[req.query.sortBy, req.query.sortDir]];
    if (req.query.sortBy == 'place') {
      order = [[{model: models.Place, as: 'place'}, 'name', req.query.sortDir]];
    }
    const partners = await models.Partner.findAndCountAll({
      where,
      attributes: ['id', 'name', 'phone', 'email'],
      include,
      order,
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(partners);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-place', async (req, res) => {
  try {
    const partners = await models.Partner.findAll({
      where: {
        [Op.or]: [
          {placeId: `${req.query.place}`},
          {
            id: {
              [Op.in]: [1, 24]
            }
          }
        ]
      }
    });
    return res.json(partners);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/names', async (req, res) => {
  try {
    const names = await models.Partner.findAll({
      attributes: [
        [models.Sequelize.fn('DISTINCT', models.sequelize.col('name')), 'name']
      ]
    });
    return res.json(names);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-name', async (req, res) => {
  try {
    const partners = await models.Partner.findAll({
      where: {
        name: {[Op.like]: `%${req.query.name}%`},
      },
      attributes: [
        [models.Sequelize.fn('DISTINCT', models.sequelize.col('name')), 'name']
      ],
      limit: 10
    });
    return res.json(partners);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const include = req.user.role === ROLES.ADMIN
      ? {model: models.Place, as: 'place', attributes: ['id', 'name']}
      : null;
    const partner = await models.Partner.findByPk(req.params.id, {include});
    return res.json(partner);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res) => {
  try {
    const partner = await models.Partner.create(req.body)
    if (req.file) {
      const folder = `partner/${partner.id}/logo`;
      const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
      partner.logoImage = `${env.awsBucketUrl}/${uploadedFileUrl}`;
      await partner.save();
    }
    return res.json(partner);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    if (req.file) {
      const folder = `partner/${req.body.id}/logo`;
      const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
      req.body.logoImage = `${env.awsBucketUrl}/${uploadedFileUrl}`;
    }
    let partner = await models.Partner.findByPk(req.body.id);
    partner = Object.assign(partner, req.body);
    await partner.save();
    return res.json(partner);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
