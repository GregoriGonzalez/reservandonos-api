const env = require('../config/env');
const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const {Validator} = require('node-input-validator');
const moment = require("moment");
const awsModule = require('../helpers/aws');
const {PAYMENTS_GATEWAY, PAYMENTS_STATUS, BILLINGS_STATUS} = require("../helpers/constants.helper");
const ElectronicInvoicing = require("../helpers/electronicInvoicing.helper");
const {getPayPalPaymentOrder, getPayPalPaymentOrderCapture, createPayPalOrder} = require("../helpers/paypal.helper");
const {
  createPaymentIntent, retrieveCustomer, fetchCardsOfCustomer,
  createCustomer, deleteCardOfTheCustomer, setCardByDefault, getOrder, attachPaymentMethodToCustomer
} = require("../helpers/stripe.helper");
const {divide, sum, multiply} = require("../helpers/calculate.helper");
const {HandleError, AppError, InternalProcessError} = require("../helpers/error.helper");
const {emailModule: Emails} = require('../helpers/email.helper');
const Cuts = require('./../helpers/cuts.helper');
const Notifications = require("../helpers/notifications.helper");
const sequelize = require("sequelize");

/**************************** GET ***********************************/

/*
* Get  payment details
*/
router.get('/details/:paymentId', async (req, res) => {
  try {
    const {paymentId} = req.params;
    const payment = await models.Payments.findByPk(paymentId);
    if (payment) {
      let result;
      if (payment.paymentGateway === PAYMENTS_GATEWAY.STRIPE) {
        const {charges: {data}} = await getOrder(payment.transactionId);
        result = {
          type: payment.paymentGateway,
          transactionId: payment.transactionId,
          amount: await divide(2, data[0]?.amount, 100),
          description: payment.comment,
          currency: data[0]?.currency,
          status: payment.statusPayment,
          created: moment.unix(data[0]?.created).format('DD/MM/YYYY hh:mm'),
          card: {
            brand: data[0].payment_method_details.card?.brand?.toUpperCase(),
            last4: data[0].payment_method_details.card?.last4,
            exp_year: data[0].payment_method_details.card?.exp_year,
            exp_month: data[0].payment_method_details.card?.exp_month,
          },
          error: data[0].failure_code
        }
      } else if (payment.paymentGateway === PAYMENTS_GATEWAY.PAYPAL) {
        const {data} = await getPayPalPaymentOrder(payment.transactionId);
        const pu = data?.purchase_units;
        const capture = pu[0]?.payments?.captures;
        const payer = data?.payer;
        result = {
          type: payment.paymentGateway,
          transactionId: payment.transactionId,
          amount: pu[0]?.amount?.value,
          currency: pu[0]?.amount?.currency_code,
          description: payment.comment,
          status: payment.statusPayment,
          created: moment(capture[0]?.create_time).format('DD/MM/YYYY hh:mm'),
          customer: {
            full_name: payer?.name.given_name + ' ' + payer?.name?.surname,
            email: payer?.email_address
          }
        }
      } else {
        result = {
          type: payment.paymentGateway,
          transactionId: payment.transactionId,
          status: payment.statusPayment,
          url: payment.backupFile,
          amount: payment.amount,
          currency: payment.currency,
          description: payment.comment,
          created: moment(payment.paymentDate).format('DD/MM/YYYY hh:mm')
        }
      }
      return res.json(result);
    } else {
      throw new AppError('Payment not found', AppError.TYPE_ERROR.RESOURCE_NOT_FOUND);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Get place payment history resume
 */
router.get('/history/resume', async (req, res) => {
  try {
    const {placeId, start, end, sellerId, status, pageIndex, pageSize, sortBy, sortDir} = req.query;
    let options = {
      attributes: [
        'statusPayment',
        [sequelize.literal('ROUND(SUM(amount), 2)'), 'amount'],
        [sequelize.literal('ROUND(SUM(`net_amount`), 2)'), 'net_amount'],
      ],
      where: {
        paymentDate: {
          [Op.between]: [
            moment(start).startOf('d').format('YYYY-MM-DD HH:mm:ss'),
            moment(end).endOf('d').format('YYYY-MM-DD HH:mm:ss')
          ]
        }
      },
      group: ['place.sellerId', 'statusPayment']
    };
    if (placeId !== '') {
      options.where.placeId = placeId;
    }
    if (status !== 'ALL') {
      options.where.statusPayment = status.toUpperCase()
    }
    const wherePlace = {};
    if (sellerId !== '0') {
      wherePlace.sellerId = sellerId;
    }

    options.include = [{
      model: models.Place,
      attributes: ['sellerId'],
      as: 'place',
      required: true,
      where: wherePlace,
      include: [{
        model: models.Seller,
        as: 'sellers',
        attributes: ['name']
      }]
    }];

    const payments = await models.Payments.findAll(options);
    let totalAmount = 0;
    let totalNetAmount = 0;

    let doughnutChartData = [];
    let doughnutChartLabels = [];
    for (const payment of payments) {
      totalAmount = await sum(2, totalAmount, payment.amount);
      totalNetAmount = await sum(2, totalNetAmount, payment.net_amount);
      if  (payment?.statusPayment === 'APPROVED') {
        payment['translat'] = 'APROBADO';
      } else if (payment?.statusPayment === 'DECLINED') {
        payment['translat'] = 'RECHAZADO';
      } else if (payment?.statusPayment === 'IN_PROCESS') {
        payment['translat'] = 'EN PROCESO';
      }
    }

    for (const payment of payments) {
      const percent = await multiply(2, await divide(2, payment.amount, totalAmount), 100);
      doughnutChartData.push(payment.amount);
      doughnutChartLabels.push(
        `${payment?.place?.sellers?.name?.split(' ')[0]}\n
         ${payment?.translat}\n
         ${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'USD' }).format(payment.amount || 0)}\n
         ${percent}%`
      );
    }

    return res.json({payments, doughnutChartData, doughnutChartLabels, totalAmount, totalNetAmount});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Get place payment history
 */
router.get('/history', async (req, res) => {
  try {
    const {placeId, start, end, sellerId, status = 'ALL', pageIndex, pageSize, sortBy, sortDir} = req.query;
    let options = {
      order: [['paymentDate', sortDir]],
      limit: +pageSize || 1,
      offset: +pageIndex * +pageSize || 0,
      where: {
        paymentDate: {
          [Op.between]: [
            moment(start).startOf('d').format('YYYY-MM-DD HH:mm:ss'),
            moment(end).endOf('d').format('YYYY-MM-DD HH:mm:ss')
          ]
        }
      }
    };
    if (placeId !== '') {
      options.where.placeId = placeId;
    }
    if (status !== 'ALL') {
      options.where.statusPayment = status.toUpperCase()
    }
    if (sellerId !== '0') {
      options.include = [{
        model: models.Place,
        as: 'place',
        required: true,
        where: {sellerId}
      }];
    }
    const payments = await models.Payments.findAndCountAll(options);
    return res.json(payments);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:placeId', async (req, res) => {
  try {
    const placeId = req.params.placeId
    const order = [[req.query.sortBy, req.query.sortDir]];
    const payments = await models.PlacePayment.findAndCountAll({
      where: {placeId},
      attributes: ['id', 'placeId', 'createdAt', 'value', 'pdf', 'xml'],
      order,
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(payments);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * get available payment methods of a place by her id
 */
router.get(
  '/stripe/available-payment-methods/:placeId',
  async (req, res) => {
    try {
      const {placeId} = req.params;
      const place = await models.Place.findByPk(placeId);
      if (place) {
        if (place.stripeCustomerId) {
          const customer = await retrieveCustomer(place.stripeCustomerId);
          const cardList = await fetchCardsOfCustomer(place.stripeCustomerId)
          return res.json({customer, list: cardList, collectionAuto: place.stripeCollectionAuto});
        } else {
          return res.json({customer: null, list: null});
        }
      } else {
        return res.status(400).json({error: true, message: "Place not found"});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.get('/:paymentId/download/electronic-invoice', async (req, res) => {
  try {
    const {paymentId} = req.params;
    const {format = 'pdf'} = req.query;
    const payment = await models.Payments.findByPk(paymentId);
    if (payment && payment.electronicInvoiceId) {
      const readStreamFile = await ElectronicInvoicing.download(payment.placeId, payment.electronicInvoiceId, format);
      return readStreamFile.pipe(res);
    } else {
      return res.status(400).json({error: true, message: 'Electronic invoice not found'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**************************** POST ***********************************/

router.post('/upload/:placeId/:id', async (req, res) => {
  try {
    if (req.file) {
      const placeId = req.params.placeId;
      const id = req.params.id;
      const folder = `place/${placeId}/payments`;
      const uploadedFileUrl = await awsModule.uploadFile(folder, req.file);

      const fieldToUpdate = req.file.mimetype == 'application/pdf' ? 'pdf' : 'xml';
      await models.PlacePayment.update({[fieldToUpdate]: `${env.awsBucketUrl}/${uploadedFileUrl}`}, {where: {id}});

      return res.json('ok');
    } else {
      return res.status(400).json('No file');
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 *  Register payment of a cut made through the payment gateway
 */
router.post(
  '/invoiced/cut/:idCut',
  async (req, res, next) => {
    const vPG = new Validator(req.body, {
      paymentGateway: 'required|in:' + Object.values(PAYMENTS_GATEWAY).join(','),
    }, {
      'paymentGateway.required': 'paymentGateway is required',
      'paymentGateway.in': 'Options available: ' + Object.values(PAYMENTS_GATEWAY).join(','),
    });
    if (await vPG.check()) {
      let fields = {};
      if (req.body.paymentGateway === PAYMENTS_GATEWAY.EXTERNAL) {
        fields = {
          amount: ['required', 'numeric'],
          url: ['required', 'url']
        }
      } else if (req.body.paymentGateway === PAYMENTS_GATEWAY.CASH) {
        fields = {
          amount: ['required', 'numeric']
        }
      } else {
        fields = {
          amount: ['required', 'numeric'],
          transactionId: ['required', 'string']
        };
      }

      const fV = new Validator(req.body, fields, {
        'transactionId': 'transactionId is required and string',
        'amount': 'amount must be numeric and greater than zero',
        'url': 'url is required and must be an url valid'
      });
      if ((await fV.check())) {
        next();
      } else {
        return res.status(422).json(fV.errors);
      }
    } else {
      return res.status(422).json(vPG.errors);
    }
  },
  async (req, res) => {
    try {
      const {idCut} = req.params;
      const {paymentGateway, url, amount, description, currency} = req.body;
      const billing = await models.Billings.findByPk(idCut);
      if (billing) {
        let paymentRegistered = {};
        if ([PAYMENTS_GATEWAY.STRIPE, PAYMENTS_GATEWAY.OXXO, PAYMENTS_GATEWAY.PAYPAL].includes(paymentGateway)) {
          paymentRegistered.statusPayment = PAYMENTS_STATUS.PROCESS;
          billing.status = Cuts.getStatusByStatusPayment(paymentRegistered.statusPayment);
          await billing.save();
        } else if ([PAYMENTS_GATEWAY.EXTERNAL, PAYMENTS_GATEWAY.CASH].includes(paymentGateway)) {
          paymentRegistered = await models.Payments.create({
            paymentGateway,
            amount,
            fee: 0,
            net_amount: amount,
            paymentDate: moment().format('YYYY-MM-DD hh:mm'),
            statusPayment: paymentGateway === PAYMENTS_GATEWAY.EXTERNAL ? PAYMENTS_STATUS.PROCESS : PAYMENTS_STATUS.APPROVED,
            comment: description,
            currency: currency || 'MXN',
            backupFile: url,
            placeId: billing.placeId,
            billingId: billing.id
          });
          billing.paymentUrl = url;
          billing.status = Cuts.getStatusByStatusPayment(paymentRegistered.statusPayment);
          await billing.save();
          if (paymentRegistered.statusPayment === PAYMENTS_STATUS.APPROVED) {
            // ElectronicInvoicing.generate(billing.placeId, paymentRegistered)
            //   .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
            // Emails.dispatchPaymentByBillingConfirmed(paymentRegistered.id)
            //   .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
            // Notifications.dispatchPaymentConfirmed(paymentRegistered.id)
            //   .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
          }
        }
        return res.json({billing});
      } else {
        return res.status(400).json({error: true, message: 'Billing not found'});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * Create payment intent by Stripe
 */
router.post(
  '/stripe/create-payment-intent',
  async (req, res, next) => {
    const v = new Validator(req.body, {
      placeId: 'required|numeric',
      paramsPaymentIntent: 'required|object'
    }, {
      'placeId': 'placeId is required and numeric greater than zero',
      'paramsPaymentIntent': 'paramsPaymentIntent is required, type object, property min required: {amount, description, currency}'
    });
    v.check().then((matched) => {
      if (!matched) {
        return res.status(422).json(v.errors);
      }
      next();
    });
  },
  async (req, res) => {
    try {
      const {paramsPaymentIntent, placeId} = req.body;
      const place = await models.Place.findByPk(placeId);
      if (place) {
        const paymentIntent = await createPaymentIntent({
          ...paramsPaymentIntent,
          amount: parseInt(paramsPaymentIntent.amount, 10) * 100
        }).catch(e => {
          return res.status(400).json({error: true, message: e});
        });
        return res.json({
          clientSecret: paymentIntent.client_secret,
          extra: paymentIntent
        });
      } else {
        return res.status(400).json({error: true, message: 'Place not found'});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * Register card how payment method
 */
router.post(
  '/stripe/attach-payment-method-to-customer',
  async (req, res, next) => {
    const v = new Validator(req.body, {
      placeId: 'required|numeric',
      paymentMethodId: 'required'
    }, {
      'placeId.required': 'placeId is required and numeric',
      'placeId.numeric': 'placeId must be numeric',
      'paymentMethodId': 'paymentMethodId is required'
    });
    v.check().then((matched) => {
      if (!matched) {
        return res.status(422).json(v.errors);
      }
      next();
    });
  },
  async (req, res) => {
    try {
      const {placeId, paymentMethodId} = req.body;
      const place = await models.Place.findByPk(placeId);
      if (place) {
        let customer;
        if (place.stripeCustomerId) {
          customer = await retrieveCustomer(place.stripeCustomerId);
        } else {
          customer = await createCustomer(place);
        }
        await place.update({
          stripeCustomerId: customer.id
        });
        await attachPaymentMethodToCustomer(customer.id, paymentMethodId);
        const cardsAvailable = await fetchCardsOfCustomer(customer.id);
        if (cardsAvailable?.data?.length === 1) {
          await setCardByDefault(place.stripeCustomerId, paymentMethodId);
          await place.update({stripeHasDefaultCard: true});
        }
        return res.json({
          customer: await retrieveCustomer(customer.id),
          list: await fetchCardsOfCustomer(customer.id)
        });
      } else {
        return res.status(400).json({error: true, message: "Place not found"});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * Set default payment method
 */
router.post(
  '/stripe/set-card-default',
  async (req, res, next) => {
    const v = new Validator(req.body, {
      placeId: 'required|numeric',
      paymentMethodId: 'required|string'
    }, {
      'placeId.required': 'placeId is required',
      'placeId.numeric': 'placeId must be numeric',
      'paymentMethodId.required': 'paymentMethodId is required',
      'paymentMethodId.string': 'paymentMethodId must be string',
    });
    v.check().then((matched) => {
      if (!matched) {
        return res.status(422).json(v.errors);
      }
      next();
    });
  },
  async (req, res) => {
    try {
      const {placeId, paymentMethodId} = req.body;
      const place = await models.Place.findByPk(placeId);
      if (place) {
        if (place.stripeCustomerId) {
          await setCardByDefault(place.stripeCustomerId, paymentMethodId);
          await place.update({stripeHasDefaultCard: true});
          return res.json({
            customer: await retrieveCustomer(place.stripeCustomerId),
            list: await fetchCardsOfCustomer(place.stripeCustomerId),
          });
        } else {
          return res.status(400).json({error: true, message: 'Place have not identify of customer'});
        }
      } else {
        return res.status(400).json({error: true, message: "Place not found"});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * Delete payment method of a customer
 */
router.post(
  '/stripe/delete-card-to-client',
  async (req, res, next) => {
    const v = new Validator(req.body, {
      placeId: 'required|numeric',
      paymentMethodId: 'required|string'
    }, {
      'placeId.required': 'placeId is required',
      'placeId.numeric': 'placeId must be numeric',
      'paymentMethodId.required': 'placeId is required',
      'paymentMethodId.string': 'placeId must be string',
    });
    v.check().then((matched) => {
      if (!matched) {
        return res.status(422).json(v.errors);
      }
      next();
    });
  },
  async (req, res) => {
    try {
      const {placeId, paymentMethodId} = req.body;
      const place = await models.Place.findByPk(placeId);
      if (place) {
        if (place.stripeCustomerId) {
          await deleteCardOfTheCustomer(paymentMethodId);
          const cardList = await fetchCardsOfCustomer(place.stripeCustomerId);
          if (cardList?.data?.length === 0) {
            await place.update({stripeHasDefaultCard: false});
          }
          return res.json({
            customer: await retrieveCustomer(place.stripeCustomerId),
            list: cardList
          });
        } else {
          return res.status(400).json({error: true, message: 'Place have not identify of customer'});
        }
      } else {
        return res.status(400).json({error: true, message: "Place not found"});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * Create orden into PayPal
 */
router.post(
  '/paypal/create-order',
  async (req, res) => {
    try {
      const {data} = req.body;
      const order = await createPayPalOrder(data);
      return res.json({error: false, order: order.data});
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.post('/paypal/capture-order/:orderId', async (req, res) => {
  try {
    const capture = await getPayPalPaymentOrderCapture(req.params.orderId);
    return res.json({error: false, capture: capture.data});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Upload payment file
 */
router.post('/backup-file',
  async (req, res, next) => {
    req.body.file = req.file;
    const v = new Validator(req.body, {
      dir: 'required|string',
      file: 'required|mime:jpg,jpeg,png,pdf'
    });
    if (await v.check()) {
      next();
    } else {
      return res.status(422).json(v.errors);
    }
  },
  async (req, res) => {
    try {
      if (req.file) {
        const folder = req.body.dir;
        const uploadedFileUrl = await awsModule.uploadFile(folder, req.file);
        const paymentUrl = `${env.awsBucketUrl}/${uploadedFileUrl}`;
        return res.json({url: paymentUrl});
      } else {
        return res.status(400).json({error: true, message: 'Not file'});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**************************** PUT ***********************************/

router.put('/generate-electronic-invoice/:paymentId',
  async (req, res) => {
    try {
      const {paymentId} = req.params;
      const payment = await models.Payments.findByPk(paymentId);
      if (payment) {
        await ElectronicInvoicing.generate(payment.placeId, payment);
        return res.json({error: false, payment});
      } else {
        throw new AppError('Payment not found', AppError.TYPE_ERROR.RESOURCE_NOT_FOUND);
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.put('/:paymentId/status',
  async (req, res, next) => {
    const v = new Validator(req.body, {
      status: 'required|string|in:' + Object.values(PAYMENTS_STATUS).join(',')
    });
    if (await v.check()) {
      next();
    } else {
      return res.status(422).json(v.errors);
    }
  },
  async (req, res) => {
    try {
      const {paymentId} = req.params;
      const {status} = req.body;
      const payment = await models.Payments.findByPk(paymentId);
      if (payment) {
        payment.statusPayment = status;
        await payment.save();
        const billing = await payment.getBilling();
        billing.status = Cuts.getStatusByStatusPayment(payment.statusPayment);
        await billing.save();

        if (payment.statusPayment === PAYMENTS_STATUS.APPROVED) {
          ElectronicInvoicing.generate(billing.placeId, payment)
            .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
          Emails.dispatchPaymentByBillingConfirmed(paymentId)
            .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
          Notifications.dispatchPayment(paymentId)
            .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
        } else if (payment.statusPayment === PAYMENTS_STATUS.DECLINED) {
          Emails.dispatchPaymentByBillingReject(paymentId)
            .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
          Notifications.dispatchPayment(paymentId)
            .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
        }
        return res.json(payment);
      } else {
        throw new AppError("Payment not found", AppError.TYPE_ERROR.RESOURCE_NOT_FOUND, 400);
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

/**
 * enable or disable collection auto
 */
router.put('/toggle-collection-cut', async (req, res) => {
  try {
    const {placeId, checked} = req.body;
    const payments = await models.Place.update({stripeCollectionAuto: checked}, {where: {id: placeId}});
    return res.json({payments});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**************************** DELETE ***********************************/

router.delete('/:placeId/:fileType/:id', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    const fileType = req.params.fileType;
    const id = req.params.id;
    await models.PlacePayment.update({[fileType]: null}, {where: {id}});
    // TODO ¿es necesario borrar el archivo de S3?
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
