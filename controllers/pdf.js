const express = require("express");
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const PDF = require('../helpers/pdf.helper');
const moment = require("moment-timezone");
const {Validator} = require("node-input-validator");
const Sanitize = require("express-validator");
const {RESERVATION_ORIGIN, RESERVATION_STATUS, SPONSOR_KEYS, PAYMENTS_STATUS} = require("../helpers/constants.helper");
const {
  getCommissionByChargeOfReservation,
  sum,
  multiply,
  unitsDeliveredBySponsor,
  getChargeApplyToTheReservation
} = require("../helpers/calculate.helper");
const {onlyMessage} = require("../helpers/utils.helper");
const {HandleError} = require("../helpers/error.helper");
const {typeChargesToApplyInRangeDate} = require("../helpers/query.helper");

router.get('/coupon/:code', async (req, res) => {
  try {
    const {code} = req.params;
    const coupon = await models.Coupons.findOne({
      attributes: ['id', 'code'], where: {code}, include: [{
        as: 'promotion', model: models.Promotions, attributes: ['id', 'title', 'description', 'imageUrl'], include: [{
          as: 'place', model: models.Place, attributes: ['id', 'name', 'address', 'logoImage']
        }]
      }]
    });
    if (coupon) {
      const html = await PDF.buildTemplate('coupon', {
        place: {
          name: coupon.promotion.place.name,
          address: coupon.promotion.place.address,
          logo: coupon.promotion.place.logoImage
        }, promotion: {
          start: moment(coupon.promotion.start).format('DD-MM-YYYY'),
          end: moment(coupon.promotion.end).format('DD-MM-YYYY'),
          logo: coupon.promotion.imageUrl,
          title: coupon.promotion.title
        }, coupon: {
          code: coupon.code
        }
      });

      const readable = await PDF.downloadPdf(html);
      res.setHeader('Content-Type', 'application/pdf');
      return readable.pipe(res);
    } else {
      return res.status(400).json({error: true, message: 'Code invalid'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get(
  '/business-group',
  async (req, res, next) => {
    const v = new Validator(req.query, {
      groupBusinessId: 'required|numeric', start: 'required|date', end: 'required|date'
    });
    if (await v.check()) {
      next();
    } else {
      return res.status(422).send(v.errors);
    }
  },
  async (req, res) => {
    try {
      const {groupBusinessId, start, end} = req.query;
      const groupBusiness = await models.GroupsBusinessPlaces.findAll({
        where: {groupsBusinessId: groupBusinessId, isActive: 1},
        include: ['place']
      });
      if (groupBusiness.length) {
        const data = [];
        for (const gb of groupBusiness) {
          // Reservandonos
          const resumeByPartner = {
            reservandonos: {
              google: [],
              others: []
            },
            others: 0
          };
          const resumeCocaCola = {
            delivered: 0,
            total: 0
          }
          const place = gb.place;
          const typeCharges = await place.getTypeCharges({...typeChargesToApplyInRangeDate(start, end)});
          const reservationsReservandonos = await place.getReservation({
            attributes: ['id', 'date', 'status', 'origin', 'peopleCount', 'peopleSeating', 'invoiceAmount', 'partnerId'],
            where: {
              date: {
                [Op.between]: [start, end]
              },
              status: {
                [Op.in]: [RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED]
              },
              partnerId: 1
            },
            include: [{
              model: models.ReservationSponsors,
              as: 'reservationSponsor',
              required: false
            }]
          });
          for (const reservation of reservationsReservandonos) {
            const people = reservation.peopleSeating > 0 ? reservation.peopleSeating : reservation.peopleCount;
            const byCharge = await getCommissionByChargeOfReservation(reservation, typeCharges, place.fixedCharge);
            const chargeToApply = await getChargeApplyToTheReservation(reservation, typeCharges);
            const array = (RESERVATION_ORIGIN.GOOGLE === reservation.origin)
              ? resumeByPartner.reservandonos.google
              : resumeByPartner.reservandonos.others;
            const item = array.find(item => item.key === chargeToApply.type + chargeToApply.value);
            if (item) {
              item.people += people;
              item.amount = await sum(2, item.amount, byCharge);
            } else {
              array.push({
                type: chargeToApply.type,
                value: chargeToApply.value,
                key: chargeToApply.type + chargeToApply.value,
                people: people,
                amount: byCharge
              });
            }
          }
          const {
            quantity,
            commission
          } = await unitsDeliveredBySponsor(reservationsReservandonos, SPONSOR_KEYS.BOTTLE_COCA_COLA_WITHOUT_SUGAR);
          resumeCocaCola.delivered = quantity;
          resumeCocaCola.total = commission;

          let totalByGoogle = 0;
          for (const item of resumeByPartner.reservandonos.google) {
            totalByGoogle = await sum(2, totalByGoogle, item.amount);
          }
          let totalByOthers = 0;
          for (const item of resumeByPartner.reservandonos.others) {
            totalByOthers = await sum(2, totalByOthers, item.amount);
          }

          const totalByCharge = await sum(2, totalByGoogle, totalByOthers);
          const subtotal = await sum(2, place.monthlyPlanPrice, totalByCharge, resumeCocaCola.total);
          const iva = await multiply(2, subtotal, place.taxToApply);
          const total = await sum(2, subtotal, iva);

          const sumPeople = await place.getReservation({
            attributes: [[models.Sequelize.literal('SUM(IF(peopleSeating > 0, peopleSeating, peopleCount))'), 'pax']],
            where: {
              date: {
                [Op.between]: [start, end]
              },
              status: {
                [Op.in]: [RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED]
              },
              [Op.or]: [{partnerId: {[Op.ne]: 1}}, {partnerId: {[Op.is]: null}}],
            },
            raw: true
          });
          resumeByPartner.others = sumPeople[0].pax;
          const item = {
            name: place.name,
            monthlyPlanPrice: place.monthlyPlanPrice,
            resumeByPartner,
            resumeCocaCola,
            subtotal,
            iva,
            total
          };
          data.push(item);
        }
        const html = await PDF.buildTemplate('businessGroup', {
          title: `Resumen desde ${moment(start, 'YYYY-MM-DD').format('DD/MM/YYYY')} hasta ${moment(end, 'YYYY-MM-DD').format('DD/MM/YYYY')}`,
          data
        });
        const readable = await PDF.downloadPdf(html, {
          landscape: true,
          margin: {top: '0.5cm', right: '0.5cm', left: '0.5cm', bottom: '0.5cm'},
        });
        res.setHeader('Content-Type', 'application/pdf');
        return readable.pipe(res);
      } else {
        return res.status(400).json({error: true, message: 'Group not found'});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.get(
  '/commissions-by-seller',
  [
    async (req, res, next) => {
      const rules = {
        startDate: 'required|date',
        endDate: 'required|date',
        sellerId: 'numeric'
      };
      const validator = new Validator(req.query, rules);
      if (await validator.check()) {
        next();
      } else {
        return res.status(422).send(onlyMessage(validator.errors));
      }
    },
    Sanitize.query('sellerId').isNumeric().toInt()
  ],
  async (req, res) => {
    try {
      const {startDate, endDate, sellerId} = req.query;
      const data = {
        title: `Comisiones desde: ${moment(startDate).format('DD-MM-YYYY')} hasta: ${moment(endDate).format('DD-MM-YYYY')}`,
        paymentsTotal: 0,
        totalByChargeWithoutIva: 0,
        totalByChargeWithIva: 0,
        totalWithDiscountWithoutIva: 0,
        totalWithDiscountWithIva: 0,
        totalCocaColaDelivered: 0,
        totalCommissionsCocaCola: 0,
        rows: [],
        totalIva: 0
      };
      const wherePlace = {};
      if (sellerId) {
        wherePlace.sellerId = sellerId;
      }
      const payments = await models.Payments.findAll({
        where: {
          statusPayment: PAYMENTS_STATUS.APPROVED,
          paymentDate: {
            [Op.between]: [
              moment(startDate).startOf('days').format('YYYY-MM-DD HH:mm:ss'),
              moment(endDate).endOf('days').format('YYYY-MM-DD HH:mm:ss')
            ]
          }
        },
        include: [
          {
            model: models.Billings,
            as: 'billing',
            required: true
          }, {
            model: models.Place,
            as: 'place',
            attributes: ['id', 'name', 'fixedCharge', 'hasElectronicBillingEnabled'],
            required: true,
            include: [
              {
                model: models.TypeCharge,
                as: 'typeCharges',
                required: false
              }, {
                model: models.Seller,
                as: 'sellers',
                required: false
              }
            ],
            where: wherePlace
          }],
        order: [['paymentDate', 'asc']]
      });
      for (const payment of payments) {
        data.paymentsTotal++;
        const place = payment.place;
        const billing = payment.billing;
        const typeCharges = place.typeCharges;
        const seller = place.sellers;
        const hasElectronicBillings = place.hasElectronicBillingEnabled;
        const reservations = await billing.getReservation({
          where: {
            status: {[Op.in]: [RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED]},
            partnerId: 1
          },
          include: [{
            model: models.ReservationSponsors,
            as: 'reservationSponsor',
            required: false
          }]
        });
        const row = {
          descriptionCut: payment.comment,
          paymentDate: moment(payment.paymentDate).format('DD-MM-YYYY'),
          nameSeller: seller?.name ?? 'Sin Vendedor',
          commissionsByChargeWithoutIva: 0,
          commissionsByChargeWithIva: 0,
          cocaColaDelivered: 0,
          commissionsByCocaCola: 0,
          totalWithoutIva: 0,
          totalWithIva: 0,
          iva: 0
        };
        for (const reservation of reservations) {
          const byCharge = await getCommissionByChargeOfReservation(reservation, typeCharges, place.fixedCharge)
          row.commissionsByChargeWithoutIva = await sum(2, row.commissionsByChargeWithoutIva, byCharge);
        }
        row.commissionsByChargeWithIva = hasElectronicBillings ? await multiply(2, row.commissionsByChargeWithoutIva, await sum(2, 1, billing.appliedTax)) : 0;
        row.iva = hasElectronicBillings ? await multiply(2, row.commissionsByChargeWithoutIva, await sum(2, billing.appliedTax)) : 0;
        const {
          commission,
          quantity
        } = await unitsDeliveredBySponsor(reservations, SPONSOR_KEYS.BOTTLE_COCA_COLA_WITHOUT_SUGAR);
        row.cocaColaDelivered = quantity;
        row.commissionsByCocaCola = commission;

        row.totalWithoutIva = await sum(2, row.commissionsByChargeWithoutIva, row.commissionsByCocaCola);
        row.totalWithIva = hasElectronicBillings ? await sum(2, row.totalWithoutIva, row.iva) : row.totalWithoutIva;
        data.rows.push(row);
        data.totalByChargeWithoutIva = await sum(2, data.totalByChargeWithoutIva, row.commissionsByChargeWithoutIva);
        data.totalByChargeWithIva = await sum(2, data.totalByChargeWithIva, row.commissionsByChargeWithIva);
        data.totalIva = await sum(2, data.totalIva, row.iva);
        data.totalWithDiscountWithoutIva = await sum(2, data.totalWithDiscountWithoutIva, row.totalWithoutIva);
        data.totalWithDiscountWithIva = await sum(2, data.totalWithDiscountWithIva, row.totalWithoutIva, row.iva);
        data.totalCocaColaDelivered = await sum(2, data.totalCocaColaDelivered, row.cocaColaDelivered);
        data.totalCommissionsCocaCola = await sum(2, data.totalCommissionsCocaCola, row.commissionsByCocaCola);
      }
      const html = await PDF.buildTemplate('commissionsBySeller', data);
      const readable = await PDF.downloadPdf(html, {
        landscape: true,
        margin: {top: '0.5cm', right: '0.5cm', left: '0.5cm', bottom: '0.5cm'},
      });
      res.setHeader('Content-Type', 'application/pdf');
      return readable.pipe(res);
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

module.exports = router;

