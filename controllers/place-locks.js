const express = require('express');
const router = express.Router();
const moment = require('moment');
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");
const Op = models.Sequelize.Op;

router.post('/', async (req, res) => {
  try {
    const result = await models.PlaceLock.create(req.body)
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const deleted = await models.PlaceLock.destroy({where: {id: req.params.id}});
    if (deleted == 1) {
      return res.json({destroy: true, id: req.params.id});
    } else {
      return res.json({destroy: false, id: req.params.id});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:placeId', async (req, res) => {
  try {
    const result = await models.PlaceLock.findAll({where: {placeId: req.params.placeId}, order: ['date']});
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
