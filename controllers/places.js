const express = require('express');
const router = express.Router();
const moment = require('moment');
const models = require('../models');
const Op = models.Sequelize.Op;
const {emailModule} = require('../helpers/email.helper');
const env = require('../config/env');
const encryption = require('../helpers/encryption');
const conektaModule = require('../helpers/conekta');
const smsModule = require('../helpers/sms');
const {HandleError, InternalProcessError} = require("../helpers/error.helper");
const sequelize = require("sequelize");
const {SPONSOR_KEYS, SPONSOR_PLACE_TYPE} = require("../helpers/constants.helper");
const {trailingComma} = require("nodemailer/.prettierrc");
const {typeChargesToApplyInRangeDate} = require("../helpers/query.helper");

// const Redis = require('./../redis');

function orderList(objs, key, type) {
  return type === 'desc' ? objs.sort((a, b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0)) :
    objs.sort((a, b) => (a[key] < b[key]) ? 1 : ((b[key] < a[key]) ? -1 : 0));
}

/************************************ GET ******************************/


router.get('/by-name/:name', async (req, res) => {
  try {
    const name = req.params.name;
    const places = await models.Place.findAll({
      where: {name: {[Op.like]: `%${name}%`}},
      attributes: ['id', 'name']
    });
    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-token/:token', async (req, res) => {
  try {
    const placeId = JSON.parse(encryption.decrypt(req.params.token));
    const place = await models.Place.findByPk(placeId, {attributes: ['isActive', 'monthlyPlanPrice']});
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/user/places', async (req, res) => {
  try {
    const email = req.user.email;
    const queryRes = await models.User.findAndCountAll({
      where: {email},
      attributes: ['id', 'placeId'],
      include: {
        model: models.Place,
        as: 'place',
        attributes: ['name', 'typeAccount']
      }
    });
    res.json(queryRes);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/user-place/zoom', async (req, res) => {
  try {
    const place = await models.Place.findOne({where: {id: req.user.placeId}, attributes: ['zoom']});
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/userPlaces/:userId', async (req, res) => {
  try {
    const places = await models.Place.findAll({
      include: [
        {
          model: models.User,
          as: 'usersOfPlace',
          attributes: [],
          through: {where: {userId: req.params.userId}},
          required: true
        },
        {model: models.StayTime, as: 'stayTimes', attributes: ['peopleCount', 'time']},
        {
          model: models.Service, as: 'services', attributes: ['id', 'name', 'start', 'end', 'weekdays'],
          order: [['name', 'asc'],]
        },
        {model: models.Partner, as: 'partners', attributes: ['id', 'name']},
        {model: models.BillingFrequency, as: 'billingFrequency', attributes: ['id', 'description', 'frequency']},
        {model: models.TypeCharge, as: 'typeCharges', attributes: ['id', 'description', 'type', 'value', 'origin']},
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          required: false,
          include: [{
            model: models.Sponsor,
            as: 'sponsor'
          }]
        },
        'categories'
      ],
      where: {
        isActive: 1
      }
    });
    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/payed/:placeId', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    const date = new Date();
    const day = date.getDate()
    const month = date.getMonth() + 1;
    const payment = await models.PlacePayment.findOne({
      where: {
        [Op.and]: models.sequelize.where(models.sequelize.fn("MONTH", models.sequelize.col('createdAt')), month),
        placeId
      }
    });
    if (payment) {
      return res.json({payed: true});
    } else {
      const place = await models.Place.findByPk(placeId, {attributes: ['payDay', 'extraTimeDate']});
      if (day > place.payDay) {
        return res.json({payed: false, remainingDays: place.extraTimeDate - day});
      } else {
        return res.json({payed: true});
      }
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/invitePartner/:placeId', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    const place = await models.Place.findOne({where: {id: placeId}})
    emailModule.sendIntitationRsv(place.name, place.id, place.email)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));
    return res.status(201).send({message: 'El correo fue enviado'});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/registerPlace/:placeId', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    const place = await models.Place.findOne({
      where: {id: placeId},
      attributes: ['id', 'name', 'typeChargeId'],
      include: [
        {model: models.TypeCharge, as: 'typeCharges', attributes: ['id', 'description', 'type', 'value', 'origin']},
        {model: models.User, as: 'usersOfPlace', through: {where: {placeId: req.params.placeId}}, required: true}
      ]
    });
    if (place && place.dataValues.usersOfPlace.length) {
      place.usersOfPlace = place.usersOfPlace.map(value => {
        value.dataValues = {
          ...value.dataValues,
          notificationEmail: value.UsersPlaces.notificationEmail,
          notificationWhatsApp: value.UsersPlaces.notificationWhatsApp
        }
        return value;
      });
    }
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/selectedPlace/:placeId', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    const place = await models.Place.findOne({
      where: {id: placeId},
      attributes: ['id', 'name', 'numberMaxMsm', 'zoom', 'events', 'advancePaymentPolicyNumberPeople', 'advancePaymentPolicyEnable', 'advancePaymentPolicyFeePerPerson', 'cancellationFeePerPerson', 'cancellationPolicyEnable', 'numberPersonPolicyCancellation', 'priorityPaymentWidget', 'planId', 'paymentGatewayPublicKey', 'onlineSales', 'primaryColor', 'secondaryColor', 'typeAccount', 'logoImage'], // TODO
      include: [
        {model: models.StayTime, as: 'stayTimes', attributes: ['peopleCount', 'time']},
        {
          model: models.Service, as: 'services', attributes: ['id', 'name', 'start', 'end', 'weekdays'],
          order: [['name', 'asc'],]
        },
        {model: models.Partner, as: 'partners', attributes: ['id', 'name']},
        {model: models.TypeCharge, as: 'typeCharges', attributes: ['id', 'description', 'type', 'value', 'origin']},
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          include: [{
            model: models.Sponsor,
            as: 'sponsor'
          }]
        }, 'categories'
      ]
    });
    // messages limit
    const dateEnd = moment().endOf('month').format('YYYY-MM-DD');
    const dateStart = moment().startOf('month').format('YYYY-MM-DD');
    const allMonthReservations = await models.Reservation.findAll({
      where: {placeId: req.body.placeId, date: {[Op.between]: [dateStart, dateEnd]}},
      raw: true,
      attributes: ['id', 'notificationSms', 'notificationWhatsapp']
    });
    let countMsm, countWhatsapp;
    countMsm = allMonthReservations.filter(elem => elem.notificationSms === 1);
    countWhatsapp = allMonthReservations.filter(elem => elem.notificationWhatsapp === 1);
    const limits = {msm: countMsm.length, whatsapp: countWhatsapp.length};
    if (place) {
      place.dataValues['limits'] = limits;
    }
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/full/:placeId', async (req, res) => {
  try {
    let place = await models.Place.findOne({
      where: {id: req.params.placeId},
      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']},
      include: [
        {
          model: models.Zone, as: 'zones', attributes: ['id', 'name', 'coordX', 'coordY'], include: [
            {
              model: models.Layout, as: 'layouts', attributes: ['id'], include: [
                {
                  model: models.Table,
                  as: 'tables',
                  attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt', 'placeId', 'layoutId']},
                },
                {
                  model: models.Group,
                  as: 'groups',
                  attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt', 'placeId', 'layoutId']},
                }
              ]
            }
          ]
        },
        {model: models.StayTime, as: 'stayTimes', attributes: ['peopleCount', 'time']},
        {
          model: models.Service, as: 'services', attributes: ['id', 'name', 'start', 'end', 'weekdays'], include: [
            {model: models.ServiceZone, as: 'serviceZones', attributes: ['zoneId', 'layoutId', 'smokeArea']}
          ],
          order: [['name', 'asc'],]
        },
        {model: models.Partner, as: 'partners', attributes: ['id', 'name']}
      ]
    });
    if (place.planId == 1) {
      const openingDays = await models.OpeningDay.findAll({where: {placeId: place.id}})
      place = {...place.dataValues, openingDays}
    }
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/sendSMS/:placeId', async (req, res) => {
  try {
    const now = moment();
    const result = await models.SmsPlaces.count({
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['id']
        },
        {
          model: models.User,
          as: 'users',
          attributes: ['id']
        }
      ],
      where: {
        [Op.and]: [
          models.Sequelize.where(models.Sequelize.fn('YEAR', models.Sequelize.col('SmsPlaces.createdAt')), now.format('YYYY')),
          models.Sequelize.where(models.Sequelize.fn('MONTH', models.Sequelize.col('SmsPlaces.createdAt')), now.format('MM')),
          {placeId: req.params.placeId}
        ]
      }
    });
    const place = await models.Place.findOne({
      attributes: ['numberMaxMsm'],
      where: {
        id: req.params.placeId
      }
    });
    return res.json({available: place.numberMaxMsm - result, max: place.numberMaxMsm, send: result});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/sms-sent/:placeId', async (req, res) => {
  try {
    const where = {placeId: req.params.placeId};
    const start = req.query.start || moment().format();
    const end = req.query.end || moment().format();
    where['createdAt'] = {
      [Op.and]: {[Op.gte]: start, [Op.lte]: end}
    }
    const sentList = await models.SmsPlaces.findAndCountAll({where});
    return res.json(sentList);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/sponsor-by-id', async (req, res) => {
  try {
    let where = {};
    let whereReservations = {};
    let whereLoadManual = {};
    if (req.query.term && req.query.term != '') {
      where[Op.or] = [
        {name: {[Op.like]: `%${req.query.term}%`}}
      ];
    }
    if (req.query.cities && req.query.cities != '') {
      let city = (req.query.cities).split(',');
      where.cityId = {[Op.in]: city};
      where.cityId = {[Op.in]: city};
    }
    if (req.query.placeId && req.query.placeId != '') {
      where['id'] = req.query.placeId;
      whereLoadManual['placeId'] = req.query.placeId;
    }

    const countPlaces = await models.Place.count({
      where: where,
      include: [
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['isActive', 'type', 'value', 'placeId'],
          where: {
            sponsorId: 2
          }
        }
      ]
    });

    if (req.query.dateStart && req.query.dateEnd) {
      const obj = {
        [Op.gte]: req.query.dateStart,
        [Op.lte]: req.query.dateEnd
      };
      whereReservations.date = obj;
      whereLoadManual.date = obj;
    } else if (req.query.dateStart) {
      whereReservations.date = {[Op.gte]: req.query.dateStart};
      whereLoadManual.date = {[Op.gte]: req.query.dateStart};
    } else if (req.query.dateEnd) {
      whereReservations.date = {[Op.lte]: req.query.dateEnd};
      whereLoadManual.date = {[Op.lte]: req.query.dateEnd};
    }

    let sumCocas = await models.Place.findAll({
      where: where,
      include: [
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['isActive', 'type', 'value', 'placeId'],
          where: {
            sponsorId: 2
          }
        },
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          attributes: [[models.Sequelize.fn('sum', models.Sequelize.col('quantity')), 'total_coca_cola']],
          include: [
            {
              model: models.Reservation,
              as: 'reservation',
              where: whereReservations,
              attributes: ['id']
            }
          ]
        }
      ]
    });

    const numberCocas = sumCocas.length > 0 && sumCocas[0]['reservationSponsor'].length > 0 ? Number(sumCocas[0]['reservationSponsor'][0]['dataValues']['total_coca_cola']) : 0;

    const places = await models.Place.findAll({
      attributes: ['name', 'address', 'id', 'cityId', 'logoImage'],
      where: where,
      include: [
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['isActive', 'type', 'value', 'placeId'],
          where: {
            sponsorId: 2
          }
        },
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          attributes: ['quantity'],
          include: [
            {
              model: models.Reservation,
              as: 'reservation',
              where: whereReservations,
              attributes: ['id']
            }
          ]
        }
      ]
    });

    const sumSponsorPlaceManual = await models.SponsorPlaceManual.findAll({
      attributes: [[models.Sequelize.fn('sum', models.Sequelize.col('value')), 'total_coca_cola']],
      where: {...whereLoadManual, value: {[Op.lt]: 0}}
    });

    const numberCocasManual = sumSponsorPlaceManual.length > 0 ? Number(sumSponsorPlaceManual[0]['dataValues']['total_coca_cola']) * -1 : 0;

    const placeSponsorManual = await models.Place.findAll({
      attributes: ['name', 'address', 'id', 'cityId', 'logoImage'],
      where: where,
      include: [
        {
          model: models.SponsorPlaceManual,
          as: 'sponsorPlaceManual',
          where: {...whereLoadManual, value: {[Op.lt]: 0}}
        }
      ]
    });

    let rows = [];
    let excl = placeSponsorManual;
    for (let place of places) {
      let sum = 0;
      let sumManual = 0;
      place.dataValues.reservationSponsor.forEach(rsv => sum += rsv.quantity);
      const manual = placeSponsorManual.find(elem => elem.id == place.dataValues['id']);
      if (manual) {
        manual.dataValues.sponsorPlaceManual.forEach(rsv => sumManual += rsv.value);
      }
      excl = excl.filter(elem => elem.id != place.dataValues['id']);
      rows.push({
        name: place.dataValues['name'],
        logoImage: place.dataValues['logoImage'],
        address: place.dataValues['address'],
        cityId: place.dataValues['cityId'],
        id: place.dataValues['id'],
        cocaColas: sum + (sumManual * -1)
      });
    }


    for (const row of excl) {
      let sumManual = 0;
      row.dataValues.sponsorPlaceManual.forEach(rsv => sumManual += rsv.value);
      rows.push({
        name: row.dataValues['name'],
        logoImage: row.dataValues['logoImage'],
        address: row.dataValues['address'],
        cityId: row.dataValues['cityId'],
        id: row.dataValues['id'],
        cocaColas: sumManual * -1
      });
    }

    rows = orderList(rows, req.query.sortBy, req.query.sortDir);

    return res.json({count: countPlaces, totalCocas: numberCocas + numberCocasManual, metaCocas: 50000, rows: rows});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id/type-charges', async (req, res, next) => {
  try {
    const {id} = req.params;
    const {start = null, end = null} = req.query;
    const place = await models.Place.findOne({
      attributes: ['id', 'name', 'fixedCharge', 'monthlyPlanPrice', 'hasElectronicBillingEnabled'],
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          ...typeChargesToApplyInRangeDate(start, end)
        }
      ],
      where: {id}
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-city', async (req, res) => {
  try {
    let city = '';
    let where = {
      isActive: 1
    };
    if (req.query.cities) {
      city = (req.query.cities).split(',');
      where.cityId = {[Op.in]: city};
    }
    const places = await models.Place.findAll({
      attributes: ['id', 'name'],
      include: [{
        attributes: ['id'],
        model: models.City,
        as: 'cities',
      }],
      where: where
    });
    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/free', async (req, res) => {
  try {
    if (req.query.key !== env.urlKey) {
      return res.status(403).json({message: 'Forbidden'});
    }
    const place = await models.Place.findAll({
      attributes: ['id', 'name', 'tablesCount', 'timezone', 'enableGoogleReserve', 'methodReserve'],
      include: [{
        model: models.OpeningDay,
        as: 'openingDays',
        attributes: ['weekday', 'start', 'end', 'start2', 'end2', 'smokeArea']
      }],
      where: {
        enableGoogleReserve: 1
      }
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/all', async (req, res) => {
  try {
    const result = await models.Place.findAndCountAll({
      attributes: ['id', 'name']
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const place = await models.Place.findByPk(req.params.id, {
      attributes: {include: ['planId', 'ownerId']},
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          required: false
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          include: ['sponsor'],
          where: {
            // isActive: true,
            type: SPONSOR_PLACE_TYPE.COMMISSION
          },
          required: false
        }]
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/', async (req, res) => {
  try {
    const {
      pdf = false, sortBy = '', sortDir = '', dateStart = '', dateEnd = '',
      dateStart1 = '', dateEnd1 = '', cities = '', seller = '', term = ''
    } = req.query;
    let order;
    let where = {};

    switch (sortBy) {
      case 'plan':
        order = [{model: models.Plan, as: 'plan'}, 'name', sortDir];
        break;
      case 'cut':
        order = [{model: models.BillingFrequency, as: 'billingFrequency'}, 'description', sortDir];
        break;
      case 'seller':
        order = [{model: models.Seller, as: 'sellers'}, 'name', sortDir];
        break;
      case 'place':
        order = ['name', sortDir];
        break;
      default:
        order = [sortBy, sortDir];
        break;
    }

    if (dateStart && dateEnd && dateStart1 && dateEnd1) {
      where.createdAt = {
        [Op.or]: [{
          [Op.between]: [dateStart, dateEnd]
        }, {
          [Op.between]: [dateStart1, dateEnd1]
        }]
      }
    } else if (dateStart && dateEnd) {
      where.createdAt = {
        [Op.between]: [dateStart, dateEnd]
      };
    }

    if (cities) {
      where.cityId = {[Op.in]: cities.split(',')};
    }

    if (seller) {
      where.sellerId = {[Op.in]: seller.split(',')};
    }

    where[Op.or] = [
      {name: {[Op.like]: `%${term}%`}},
      {email: {[Op.like]: `%${term}%`}},
      {phone: {[Op.like]: `%${term}%`}}
    ];

    const ttActivePlace = await models.Place.count({
      attributes: ['id'],
      where: where,
    });

    let optionsPlaces = {
      include: [
        {model: models.Plan, as: 'plan'},
        {model: models.BillingFrequency, as: 'billingFrequency', attributes: ['description', 'frequency']},
        {model: models.Seller, as: 'sellers', attributes: ['name']},
        {model: models.SponsorPlaces, as: 'sponsorsPlaces', required: false, where: {sponsorId: 2}}
      ],
      where: where
    };

    if (pdf === false) {
      optionsPlaces = {
        ...optionsPlaces,
        order: [order],
        offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
        limit: +req.query.pageSize || 10
      }
    }

    const allPlaces = await models.Place.findAndCountAll(optionsPlaces);
    return res.json({count: allPlaces.count, rows: allPlaces.rows, totalActive: ttActivePlace});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/************************************ POST ******************************/

router.post('/webhook', async (req, res) => {
  try {
    if (req.query.key !== env.urlKey) {
      return res.status(403).json({message: 'Forbidden'});
    }
    switch (req.body.type) {
      case 'subscription.paid':
        const place = await models.Place.findOne({
          where: {paymentCustomerId: req.body.data.object.customer_id}
        });
        if (place) {
          place.dueDate = moment.unix(req.body.data.object.billing_cycle_end).utc().format();
          await place.save();
        } else {
          // TODO ¿qué regresar?
        }
        break;
    }
    return res.sendStatus(200);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/sendSms', async (req, res) => {
  try {
    const customers = req.body.customers;
    for (const item of customers) {
      await smsModule.sendSMS(item.phone, req.body.message);
      await models.SmsPlaces.create({
        phone: item.phone,
        message: req.body.message,
        userId: req.body.userId,
        placeId: req.body.placeId
      });
    }
    return res.json({send: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res) => {
  try {
    const place = await models.Place.create(req.body)
    emailModule.verifyEmailIdentity(place.email)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));
    emailModule.recurrentCardEmail(place.id)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/************************************ PUT ******************************/

router.put('/change-status/:placeId', async (req, res) => {
  try {
    const id = req.params.placeId;
    const isActive = req.body.newStatus;
    const upd = await models.Place.update({isActive}, {where: {id}});
    // enviar correo de cambio de status
    emailModule.placeChangeStatus(id)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {place: upd.dataValues})));
    return res.json(upd);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/update-zoom', async (req, res) => {
  try {
    const placeId = req.user.placeId;
    const zoom = req.body.zoom;
    await models.Place.update({zoom}, {where: {id: placeId}});
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/update-reserve-online', async (req, res) => {
  try {
    const placeId = req.user.placeId;
    const onlineSales = req.body.online;
    await models.Place.update({onlineSales}, {where: {id: placeId}});
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/add-recurrent-card', async (req, res) => {
  try {
    const placeId = JSON.parse(encryption.decrypt(req.body.token));
    const planId = `plan-${placeId}`;
    const cardToken = req.body.cardToken;
    const privateKey = env.paymentGatewayPrivateKey;
    const place = await models.Place.findByPk(placeId, {attributes: ['monthlyPlanPrice', 'paymentCustomerId', 'name', 'paymentEmail']});
    let plan = await conektaModule.plan.get(planId);
    if (plan && plan.id) { // actualizar precio de ser necesario
      console.log('amount', plan.amount)
      if (plan.amount != place.monthlyPlanPrice) {
        const newPlanData = {id: planId, amount: place.monthlyPlanPrice * 100};
        plan = await conektaModule.plan.update(newPlanData);
      }
    } else {
      const planData = {
        "id": planId,
        "name": `Plan para ${place.name}`,
        "amount": place.monthlyPlanPrice * 100,
        "currency": "MXN",
        "interval": "month"
      }
      plan = await conektaModule.plan.create(planData);
    }

    let customer = place.paymentCustomerId ? await conektaModule.customer.get(place.paymentCustomerId, privateKey) : null;
    if (!customer || !customer.toObject().id) {
      console.log('no existe el cliente, se hará uno nuevo');
      const customerData = {
        name: `${place.name}`,
        email: place.paymentEmail,
      }
      customer = await conektaModule.customer.create(customerData);
    }
    console.log('customer', customer)

    const paymentSource = await conektaModule.createPaymentSource(customer.id, cardToken.id, privateKey);
    console.log('payment source', paymentSource);

    const subscription = await conektaModule.subscription.create(customer.id, planId, paymentSource.id);
    const date = new Date();
    const payDay = date.getDate();
    await models.Place.update({
      'paymentCustomerId': customer.id,
      'paymentCardId': paymentSource.id,
      'paymentSubscriptionId': subscription.id,
      'isActive': true,
      payDay
    }, {where: {id: placeId}});
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    let place = await models.Place.findByPk(req.body.id);
    place = Object.assign(place, req.body);
    await place.save();
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/************************************ PUT ******************************/

router.delete('/:id', async (req, res) => {
  try {
    const deleted = await models.Place.destroy({
      where: {
        id: req.params.id
      }
    });
    return res.json(deleted);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
