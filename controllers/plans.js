const express = require('express');
const router = express.Router();
const models  = require('../models');
const Op = models.Sequelize.Op;
const auth = require('../middlewares/auth');
const payment = require('../helpers/conekta');
const {emailModule: email} = require('../helpers/email.helper');
const {HandleError} = require("../helpers/error.helper");
const {ROLES} = require("../helpers/constants.helper");
const Redis = require("../redis");

router.get('/', async (req, res) => {
  try {
    const data = await Redis.getRegister(`plans`);
    if (data) {
      console.log('REDIS-plans');
      return res.json(data);
    }
    const result = await   models.Plan.findAndCountAll({
      where: {
        name: {
          [Op.like]: `%${req.query.term}%`
        }
      },
      order: [
        [req.query.sortBy, req.query.sortDir]
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    Redis.setRegister(result, `plans`);
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const plan = await models.Plan.findByPk(req.params.id, {});
    return res.json(plan);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', auth.hasRole(ROLES.ADMIN), async (req, res) => {
  try {
    const plan = await models.Plan.create(req.body);
    return res.json(plan);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', auth.hasRole(ROLES.ADMIN), async (req, res) => {
  try {
    let plan = await models.Plan.findByPk(req.body.id);
    plan = Object.assign(plan, req.body);
    await plan.save();
    await payment.plan.update(plan);
    return res.json(plan);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/send-card-email/:placeId', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    await email.recurrentCardEmail(placeId);
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
