const express = require('express');
const router = express.Router();
const models = require('../models');
const {Validator} = require('node-input-validator');
const {onlyMessage} = require("../helpers/utils.helper");
const awsModule = require("../helpers/aws");
const env = require("../config/env");
const {FOLDERS} = require("../helpers/constants.helper");
const {ReplaceKeyByValue} = require("../helpers/string.helper");
const moment = require("moment-timezone");
const {HandleError} = require("../helpers/error.helper");
const sequelize = require("sequelize");
const Op = models.Sequelize.Op;

/**
 * Resumen de Codigos redimidos
 */
router.get('/resume', async (req, res) => {
  try {
    const { dateStart, dateEnd, placeId } = req.query;
    let whereCoupons = {};
    // let wherePlace = placeId ? {id: placeId} : {};
    let wherePromotions = placeId ? {placeId} : {};

    if (dateStart && dateEnd) {
      whereCoupons.createdAt = {
        [Op.gte]: moment(dateStart).startOf('day'),
        [Op.lte]: moment(dateEnd).endOf('day')
      };
    } else if (dateStart) {
      whereCoupons.createdAt = {[Op.gte]: moment(dateStart).startOf('day')};
    } else if (dateEnd) {
      whereCoupons.createdAt = {[Op.lte]: moment(dateEnd).startOf('day')};
    }

    const summary = await models.Promotions.findAll({
      attributes: [],
      where: wherePromotions,
      include: [
        {
          model: models.Coupons,
          as: 'coupons',
          attributes: [
            [sequelize.literal('SUM(IF(redeemed = 1, 1, 0))'), 'redeemed'],
            [sequelize.literal('SUM(IF(redeemed = 0, 1, 0))'), 'available']
          ],
          where: whereCoupons
        }
      ],
      subQuery: false
    });

/*
    const countPlaces = await models.Place.count({
      distinct: true,
      required: true,
      include: [
        {
          model: models.Promotions,
          as: 'promotions',
          required: true
        }
      ],
      where: wherePlace,
      subQuery: false
    });
*/
    let doughnutChartData = [0, 0];
    let doughnutChartLabels = ['Códigos Redimidos', 'Códigos Descargados'];

    const coupons = summary[0].coupons.length > 0 ? summary[0].coupons[0] : {redeemed: 0, available: 0};

    if (summary[0].coupons.length > 0) {
      doughnutChartData = [
        Number(coupons.dataValues.redeemed),
        Number(coupons.dataValues.redeemed) + Number(coupons.dataValues.available)
      ]
    }

    return res.json({error: false, doughnutChartData, doughnutChartLabels});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Listado detallado de lugares con Places
 */
router.get('/places', async (req, res) => {
  try {
    const { pageIndex, pageSize, placeId, dateStart, dateEnd } = req.query;
    let wherePlace = placeId ? {id: placeId} : {};
    let wherePromotions = placeId ? {placeId} : {};
    let whereCoupons = {};

    if (dateStart && dateEnd) {
      whereCoupons.createdAt = {
        [Op.gte]: moment(dateStart).startOf('day'),
        [Op.lte]: moment(dateEnd).endOf('day')
      };
    } else if (dateStart) {
      whereCoupons.createdAt = {[Op.gte]: moment(dateStart).startOf('day')};
    } else if (dateEnd) {
      whereCoupons.createdAt = {[Op.lte]: moment(dateEnd).startOf('day')};
    }

    const places = await models.Place.findAndCountAll({
      distinct: true,
      attributes: ['id', 'name', 'logoImage'],
      where: wherePlace,
      required: true,
      include: [
        {
          model: models.Promotions,
          as: 'promotions',
          required: true,
          where: wherePromotions,
          attributes: ['id', 'title'],
          include: [
            {
              model: models.Coupons,
              as: 'coupons',
              where: whereCoupons
            }
          ]
        }
      ],
      order: ['name'],
      offset: (+pageIndex * +pageSize) || 0,
      limit: +pageSize || 10,
      subQuery: false,
      logging: true
    });

    let info = [];
    for (const place of places?.rows) {
      let downloads = 0, redeemed = 0;
      for (const promotion of place?.promotions) {
         downloads +=  promotion?.coupons.length;
         redeemed +=  promotion?.coupons.filter(elem => elem.redeemed).length;
      }
      info.push({
        id: place.id,
        name: place.name,
        logoImage: place.logoImage,
        downloads: downloads,
        redeemed: redeemed
      });
    }

    return res.json({error: false, count: places.count, places: info});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/place/:placeId', async (req, res) => {
  try {
    const {placeId} = req.params;
    const promotions = await models.Promotions.findAll({
      where: {
        placeId
      }
    });
    return res.json({error: false, promotions});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/widget/place/:placeId', async (req, res) => {
  try {
    const now = moment().format('YYYY-MM-DD');
    const {placeId} = req.params;
    const promotions = await models.Promotions.findAll({
      where: {
        placeId,
        start: {
          [Op.lte]: now,
        },
        end: {
          [Op.gte]: now
        }
      }
    });
    return res.json({error: false, promotions});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const {id: promotionId} = req.params;
    const promotion = await models.Promotions.findByPk(promotionId);
    if (promotion) {
      return res.json({error: false, promotion});
    } else {
      return res.status(400).json({error: true, message: "Promotion not found"});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/',
  async (req, res, next) => {
    req.body.file = req.file;
    const v = new Validator(req.body, {
      placeId: 'required|numeric',
      title: 'required|string',
      isActive: 'required|boolean',
      description: 'string',
      start: 'required|date',
      end: 'required|date',
      file: 'mime:jpg,png,jpeg'
    });
    if (!await v.check()) {
      return res.status(422).send(onlyMessage(v.errors));
    }
    next();
  },
  async (req, res) => {
    try {
      const {placeId, title, description, isActive, start, end} = req.body;
      const {file} = req;

      const place = await models.Place.findByPk(placeId);
      if (place) {
        let imageUrl;
        if (file) {
          const folder = ReplaceKeyByValue(FOLDERS.PROMOTIONS, {placeId});
          const uploadedFileUrl = await awsModule.uploadFile(folder, req.file);
          imageUrl = `${env.awsBucketUrl}/${uploadedFileUrl}`;
        }
        const promotionAdded = await models.Promotions.create({
          placeId, title, isActive, description, imageUrl, start, end
        });
        return res.json({error: false, promotion: promotionAdded});
      } else {
        return res.status(400).json({error: true, message: `Place not found`});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.put('/:id',
  async (req, res, next) => {
    req.body.file = req.file;
    const v = new Validator(req.body, {
      title: 'required|string',
      isActive: 'required|boolean',
      description: 'string',
      file: 'mime:jpg,png,jpeg',
      start: 'required|date',
      end: 'required|date'
    });
    if (!await v.check()) {
      return res.status(422).send(onlyMessage(v.errors));
    }
    next();
  },
  async (req, res) => {
    try {
      const {id: promotionId} = req.params;
      const {title, description, isActive, start, end} = req.body;
      const {file} = req;

      const promotion = await models.Promotions.findByPk(promotionId);
      if (promotion) {
        let imageUrl = promotion.imageUrl;
        if (file) {
          const folder = ReplaceKeyByValue(FOLDERS.PROMOTIONS, {placeId: promotion.placeId});
          const uploadedFileUrl = await awsModule.uploadFile(folder, req.file);
          imageUrl = `${env.awsBucketUrl}/${uploadedFileUrl}`;
        }
        const promotionUpdated = await promotion.update({title, description, isActive, imageUrl, start, end});
        return res.json({error: false, promotion: promotionUpdated});
      } else {
        return res.status(400).json({error: true, message: `Promotion not found`});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.delete('/:id', async (req, res) => {
  try {
    const {id: promotionId} = req.params;
    const promotion = await models.Promotions.findByPk(promotionId);
    if (promotion) {
      await promotion.destroy({force: true});
      return res.json({error: false, promotion});
    } else {
      return res.status(400).json({error: true, message: "Promotion not found"});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
