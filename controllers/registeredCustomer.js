const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const models = require("../models");
const {Validator} = require('node-input-validator');
const env = require('../config/env')
const Op = models.Sequelize.Op;
const {emailModule} = require('../helpers/email.helper');
const {decrypt} = require('../helpers/encryption');
const moment = require('moment');
const awsModule = require('../helpers/aws');
const pointsModule = require('../helpers/points');
const {createNotification} = require('../helpers/notificationsCustomer');
const {HandleError} = require("../helpers/error.helper");
const {onlyMessage} = require("../helpers/utils.helper");
const {POINTS} = require("../helpers/constants.helper");

async function getInfoFromToken(req) {
  return new Promise((resolve, reject) => {
    const token = req.headers.authorization && req.headers.authorization.split(' ')[1] || null;
    jwt.verify(token, env.jwtSecret, function (err, decoded) {
      if (err) {
        resolve(null);
      }
      resolve(decoded?.user ? decoded?.user : decoded);
    });
  });
}

function checkImage(file) {
  if (file)
    if (file.length != 0)
      if (file.mimetype.indexOf('image') >= 0)
        return true;

  return false;
}

function verifyToken(req, res, next) {
  const token = req.headers.authorization && req.headers.authorization.split(' ')[1] || null;
  if (!token) {
    return res.status(401).json({error: {message: 'Usuario no autenticado.'}})
  }
  jwt.verify(token, env.jwtSecret, function (err, decoded) {
    if (err) {
      res.status(401).json({error: {message: 'Usuario no autenticado.'}})
    } else {
      next()
    }
  });
}

router.get('/coupons', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    const coupons = await models.Coupons.findAll({
      where: {
        registeredCustomerId: user?.id
      },
      include: [{
        model: models.Promotions,
        as: 'promotion',
        include: [{
          model: models.Place,
          as: 'place',
          attributes: ['name', 'logoImage']
        }]
      }],
      order: [['id', 'DESC']]
    });
    return res.json(coupons);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/register',
  async (req, res, next) => {
    const rules = {
      name: 'required',
      lastName: 'required',
      email: 'required|email',
      lada: 'required',
      phone: 'required|phoneNumber'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const registeredCustomer = req.body;
      const oldCustomer = await models.RegisteredCustomer.findOne({where: {[Op.or]: [{email: registeredCustomer.email}, {phone: registeredCustomer.phone}]}});
      if (oldCustomer) {
        return res.status(400).json({
          error: {
            message: "El email o el teléfono ya estan registrados.",
          }
        });
      }

      if (!registeredCustomer.facebookUser) {
        if (!registeredCustomer.password || registeredCustomer.password.length < 8 || registeredCustomer.password.length > 255) {
          return res.status(400).json({
            error: {
              message: "El password debe tener entre [8 - 255] caracteres.",
            }
          });
        }
        registeredCustomer.password = await bcrypt.hash(registeredCustomer.password, 10);
      } else {
        try {
          delete registeredCustomer.password
        } catch (error) {
        }
      }
      //Se agregan puntos
      registeredCustomer.code = pointsModule.randomCode(10);
      //Se revisa si viene con codigo referenceCode, se valida y se guarda la referencia
      if (registeredCustomer.referenceCode) {
        let bonusCodes = await models.BonusCode.findOne({
          raw: true,
          where: {code: registeredCustomer.referenceCode, isActive: 1}
        });
        if (bonusCodes) {
          registeredCustomer.referenceRegisteredCustomerId = bonusCodes.registeredCustomerId
        } else {
          return res.status(400).json({
            error: {
              message: "El código de referencia no es valido.",
            }
          })
        }
      }
      models.RegisteredCustomer.create(registeredCustomer)
        .then(user => {
          //Se agregan puntos por ser nuevo
          pointsModule.savePointOfMovement(true, user.id, POINTS.NEW_USER);
          return res.json({
            registered: 'OK'
          })
          //Se agregan puntos
        })
        .catch(function (err) {
          if (err.errors && Array.isArray(err.errors)) {
            var errors = err.errors;
            var obj = {}
            for (var i = 0; i < errors.length; i++) {
              var error = errors[i];
              if (error.path) {
                obj[error.path] = {message: error.message}
              }
            }
            return res.status(400).json(obj);
          } else {
            return res.status(400).json({
              error: {
                message: "No podemos procesar su transacción en este momento",
              }
            });
          }
        });
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.post('/login',
  async (req, res, next) => {
    const rules = {
      email: 'required|email',
      password: 'required',
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      let user = await models.RegisteredCustomer.findOne({
        attributes: {
          include: [
            [
              models.Sequelize.literal("( SELECT IFNULL(afterPoints,0) FROM `CustomerPoints` AS C WHERE C.registeredCustomerId = RegisteredCustomer.id ORDER by createdAt DESC LIMIT 1 )"),
              'points'
            ]
          ]
        }, where: {email: req.body.email}, raw: true
      });
      if (user) {
        //VERIFICAR LA CONTRASEÑA
        let remember = req.body.remember == true;
        let days = remember ? 7 : 1;
        let isCorrect = await bcrypt.compare(req.body.password, user.password);
        if (isCorrect) {
          delete user.password;
          //delete user.points;
          delete user.createdAt;
          delete user.updatedAt;
          //GENERAR EL TOKEN CON UNA VALIDES DE UNA HORA
          // exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * days),
          // clockTolerance: 60
          const token = jwt.sign(user, env.jwtSecret, {
            expiresIn: '4w'
          });
          //SE RETORNA EL TOKEN CON LOS DATOS DEL USUARIO
          return res.json({token, user});
        } else {
          //CONTRASEÑA INCORRECTA
          return res.status(400).json({
            error: {
              message: "La contraseña es incorrecta.",
            }
          })
        }
      } else {
        //NO SE ENCONTRO EL USUARIO
        return res.status(404).json({
          error: {
            message: "Usuario no encontrado.",
          }
        })
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.post('/loginFacebook',
  async (req, res, next) => {
    const rules = {email: 'required|email'};
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      let remember = req.body.remember == true;
      let days = remember ? 7 : 1;
      //BUSCAR AL CUSTOMER POR EMAIL
      let user = await models.RegisteredCustomer.findOne({
        attributes: {
          include: [
            [
              models.Sequelize.literal("( SELECT IFNULL(afterPoints,0) FROM `CustomerPoints` AS C WHERE C.registeredCustomerId = RegisteredCustomer.id ORDER by createdAt DESC LIMIT 1 )"),
              'points'
            ]
          ]
        }, where: {email: req.body.email, facebookUser: true}, raw: true
      });
      if (user) {
        //VERIFICAR LA CONTRASEÑA
        let isCorrect = true
        if (isCorrect) {
          delete user.password;
          //delete user.points;
          delete user.createdAt;
          delete user.updatedAt;

          //GENERAR EL TOKEN CON UNA VALIDES DE UNA HORA
          var token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * days),
            user,
            clockTolerance: 60
          }, env.jwtSecret);

          //SE RETORNA EL TOKEN CON LOS DATOS DEL USUARIO
          return res.json({
            token,
            user
          })

        } else {
          //CONTRASEÑA INCORRECTA
          return res.status(400).json({
            error: {
              message: "La contraseña es incorrecta.",
            }
          })
        }
      } else {
        //NO SE ENCONTRO EL USUARIO
        return res.status(404).json({
          error: {
            message: "Usuario no encontrado.",
          }
        })
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.get('/getReservations/', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      try {
        let email = user.email;
        let isBefore = req.query.isBefore == "true";
        const now = (new Date()).toISOString();
        let where = {[Op.and]: models.Sequelize.literal(`(EXISTS(SELECT * FROM Costumers as C WHERE C.id = Reservation.costumerId AND C.email = '${email}')) = 1`)}
        if (isBefore) {
          where.datetime = {
            [Op.lte]: now
          }
        } else {
          where.datetime = {
            [Op.gte]: now
          }
        }
        let reservations = await models.Reservation.findAll({
          where: where,
          attributes: {
            include: [
              [
                models.Sequelize.literal("( SELECT AVG(score) FROM `Reviews` as R WHERE R.placeId = Reservation.placeId )"),
                'score'
              ],
              [
                models.Sequelize.literal("(SELECT ABS(SUM(points)) FROM `CustomerPoints` as CP , `BonusCodes` as BC WHERE CP.id = BC.customerPointId && BC.reservationId = Reservation.id)"),
                'bonusCodePoints'
              ]
            ]
          },
          include: [
            {
              model: models.Place,
              as: 'place',
              attributes: ['name', 'logoImage']
            },
          ],
          order: [
            [req.query.sortBy || 'datetime', req.query.sortDir || 'desc']
          ],
          raw: true
        })
        return res.json({reservations: reservations});
      } catch (error) {
        return res.json({reservations: []});
      }
    } else {
      return res.json({reservations: []});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/getReviews/', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      let email = user.email;
      let customers = await models.Costumer.findAll({where: {email: email}, raw: true});
      if (Array.isArray(customers) && customers.length) {
        let reviews = [];
        for (let i = 0; i < customers.length; i++) {
          const customer = customers[i];
          let customerReservations = await models.Reservation.findAll({where: {costumerId: customer.id}, raw: true})
          for (let j = 0; j < customerReservations.length; j++) {
            const reservation = customerReservations[j];
            let review = await models.Review.findOne({
              where: {reservationId: reservation.id},
              include: [{
                model: models.Place,
                as: 'place',
                attributes: ['name', 'logoImage']
              }],
              raw: true
            })
            if (review) {
              reviews.push(review);
            }
          }
        }
        return res.json({reviews: reviews});
      } else {
        return res.json({reviews: []});
      }
    } else {
      return res.json({reviews: []});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/changePassword',
  verifyToken,
  async (req, res, next) => {
    const rules = {
      oldPassword: 'required',
      newPassword: 'required'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const newPassword = req.body.newPassword;
      const oldPassword = req.body.oldPassword;
      if (newPassword.length < 8 || newPassword.length > 255)
        return res.status(400).json({
          error: {
            message: "El password debe tener entre [8 - 255] caracteres.",
          }
        });

      const user = await getInfoFromToken(req);
      if (user) {
        let userT = await models.RegisteredCustomer.findOne({where: {email: user.email}});
        if (userT) {
          //VERIFICAR LA CONTRASEÑA
          let isCorrect = await bcrypt.compare(oldPassword, userT.password);
          if (isCorrect) {
            userT.password = await bcrypt.hash(newPassword, 10);
            await userT.save();
            return res.json({change: "OK"})
          } else {
            return res.status(400).json({error: {message: 'El password actual no es correcto.'}})
          }
        }
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

//Agregar favorites
router.post('/favorites',
  verifyToken,
  async (req, res, next) => {
    const rules = {placeId: 'required'};
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const placeId = req.body.placeId;
      const user = await getInfoFromToken(req);
      if (user) {
        let favoritesCount = await models.Favorite.count({where: {registeredCustomerId: user.id, placeId}});
        if (favoritesCount == 0) {
          await models.Favorite.create({registeredCustomerId: user.id, placeId});
          return res.json({message: 'Se agrego favorito'});
        } else
          return res.json({message: 'Ya es favorito'});
      }
    } catch (e) {
      HandleError(req, res, e);
    }

  }
);

//Obtener favorites
router.get('/favorites',
  verifyToken,
  async (req, res) => {
    try {
      const user = await getInfoFromToken(req);
      if (user) {
        //let favorites = await models.Favorite.findAll({ where: { registeredCustomerId: user.id } });
        let favorites = await models.Place.findAll({
          attributes: {
            include: [
              [
                models.Sequelize.literal("( SELECT AVG(score) FROM `Reviews` as R WHERE R.placeId = Place.id )"),
                'score'
              ]
            ]
          },
          include: [
            {
              model: models.Favorite,
              attributes: ['id'],
              as: 'favorites',
            }],
          where: models.Sequelize.literal('favorites.registeredCustomerId = ' + user.id)
        });
        return res.json({favorites});
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.get('/notifications', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      let where = {registeredCustomerId: user.id}
      let notifications = await models.CostumerNotification.findAll({
        where,
        order: [
          ['createdAt', 'desc']
        ],
        limit: 10,
        raw: true
      });
      await models.CostumerNotification.update({viewed: true}, {where});
      return res.json({notifications});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/countnotifications', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      let count = await models.CostumerNotification.count({where: {registeredCustomerId: user.id, viewed: false}});
      return res.json({count});
    } else {
      return res.status(401).json({error: {message: 'Usuario no autenticado.'}});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/notifications',
  async (req, res, next) => {
    const rules = {
      message: 'required',
      registeredCustomerId: 'required'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      let r = await createNotification(data)
      return res.status(201).json(r);
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.delete('/favorites/place/:id', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      const placeId = req.params.id;
      let d = await models.Favorite.destroy({where: {registeredCustomerId: user.id, placeId}});
      return res.json(d);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/favorites/:id', verifyToken, async (req, res) => {
  try {
    const id = req.params.id;
    let d = await models.Favorite.destroy({where: {id}});
    return res.json(d);
  } catch (e) {
    HandleError(req, res, e);
  }
});

//Get users por term
router.get('/', async (req, res) => {
  try {
    let cuenta = req.query.cuenta || 1
    let response = {count: 0, rows: []};
    const reviewProfile = req.query.reviewProfile || 1;

    if (cuenta == '1') {
      let where = {email: {[Op.not]: null}};
      if (reviewProfile == 1) {
        where.reviewProfile = 1;
      }
      if (req.query.term) {
        where = {
          ...where,
          [Op.or]: [
            models.Sequelize.literal(`CONCAT(RegisteredCustomer.name, ' ', RegisteredCustomer.lastName ) LIKE '%${req.query.term}%'`),
            {email: {[Op.like]: `%${req.query.term}%`}},
            {phone: {[Op.like]: `%${req.query.term}%`}}
          ]
        };
      }
      let sortBy = req.query.sortBy || 'name'
      if (sortBy == 'fullname')
        sortBy = 'name'
      if (sortBy == 'countryCode')
        sortBy = 'lada'

      response.count = await models.RegisteredCustomer.count({
        where: where,
        distinct: true,
        col: 'RegisteredCustomer.email'
      });

      response.rows = await models.RegisteredCustomer.findAll({
        attributes: {
          exclude: ['name', `lastName`, `password`, `acceptTerms`, `points`, `facebookUser`, `updatedAt`],
          include: [
            [
              models.Sequelize.literal("(SELECT GROUP_CONCAT(tags SEPARATOR ',' ) FROM `Costumers` as CS WHERE CS.`email`= RegisteredCustomer.`email` && tags != '')"),
              'myTags'
            ],
            [
              models.Sequelize.literal("RegisteredCustomer.lada"),
              'countryCode'
            ],
            [
              models.Sequelize.literal("1"),
              'cuenta'
            ],
            [
              models.Sequelize.literal("(SELECT COUNT(*) FROM `Costumers`, Reservations WHERE Costumers.email = RegisteredCustomer.`email` && Costumers.id = Reservations.costumerId)"),
              'countReservations'
            ],
            [
              models.Sequelize.literal("(CONCAT(RegisteredCustomer.name, ' ', RegisteredCustomer.lastName ))"),
              'fullname'
            ],
            'reviewProfile'
          ]
        },
        include: [
          {
            model: models.City,
            attributes: ['name'],
            as: 'city',
          },
          {
            model: models.Country,
            attributes: ['name'],
            as: 'country',
          },
          {
            model: models.State,
            attributes: ['name'],
            as: 'state',
          }
        ],
        offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
        limit: +req.query.pageSize || 10,
        raw: true,
        where: where,
        group: ['email'],
        order: [[req.query.sortBy || 'RegisteredCustomer.email', req.query.sortDir || 'desc']],
      });
      return res.json(response);
    } else if (cuenta == '0') {
      console.log("Invitado");
      let where = {
        [Op.and]: {
          email: { [Op.not]: null },
          email: { [Op.ne]: ''}
        },
        [Op]: models.Sequelize.literal("EXISTS(SELECT `email` FROM `RegisteredCustomers` as RC WHERE RC.`email` = Costumer.`email` ) = 0")
      };
      if (req.query.term) {
        where = {
          ...where,
          [Op.or]: [
            {fullname: {[Op.like]: `%${req.query.term}%`}},
            {email: {[Op.like]: `%${req.query.term}%`}},
            {phone: {[Op.like]: `%${req.query.term}%`}}
          ]
        };
      }
      response.count = await models.Costumer.count({
        where: where,
        distinct: false,
        col: 'Costumer.email'
      });

      response.rows = await models.Costumer.findAll({
        attributes: {
          exclude: ['notes', `userGoogleReserve`, `language`, `validEmail`, `emailComplaints`, `paymentCustomerId`, `subscribedEmails`, `updatedAt`, `deletedAt`, `placeId`],
          include: [

            [
              models.Sequelize.literal("0"),
              'cuenta'
            ],
            [
              models.Sequelize.literal("''"),
              'imgUrl'
            ],
            [
              models.Sequelize.literal("(SELECT Count(*) FROM Reservations r WHERE r.costumerId = Costumer.id)"),
              'reservaciones'
            ],
            [
              models.Sequelize.literal(" (SELECT p.name FROM Places p WHERE p.id = Costumer.placeId)"),
              'restaurante'
            ]
          ]
        },
        offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
        limit: +req.query.pageSize || 10,
        raw: true,
        where: where,
        order: [[req.query.sortBy || 'email', req.query.sortDir || 'desc']],
      });
      return res.json(response);
    }
    return res.json(response);
  } catch (e) {
    HandleError(req, res, e);
  }
});

//Obtener tags del customer
router.get('/tags/invitado/:id', async (req, res) => {
  try {
    let tags = "";
    let costumer = await models.Costumer.findOne({
      attributes: ['tags'],
      where: {id: req.params.id, tags: {[Op.ne]: ''}},
      raw: true,
    })
    if (costumer)
      tags = costumer.tags
    return res.json({tags});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/tags/cuenta/:email', async (req, res) => {
  try {
    let tags = "";
    let email = req.params.email;
    let tagsC = await models.Costumer.findAll({
      attributes: ['tags'],
      where: {email, tags: {[Op.ne]: ''}},
      raw: true,
    })
    let tagsRC = await models.RegisteredCustomer.findAll({
      atributes: ['tags'],
      where: {email, tags: {[Op.ne]: ''}},
      raw: true,
    })
    tags = [...tagsC, ...tagsRC].map(t => t.tags).join(' ,');
    return res.json({tags});
  } catch (e) {
    HandleError(req, res, e);
  }
});

//Modificar tags del customer
router.post('/tags/cuenta/:email',
  async (req, res, next) => {
    const rules = {tags: 'required'};
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      let email = req.params.email;
      await models.RegisteredCustomer.update({tags: ''}, {where: {email}})
      await models.Costumer.update({tags: ''}, {where: {email}})
      await models.RegisteredCustomer.update({tags: data.tags}, {where: {email}})
      return res.json({message: "ok"});
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);

router.post('/tags/invitado/:id',
  async (req, res, next) => {
    const rules = {tags: 'required'};
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const id = req.params.id
      await models.Costumer.update({tags: data.tags}, {where: {id}})
      return res.json({message: "ok"});
    } catch (e) {
      HandleError(req, res, e);
    }
  }
);
//Octner info del customer
router.get('/cuenta/:id', async (req, res) => {
  try {
    const where = {id: req.params.id};
    let registeredCustomer = await models.RegisteredCustomer.findOne({
      attributes: {
        exclude: ['password'],
        include: [
          [
            models.Sequelize.literal("RegisteredCustomer.lada"),
            'countryCode'
          ],
          [
            models.Sequelize.literal("(CONCAT(RegisteredCustomer.name, ' ', RegisteredCustomer.lastName ))"),
            'fullname'
          ],
        ]

      },
      include: [
        {
          model: models.City,
          attributes: ['name'],
          as: 'city',
        },
        {
          model: models.Country,
          attributes: ['name'],
          as: 'country',
        },
        {
          model: models.State,
          attributes: ['name'],
          as: 'state',
        }
      ],
      where: where,
      raw: true
    })
    return res.json(registeredCustomer);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/invitado/:id', async (req, res) => {
  try {
    const where = {id: req.params.id};
    let customer = await models.Costumer.findOne({
      attributes: {
        include: [
          [
            models.Sequelize.literal("''"),
            'imgUrl'
          ],
        ]
      },
      where: where,
      raw: true
    })
    return res.json(customer);
  } catch (e) {
    HandleError(req, res, e);
  }
});
//Octener reservaciones del customer
router.get('/reservations/invitado/:id', async (req, res) => {
  try {
    let costumerId = req.params.id;
    let isBefore = req.query.isBefore == "true";
    const now = (new Date()).toISOString();
    let where = {costumerId}
    if (isBefore) {
      console.log('isBefore');
      where.datetime = {
        [Op.lte]: now
      }
    } else {
      console.log('isAfter');
      where.datetime = {
        [Op.gte]: now
      }
    }
    let reservations = await models.Reservation.findAndCountAll({
      where: where,
      attributes: {
        include: [
          [
            models.Sequelize.literal("( SELECT AVG(score) FROM `Reviews` as R WHERE R.placeId = Reservation.placeId )"),
            'score'
          ]
        ]
      },
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['name', 'logoImage']
        },
      ],
      order: [
        [req.query.sortBy || 'createdAt', req.query.sortDir || 'asc']
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10,
      raw: true
    })
    return res.json(reservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservations/cuenta/:email', async (req, res) => {
  try {
    let email = req.params.email || "";
    let isBefore = req.query.isBefore == "true";
    const now = (new Date()).toISOString();
    let where = {[Op.and]: models.Sequelize.literal(`(EXISTS(SELECT * FROM Costumers as C WHERE C.id = Reservation.costumerId AND C.email = '${email}')) = 1`)}
    if (isBefore) {
      console.log('isBefore');
      where.datetime = {
        [Op.lte]: now
      }
    } else {
      console.log('isAfter');
      where.datetime = {
        [Op.gte]: now
      }
    }
    console.log(now)
    let reservations = await models.Reservation.findAndCountAll({
      where: where,
      attributes: {
        include: [
          [
            models.Sequelize.literal("( SELECT AVG(score) FROM `Reviews` as R WHERE R.placeId = Reservation.placeId )"),
            'score'
          ],
          [
            models.Sequelize.literal("(SELECT ABS(SUM(points)) FROM `CustomerPoints` as CP , `BonusCodes` as BC WHERE CP.id = BC.customerPointId && BC.reservationId = Reservation.id)"),
            'bonusCodePoints'
          ]
        ]
      },
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['name', 'logoImage']
        },
      ],
      order: [
        [req.query.sortBy || 'createdAt', req.query.sortDir || 'asc']
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10,
      raw: true
    })
    return res.json(reservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservationsbonus/cuenta/:email', async (req, res) => {
  try {
    let email = req.params.email || "";
    let where = models.Sequelize.and(
      models.Sequelize.literal(`(EXISTS(SELECT * FROM Costumers as C WHERE C.id = Reservation.costumerId AND C.email = '${email}')) = 1`),
      //models.Sequelize.literal(`(EXISTS(SELECT * FROM BonusCodes as B WHERE B.reservationId = Reservation.id)) = 1`),
    )
    let reservations = await models.Reservation.findAll({
      where: where,
      attributes: {
        include: [
          [
            models.Sequelize.literal("( SELECT AVG(score) FROM `Reviews` as R WHERE R.placeId = Reservation.placeId )"),
            'score'
          ],
          [
            models.Sequelize.literal("(SELECT ABS(SUM(points)) FROM `CustomerPoints` as CP WHERE CP.id = bonusCode.customerPointId )"),
            'bonusCode.points'
          ]
        ]
      },
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['name', 'logoImage']
        },
        {
          model: models.BonusCode,
          as: 'bonusCode',
          where: {
            id: {[Op.not]: null}
          }
        },
      ],
      order: [
        ['createdAt', 'desc']
      ],
      limit: 2,
      raw: true
    })
    return res.json(reservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

//Octener reviews del customer
router.get('/reviews/cuenta/:email', async (req, res) => {
  try {
    let email = req.params.email || "";
    let reviews = await models.Review.findAndCountAll({
      raw: true,
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['name']
        },
        {
          model: models.Reservation,
          as: 'reservation',
          attributes: ['costumerId'],
          include: [
            {
              model: models.Costumer,
              as: 'costumer',
              attributes: ['email'],
            }
          ]
        },
      ],
      where: {
        [Op.and]: models.Sequelize.literal("`reservation->costumer`.email = '" + email + "' ")
      },
      order: [
        [req.query.sortBy || 'createdAt', req.query.sortDir || 'asc']
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10,
      raw: true
    })
    return res.json(reviews);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reviews/invitado/:id', async (req, res) => {
  try {
    let costumerId = req.params.id;
    let reviews = await models.Review.findAndCountAll({
      raw: true,
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['name']
        },
        {
          model: models.Reservation,
          as: 'reservation',
          attributes: ['costumerId'],
          include: [
            {
              model: models.Costumer,
              as: 'costumer',
              attributes: ['email'],
            }
          ]
        },
      ],
      where: {
        [Op.and]: models.Sequelize.literal("`reservation`.costumerId = '" + costumerId + "' ")
      },
      order: [
        [req.query.sortBy || 'createdAt', req.query.sortDir || 'asc']
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10,
      raw: true
    })
    return res.json(reviews);
  } catch (e) {
    HandleError(req, res, e);
  }
});


router.get('/resetPasswordEmail/:email', async (req, res) => {
  try {
    let registeredCustomer = await models.RegisteredCustomer.findOne({
      where: {email: req.params.email || ""}
    });
    if (registeredCustomer) {
      await models.RegisteredCustomer.update({resetPasswordDatetime: (new Date()).toISOString()}, {where: {id: registeredCustomer.id}});
      emailModule.resetPasswordEmail(registeredCustomer.name + ' ' + registeredCustomer.lastName, registeredCustomer.email, registeredCustomer.id);
      return res.status(200).send({
        message: ''
      });
    } else {
      return res.status(422).send({
        message: 'El usuario no se encuentra registrado'
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/resetPassword', async (req, res) => {
  try {
    const id = decrypt(req.body.key);
    const password = await bcrypt.hash(req.body.newPassword, 10);
    let registeredCustomer = await models.RegisteredCustomer.findOne({
      where: {id: id}
    });
    if (registeredCustomer) {
      const difMinutes = moment().diff(registeredCustomer.resetPasswordDatetime, 'minutes');
      if (difMinutes <= 30) {
        await models.RegisteredCustomer.update({
          password: password,
          resetPasswordDatetime: null
        }, {where: {id: registeredCustomer.id}});
        emailModule.resetPasswordSuccessEmail(registeredCustomer.name + ' ' + registeredCustomer.lastName, registeredCustomer.email);
        return res.status(200).send({
          message: ''
        });
      } else {
        return res.status(422).send({message: 'El tiempo para cambiar contraseña ha expirado, intente de nuevo.'});
      }
    } else {
      return res.status(422).send({message: 'El usuario no se encuentra registrado. - ' + registeredCustomer.id});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

//Obtener favorites Admin
router.get('/favoritesFromAdmin', async (req, res) => {
  try {
    let response = {count: 0, rows: []};
    const excludeProperties = ['dueDate', 'payDay', 'extraTimeDate', 'monthlyPlanPrice', 'sentPersonPrice',
      'timeBeforeClose', 'cancellationFeePerPerson', 'numberPersonPolicyCancellation', 'advancePaymentPolicyEnable',
      'advancePaymentPolicyFeePerPerson', 'advancePaymentPolicyNumberPeople',
      'numberMaxMsm', 'numberMaxWhatsapp', 'typeWidget', 'isReservationsBlocked',
      'onlineSales', 'hasBooking', 'blockedDateStart', 'blockedHourStart', 'events', 'blockedDateEnd', 'blockedHourEnd',
      'timezone', 'tablesCount', 'cancellationPolicyEnable', 'priorityPaymentWidget', 'paymentGatewayPublicKey',
      'paymentGatewayPrivateKey', 'paymentCustomerId', 'paymentCardId', 'paymentSubscriptionId', 'paymentEmail',
      'isActive', 'website', 'facebook', 'instagram', 'twitter', 'enableGoogleReserve', 'methodReserve', 'zoom',
      'theme', 'primaryColor', 'secondaryColor', , 'intervalWidget', 'typeAccount',
      'fixedCharge', 'updatedAt', 'deletedAt', 'billingFrequencyId',
      'cityId', 'ownerId', 'planId', 'stateId', 'typeChargeId', 'sellerId', 'score'];
    let favorites = await models.Place.findAll({
      attributes: {
        exclude: excludeProperties,
        include: [
          [
            models.Sequelize.literal("( SELECT IFNULL(AVG(score),0) FROM `Reviews` as R WHERE R.placeId = Place.id )"),
            'score'
          ]
        ]
      },
      include: [
        {
          model: models.Favorite,
          attributes: ['registeredCustomerId'],
          as: 'favorites',
        }],
      subQuery: false,
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10,
      where: models.Sequelize.literal('favorites.registeredCustomerId = ' + req.query.id),
      raw: true,
      order: [[req.query.sortBy || 'name', req.query.sortDir || 'desc']],
    });
    response.count = favorites.length;
    response.rows = favorites;
    return res.json(response);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/updatePersonalInformation',
  verifyToken,
  async (req, res, next) => {
    const rules = {
      name: 'required',
      lastName: 'required',
      //email: 'required|email',
      lada: 'required',
      phone: 'required|phoneNumber',
      sex: 'required',
      countryId: 'required',
      stateId: 'required',
      //cityId: 'required'
    };
    const validator = new Validator(req.body, rules);
    if (await validator.check()) {
      next();
    } else {
      return res.status(422).send(onlyMessage(validator.errors));
    }
  },
  async (req, res) => {
    try {
      const user = await getInfoFromToken(req);
      if (user) {
        let userphone = await models.RegisteredCustomer.findOne({
          where: {phone: req.body.phone, email: {[Op.not]: user.email}}
        });
        if (userphone) {
          return res.status(400).json({message: 'Este teléfono está registrado en otra cuenta.'})
        } else {
          let userT = await models.RegisteredCustomer.findOne({
            attributes: {
              exclude: ['points', 'updatedAt', 'createdAt', 'password'],
            },
            where: {id: req.body.id}
          });
          if (req.file) {
            if (checkImage(req.file)) {
              const folder = `customers/${user.id}`;
              const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
              req.body.imgUrl = `${env.awsBucketUrl}/${uploadedFileUrl}`;
            } else
              return res.status(400).json({message: 'El archivo debe ser una imagen.'});
          }
          if (req.body.imgUrl) {
            userT.imgUrl = req.body.imgUrl;
          }
          userT.name = req.body.name;
          userT.lastName = req.body.lastName;
          //userT.email = req.body.email;
          userT.lada = req.body.lada;
          userT.phone = req.body.phone;
          userT.countryId = req.body.countryId;
          userT.sex = req.body.sex;
          userT.stateId = req.body.stateId;
          //userT.cityId = req.body.cityId;
          if (userT) {
            await userT.save();
            return res.json({message: "El usuario se actualizo correctamente.", user: userT})
          } else {
            return res.status(400).json({message: 'El usuario no existe.'})
          }
        }
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  });

router.post('/updateImageReview', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      const review = await models.Review.findOne({
        attributes: {
          exclude: ['updatedAt', 'createdAt', 'deletedAt'],
        },
        where: {id: req.body.id}
      });
      if (req.file) {
        if (checkImage(req.file)) {
          const folder = `reviews/${req.body.id}`;
          const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
          req.body.imgUrl = `${env.awsBucketUrl}/${uploadedFileUrl}`;
        } else
          return res.status(400).json({message: 'El archivo debe ser una imagen.'})
      }
      if (req.body.imgUrl) {
        review.imgUrl = req.body.imgUrl;
      }
      if (review) {
        await review.save();
        const reservation = await models.Reservation.findByPk(review.reservation, {
          attributes: ['id'],
          include: [{
            model: models.Costumer,
            as: 'costumer',
            attributes: ['id', 'email']
          }]
        });
        if (reservation?.costumer?.email) {
          await models.RegisteredCustomer.update({reviewProfile: true},{where: {email: reservation.costumer.email}});
        }
        return res.json({message: "La imagen se subio correctamente.", data: review})
      } else {
        return res.status(400).json({message: 'El review no existe.'})
      }
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/ActiveImageReview', async (req, res) => {
  try {
    const review = await models.Review.findOne({
      attributes: {
        exclude: ['updatedAt', 'createdAt', 'deletedAt'],
      },
      where: {id: req.body.id}
    });
    if (review) {
      if (!review.isValidImg && req.body.isValidImg) {
        review.isValidImg = true;
        await review.save();
        await createNotification({
          message: 'Tu foto ha sido validada.',
          registeredCustomerId: req.body.registeredCustomerId
        });
        await pointsModule.savePointOfMovement(true, req.body.registeredCustomerId, POINTS.IMAGE_REVIEW);
        await models.RegisteredCustomer.update({
          reviewProfile: false
        }, {
          where: {id: req.body.registeredCustomerId}
        });
        return res.json({message: "La imagen se validó correctamente.", success: true})
      } else {
        return res.status(400).json({message: 'La imágen no cumple con el estatus correcto.', success: false})
      }
    } else {
      return res.status(400).json({message: 'El review no existe.', success: false})
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

//Puntos
router.get('/CustomerPoints', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      const customerPoint = await models.CustomerPoint.findOne({
        attributes: {
          exclude: ['createdAt', 'updatedAt']
        },
        raw: true,
        where: {registeredCustomerId: user.id},
        order: [['createdAt', 'desc']]
      });
      const bonusCodes = await models.BonusCode.findAll({
        attributes: {
          include: [
            [
              models.Sequelize.literal("( SELECT DATE_ADD(BonusCode.createdAt, INTERVAL 365 DAY) )"),
              'expired'
            ],
            [
              models.Sequelize.literal("( SELECT ABS(IFNULL(points,0)) FROM `CustomerPoints` AS C WHERE C.id = BonusCode.customerPointId LIMIT 1 )"),
              'points'
            ]
          ],
          exclude: [`id`, `registeredCustomerId`, `customerPointId`, 'createdAt', 'updatedAt']
        },
        raw: true,
        where: {registeredCustomerId: user.id, isActive: 1},
        order: [['createdAt', 'desc']]
      });
      return res.json({points: customerPoint ? customerPoint.afterPoints : 0, codesPending: bonusCodes})
    } else {
      return res.status(400).json({message: 'Usuario no identificado.'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/CustomerPoints/:id', async (req, res) => {
  try {
    const customerPoint = await models.CustomerPoint.findOne({
      attributes: {
        exclude: ['createdAt', 'updatedAt']
      },
      raw: true,
      where: {registeredCustomerId: req.params.id},
      order: [['createdAt', 'desc']]
    });
    const bonusCodes = await models.BonusCode.findAll({
      attributes: {
        include: [
          [
            models.Sequelize.literal("( SELECT DATE_ADD(BonusCode.createdAt, INTERVAL 365 DAY) )"),
            'expired'
          ],
          [
            models.Sequelize.literal("( SELECT ABS(IFNULL(points,0)) FROM `CustomerPoints` AS C WHERE C.id = BonusCode.customerPointId LIMIT 1 )"),
            'points'
          ]
        ],
        exclude: [`id`, `registeredCustomerId`, `customerPointId`, 'createdAt', 'updatedAt']
      },
      raw: true,
      where: {registeredCustomerId: req.params.id, isActive: 1},
      order: [['createdAt', 'desc']]
    });
    return res.json({points: customerPoint ? customerPoint.afterPoints : 0, codesPending: bonusCodes})
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/ChangePoints', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      let movement = await pointsModule.savePointOfMovement(false, req.body.id, req.body.points);
      if (movement.success) {
        //Crea un codigo
        const strCode = pointsModule.randomCode(10);
        await models.BonusCode.create({
          registeredCustomerId: movement.CustomerPoint.registeredCustomerId,
          customerPointId: movement.CustomerPoint.id,
          code: strCode
        });
        //Regresa el codigo
        await createNotification({
          message: 'Has canjeado ' + req.body.points + ' de puntos.',
          registeredCustomerId: req.body.id
        });
        return res.json({
          afterPoints: movement.CustomerPoint.afterPoints,
          changedPoints: req.body.points,
          code: strCode
        })
      } else {
        return res.json({message: movement.message});
      }
    } else {
      return res.status(400).json({message: 'Usuario no identificado.'})
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/updateImageReservation', verifyToken, async (req, res) => {
  try {
    const user = await getInfoFromToken(req);
    if (user) {
      let reservation = await models.Reservation.findOne({where: {id: req.body.id}});
      if (req.file) {
        if (checkImage(req.file)) {
          const folder = `reservations/${req.body.id}`;
          const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
          req.body.imgUrl = `${env.awsBucketUrl}/${uploadedFileUrl}`;
        } else
          return res.status(400).json({message: 'El archivo debe ser una imagen.'})
      }
      if (req.body.imgUrl) {
        reservation.imgUrl = req.body.imgUrl;
      }
      if (reservation) {
        await reservation.save();
        return res.json({message: "La imagen se subio correctamente.", data: reservation})
      } else {
        return res.status(400).json({message: 'El reservación no existe.'})
      }
    } else {
      return res.status(400).json({message: 'User not found'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/ChangeCode', async (req, res) => {
  try {
    if (req.body.reservationId) {
      const bonusCode = await models.BonusCode.findOne({
        where: {registeredCustomerId: req.body.id, code: req.body.code}, raw: true
      });
      if (bonusCode) {
        if (bonusCode.isActive) {
          if (bonusCode.createdAt) {
            await models.BonusCode.update({
              isActive: false,
              reservationId: req.body.reservationId
            }, {
              where: {id: bonusCode.id}
            });
            return res.json({message: 'Código fue canjeado exitosamente', success: true});
          } else {
            return res.json({message: 'Código ya fue canjeado', success: false});
          }
        } else {
          return res.json({message: 'Código ya fue canjeado', success: false});
        }
      } else {
        return res.json({message: 'Código no existente o ya no es valido', success: false});
      }
    } else {
      return res.json({message: 'El campo reservationId es requerido', success: false});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/id/:id/code', async (req, res) => {
  try {
    let registeredCustomerId = req.params.id
    let code = req.body.code || "NO HAY"
    let bonusCode = await models.BonusCode.findOne({
      attributes: {
        include: [
          [
            models.Sequelize.literal("(SELECT ABS(SUM(points)) FROM `CustomerPoints` as CP WHERE CP.id = BonusCode.customerPointId )"),
            'bonusCodePoints'
          ]
        ]
      },
      where: {registeredCustomerId, code},
      raw: true
    });
    if (bonusCode) {
      if (bonusCode.isActive) {
        return res.json(bonusCode);
      } else {
        return res.json({message: 'Código ya fue canjeado', success: false});
      }
    } else {
      return res.json({message: 'Código no existente o ya no es valido', success: false});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
