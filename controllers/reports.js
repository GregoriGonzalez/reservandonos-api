const express = require('express');
const router = express.Router();
const moment = require('moment');
const models = require('../models');
const sequelize = models.sequelize;
const Op = models.Sequelize.Op;
const {sum, invoiceOfThePlaceForItsBookings, invoiceByReservation, multiply} = require('../helpers/calculate.helper');
const {BILLINGS_STATUS, RESERVATION_STATUS, SPONSOR_PLACE_TYPE} = require("../helpers/constants.helper");
const {HandleError} = require("../helpers/error.helper");

const calculateStatisticsOfReservations = async (reservations = [], typeCharges = [] = [], place, withSponsor = false) => {
  const global = {
    totalReservation: reservations.length,
    peopleAttended: 0,
    totalTypeCharge: 0,
    cocaColaDelivered: 0,
    cocaColaDiscount: 0,
    finalCommission: 0,
    iva: 0
  };
  for (const reservation of reservations) {
    if (!global[reservation.status]) {
      global[reservation.status] = 0;
    }
    global[reservation.status]++;

    if ([RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(reservation.status)) {
      const attended = reservation.peopleSeating > 0 ? reservation.peopleSeating : reservation.peopleCount;
      global.peopleAttended = await sum(0, global.peopleAttended, attended);

      const amount = await invoiceByReservation(reservation, typeCharges, place);
      global.finalCommission = await sum(2, global.finalCommission, amount);

      if (withSponsor) {
        global.cocaColaDelivered =
          await sum(
            0,
            global.cocaColaDelivered,
            reservation.reservationSponsor.length > 0 ? reservation.reservationSponsor[0].quantity : 0
          );
        global.cocaColaDiscount =
          await sum(
            2,
            global.cocaColaDiscount,
            reservation.reservationSponsor.length > 0 ? reservation.reservationSponsor[0].totalCommission || 0 : 0
          );
      }
    }
  }
  global.totalTypeCharge = await sum(2, global.finalCommission, await multiply(2, -1, global.cocaColaDiscount));
  global.iva = await multiply(2, global.finalCommission, await sum(2, 1, (place.taxToApply || 0)));
  return global;
};

const accumulateInvoicesForCollections = async (billing, place, objAccumulator) => {
  const {commissionByCharges} = await invoiceOfThePlaceForItsBookings(place, place.reservation, place.typeCharges, billing);
  if (billing.status === BILLINGS_STATUS.VERIFIED) {
    objAccumulator.verified = await sum(2, objAccumulator.verified, commissionByCharges.completed);
  } else if (billing.status === BILLINGS_STATUS.CONFIRMED) {
    objAccumulator.paid = await sum(2, objAccumulator.paid, commissionByCharges.completed);
  } else if (billing.status === BILLINGS_STATUS.PENDING) {
    objAccumulator.pending = await sum(2, objAccumulator.pending, commissionByCharges.completed);
  } else if (billing.status === BILLINGS_STATUS.LATE) {
    objAccumulator.lost = await sum(2, objAccumulator.lost, commissionByCharges.completed);
  }
  return objAccumulator;
}


/**
 * get reservation statistics given a date range and place identifier
 * @type {Router}
 */
router.get('/statistics/reservation/by/range-place', async (req, res) => {
  try {
    const {start, end, placeId, status = ''} = req.query;
    const place = await models.Place.findOne({
      attributes: ['id', 'name', 'sentPersonPrice', 'fixedCharge', 'hasElectronicBillingEnabled', 'taxToApply'],
      where: {id: placeId}
    });
    if (place) {
      const typeCharges = await place.getTypeCharges();
      const sponsorsPlaces = await place.getSponsorsPlaces({
        where: {
          [Op.and]: [
            {type: SPONSOR_PLACE_TYPE.COMMISSION},
            {isActive: 1}
          ]
        },
        include: [
          {
            model: models.Sponsor,
            as: 'sponsor'
          }
        ]
      });

      const optionsReservations = {
        attributes: [
          'id', 'date', 'peopleCount', 'peopleSeating', 'status', 'type',
          'origin', 'newCostumer', 'invoiceAmount', 'invoiceAmountTotal', 'costumerId',
          'partnerId', 'placeId'
        ],
        where: {
          date: {
            [Op.between]: [start, end]
          }
        },
        include: [
          {
            model: models.Costumer,
            as: 'costumer'
          },
          {
            model: models.ReservationSponsors,
            as: 'reservationSponsor'
          }
        ]
      };
      let reservations = await place.getReservation(optionsReservations);
      const statistics = await calculateStatisticsOfReservations(reservations, typeCharges, place, sponsorsPlaces.length > 0);
      if (status !== '') {
        reservations = await place.getReservation({
          ...optionsReservations,
          where: {
            ...optionsReservations.where,
            status: status
          }
        });
      }

      return res.json({place, reservations, typeCharges, sponsorsPlaces, statistics});
    } else {
      return res.status(400).json({error: 'place not found'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * get reservation or billing statistics given a date range and Nº places
 */
router.get('/number-of-places-by-zones', async (req, res) => {
  try {
    const {cities} = req.query;
    const citiesId = cities?.split(',');
    const whereCities = citiesId ? {
      cityId: {[Op.in]: citiesId}
    } : {};
    const totalPlaces = await models.Place.count({
      where: whereCities
    });
    const placesActive = await models.Place.count({
      where: {
        isActive: 1
      }
    });
    return res.json({placesActive, totalPlaces});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 *
 */
router.get('/collection/resume', async (req, res) => {
  try {
    const {
      dateStart = moment().startOf("months").format('YYYY-MM-DD'),
      dateEnd = moment().format('YYYY-MM-DD'),
      cities = '',
      seller = '',
      place = '',
      status = '',
      label = ''
    } = req.query;
    const citiesId = cities !== '' ? cities?.split(',') : [];
    const placesId = place !== '' ? place?.split(',') : [];
    const statusNames = status !== '' ? status?.split(',') : [];
    const sellersId = seller !== '' ? seller?.split(',') : [];

    const includeReservation = {
      model: models.Reservation,
      as: 'reservation',
      required: false,
      attributes: ['id', 'status', 'origin', 'invoiceAmount', 'billingId', 'partnerId', 'date', 'peopleCount', 'peopleSeating'],
      where: {
        partnerId: 1,
        billingId: {[Op.not]: null},
        status: {[Op.notIn]: [RESERVATION_STATUS.WAIT_LIST, RESERVATION_STATUS.WAIT_LIST_LOCAL]},
        date: {
          [Op.between]: [dateStart, dateEnd]
        }
      },
      include: [
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          required: false
        }
      ],
    };
    const optionsSearchPlaces = {
      attributes: ['id', 'name', 'cityId', 'phone', 'email', 'logoImage', 'isActive', 'fixedCharge', 'sellerId', 'hasElectronicBillingEnabled'],
      where: {},
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          required: false
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          where: {
            type: SPONSOR_PLACE_TYPE.COMMISSION
          },
          required: false
        },
        includeReservation
      ]
    }
    const optionsSearchBillings = {
      attributes: ['id', 'placeId', 'status', 'label', 'fixedChargeApplied', 'appliedTax', 'dateStart', 'dateEnd'],
      where: {
        dateStart: {
          [Op.gte]: dateStart
        },
        dateEnd: {
          [Op.lte]: dateEnd
        },
      },
      include: [{
        model: models.Place,
        as: 'place',
        ...optionsSearchPlaces
      }]
    };
    if (citiesId.length) {
      optionsSearchPlaces.where.cityId = {
        [Op.in]: citiesId
      }
    }
    if (placesId.length) {
      optionsSearchPlaces.where.id = {
        [Op.in]: placesId
      }
      optionsSearchBillings.where.placeId = {
        [Op.in]: placesId
      }
    }
    if (sellersId.length) {
      optionsSearchPlaces.where.sellerId = {
        [Op.in]: sellersId
      }
    }
    if (statusNames.length) {
      optionsSearchBillings.where.status = {
        [Op.in]: statusNames
      }
    }
    if (label !== '') {
      optionsSearchBillings.where.label = label;
    }

    let result = {
      verified: 0,
      paid: 0,
      pending: 0,
      lost: 0,
      reservations: 0,
      cuts: 0,
      dateStart,
      dateEnd
    }
    const billings = await models.Billings.findAll(optionsSearchBillings);
    for (const billing of billings) {
      const place = billing.place;
      if (place) {
        result.reservations += place.reservation.length;
        result.cuts++;
        result = await accumulateInvoicesForCollections(billing, place, result);
      }
    }
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 *
 */
router.get('/collections', async (req, res) => {
  try {
    const {
      ranges = '[]',
      cities = '',
      seller = '',
      place = '',
      status = '',
      label = '',
      pageIndex = 0,
      pageSize = 10
    } = req.query;
    const dateRanges = JSON.parse(ranges);
    const citiesId = cities !== '' ? cities?.split(',') : [];
    const placesId = place !== '' ? place?.split(',') : [];
    const statusNames = status !== '' ? status?.split(',') : [];
    const sellersId = seller !== '' ? seller?.split(',') : [];

    const includeReservation = {
      model: models.Reservation,
      as: 'reservation',
      required: false,
      attributes: ['id', 'status', 'origin', 'invoiceAmount', 'billingId', 'partnerId', 'date', 'peopleCount', 'peopleSeating'],
      where: {
        partnerId: 1,
        billingId: {[Op.not]: null},
        status: {[Op.notIn]: [RESERVATION_STATUS.WAIT_LIST, RESERVATION_STATUS.WAIT_LIST_LOCAL]},
        [Op.or]: dateRanges.map(value => ({
          date: {
            [Op.between]: [moment(value.dateStart).format('YYYY-MM-DD'), moment(value.dateEnd).format('YYYY-MM-DD')]
          }
        }))
      },
      include: [
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          required: false
        }
      ],
    };
    const optionsSearchPlaces = {
      attributes: ['id', 'name', 'cityId', 'phone', 'email', 'logoImage', 'isActive', 'fixedCharge', 'sellerId', 'collectionNotes', 'typeAccount', 'hasElectronicBillingEnabled', 'emailPersonInCharge', 'personInCharge', 'facturapiCfdi', 'taxRecord', 'taxToApply'],
      where: { isActive: 1},
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          required: false
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          where: {
            type: SPONSOR_PLACE_TYPE.COMMISSION
          },
          required: false
        },
        {
          model: models.Seller,
          as: 'sellers',
          required: false
        },
        includeReservation
      ]
    }
    const optionsSearchBillings = {
      attributes: ['id', 'placeId', 'status', 'dateStart', 'dateEnd', 'label', 'paymentUrl', 'appliedTax', 'fixedChargeApplied'],
      where: {
        [Op.or]: dateRanges.map(value => ({
          dateStart: {
            [Op.gte]: moment(value.dateStart).format('YYYY-MM-DD')
          },
          dateEnd: {
            [Op.lte]: moment(value.dateEnd).format('YYYY-MM-DD')
          }
        }))
      },
      include: [{
        model: models.Place,
        as: 'place',
        ...optionsSearchPlaces
      }, {
        model: models.Payments,
        as: 'payments',
        required: false
      }],
      offset: (+pageIndex * +pageSize) || 0,
      limit: +pageSize || 10
    };
    if (citiesId.length) {
      optionsSearchPlaces.where.cityId = {
        [Op.in]: citiesId
      }
    }
    if (placesId.length) {
      optionsSearchPlaces.where.id = {
        [Op.in]: placesId
      }
      optionsSearchBillings.where.placeId = {
        [Op.in]: placesId
      }
    }
    if (sellersId.length) {
      optionsSearchPlaces.where.sellerId = {
        [Op.in]: sellersId
      }
    }
    if (statusNames.length) {
      optionsSearchBillings.where.status = {
        [Op.in]: statusNames
      }
    }
    if (label !== '') {
      optionsSearchBillings.where.label = label;
    }

    const collections = [];
    let reservations = 0;
    const cuts = await models.Billings.count({where: optionsSearchBillings.where});
    const billings = await models.Billings.findAll(optionsSearchBillings);
    for (const billing of billings) {
      const place = billing.place;
      if (place) {
        const result = {
          id: billing.id,
          appliedTax: billing.appliedTax,
          place: place.name,
          placeId: place.id,
          email: place.emailPersonInCharge ? place.emailPersonInCharge : place.email,
          image: place.logoImage,
          phone: place.phone,
          seller: place.sellers?.name,
          collectionNotes: place.collectionNotes,
          dateStart: billing.dateStart,
          dateEnd: billing.dateEnd,
          paymentUrl: billing.paymentUrl,
          payments: billing.payments,
          collections: {
            verified: 0,
            paid: 0,
            pending: 0,
            lost: 0
          },
          status: billing.status,
          label: billing.label,
          reservationsToChange: place?.typeAccount == 'rsv' ? place?.reservation?.filter(elem => [RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.CONFIRMED].includes(elem.status) && elem.billingId === billing.id).length :
            place?.reservation?.filter(elem => [RESERVATION_STATUS.SEATED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.CONFIRMED].includes(elem.status) && elem.billingId === billing.id).length,
          personInCharge: place?.personInCharge,
          facturapiCfdi: place?.facturapiCfdi,
          taxRecord: place?.taxRecord,
          taxToApply: place?.taxToApply,
          hasElectronicBillingEnabled: place?.hasElectronicBillingEnabled
        }
        result.collections = await accumulateInvoicesForCollections(billing, place, result.collections);
        reservations += place.reservation.length;
        collections.push(result);
      }
    }
    return res.json({collections, reservations, cuts});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 *
 */
router.get('/reservations/resume', async (req, res) => {
  try {
    const {
      dateStart = moment().startOf("months").format('YYYY-MM-DD'),
      dateEnd = moment().format('YYYY-MM-DD'),
      cities = '',
      seller = '',
      place = '',
      status = '',
      origin = 'ALL'
    } = req.query;
    const citiesId = cities !== '' ? cities?.split(',') : [];
    const placesId = place !== '' ? place?.split(',') : [];
    const statusNames = status !== '' ? status?.split(',') : [];
    const sellersId = seller !== '' ? seller?.split(',') : [];

    const includeReservation = {
      model: models.Reservation,
      as: 'reservation',
      required: true,
      attributes: ['id', 'status', 'origin', 'invoiceAmount', 'partnerId', 'date', 'peopleCount', 'peopleSeating'],
      where: {
        partnerId: 1,
        date: {[Op.between]: [dateStart, dateEnd]},
        status: {
          [Op.in]: [
            RESERVATION_STATUS.GONE,
            RESERVATION_STATUS.SEATED,
            RESERVATION_STATUS.CONFIRMED,
            RESERVATION_STATUS.RE_CONFIRMED,
            RESERVATION_STATUS.PENDING,
            RESERVATION_STATUS.NO_SHOW,
            RESERVATION_STATUS.CANCELLED
          ]
        }
      },
      include: [
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          required: false
        }
      ],
    };
    const optionsSearchPlaces = {
      attributes: ['id', 'name', 'cityId', 'phone', 'email', 'fixedCharge'],
      where: {isActive: 1},
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          required: false
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          where: {
            type: SPONSOR_PLACE_TYPE.COMMISSION
          },
          required: false
        },
        includeReservation
      ]
    }

    if (citiesId.length) {
      optionsSearchPlaces.where.cityId = {
        [Op.in]: citiesId
      }
    }
    if (placesId.length) {
      optionsSearchPlaces.where.id = {
        [Op.in]: placesId
      }
    }
    if (sellersId.length) {
      optionsSearchPlaces.where.sellerId = {
        [Op.in]: sellersId
      }
    }
    if (statusNames.length) {
      includeReservation.where.status = {
        [Op.in]: statusNames
      }
    }
    if (origin !== 'ALL') {
      includeReservation.where.origin = origin;
    }

    let result = {
      dateStart,
      dateEnd,
      totalPlace: 0,
      reservations: 0,
      peopleCount: 0,
      peopleSeating: 0,
      status: {}
    }
    const places = await models.Place.findAll(optionsSearchPlaces);
    for (const place of places) {
      if (place.reservation.length) {
        result.totalPlace++;
        result.reservations += place.reservation.length;
        for (const reservation of place.reservation) {
          result.peopleCount = await sum(0, result.peopleCount, reservation.peopleCount);
          result.peopleSeating = await sum(0, result.peopleSeating, reservation.peopleSeating);
          if (!Object.keys(result.status).includes(reservation.status)) {
            result.status[reservation.status] = 0;
          }
          result.status[reservation.status]++;
        }
      }
    }
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 *
 */
router.get('/reservations/group-by/places', async (req, res) => {
  try {
    const {
      ranges = '[]',
      cities = '',
      seller = '',
      place = '',
      status = '',
      origin = 'ALL'
    } = req.query;
    const dateRanges = JSON.parse(ranges);
    const citiesId = cities !== '' ? cities?.split(',') : [];
    const placesId = place !== '' ? place?.split(',') : [];
    const statusNames = status !== '' ? status?.split(',') : [];
    const sellersId = seller !== '' ? seller?.split(',') : [];

    const includeReservation = {
      model: models.Reservation,
      as: 'reservation',
      required: true,
      attributes: [
        'id', 'peopleCount', 'peopleSeating'
      ],
      where: {
        partnerId: 1,
        status: {
          [Op.in]: [
            RESERVATION_STATUS.GONE,
            RESERVATION_STATUS.SEATED,
            RESERVATION_STATUS.CONFIRMED,
            RESERVATION_STATUS.RE_CONFIRMED,
            RESERVATION_STATUS.PENDING,
            RESERVATION_STATUS.NO_SHOW,
            RESERVATION_STATUS.CANCELLED
          ]
        },
        [Op.or]: dateRanges.map(value => ({
          date: {
            [Op.between]: [
              moment(value.dateStart).format('YYYY-MM-DD'),
              moment(value.dateEnd).format('YYYY-MM-DD')
            ]
          }
        }))
      }
    };
    const optionsSearchPlaces = {
      attributes: ['id', 'name', 'cityId', 'phone', 'email', 'fixedCharge', 'logoImage'],
      where: {},
      include: [
        includeReservation
      ]
    }

    if (citiesId.length) {
      optionsSearchPlaces.where.cityId = {
        [Op.in]: citiesId
      }
    }
    if (placesId.length) {
      optionsSearchPlaces.where.id = {
        [Op.in]: placesId
      }
    }
    if (sellersId.length) {
      optionsSearchPlaces.where.sellerId = {
        [Op.in]: sellersId
      }
    }
    if (statusNames.length) {
      includeReservation.where.status = {
        [Op.in]: statusNames
      }
    }
    if (origin !== 'ALL') {
      includeReservation.where.origin = origin;
    }

    let result = {
      totalPlace: 0,
      reservations: 0,
      places: []
    }
    const places = await models.Place.findAll(optionsSearchPlaces);
    for (const place of places) {
      if (place.reservation.length) {
        const itemPlace = {
          id: place.id,
          image: place.logoImage,
          place: place.name,
          email: place.email,
          phone: place.phone,
          peopleSent: 0,
          peopleAttended: 0
        };
        result.totalPlace++;
        result.reservations += place.reservation.length;
        for (const reservation of place.reservation) {
          itemPlace.peopleSent = await sum(0, itemPlace.peopleSent, reservation.peopleCount);
          itemPlace.peopleAttended = await sum(0, itemPlace.peopleAttended, reservation.peopleSeating);
        }
        result.places.push(itemPlace);
      }
    }
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 *
 */
router.get('/places/resume', async (req, res) => {
  try {
    let {
      dateStart = '',
      dateEnd = '',
      cities = '',
      seller = ''
    } = req.query;
    const citiesId = cities !== '' ? cities?.split(',') : [];
    const sellersId = seller !== '' ? seller?.split(',') : [];

    const optionsActiveAndInactive = {
      attributes: ['isActive', [sequelize.fn('COUNT', sequelize.col('isActive')), 'value']],
      group: ['isActive'],
      where: {}
    };
    const optionsSellers = {
      attributes: ['sellerId'],
      include: [{
        attributes: ['id', 'name', [sequelize.fn('count', sequelize.col('*')), 'total']],
        model: models.Seller,
        as: 'sellers',
        required: true
      }],
      group: ['sellers.id'],
      where: {}
    };

    if (dateStart !== '') {
      dateStart = moment(dateStart).startOf('d').format('YYYY-MM-DD HH:mm:ss');
    }
    if (dateEnd !== '') {
      dateEnd = moment(dateEnd).endOf('d').format('YYYY-MM-DD HH:mm:ss');
    }

    if (dateStart !== '' && dateEnd !== '') {
      optionsActiveAndInactive.where.createdAt = {[Op.between]: [dateStart, dateEnd]};
      optionsSellers.where.createdAt = {[Op.between]: [dateStart, dateEnd]};
    } else if (dateStart !== '') {
      optionsActiveAndInactive.where.createdAt = {[Op.gte]: dateStart}
      optionsSellers.where.createdAt = {[Op.gte]: dateStart};
    } else if (dateStart !== '') {
      optionsActiveAndInactive.where.createdAt = {[Op.lte]: dateEnd};
      optionsSellers.where.createdAt = {[Op.lte]: dateEnd};
    }
    if (citiesId.length) {
      optionsActiveAndInactive.where.cityId = {[Op.in]: citiesId};
      optionsSellers.where.cityId = {[Op.in]: citiesId};
    }
    if (sellersId.length) {
      optionsActiveAndInactive.where.sellerId = {[Op.in]: sellersId};
      optionsSellers.where.sellerId = {[Op.in]: sellersId};
    }

    let result = {
      dateStart,
      dateEnd,
      active: 0,
      inactive: 0,
      sellers: []
    }

    const queryActiveAndInactive = await models.Place.findAll(optionsActiveAndInactive);
    const querySellers = await models.Place.findAll(optionsSellers);

    result.active = queryActiveAndInactive.find(value => value.isActive === true)?.dataValues.value;
    result.inactive = queryActiveAndInactive.find(value => value.isActive === false)?.dataValues.value;
    result.sellers = querySellers.map(value => value.sellers);

    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
