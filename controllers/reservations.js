const express = require('express');
const io = require('../socket');
const router = express.Router();
const moment = require('moment-timezone');
const models = require('../models');
const Op = models.Sequelize.Op;
const {typeChargesToApplyInRangeDate} = require("../helpers/query.helper");
const {emailModule} = require('../helpers/email.helper');
const smsModule = require('../helpers/sms');
const payment = require('../helpers/conekta');
const paymentMiddleware = require('../middlewares/paymentGateway');
const encryption = require('../helpers/encryption');
const conektaModule = require('../helpers/conekta');
const Notifications = require('../helpers/notifications.helper');
const {moduleWhatsApp} = require('../helpers/toolsSendWhatsApp');
const {moduleBitacora} = require('../helpers/bitacora');
const pointsModule = require('../helpers/points');
const {createNotification} = require('../helpers/notificationsCustomer');
const {sum, invoiceByReservation, multiply} = require("../helpers/calculate.helper");
const {
  RESERVATION_STATUS,
  RESERVATION_ORIGIN,
  ROLES,
  CANCELED_BY,
  TYPE_NOTIFICATION_BY_RESERVATION,
  ACCOUNT_TYPE,
  POINTS
} = require("../helpers/constants.helper");
const {confirmedReservation, cancelledReservation} = require('../helpers/reservation.helper');
const {HandleError, InternalProcessError} = require("../helpers/error.helper");
const {ModuleZoho} = require("../helpers/zoho.helper");
const Sanitize = require("express-validator");
const helpGoogle = require("../helpers/googleReserve");
const {checkReservationsToRegisterCustomerByReference, savePointOfMovement, getRegisterCustomerById} = require("../helpers/points");

const checkingPointsCustomer = async (emailCustomer, placeIsSpecial = false) => {
  const check = await checkReservationsToRegisterCustomerByReference(emailCustomer)
  if (check) {
    if (check.count === 1 && check?.customer?.referenceRegisteredCustomerId) {
      const customerReference = await getRegisterCustomerById(check.customer.referenceRegisteredCustomerId);
      if (customerReference) {
        await savePointOfMovement(true, check.customer.id, POINTS.FIRST_RESERVE_WITH_REFERENCE);
        await savePointOfMovement(true, customerReference.id, POINTS.FIRST_RESERVE_WITH_REFERENCE);
      }
    } else {
      await savePointOfMovement(true, check.customer.id, placeIsSpecial ? POINTS.EFFECTIVE_RESERVE_PLACE_SPECIAL : POINTS.EFFECTIVE_RESERVE);
    }
  }
}

const rsvConfirmed = async (token, req, res, force = false, reConfirmed = false) => {
  try {
    if (token) {
      const reservation = await models.Reservation.findByPk(token.reservationId, {
        attributes: ['id', 'date', 'datetime', 'start', 'peopleCount', 'status', 'smokeArea', 'notes', 'billingId', 'placeId'],
        include: [{
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'timezone'],
        }, {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['id', 'email', 'fullname', 'countryCode', 'phone', 'validEmail', 'emailComplaints']
        }]
      });
      if (reservation) {
        if (!reservation.billingId) {
          if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status)) {
            return res.json({reservation, cannotConfirmed: true, status: reservation.status});
          }

          const now = moment().tz(reservation.place.timezone);
          const date = moment.tz(reservation.date, reservation.place.timezone).startOf('days').add(reservation.start, 'minutes');
          const oldReservation = now.isAfter(date);
          if (oldReservation) {
            return res.json({reservation, cannotConfirmed: true});
          }

          if (![RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.CONFIRMED].includes(reservation.status) || force) {
            reservation.status = reConfirmed ? RESERVATION_STATUS.RE_CONFIRMED : RESERVATION_STATUS.CONFIRMED;
            confirmedReservation(reservation, reConfirmed, null, emailModule);
            return res.json({reservation, cannotConfirmed: false});
          } else {
            return res.json({reservation, cannotConfirmed: false});
          }
        } else {
          return res.json({reservation, cannotConfirmed: true});
        }
      } else {
        return res.status(400).send('No existe la reserva');
      }
    } else {
      return res.sendStatus(522);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
}

const rsvCancelation = async (token, req, res, force = false, isClient) => {
  try {
    if (token) {
      const reservation = await models.Reservation.findByPk(token.reservationId, {
        attributes: ['id', 'date', 'datetime', 'start', 'peopleCount', 'status', 'smokeArea', 'billingId', 'placeId'],
        include: [{
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'timezone'],
        }, {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['id', 'email', 'fullname', 'countryCode', 'phone', 'validEmail', 'emailComplaints']
        }]
      });

      if (reservation) {
        if (!reservation.billingId) {
          if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status)) {
            return res.json({reservation, cannotCancelled: true, status: reservation.status});
          }

          const now = moment().tz(reservation.place.timezone);
          const date = moment.tz(reservation.date, reservation.place.timezone).startOf('days').add(reservation.start, 'minutes');
          const oldReservation = now.isAfter(date);
          if (oldReservation) {
            return res.json({reservation, cannotCancelled: true});
          }

          if (![RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.CONFIRMED].includes(reservation.status) || force) {
            reservation.status = RESERVATION_STATUS.CANCELLED;
            cancelledReservation(reservation, isClient, emailModule, moduleWhatsApp);
            return res.json({reservation, cannotCancelled: false});
          } else {
            return res.json({reservation, cannotCancelled: false});
          }
        } else {
          return res.json({reservation, cannotCancelled: true});
        }
      } else {
        return res.status(400).send('No existe la reserva') // TODO traducción
      }
    } else {
      return res.sendStatus(522);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
}

const newResNotMessage = async (reservation) => {
// TODO traducir textos en archivo i18n
  let message = '';
  if (reservation.status === RESERVATION_STATUS.WAIT_LIST || reservation.status === RESERVATION_STATUS.WAIT_LIST_LOCAL) {
    message = `Nueva reservación en espera\n`;
  } else if (reservation.status === RESERVATION_STATUS.CONFIRMED || reservation.status === RESERVATION_STATUS.RE_CONFIRMED) {
    message = "Reserva confirmada\n";
  } else if (reservation.status === RESERVATION_STATUS.SEATED) {
    message = "Se ocupó una mesa\n";
  } else if (reservation.status === RESERVATION_STATUS.PENDING) {
    message = 'Reserva pendiente\n';
  }
  if (message !== '') {
    message = await getTableData(reservation, message);
  }
  return message;
}

const getTableData = async (reservation, message) => {
  message = message + `${reservation.date}, `;
  if (reservation.tableId) {
    const table = await models.Table.findOne({where: {id: reservation.tableId}});
    message = message + `mesa: ${table.number}, `;
  }
  if (reservation.groupId) {
    const group = await models.Group.findOne({where: {id: reservation.dataValues.groupId}});
    message = message + `grupo: ${group.numbers}, `;
  }
  return message + `${reservation.peopleCount} personas.`;
}

const placeIsBlocked = async (place, reservation) => {
  if (place.isReservationsBlocked) {
    const date = moment(reservation.datetime).format('YYYY-MM-DD');
    if (date >= place.blockedDateStart && date <= place.blockedDateEnd) {
// Dentro del bloque de días
      const time = reservation.start;
      if (time >= place.blockedHourStart && time <= place.blockedHourEnd) {
// Dentro del bloque de horas
        return true;
      } else {
// Fuera del bloque de horas
        return false;
      }
    } else {
// Fuera del bloque de días
      return false;
    }
  }
  return false;
}

const sendNotification = async (reservation, message) => {
  const IO = io.getIO();
  const placeId = reservation.placeId.toString();
  const obj = {
    createdAt: reservation.createdAt,
    message,
    reservationStatus: reservation.status,
    reservationId: reservation.id,
    reservationType: reservation.type,
    advancePayment: reservation.advancePaymentId
  };

  IO.to(placeId).emit('new-notification', obj);
  if (reservation?.partnerId == 1) {
    IO.emit("new-reservation-admin", obj);
  }
}

const getStatusMessage = async (polyglot, reservation, placeName, role) => {
  let message = '';
  let hour = '';
  switch (reservation.status) {
    case RESERVATION_STATUS.WAIT_LIST_LOCAL:
      message = role === ROLES.FASTFOOD ? polyglot.t('enterOrder') : polyglot.t('enterLocalWaitList');
      break;
    case RESERVATION_STATUS.WAIT_LIST:
      message = polyglot.t('enterWaitList');
      break;
    case RESERVATION_STATUS.CANCELLED:
      message = polyglot.t('reservationCancelled');
      break;
    case RESERVATION_STATUS.CONFIRMED:
      hour = moment().startOf('day').add(reservation.start, 'minutes').format('HH:mm');
      message = polyglot.t('reservationConfirmed', {placeName, reservationDate: reservation.date, hour});
      break;
    case RESERVATION_STATUS.RE_CONFIRMED:
      hour = moment().startOf('day').add(reservation.start, 'minutes').format('HH:mm');
      message = polyglot.t('reservationReConfirmed', {placeName, reservationDate: reservation.date, hour});
      break;
  }
  return message;
}

const sendSMS = async (reservation, polyglot, placeName, customer, role) => {
  const customerPhone = customer.countryCode + customer.phone;
  const message = getStatusMessage(polyglot, reservation, placeName, role);
  smsModule.sendSMS(customerPhone, message);
  models.SmsPlaces.create({
    phone: customerPhone,
    message: message,
    userId: reservation.creatorUserId,
    placeId: reservation.placeId
  });
}

const checkoutAvailable = async (req) => {
  let check, optFilter;
  req.body.end = req.body.end ? req.body.end : req.body.start + 30;
  check = false;
  optFilter = {
    placeId: req.body.placeId,
    date: req.body.date,
    status: {[Op.notIn]: [RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.GONE, RESERVATION_STATUS.NO_SHOW]},
    [Op.or]: [
      {
        [Op.and]: [
          {start: {[Op.lte]: req.body.start}},
          {end: {[Op.gt]: req.body.end}}
        ],
      },
      {
        [Op.and]: [
          {start: {[Op.gt]: req.body.start}},
          {start: {[Op.lt]: req.body.end}}
        ]
      },
      {
        [Op.and]: [
          {end: {[Op.gt]: req.body.start}},
          {end: {[Op.lt]: req.body.end}}
        ]
      }
    ]
  };

  if (req.body.isGroup) {
    optFilter['groupId'] = req.body.groupId;
    check = true;
  }
  if (!req.body.isGroup && req.body.tableId !== null) {
    optFilter['tableId'] = req.body.tableId;
    check = true;
  }

  return new Promise((resolve, reject) => {
    if (check) {
      models.Reservation.findAll({
        where: optFilter,
        raw: true
      }).then(response => {
        resolve({available: response.length > 0 ? false : true, check: true})
      }, err => {
        resolve({available: true, check: true});
      });
    } else {
      resolve({available: true, check: false});
    }
  });
}

const checkoutBlocked = async (req) => {
  let date = moment().format('YYYY-MM-DD');
  let time = moment.duration(moment().format('H:mm')).asMinutes();
  return new Promise((resolve, reject) => {
    models.Place.findOne({
      where: {
        id: req.body.placeId,
        isReservationsBlocked: 1,
        [Op.and]: [
          {blockedDateStart: {[Op.lte]: date}},
          {blockedDateEnd: {[Op.gte]: date}}
        ]
      },
      raw: true
    }).then(data => {
      if (data && data.isReservationsBlocked) {
        if (data.blockedDateStart === date && data.blockedDateEnd === date) {
          if (time > data.blockedHourStart && data.blockedHourEnd > time) {
            resolve({blocked: true});
          } else {
            resolve({blocked: false});
          }
        } else if (data.blockedDateStart <= date && data.blockedDateEnd > date) {
          if (data.blockedDateStart === date && time > data.blockedHourStart) {
            resolve({blocked: true});
          } else {
            if (data.blockedDateStart < date) {
              resolve({blocked: true});
            } else {
              resolve({blocked: false});
            }
          }
        } else if (data.blockedDateStart <= date && data.blockedDateEnd === date) {
          if (data.blockedHourEnd > time) {
            resolve({blocked: true});
          } else {
            resolve({blocked: false});
          }
        } else if (data.blockedDateStart > date) {
          resolve({blocked: false});
        } else {
          resolve({blocked: false});
        }
      } else {
        resolve({blocked: false});
      }
    }, err => {
      resolve({blocked: false});
    });
  });
}

const checkoutAvailableWidgetFree = async (req) => {
  req.body['dayNumber'] = moment(req.body.date).day();
  return new Promise((resolve, reject) => {
    if (typeof (req.body.zoneId) === "object" && req.body.zoneId == null && req.body.origin === 'WEBSITE') {
      models.OpeningDay.sqlBatchAvailabilityLookup(req.body, req.body.placeId.toString(), req.body.start, false).then(data => {
        if (data.length > 0) {
          let condition;
          if (data[0].maxCapacity && data[0].maxCapacity > 0) {
            condition = data[0].ocupados < data[0].maxCapacity ? true : false;
            resolve({reservation: condition});
          } else {
            resolve({reservation: true});
          }
        } else {
          resolve({reservation: true});
        }
      }, err => {
        resolve({reservation: true});
      });
    } else {
      resolve({reservation: true});
    }
  });
}

const getReservation = async (req) => {
  return await models.Reservation.findByPk(req.params.reservationId, {
    attributes: ['id', 'date', 'start', 'peopleCount', 'smokeArea', 'status', 'zoneId', 'tableId', 'serviceId', 'groupId', 'costumerId', 'duration', 'dateTime', 'notes', 'smokeArea'],
    include: [{
      model: models.Costumer,
      as: 'costumer',
      attributes: ['fullname', 'countryCode', 'phone', 'email', 'notes']
    }, {
      model: models.Place,
      as: 'place',
      attributes: ['name', 'id']
    }]
  });
}

const seendMessageRegisteredCustomer = async (reservation, message) => {
  try {
    const c = await models.Costumer.findOne({where: {id: reservation.costumerId}})
    if (c) {
      let registeredCustomer = await models.RegisteredCustomer.findOne({
        where: {email: c.email}
      });
      if (registeredCustomer) {
        await createNotification({registeredCustomerId: registeredCustomer.id, message});
      }
    }
  } catch (error) {
  }
}

const seendMessageRegisteredCustomerStatus = async (reservation, place) => {
  switch (reservation.status) {
    case RESERVATION_STATUS.CANCELLED:
      seendMessageRegisteredCustomer(reservation, "Tu reserva ha sido reserva cancelada en " + place.name + ".");
      break;
    case RESERVATION_STATUS.CONFIRMED:
      seendMessageRegisteredCustomer(reservation, "Tu reserva ha sido confirmada en " + place.name + ".");
      break;
  }
}

/******************************** GET *******************************/

router.get('/reservandonos/short/:code', async (req, res) => {
  try {
    const short = req.params.code;
    const s = await models.Shorter.findOne({where: {short}, attributes: ['long', 'type']});
    return res.json(s);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/place-date/:placeId/:date', async (req, res) => {
  try {
    const reservations = await models.Reservation.findAll({
      where: {placeId: req.params.placeId, date: req.params.date},
      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt', 'idempotencyToken']},
      include: [
        {
          model: models.Costumer,
          as: 'costumer',
          attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt',]}
        },
        {
          model: models.Zone,
          as: 'zone',
          attributes: ['id', 'name']
        },
        {
          model: models.Table,
          as: 'table',
          attributes: ['id', 'number', 'deletedAt'],
          paranoid: false
        },
        {
          model: models.Group,
          as: 'group',
          attributes: ['id', 'numbers', 'deletedAt'],
          paranoid: false,
          include: [{
            model: models.Table,
            as: 'tables',
            attributes: ['id']
          }]
        }
      ]
    });
    return res.json(reservations)
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/email/reconfirm', async (req, res, next) => { // TODO ¿esta función quién la llama, borrar?
  try {
    return res.json(await emailModule.reconfirmEmail());
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/simple/:reservationId', async (req, res) => {
  try {
    const reservation = await getReservation(req);
    reservation.start = moment.utc(moment.duration(reservation.start, "minutes").asMilliseconds()).format("HH:mm")
    return res.json(reservation);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/wordpress/:reservationId', async (req, res) => {
  try {
    const reservation = await getReservation(req);
    return res.json(reservation);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/token/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    if (token) {
      const reservation = await models.Reservation.findByPk(token.reservationId, {
        attributes: ['id', 'date', 'start', 'peopleCount', 'smokeArea', 'status', 'advancePaymentId', 'reviewed'],
        include: [{
          model: models.Partner,
          as: 'partner',
          attributes: ['logoImage']
        }, {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'logoImage', 'paymentGatewayPublicKey', 'paymentGatewayPrivateKey', 'cancellationFeePerPerson'],
        }]
      });
      if (isNaN(reservation.advancePaymentId)) {
        const order = await conektaModule.order.get(reservation.advancePaymentId, reservation.place.paymentGatewayPrivateKey);
        reservation.advancePaymentId = order.amount / 100;
      }
      return res.json(reservation);
    } else {
      return res.sendStatus(522);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservationById/:id', async (req, res) => {
  try {
    const r = await models.Reservation.findByPk(req.params.id, {
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: {
            include: ['planId']
          }
        },
        {
          model: models.Costumer,
          as: 'costumer',
          include: [{
            model: models.Reservation,
            as: 'reservations',
            attributes: ['date', 'status']
          }]
        }
      ]
    });
    return res.json(r);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/bitacora/:id', async (req, res) => {
  try {
    const bitacora = await models.Bitacora.findAll({
      include: [
        {
          model: models.User,
          as: 'users',
          attributes: ['fullname']
        }
      ],
      where: {
        tableName: 'Reservations',
        tableId: req.params.id,
      },
      order: [['id', 'Desc']]
    });
    return res.json(bitacora);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/get-token/:id', async (req, res) => {
  try {
    const reservationId = req.params.id;
    const token = encryption.encrypt(JSON.stringify({reservationId}));
    return res.json(token);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/encrypt', async (req, res) => {
  try {
    const response = encryption.encrypt(JSON.stringify({
      reservationId: '440398'
    }));
// placeId: 1030,
// name: 'Gregori Gonzalez',
// email: 'gregori.gonzalez.90@gmail.com'
    return res.json({data: response});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-date-and-service', async (req, res) => {
  try {
    let where = {
      [Op.or]: [{serviceId: req.query.serviceId}, {serviceId: {[Op.is]: null}}],
      placeId: req.user.placeId,
      date: req.query.date,
    };

    if (!req.query.serviceId) {
      where['[Op.and]'] = {
        start: {[Op.gt]: req.query.start},
        end: {[Op.lte]: req.query.end}
      }
    }
    const result = await models.Reservation.findAll({
      attributes: ['id', 'datetime', 'start', 'end', 'duration', 'peopleCount', 'status', 'isGroup', 'groupId', 'type', 'advancePaymentId'],
      include: [
        {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['fullname', 'email', 'phone', 'notes']
        },
        {
          model: models.Zone,
          as: 'zone',
          attributes: ['id', 'name']
        },
        {
          model: models.Table,
          as: 'table',
          attributes: ['id', 'number', 'deletedAt'],
          paranoid: false
        },
        {
          model: models.Group,
          as: 'group',
          attributes: ['id', 'numbers', 'deletedAt'],
          paranoid: false,
          include: [{
            model: models.Table,
            as: 'tables',
            attributes: ['id']
          }]
        }
      ],
      where,
      order: [['start', 'asc']]
    });
    const resWithoutDel = result.filter(rs => ((rs.dataValues.status == RESERVATION_STATUS.WAIT_LIST || rs.dataValues.status == RESERVATION_STATUS.WAIT_LIST_LOCAL) || rs.dataValues.groupId && rs.dataValues.group.dataValues.deletedAt == null) || (rs.dataValues.table && rs.dataValues.table.deletedAt == null));
    const reservations = resWithoutDel.map(rs => {
      const r = rs.dataValues;
      if (r.status != RESERVATION_STATUS.WAIT_LIST_LOCAL && r.status != RESERVATION_STATUS.WAIT_LIST) {
        r.tableNumbers = !r.groupId ? r.table.number : r.group.numbers;
        r.tables = !r.groupId ? [{id: r.table.id}] : r.group.tables.map(t => ({id: t.id}));
        delete r.table;
        delete r.group;
      }
      return r;
    });
    return res.json(reservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/statistics', async (req, res) => {
  try {
    const {placeId, origin, newCostumer, status, dateStart, dateEnd, partnerName} = req.query;
    const wherePlace = {};
    const whereReservations = {};
    const wherePartnerName = {};

    if (placeId) {
      wherePlace.id = placeId;
      whereReservations.placeId = placeId;
    }
    if (partnerName) {
      wherePartnerName.name = {
        [Op.like]: `%${partnerName}%`
      };
    }
    if (req.user.placeId) {
      whereReservations.placeId = req.user.placeId;
    }
    if (req.user.partnerId) {
      whereReservations.partnerId = req.user.partnerId;
    }
    if (origin) {
      whereReservations.origin = origin;
    }
    if (newCostumer) {
      whereReservations.newCostumer = newCostumer === 'true';
    }
    if (status) {
      whereReservations.status = status;
    } else {
      whereReservations.status = {[Op.notIn]: [RESERVATION_STATUS.WAIT_LIST, RESERVATION_STATUS.WAIT_LIST_LOCAL]};
    }
    if (dateStart && dateEnd) {
      whereReservations.date = {
        [Op.gte]: moment(dateStart).startOf('day'),
        [Op.lte]: moment(dateEnd).endOf('day')
      };
    } else if (dateStart) {
      whereReservations.date = {[Op.gte]: moment(dateStart).startOf('day')};
    } else if (dateEnd) {
      whereReservations.date = {[Op.lte]: moment(dateEnd).startOf('day')};
    }

    const responseProduct = {
      valueSponsor: 0,
      sumProductSponsor: 0,
      commissionSponsor: 0,
      isSponsor: false
    };
    const statistics = {
      income: 0,
      commission: 0,
      revenue: 0,
      peopleSeating: 0,
      peopleCount: 0,
      costumers: {
        new: 0,
        recurrent: 0
      },
      origin: {
        website: 0,
        phone: 0,
        whatsapp: 0,
        walkIn: 0,
        google: 0,
        app: 0,
        facebook: 0,
        twitter: 0,
        instagram: 0
      },
      status: {},
      totalToPay: 0,
      iva: 0,
      taxToApply: 0,
      detour: 0
    };

    const places = await models.Place.findAll({
      where: wherePlace,
      attributes: [
        'id', 'name', 'typeAccount', 'fixedCharge', 'sentPersonPrice', 'taxToApply'
      ],
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          required: false,
          attributes: ['type', 'value', 'origin', 'id'],
          ...typeChargesToApplyInRangeDate(dateStart, dateEnd)
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['value', 'id', 'placeId'],
          required: false,
          where: {
            type: 'COMMISSION'
          }
        },
        {
          model: models.Reservation,
          as: 'reservation',
          required: true,
          where: whereReservations,
          attributes: [
            'id', 'peopleCount', 'newCostumer', 'origin', 'peopleSeating', 'origin', 'partnerId', 'status', 'invoiceAmount', 'invoiceAmountTotal', 'reservationIdDetour'
          ],
          include: [
            {
              model: models.Partner,
              as: 'partner',
              attributes: ['sentPersonFee'],
              required: partnerName !== '',
              where: wherePartnerName
            },
            {
              model: models.ReservationSponsors,
              as: 'reservationSponsor',
              attributes: ['totalCommission', 'quantity', 'reservationId'],
              required: false
            }
          ]
        }
      ]
    });
    if (places.length) {
      for (const place of places) {
        const typeCharges = place.typeCharges;
        const reservations = place.reservation;
        for (const reservation of reservations) {
          if (reservation?.reservationIdDetour) {
            statistics.detour++;
          }

          if (reservation.newCostumer) {
            statistics.costumers.new = await sum(0, statistics.costumers.new, 1);
          } else {
            statistics.costumers.recurrent = await sum(0, statistics.costumers.recurrent, 1);
          }

          const originReservation = (reservation.origin === RESERVATION_ORIGIN.WALK_IN) ? 'walkIn' : reservation.origin.toLowerCase();
          statistics.origin[originReservation] = await sum(0, statistics.origin[originReservation], 1);

          if (statistics.status[reservation.status] === undefined) {
            statistics.status[reservation.status] = 0;
          }
          statistics.status[reservation.status]++;

          statistics.peopleCount = await sum(0, statistics.peopleCount, reservation.peopleCount);

          if ([RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(reservation.status)) {
            const seating = reservation.peopleSeating > 0 ? reservation.peopleSeating : reservation.peopleCount;
            statistics.peopleSeating = await sum(0, statistics.peopleSeating, seating);

            const {byCharge} = await invoiceByReservation(reservation, typeCharges, place, true);
            statistics.commission = await sum(2, statistics.commission, byCharge);
          }
        }
      }
      if (whereReservations.placeId) {
        const place = places[0];
        for (const reservation of place.reservation) {
          if ([RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(reservation.status)) {
            for (const sponsor of reservation.reservationSponsor) {
              responseProduct.isSponsor = true;
              responseProduct.valueSponsor = await sum(2, responseProduct.valueSponsor, sponsor.totalCommission || 0);
              responseProduct.sumProductSponsor = await sum(2, responseProduct.sumProductSponsor, sponsor.quantity || 0);
            }
            responseProduct.commissionSponsor = responseProduct.valueSponsor;
          }
        }
        statistics.totalToPay = await sum(2, statistics.commission, responseProduct.commissionSponsor);
        statistics.taxToApply = await multiply(2, 100, place.taxToApply);
        statistics.iva = await multiply(2, statistics.totalToPay, await sum(2, 1, place.taxToApply));
      }
    }
    return res.json({...responseProduct, ...statistics});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservandonos-statistics', async (req, res) => {
  try {
    const {placeId, dateStart, dateEnd} = req.query;
    whereReservations = {
      [Op.or]: [{partnerId: 1}, {origin: RESERVATION_ORIGIN.GOOGLE}],
      status: {
        [Op.notIn]: [RESERVATION_STATUS.WAIT_LIST_LOCAL, RESERVATION_STATUS.WAIT_LIST]
      }
    }
    if (dateStart && dateEnd) {
      whereReservations.date = {
        [Op.between]: [dateStart, dateEnd]
      }
    } else if (dateStart) {
      whereReservations.date = {
        [Op.gte]: dateStart
      }
    } else if (dateEnd) {
      whereReservations.date = {
        [Op.lte]: dateEnd
      }
    }

    if (placeId) {
      const place = await models.Place.findByPk(placeId);
      if (place) {
        const statistics = {
          total: 0, attendedTotal: 0, totalToPay: 0, totalByCharge: 0,
          totalBySponsor: 0
        };
        const typeCharges = await place.getTypeCharges();
        const reservations = await place.getReservation({
          attributes: ['status', 'peopleSeating', 'invoiceAmount', 'peopleCount', 'origin', 'partnerId'],
          include: [
            'reservationSponsor'
          ],
          where: whereReservations
        });
        for (const reservation of reservations) {
          statistics[reservation.status] = statistics[reservation.status] ? statistics[reservation.status] + 1 : 1;
          if ([RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(reservation.status)) {
            const attended = reservation.peopleSeating > 0 ? reservation.peopleSeating : reservation.peopleCount;
            statistics.attendedTotal = await sum(2, statistics.attendedTotal, attended);

            const {byCharge, bySponsor} = await invoiceByReservation(reservation, typeCharges, place, true);
            statistics.totalToPay = await sum(2, statistics.totalToPay, byCharge, bySponsor);
            statistics.totalByCharge = await sum(2, statistics.totalByCharge, byCharge);
            statistics.totalBySponsor = await sum(2, statistics.totalBySponsor, bySponsor);
          }
          statistics.total++;
        }
        return res.json(statistics);
      } else {
        return res.json({error: true, message: "Place not found"});
      }
    } else {
      return res.status(400).send({message: 'PlaceId is required'})
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/template', async (req, res) => {
  try {
    await moduleWhatsApp.sendWhatsAppClientRemember(30, 'latest');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservandonos', async (req, res) => {
  try {
    const {
      sortBy, sortDir, status, date, dateStart, dateEnd,
      createdAtStart, createdAtEnd, pageIndex, pageSize, placeId
    } = req.query;
    let order;
    if (sortDir) {
      switch (sortBy) {
        case 'fullname':
          order = [['costumer', 'fullname', sortDir]];
          break;
        case 'phone':
          order = [['costumer', 'phone', sortDir]];
          break;
        case 'email':
          order = [['costumer', 'email', sortDir]];
          break;
        case 'date':
          order = [['date', sortDir], ['start', sortDir]];
          break;
        case 'origin':
          order = [['origin', sortDir]];
          break
        default:
          order = [[sortBy, sortDir]];
          break;
      }
    }

    const where = {
      placeId,
      [Op.or]: [{partnerId: 1}, {origin: RESERVATION_ORIGIN.GOOGLE}]
    };

    if (status) {
      where.status = status;
    } else {
      where.status = {[Op.notIn]: [RESERVATION_STATUS.WAIT_LIST, RESERVATION_STATUS.WAIT_LIST_LOCAL]};
    }

    if (date) {
      where.date = date;
    }

    if (dateStart && dateEnd) {
      where.date = {
        [Op.gte]: moment(dateStart).startOf('day'),
        [Op.lte]: moment(dateEnd).endOf('day')
      };
    } else if (dateStart) {
      where.date = {[Op.gte]: moment(dateStart).startOf('day')};
    } else if (dateEnd) {
      where.date = {[Op.lte]: moment(dateEnd).startOf('day')};
    }

    if (createdAtStart && createdAtEnd) {
      where.createdAt = {
        [Op.gte]: moment(createdAtStart).startOf('day'),
        [Op.lte]: moment(createdAtEnd).endOf('day')
      };
    } else if (createdAtStart) {
      where.createdAt = {[Op.gte]: moment(createdAtStart).startOf('day')};
    } else if (createdAtEnd) {
      where.createdAt = {[Op.lte]: moment(createdAtEnd).startOf('day')};
    }

    let sumCocas = await models.Place.findByPk(placeId, {
      attributes: ['id'],
      include: [
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          attributes: [[models.Sequelize.fn('sum', models.Sequelize.col('quantity')), 'total_coca_cola']],
          include: [
            {
              attributes: ['id'],
              model: models.Reservation,
              as: 'reservation',
              where: where
            }
          ]
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['value'],
          where: {
            type: 'COMMISSION'
          }
        }
      ]
    });

    let valueCommision = sumCocas && sumCocas['sponsorsPlaces'].length > 0 ? sumCocas['sponsorsPlaces'][0]['dataValues']['value'] : 0;
    sumCocas = sumCocas && sumCocas['reservationSponsor'].length > 0 ? sumCocas['reservationSponsor'][0]['dataValues']['total_coca_cola'] : 0;
    let commissionSponsor = valueCommision * sumCocas;

    let count = await models.Reservation.count({
      where: where
    })

    let result = await models.Reservation.findAll({
      include: [
        {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['fullname', 'countryCode', 'phone', 'email', 'validEmail']
        },
        {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'typeAccount', 'fixedCharge'],
          include: [
            {
              model: models.TypeCharge,
              as: 'typeCharges'
            },
            {
              model: models.SponsorPlaces,
              as: 'sponsorsPlaces'
            }
          ]
        },
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor'
        }
      ],
      where: where,
      order: order,
      offset: (+pageIndex * +pageSize) || 0,
      limit: +pageSize || 10
    });
    let place = await models.Place.findOne({
      attributes: ['id', 'name'],
      include: [
        {
          model: models.TypeCharge,
          as: 'typeCharges',
          require: false,
          ...typeChargesToApplyInRangeDate(dateStart, dateEnd)
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          include: [{
            model: models.Sponsor,
            as: 'sponsor'
          }]
        }
      ],
      where: {id: placeId}
    });

    return res.json({
      place: place,
      sumCocaColas: sumCocas,
      count: count,
      rows: result,
      commissionSponsor: commissionSponsor,
      valueCommision: valueCommision
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/status', async (req, res) => {
  try {
    const where = {
      placeId: req.query.placeId,
      [Op.or]: [{partnerId: 1}, {origin: RESERVATION_ORIGIN.GOOGLE}]
    };

    if (req.query.status) {
      where.status = req.query.status;
    } else {
      where.status = {[Op.notIn]: [RESERVATION_STATUS.WAIT_LIST, RESERVATION_STATUS.WAIT_LIST_LOCAL]};
    }

    where.date = {
      [Op.gte]: moment(req.query.dateStart).startOf('day'),
      [Op.lte]: moment(req.query.dateEnd).endOf('day')
    };

    let placeCocas = await models.Place.findByPk(req.query.placeId, {
      attributes: ['id'],
      include: [
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          attributes: [[models.Sequelize.fn('sum', models.Sequelize.col('quantity')), 'total_coca_cola']],
          include: [
            {
              attributes: ['id'],
              model: models.Reservation,
              as: 'reservation',
              where: where
            }
          ]
        },
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['value', 'isActive'],
          where: {
            type: 'COMMISSION'
          }
        }
      ]
    });

    let valueCommision = placeCocas && placeCocas['sponsorsPlaces'].length > 0 ? placeCocas['sponsorsPlaces'][0]['dataValues']['value'] : 0;
    let sumCocas = placeCocas && placeCocas['reservationSponsor'].length > 0 ? placeCocas['reservationSponsor'][0]['dataValues']['total_coca_cola'] : 0;
    let isActive = placeCocas && placeCocas['sponsorsPlaces'].length > 0 && placeCocas['sponsorsPlaces'][0]['isActive'] == 1 ? true : false;
    let commissionSponsor = valueCommision * sumCocas;

    models.Reservation.findAndCountAll({
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'typeAccount', 'fixedCharge'],
          include: [
            {
              model: models.TypeCharge,
              as: 'typeCharges'
            },
            {model: models.SponsorPlaces, as: 'sponsorsPlaces'}
          ]
        },
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor'
        }
      ],
      where: where,
    }).then(result => {
      result['sumCocaCola'] = sumCocas;
      result['valueCommision'] = valueCommision;
      result['commissionSponsor'] = commissionSponsor;
      result['placeHasSponsor'] = isActive;
      return res.json(result);
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservations-and-coupons', async (req, res) => {
  try {
    const {key, placeIds} = req.query;
    const reservations = await models.Reservation.findAll({
      attributes: [
        'id', 'placeId',
        ['peopleCount', 'people'],
        [models.sequelize.fn('CONCAT', '', 'RESERVATION'), 'type'],
        [models.sequelize.fn('CONCAT', '', ''), 'code'],
        [models.sequelize.col('costumer.fullname'), 'name'],
        [models.sequelize.col('costumer.email'), 'email']
      ],
      where: {
        date: {
          [Op.gte]: moment().format('YYYY-MM-DD')
        },
        placeId: {
          [Op.in]: placeIds.split(',')
        }
      },
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: [],
        where: {
          fullname: {
            [Op.like]: `%${key}%`
          }
        },
        required: true
      }],
      order: [['date', 'asc']],
      raw: true,
      limit: 3
    });
    const couponsCostumer = await models.Coupons.findAll({
      attributes: [
        'id', 'code',
        [models.sequelize.fn('CONCAT', '', 'COUPON'), 'type'],
        [models.sequelize.col('costumer.fullname'), 'name'],
        [models.sequelize.col('costumer.email'), 'email'],
        [models.sequelize.col('promotion.placeId'), 'placeId']
      ],
      where: {
        [Op.or]: [
          {
            code: {
              [Op.like]: `%${key}%`
            }
          },
          {
            '$costumer.fullname$': {
              [Op.like]: `%${key}%`
            }
          }
        ],
        createdAt: {
          [Op.gte]: moment().subtract(1, 'months').format('YYYY-MM-DD')
        },
        '$promotion.placeId$': {
          [Op.in]: placeIds.split(',')
        }
      },
      include: [
        {
          model: models.Costumer,
          as: 'costumer',
          attributes: [],
          required: true
        },
        {
          model: models.Promotions,
          as: 'promotion',
          attributes: [],
          required: true,
        }
      ],
      order: [['createdAt', 'asc']],
      raw: true,
      limit: 3
    });
    const couponsRegisteredCustomer = await models.Coupons.findAll({
      attributes: [
        'id', 'code',
        [models.sequelize.fn('CONCAT', '', 'COUPON'), 'type'],
        [models.sequelize.fn('CONCAT',
          models.sequelize.col('registeredCustomer.name'),
          ' ',
          models.sequelize.col('registeredCustomer.lastName')
        ), 'name'],
        [models.sequelize.col('registeredCustomer.email'), 'email'],
        [models.sequelize.col('promotion.placeId'), 'placeId']
      ],
      where: {
        [Op.or]: [
          {
            code: {
              [Op.like]: `%${key}%`
            }
          },
          models.sequelize.where(
            models.sequelize.fn('CONCAT',
              models.sequelize.col('registeredCustomer.name'),
              ' ',
              models.sequelize.col('registeredCustomer.lastName')
            ), {
              [Op.like]: `%${key}%`
            }
          )
        ],
        createdAt: {
          [Op.gte]: moment().subtract(1, 'months').format('YYYY-MM-DD')
        },
        '$promotion.placeId$': {
          [Op.in]: placeIds.split(',')
        }
      },
      include: [
        {
          model: models.RegisteredCustomer,
          as: 'registeredCustomer',
          attributes: [],
          required: true,
        },
        {
          model: models.Promotions,
          as: 'promotion',
          attributes: [],
          required: true,
        }
      ],
      order: [['createdAt', 'asc']],
      raw: true,
      limit: 3
    });
    return res.json([
      ...reservations,
      ...couponsCostumer,
      ...couponsRegisteredCustomer
    ]);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', paymentMiddleware.privateKey, async (req, res) => {
  try {
    const where = {id: req.params.id};
    const r = await models.Reservation.findByPk(req.params.id, {
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: {
            include: ['planId']
          }
        },
        {
          model: models.Costumer,
          as: 'costumer',
          include: [{
            model: models.Reservation,
            as: 'reservations',
            attributes: ['date', 'status']
          }]
        }
      ]
    });

    if (!r) return res.sendStatus(404);

    if (r.serviceId && r.zoneId) {
      const servicezone = await models.ServiceZone.findOne({
        where: {serviceId: r.serviceId, zoneId: r.zoneId},
        attributes: ['smokeArea']
      });
      if (servicezone) {
        r.hasSmokeArea = servicezone.smokeArea;
      }
    }

    const reservation = r.toJSON();
    if (reservation.paymentCardId && reservation.paymentCardId != 'send') {
      const customer = await payment.customer.get(reservation.costumer.paymentCustomerId, reservation.place.paymentGatewayPrivateKey);
      const card = customer.payment_sources.toObject().data.find(c => c.id === reservation.paymentCardId);
      if (card) {
        reservation.card = {
          name: card.name,
          last4: card.last4,
          number: `************${card.last4}`,
          exp_month: card.exp_month,
          exp_year: card.exp_year,
          cvc: '***'
        };
      }
    }
    if (reservation.advancePaymentId) {
      if (isNaN(reservation.advancePaymentId)) {
        const order = await payment.order.get(reservation.advancePaymentId, reservation.place.paymentGatewayPrivateKey);
// console.log(order.charges.data[0].payment_method);
        reservation.advanceData = {
          amount: order.amount,
          name: order.charges.data[0].payment_method.name,
          last4: order.charges.data[0].payment_method.last4,
        };
      } else {
        reservation.advanceData = {amount: reservation.advancePaymentId}
      }
    }
    if (reservation.paymentOrderId) {
      const order = await payment.order.get(reservation.paymentOrderId, reservation.place.paymentGatewayPrivateKey);
      if (order) {
        reservation.order = order;
      }
    }
    return res.json(reservation);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/',
  Sanitize.query(['canceledBy', 'reports']).toBoolean(),
  Sanitize.query(['pageIndex', 'pageSize']).toInt(),
  async (req, res) => {
    try {
      const {
        pageIndex, pageSize, sortBy, sortDir,
        term, partnerName, placeId, origin, newCostumer, dateStart = '',
        dateEnd = '', dateStartTwo = '', dateEndTwo = '', date = '', status, reports,
        createdAtStart = '', createdAtEnd = '', canceledBy = false
      } = req.query;

      const whereReservations = {
        ...(req.user.role === ROLES.TELEMARKETING ? {
          partnerId: {[Op.ne]: null},
          origin: {[Op.or]: [RESERVATION_ORIGIN.WEBSITE, RESERVATION_ORIGIN.PHONE]}
        } : {})
      };

      if (req.user.placeId) {
        whereReservations.placeId = req.user.placeId;
      }
      if (placeId) {
        let ids = placeId.split(',');
        if (ids.length > 0) {
          whereReservations.placeId = {
            [Op.in]: ids
          };
        } else {
          whereReservations.placeId = placeId;
        }
      }
      if (req.user.partnerId) {
        whereReservations.partnerId = req.user.partnerId;
      }
      if (origin) {
        whereReservations.origin = origin;
      }
      if (newCostumer) {
        whereReservations.newCostumer = newCostumer === 'true';
      }

      if (canceledBy) {
        whereReservations.canceledBy = {[Op.in]: ['PARTNER', 'CALL-CENTER']};
      }

      if (status) {
        whereReservations.status = status === 'UNAVAILABLE' ? RESERVATION_STATUS.CANCELLED : status;
      } else {
        whereReservations.status = {[Op.notIn]: [RESERVATION_STATUS.WAIT_LIST, RESERVATION_STATUS.WAIT_LIST_LOCAL]};
      }

      if (date) {
        whereReservations.date = date;
      }
      if ((dateStart && dateEnd && dateStartTwo && dateEndTwo) && (dateStart !== '' && dateEnd !== '' && dateStartTwo !== '' && dateEndTwo !== '')) {
        whereReservations.date = {
          [Op.or]: [
            {
              [Op.between]: [
                moment(dateStart).format('YYYY-MM-DD'),
                moment(dateEnd).format('YYYY-MM-DD')
              ]
            }, {
              [Op.between]: [
                moment(dateStartTwo).format('YYYY-MM-DD'),
                moment(dateEndTwo).format('YYYY-MM-DD')
              ]
            }
          ]
        }
      } else if (dateStart !== '' && dateEnd !== '') {
        whereReservations.date = {
          [Op.between]: [
            moment(dateStart).format('YYYY-MM-DD'),
            moment(dateEnd).format('YYYY-MM-DD')
          ]
        };
      } else if (dateStartTwo && dateEndTwo && dateStartTwo !== '' && dateEndTwo !== '') {
        whereReservations.date = {
          [Op.between]: [
            moment(dateStartTwo).format('YYYY-MM-DD'),
            moment(dateEndTwo).format('YYYY-MM-DD')
          ]
        };
      } else if (dateStart) {
        whereReservations.date = {[Op.gte]: moment(dateStart).format('YYYY-MM-DD')};
      } else if (dateEnd) {
        whereReservations.date = {[Op.lte]: moment(dateEnd).format('YYYY-MM-DD')};
      } else if (dateStartTwo) {
        whereReservations.date = {[Op.gte]: moment(dateStartTwo).format('YYYY-MM-DD')};
      } else if (dateEndTwo) {
        whereReservations.date = {[Op.lte]: moment(dateEndTwo).format('YYYY-MM-DD')};
      }

      if (createdAtStart && createdAtEnd) {
        whereReservations.createdAt = {
          [Op.gte]: moment(createdAtStart).startOf('day'),
          [Op.lte]: moment(createdAtEnd).endOf('day')
        };
      } else if (createdAtStart) {
        whereReservations.createdAt = {[Op.gte]: moment(createdAtStart).startOf('day')};
      } else if (createdAtEnd) {
        whereReservations.createdAt = {[Op.lte]: moment(createdAtEnd).startOf('day')};
      }

      const includePartner = {
        model: models.Partner,
        as: 'partner',
        attributes: ['id', 'name', 'sentPersonFee'],
        where: {},
        required: false
      }
      if (partnerName) {
        includePartner.required = true;
        includePartner.where.name = {[Op.like]: `%${partnerName}%`}
      }

      const includeCostumer = {
        model: models.Costumer,
        as: 'costumer',
        required: false,
        attributes: ['fullname', 'countryCode', 'phone', 'email', 'validEmail']
      };
      if (term) {
        includeCostumer.where = {
          [Op.or]: [
            {fullname: {[Op.like]: `%${term}%`}},
            {phone: {[Op.like]: `%${term}%`}},
            {email: {[Op.like]: `%${term}%`}}
          ],
        }
        includeCostumer.required = true;
      }

      const includes = [
        {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'sentPersonPrice', 'isActivePartner', 'fixedCharge'],
          paranoid: false,
          required: true
        }, {
          model: models.User,
          as: 'creator',
          attributes: ['fullname'],
          required: false,
        },
        includePartner,
        includeCostumer,
        {
          model: models.Billings,
          as: 'billings',
          required: false,
          attributes: ['status', 'placeId']
        }, {
          model: models.ReservationSponsors,
          as: 'reservationSponsor',
          attributes: ['sponsorId', 'quantity', 'totalCommission'],
          required: false
        }
      ];

      let order;
      switch (sortBy) {
        case 'place':
          order = [['place', 'name', sortDir]];
          break;
        case 'partner':
          order = [[{model: models.Partner, as: 'partner'}, 'name', sortDir]];
          break;
        case 'creator':
          order = [['creator', 'fullname', sortDir]];
          break;
        case 'fullname':
          order = [[{model: models.Costumer, as: 'costumer'}, 'fullname', sortDir]];
          break;
        case 'phone':
          order = [['costumer', 'phone', sortDir]];
          break;
        case 'email':
          order = [['costumer', 'email', sortDir]];
          break;
        case 'date':
          order = [['date', sortDir], ['start', sortDir]];
          break;
        case 'origin':
          order = [['origin', sortDir]];
          break;
        default:
          order = [[sortBy, sortDir]];
          break;
      }
      const result = await models.Reservation.findAndCountAll({
        distinct: true,
        attributes: {
          include: [
            [
              models.Sequelize.literal("(SELECT ABS(SUM(points)) FROM `CustomerPoints` as CP , `BonusCodes` as BC WHERE CP.id = BC.customerPointId && BC.reservationId = Reservation.id)"),
              'bonusCodePoints'
            ]
          ]
        },
        include: includes,
        where: whereReservations,
        order,
        offset: (+pageIndex * +pageSize) || 0,
        limit: +pageSize || 10,
        subQuery: false
      });
      if (result) {
        let place;
        if (whereReservations.placeId) {
          place = await models.Place.findOne({
            attributes: ['id', 'name', 'fixedCharge', 'monthlyPlanPrice'],
            include: [
              {
                model: models.TypeCharge,
                as: 'typeCharges',
                ...typeChargesToApplyInRangeDate(dateStart, dateEnd)
              },
              {
                model: models.SponsorPlaces,
                as: 'sponsorsPlaces',
                include: [{
                  model: models.Sponsor,
                  as: 'sponsor'
                }]
              }
            ],
            where: {id: whereReservations.placeId}
          });
          result['place'] = place;
        }
        return res.json(result);
      }
    } catch (e) {
      HandleError(req, res, e);
    }
  });

/******************************** POST *******************************/

router.post('/notification/friend', async (req, res) => {
  try {
    const reservation = await models.Reservation.findByPk(req.body.reservationId, {
      attributes: ['id', 'date', 'datetime', 'start', 'placeId', 'status'],
      include: [{
        model: models.Place,
        as: 'place',
        attributes: ['id', 'name', 'address'],
      }, {
        model: models.Costumer,
        as: 'costumer',
        attributes: ['fullname']
      }]
    });

    if (reservation && req.body.phones?.length > 0 && reservation?.status !== 'CANCELLED') {
      reservation['hour'] = moment().startOf('day').add(reservation.start, 'minutes').format('HH:mm A');
      await moduleWhatsApp.sendFrinend(reservation, req.body.phones);
      return res.json(reservation);
    } else {
      return res.json({success: false})
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/:reservationId/sms', async (req, res) => {
  try {
    const reservation = await models.Reservation.findOne({
      attributes: ['id', 'creatorUserId'],
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: ['countryCode', 'phone']
      }],
      where: {
        id: req.params.reservationId,
        placeId: req.user.placeId
      }
    });
// send message
    const customerPhone = reservation.costumer.countryCode + reservation.costumer.phone;
    smsModule.sendSMS(customerPhone, req.body.message).then(status => {
      models.SmsPlaces.create({
        phone: customerPhone,
        message: req.body.message,
        userId: reservation.creatorUserId,
        placeId: req.user.placeId
      });
      return res.json({status: status});
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/checkoutLimit', async (req, res) => {
  try {
    let dateEnd = moment().endOf('month').format('YYYY-MM-DD');
    let dateStart = moment().startOf('month').format('YYYY-MM-DD');
    const data = await models.Reservation.findAll({
      where: {
        placeId: req.body.placeId,
        date: {
          [Op.between]: [dateStart, dateEnd]
        }
      },
      raw: true
    });
    let countMsm, countWhatsapp;
    countMsm = data.filter(elem => elem.notificationSms === 1);
    countWhatsapp = data.filter(elem => elem.notificationWhatsapp === 1);
    return res.json({
      msm: countMsm.length,
      whatsapp: countWhatsapp.length
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/checkoutAvailable', async (req, res) => {
  try {
    const available = await checkoutAvailable(req);
    return res.json(available);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/sponsor', async (req, res) => {
  try {
    let body = req.body;
    console.log(body);
    const time = moment().startOf('day').add(15, 'minutes').format('HH:mm');
    body.datetime = moment.tz(`${body?.type == 'CUTS' ? body?.cut?.dateEnd : body?.date} ${time}`, body.timezone).utc().format();
    const objReservation = {
      datetime: body.datetime,
      date: body?.type == 'CUTS' ? body?.cut?.dateEnd : body?.date,
      start: 15,
      end: 30,
      peopleCount: 0,
      peopleSeating: 0,
      smokeArea: 0,
      duration: 15,
      status: RESERVATION_STATUS.GONE,
      type: 'FREE',
      origin: 'WEBSITE',
      notificationEmail: 0,
      notificationSms: 0,
      notificationWhatsApp: 0,
      billingId: body?.type == 'CUTS' ? body?.cut?.id : null,
      costumerId: 506336, // usuario Reservandonos
      partnerId: 1,
      newCostumer: 0,
      placeId: body?.placeId,
      notes: 'Reserva desde el sistema',
      review: 1
    };

    console.log(objReservation)

    const reservation = await models.Reservation.create(objReservation);

    const reservationSponsors = await models.ReservationSponsors.create({
      reservationId: reservation.id,
      placeId: body?.placeId,
      sponsorId: body?.sponsorsPlaces?.sponsorId,
      quantity: body?.quantity,
      totalCommission: body?.sponsorsPlaces?.value * body?.quantity
    });
    res.json(reservationSponsors)
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/save-offline', async (req, res) => {
  try {
    const reservations = req.body.reservations;
    const customers = req.body.customers;
    for (let i = 0; i < reservations?.length; i++) {
      const reservation = reservations[i];
      const reservationId = reservation.id.toString();
      if (reservationId.includes('-')) { // reserva creada y actualizada offline
        const customer = customers.find(customer => customer.id == reservation.customerId);
        customer.id = null;
        const newCustomer = await models.Costumer.create(customer);
        reservation.costumerId = newCustomer.id;
        reservation.id = null;
        reservation.zoneId = reservation.zoneId == 0 ? null : reservation.zoneId;
        reservation.tableId = reservation.tableId == 0 ? null : reservation.tableId;
        await models.Reservation.create(reservation);
      } else { // reserva creada online pero actualizada offline
        await models.Reservation.update(reservation, {where: {id: reservation.id}})
      }
    }
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', paymentMiddleware.privateKey, async (req, res) => {
  const hosts = [
// 'http://reservandonos.com', // si redirecciona a https
// 'https://reservandonos.com',
    'http://sistema.reservandonos.com', // TODO redireccionar siempre a https
    'https://sistema.reservandonos.com',
    'http://bookercontrol.com',
    'https://bookercontrol.com',
    'http://testing.bookercontrol.com/',
    'https://testing.bookercontrol.com/',
    'http://localhost:4200',
    'http://localhost:4201'
  ];
  try {
    const role = req.user['role'] ? req.user['role'] : '';
    const placeId = req.user ? req.user.placeId : req.body.placeId;
    if (!placeId) return res.status(400).send('Missing place id');
    let customer = req.body.costumer;
    if (!customer) return res.status(400).send('Missing customer');
    const creatorUserId = req.user ? req.user.id : null; // ?
    const place = await models.Place.findOne({
      where: {id: placeId},
      include: ['cities', 'states', 'colony', 'typeFood']
    });
    if (!place) return res.status(400).send('Place not found');
    const originDomain = req.get('origin');
    if (await placeIsBlocked(place, req.body) && !hosts.includes(originDomain)) return res.status(409).send('El lugar no está tomando reservaciones por internet el día elegido. Por favor, selecciona otro día o comunícate vía telefónica.')

// si no está en hosts, no es desde booker
    if (!place.onlineSales && !hosts.includes(originDomain)) return res.status(409).send('El lugar no está tomando reservaciones por internet. Por favor, comunícate vía telefónica.')
    const widget = await checkoutAvailableWidgetFree(req);
    if (!widget.reservation) return res.status(409).send('El lugar se ha llenado. Por favor elija otra hora o fecha.')
    const checkout = await checkoutAvailable(req);
    if (!checkout.available) return res.status(409).send('La mesa asignada ha sido tomada, por favor intente nuevamente para y le asignaremos otra.') // Table taken

    const additionalCustomerFilters = [];
    if (customer.phone) {
      additionalCustomerFilters.push({countryCode: customer.countryCode}, {phone: customer.phone},)
    }
    if (customer.email) {
      additionalCustomerFilters.push({email: customer.email})
    }

    const dbCustomer = await models.Costumer.findOne({
      where: {
        placeId,
        [Op.or]: [{id: customer.id}, {[Op.and]: additionalCustomerFilters}]
      }
    });
    const isNewCustomer = dbCustomer ? false : true;

    if (dbCustomer) {
      customer.email = customer.email ? customer.email : dbCustomer.email;
      await dbCustomer.update(customer);
      customer.id = dbCustomer.id;
      customer.validEmail = !customer.email ? false : dbCustomer.validEmail;
      customer.emailComplaints = dbCustomer.emailComplaints;
    } else {
      customer.placeId = placeId;
      customer.validEmail = customer.email ? 1 : 0;
      customer['subscribedEmails'] = 1;
      customer = await models.Costumer.create(customer);
    }

    let askForCardInfo = false;
    const type = req.body.type;
    if (type != 'FREE' || req.body.advancePayment) { // TODO advancePayment tiene que ir en type
      let cardId = req.body.selectedCardId; // TODO sería mejor tener un campo extra para poner forma de registro de tarjeta...
      if (cardId == null) return res.status(400).send('Card data missing');
      if (cardId == 'send') {
        req.body.status = 'PENDING';
        req.body.paymentCardId = cardId;
        askForCardInfo = true;
        if (req.body.advancePayment) {
          req.body.advancePaymentId = req.body.adPayValue;
        }
      } else if (cardId == 'new') {
        if (!dbCustomer || !dbCustomer.paymentCustomerId) {
// create a conekta customer
          const paymentCustomer = await payment.createCustomer(customer, req.body.paymentTokenId, req.paymentGateway.privateKey);
          customer.paymentCustomerId = paymentCustomer.id;
          cardId = paymentCustomer.default_payment_source_id;
          let responseCustomer = await models.Costumer.findOne({where: {id: customer.id}});
          responseCustomer.paymentCustomerId = paymentCustomer.id;
          responseCustomer.save();
        } else {
// add payment method
          const paymentSource = await payment.createPaymentSource(customer.paymentCustomerId, req.body.paymentTokenId, req.paymentGateway.privateKey);
          cardId = paymentSource.id
        }
        if (req.body.type == "CANCELLATION_POLICY") {
          req.body.paymentCardId = cardId;
        }
        if (req.body.advancePayment) {
          const idPayment = req.body.costumer.paymentCustomerId ? req.body.costumer.paymentCustomerId : customer.paymentCustomerId;
          const order = await payment.createAdvanceCharge(req.paymentGateway.privateKey, cardId, req.body.adPayValue, idPayment);
          req.body.advancePaymentId = order.id;
        }
      } else {
        return res.status(400).send('Card data missing');
      }
    }
    req.body.costumerId = customer.id;
    req.body.placeId = placeId;
    if (!req.body.partnerId && place.planId === 1) {
      req.body.partnerId = 1;
    }
    req.body.place = place;
    const time = moment().startOf('day').add(req.body.start, 'minutes').format('HH:mm');
    req.body.datetime = moment.tz(`${req.body.date} ${time}`, place.timezone).utc().format();
    req.body.duration = req.body.duration == 0 ? 30 : req.body.duration;
    req.body.end = req.body.end == req.body.start ? req.body.start + 30 : req.body.end;
    req.body.creatorUserId = req.user ? req.user.id : null;
    req.body.newCostumer = isNewCustomer ? true : false;
    const reservation = await models.Reservation.create(req.body);
    moduleBitacora.createBitacora('CREATE', `[NEW] - Nueva Reservación`, 'Reservations', reservation.id, req.user ? req.user.id : null, reservation.placeId);

    if(reservation?.reservationIdDetour) {
      const rsv = await models.Reservation.findByPk(reservation?.reservationIdDetour);
      rsv.canceledBy = CANCELED_BY.CALLCENTER;
      rsv.status = RESERVATION_STATUS.CANCELLED;
      rsv.save();
      moduleBitacora.createBitacora('UPDATE', `[UPDATE] - RESERVA DESVIADA`, 'Reservations', reservation.reservationIdDetour, req.user ? req.user.id : null, reservation.placeId);
    }

    if (moment.tz(`${req.body.date} ${time}`, place.timezone).isAfter(moment().tz(place.timezone))) {
    // if (process.env.NODE_ENV === 'production') {
      // nuevo flujo reservandonos
      emailModule.newReservationEmail(reservation.id)
        .catch(e => HandleError(req, res, e)); // envía correo al restaurante
      if (place.typeAccount === ACCOUNT_TYPE.BOOKER) {
      } else if (place.typeAccount === ACCOUNT_TYPE.PARTNER) {
        moduleWhatsApp.sendWhatsAppNewReservation(place, reservation, customer, emailModule)
          .catch(e => HandleError(null, null, new InternalProcessError(e, {place, reservation, customer})));
      }

      try {
        if (reservation?.partnerId == 1) {
          ModuleZoho.registerCustomerB2C(customer, place, reservation?.origin);
        }
      } catch (e) {
        console.log(e)
      }

      if (isNewCustomer || customer.validEmail && customer.emailComplaints < 3) {
        if (askForCardInfo) {
          emailModule.cardInfoEmail(reservation, place, customer);
        } else if (req.body.advancePaymentId || type == "CANCELLATION_POLICY") {
          const amount = req.body.adPayValue ? req.body.adPayValue : reservation.peopleCount * place.cancellationFeePerPerson;
          emailModule.advPayCancPolEmail(reservation.id, amount);
        } else if ((req.body.notificationEmail !== false && req.body.notificationEmail !== 0) || (reservation.partnerId == 1)) {
          console.log(reservation.status);
          if (reservation.status == 'CONFIRMED' && reservation.partnerId != 1) { // TODO esta lógica se debe hacer en email.js
            emailModule.rawWithAttachment(reservation.id);
            const objReservation = reservation;
            objReservation.hour = reservation.start ? moment(reservation.date).add(reservation.start, 'minutes').format('H:mm') : '';
            objReservation.costumer = customer;
            objReservation.place = place;

            if (reservation?.partnerId == 1) {
              moduleWhatsApp.notificationClient(objReservation, reservation.status, false, true);
            }
          } else {
            emailModule.reservationEmail(reservation.id);
          }
        }
      } else {
        console.error('el correo no es válido')
      }
      const tempCustomer = req.body.customer ? req.body.customer : customer;
      if (req.body.notificationSms && tempCustomer) {
        sendSMS(reservation, req.polyglot, place.name, tempCustomer, role);
      }
    }


    seendMessageRegisteredCustomerStatus(reservation, place);

    Notifications.dispatchReservation(TYPE_NOTIFICATION_BY_RESERVATION.NEW_RES, reservation.id)
      .catch(e => HandleError(null, null, new InternalProcessError(e, {
        reservation: reservation.toJSON()
      })));


    return res.json(reservation);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/******************************** PUT *******************************/

router.put('/reservandonos/confirm/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    rsvConfirmed(token, req, res, false, false);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/reservandonos/confirm-force/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    rsvConfirmed(token, req, res, true, false);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/reservandonos/re-confirm-client/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    rsvConfirmed(token, req, res, true, true);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/reservandonos/cancel/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    rsvCancelation(token, req, res, false, false);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/reservandonos/cancel-force/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    rsvCancelation(token, req, res, true, false);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/reservandonos/cancel-client/:token', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    rsvCancelation(token, req, res, true, true);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/cancel/:id', async (req, res) => {
  try {
    const token = {reservationId: req.params.id};
    rsvCancelation(token, req, res, true, true);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:id/status', paymentMiddleware.privateKey, async (req, res) => {
  try {
    const {id} = req.params;
    const {
      status,
      commentForCancellation,
      peopleCount,
      peopleSeating,
      invoiceAmount,
      invoiceAmountTotal,
      commissionSponsors = []
    } = req.body;
    let update = {status};
    if (commentForCancellation) {
      update = {...update, commentForCancellation};
    }
    if (peopleCount) {
      update = {...update, peopleCount, peopleSeating};
    }
    if (invoiceAmount) {
      update = {...update, invoiceAmount, invoiceAmountTotal};
    }

    const reservation = await models.Reservation.findByPk(id, {
      attributes: ['id', 'date', 'type', 'costumerId', 'paymentCardId', 'placeId', 'peopleCount', 'tableId', 'groupId', 'status', 'placeId', 'partnerId']
    });
    if (reservation) {
        const customer = await models.Costumer.findOne({
          where: {id: reservation.costumerId},
          attributes: ['paymentCustomerId', 'email']
        });

        const place = await models.Place.findOne({
          where: {id: reservation.placeId},
          attributes: ['cancellationFeePerPerson', 'paymentGatewayPrivateKey', 'name', 'isSpecial']
        });
      if ([RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.NO_SHOW].includes(status) && reservation.type === 'CANCELLATION_POLICY') {
        if (reservation.paymentCardId && customer && customer.paymentCustomerId) {
          const order = await payment.createOrder(
            customer.paymentCustomerId,
            reservation.paymentCardId,
            place.paymentGatewayPrivateKey,
            reservation.id,
            place.cancellationFeePerPerson,
            reservation.peopleCount
          );
          if (order) {
            update['paymentOrderId'] = order.id;
            update['paymentChargeId'] = order.charges.data ? order.charges.data[0].id : null;
          }
        }
      }

      if (status === RESERVATION_STATUS.CANCELLED) {
        if ([ROLES.ADMIN, ROLES.TELEMARKETING].includes(req?.user?.role)) {
          update.canceledBy = CANCELED_BY.CALLCENTER;
        }

        if (ROLES.MANAGER === req?.user?.role) {
          update.canceledBy = CANCELED_BY.PARTNER;
        }
      }

      const updateResult = await models.Reservation.update(update, {where: {id: id}});
      const affectedCount = updateResult[0];

      if (update.status !== reservation.status && [RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(update.status) && reservation.partnerId === 1) {
        checkingPointsCustomer(customer.email, place?.isSpecial);
      }

      const statusesRequireEmail = [RESERVATION_STATUS.NO_SHOW, RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED];
      moduleBitacora.createBitacora('UPDATE', `[UPDATE] - Reservación ${reservation.status == status ? 'modificada desde el Sistema' : 'modificada desde el sistema de estado ' + reservation.status + ' por ' + status}`, 'Reservations', reservation.id, req.user ? req.user.id : null, reservation.placeId);
      if (affectedCount) {

        const notificationType = status === RESERVATION_STATUS.CANCELLED
          ? TYPE_NOTIFICATION_BY_RESERVATION.CAN_RES
          : TYPE_NOTIFICATION_BY_RESERVATION.UPD_RES;
        Notifications.dispatchReservation(notificationType, reservation.id)
          .catch(e => HandleError(null, null, new InternalProcessError(e)));


        const placeReser = await models.Place.findOne({where: {id: reservation.placeId}})
        if (placeReser)
          seendMessageRegisteredCustomerStatus(reservation, placeReser).catch(e => InternalProcessError(e, {
            reservation: reservation?.dataValues,
            placeReser: placeReser?.dataValues
          }));


        if (statusesRequireEmail.includes(req.body.status)) {
          emailModule.reservationEmail(id);
        }

        if (commissionSponsors.length) {
          for (const {sponsorId, value, quantity, commission: totalCommission} of commissionSponsors) {
            try {
              const rs = await models.ReservationSponsors.findOne({
                where: {
                  [Op.and]: [
                    {reservationId: reservation.dataValues.id},
                    {sponsorId: sponsorId}
                  ]
                }
              });
              if (rs) {
                await rs.update({quantity, totalCommission});
              } else {
                await models.ReservationSponsors.create({
                  reservationId: reservation.dataValues.id,
                  placeId: reservation.dataValues.placeId,
                  sponsorId,
                  quantity,
                  totalCommission
                });
              }
            } catch (e) {
              console.log('Error al registrar las comisiones por sponsor');
              console.log(sponsorId, value, quantity, totalCommission);
              console.log(e)
            }
          }
        }
      }
      return res.json(affectedCount);
    } else {
      return res.status(404).json({message: 'Reservation not found'});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:id/add-card', async (req, res) => {
  try {
    const reservation = await models.Reservation.findByPk(req.params.id, {
      include: ['costumer', {
        model: models.Place,
        as: 'place',
        attributes: ['paymentGatewayPrivateKey', 'email', 'cancellationFeePerPerson']
      }]
    });
    let cardId = null;
    let amount = null;
    if (!reservation.costumer.paymentCustomerId) { // create a customer
      const paymentCostumer = await payment.createCustomer(reservation.costumer, req.body.paymentTokenId, reservation.place.paymentGatewayPrivateKey);
      reservation.costumer.paymentCustomerId = paymentCostumer.id;
      if (reservation.type !== 'FREE') {
        reservation.paymentCardId = paymentCostumer.default_payment_source_id;
      }
      cardId = paymentCostumer.default_payment_source_id;
// console.log('cardId', cardId);
      await reservation.costumer.save();
    } else { // add payment method
// TODO ¿hay manera de ver los datos de la tarjeta antes de guardarla?
      const paymentSource = await payment.createPaymentSource(reservation.costumer.paymentCustomerId, req.body.paymentTokenId, reservation.place.paymentGatewayPrivateKey);
// check if the card is repeated
// let hasRepeated = true;
// while(hasRepeated) {
//   const customer = await payment.customer.get(reservation.costumer.paymentCustomerId, reservation.place.paymentGatewayPrivateKey);
//   const numberOfCards = customer.toObject().payment_sources.total;
//   if (numberOfCards < 2) {
//     hasRepeated = false;
//     break;
//   }
//   let del = null;
//   for (let i = 1; i < numberOfCards; i++) {
//     if (paymentSource.last4 == customer.toObject().payment_sources.data[i].last4 && paymentSource.exp_month == customer.toObject().payment_sources.data[i].exp_month && paymentSource.exp_year == customer.toObject().payment_sources.data[i].exp_year && paymentSource.brand == customer.toObject().payment_sources.data[i].brand) {
//       del = await payment.customer.deleteCard(customer, i);
//       // update reservations made with deletedCard
//       // TODO no habría que hacer esto si pudieramos checar la tarjeta antes de guardarla
//       const reservations = await models.Reservation.findAndCountAll({ where: { paymentCardId: customer.toObject().payment_sources.data[i].id }, attributes: ['id'] });
//       for(let j = 0; j < reservations.count; j++) {
//         let reservation = reservations.rows[j];
//         reservation.paymentCardId = paymentSource.id;
//         await reservation.save();
//       }
//       break;
//     } else {
//     }
//   };
//   hasRepeated = del ? true: false;
// }
      if (reservation.type !== 'FREE') {
        reservation.paymentCardId = paymentSource.id;
      }
      cardId = paymentSource.id;
    }

    if (reservation.advancePaymentId != null) {
      if (!isNaN(reservation.advancePaymentId)) {
        amount = reservation.advancePaymentId;
        const order = await payment.createAdvanceCharge(reservation.place.paymentGatewayPrivateKey, cardId, parseInt(reservation.advancePaymentId), reservation.costumer.paymentCustomerId);
// console.log(order);
        reservation.advancePaymentId = order.id;
        reservation.paymentCardId = null;
      } else {
        res.status(400).send("Hay un error con la cantidad a pagar, no se hizo la reservación");
        return;
      }
    } else {
      amount = reservation.peopleCount * reservation.place.cancellationFeePerPerson
    }

    reservation.status = 'CONFIRMED';
    await reservation.save();
    if (
// process.env.NODE_ENV === 'production' &&
      reservation.costumer && reservation.costumer.email
      && reservation.costumer.validEmail && reservation.costumer.emailComplaints < 3
    ) {
      emailModule.advPayCancPolEmail(reservation.id, amount);
    }
// send notification
    if (req.user && (req.user.placeId == reservation.placeId || req.user.role == 'ADMIN' || req.user.role == 'TELEMARKETING')) {
      Notifications.dispatchReservation(type, reservation.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
    }

    return res.json({success: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/:token/reconfirm', async (req, res) => {
  try {
    const token = JSON.parse(encryption.decrypt(req.params.token));
    if (token) {
      const reservation = await models.Reservation.findByPk(token.reservationId, {
        attributes: ['id', 'date', 'start', 'peopleCount', 'smokeArea', 'status', 'placeId'],
        include: [{
          model: models.Partner,
          as: 'partner',
          attributes: ['logoImage', 'phone']
        }, {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'logoImage', 'phone'],
        }, {
          model: models.Costumer,
          as: 'costumer',
          attributes: ['email', 'validEmail', 'emailComplaints']
        }]
      });
      if (reservation) {
        if (reservation.status === RESERVATION_STATUS.CONFIRMED) {
          reservation.status = (req.body.action === 'confirm' || req.body.action == 'RE_CONFIRMED') ? 'RE_CONFIRMED' : req.body.action === 'cancel' ? 'CANCELLED' : 'CONFIRMED';
          await reservation.save();
          if (process.env.NODE_ENV === 'production' && reservation.costumer && reservation.costumer.email && reservation.costumer.validEmail && reservation.costumer.emailComplaints < 3) {
            emailModule.reservationEmail(reservation.id);
          }
        }

        let description;
        let notificationType = '';
        if (reservation.status === 'CANCELLED') {
          notificationType = TYPE_NOTIFICATION_BY_RESERVATION.CAN_RES;
          description = '[UPDATE] - Reservación cancelada por el cliente';
        } else {
          notificationType = TYPE_NOTIFICATION_BY_RESERVATION.UPD_RES;
          description = '[UPDATE] - Reservación Re-Confirmada por el cliente';
        }
        moduleBitacora.createBitacora('UPDATE', description, 'Reservations', reservation.id, null, reservation.placeId);

        Notifications.dispatchReservation(notificationType, reservation.id)
          .catch(e => HandleError(null, null, new InternalProcessError(e)));

        return res.json(reservation);
      } else {
        return res.status(400).send('No existe la reserva') // TODO traducción
      }
    } else {
      return res.sendStatus(522);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/change-table', async (req, res) => {
  try {
    const newTableId = req.body.newTableId;
    const wasGroup = req.body.wasGroup;
    const newGroupId = req.body.newGroupId;
    const newZoneId = req.body.newZoneId;
    let update = {};
    if (newZoneId) {
      update.zoneId = newZoneId;
    }
    if (wasGroup) {
      update.groupId = null;
    }
    if (newGroupId) {
      update.isGroup = 1;
      update.groupId = newGroupId;
    } else if (newTableId) {
      update.isGroup = 0;
      update.tableId = newTableId;
    } else {
      res.status(400).send('Falta mesa/grupo');
      return;
    }
    const trans = await models.Reservation.update(update, {where: {id: req.body.reservationId}});
    if (trans) {
      Notifications.dispatchReservation(TYPE_NOTIFICATION_BY_RESERVATION.UPD_RES, req.body.reservationId)
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
      return res.json({update: true});
    } else {
      return res.json({update: false});
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', paymentMiddleware.privateKey, async (req, res) => {
  try {
    let reservation = await models.Reservation.findOne({
      include: [
        {
          model: models.Place,
          as: 'place',
          attributes: ['cancellationFeePerPerson', 'timezone', 'logoImage', 'name', 'address', 'website', 'facebook', 'instagram', 'twitter', 'isSpecial'],
          include: [
            {
              model: models.TypeCharge,
              as: 'typeCharges',
              attributes: ['id', 'description', 'type', 'value', 'origin']
            },
            {
              model: models.SponsorPlaces,
              as: 'sponsorsPlaces',
              include: [
                {
                  model: models.Sponsor,
                  as: 'sponsor'
                }
              ]
            }
          ]
        },
        {
          model: models.Partner,
          as: 'partner',
          attributes: ['logoImage']
        },
        {
          model: models.ReservationSponsors,
          as: 'reservationSponsor'
        }
      ],
      where: {
        id: req.body.id,
      }
    });
    if (!reservation) return res.status(404).send('No se pudo actualizar la reserva');
    const initialStatus = reservation.status;
    const newStatus = req.body.status;
    req.body.partnerId = req.body.partnerId ?? reservation.partnerId;
    const isSpecial = reservation?.place?.isSpecial;

    if (newStatus === RESERVATION_STATUS.CANCELLED) {
      reservation.canceledBy = CANCELED_BY.CALLCENTER;
    }

    moduleBitacora.createBitacora('UPDATE', `[UPDATE] - Reservación ${initialStatus == newStatus ? 'modificada desde el Sistema' : 'modificada desde el sistema de estado ' + initialStatus + ' por ' + newStatus}`, 'Reservations', reservation.id, req.user ? req.user.id : null, reservation.placeId);
    let costumer = await models.Costumer.findOne({
      where: {
        id: req.body.costumer.id,
        placeId: req.user.placeId
      }
    });
    let cardId = req.body.selectedCardId;
    if (req.body.type == "CANCELLATION_POLICY") {
      if (cardId == 'send') {
        emailModule.cardInfoEmail(reservation, reservation.place, costumer);
      } else if (cardId == 'new') {
        if (costumer == null || costumer.paymentCustomerId == null) {
// create a conekta customer
          paymentCostumer = await payment.createCustomer(req.body.costumer, req.body.paymentTokenId, req.paymentGateway.privateKey);
          req.body.costumer.paymentCustomerId = paymentCostumer.id;
          cardId = paymentCostumer.default_payment_source_id;
        } else {
// add payment method
          const paymentSource = await payment.createPaymentSource(costumer.paymentCustomerId, req.body.paymentTokenId, req.paymentGateway.privateKey);
          cardId = paymentSource.id
        }
      }
      if (cardId == null) {
        res.status(400).send(req.polyglot.t('emptyCard'));
        return;
      }
      req.body.paymentCardId = cardId;
    }

    if (costumer) {
      if (req.body.costumer.email != costumer.email) {
        req.body.costumer.validEmail = 1;
        req.body.costumer.emailComplaints = 0;
      }
      costumer = await costumer.update(req.body.costumer);
    } else {
      req.body.costumer.id = null; // esto hará que se duplique el Costumer, pero con un placeId diferente // TODO un cliente debe poder tener muchos lugares
      req.body.costumer.placeId = req.user.placeId;
      req.body.costumer['subscribedEmails'] = 1;
      costumer = await models.Costumer.create(req.body.costumer);
    }
    req.body.costumerId = costumer.id;
    req.body.costumer.paymentCustomerId = costumer.paymentCustomerId;

    /*if (!reservation.paymentChargeId && req.body.type && req.body.type !== 'FREE' && req.body.status === 'NO_SHOW' && reservation.status != 'PENDING') { // cobrar no show
      const order = await payment.createOrder(req.body.costumer.paymentCustomerId, reservation.paymentCardId, req.paymentGateway.privateKey, reservation.id, reservation.place.cancellationFeePerPerson, reservation.peopleCount);
      req.body.paymentOrderId = order.id;
      req.body.paymentChargeId = order.charges.data ? order.charges.data[0].id : null;
    }*/
    const time = moment().startOf('day').add(req.body.start, 'minutes').format('HH:mm');
    req.body.datetime = moment.tz(`${req.body.date} ${time}`, reservation.place.timezone).utc().format();
    req.body.placeId = req.user.placeId;

    if (req.body.tableId == 0) {
      req.body.tableId = null;
    }
    if (req.body.zoneId == 0) {
      req.body.zoneId = null;
    }
    let assignPoints = !reservation.isValidImg && req.body.isValidImg;
    Object.assign(reservation, req.body);
    reservation = await reservation.save();

    if (newStatus !== initialStatus && [RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(newStatus) && req.body.partnerId == 1) {
      checkingPointsCustomer(costumer.email, isSpecial);
    }

    if (assignPoints) { //placeId = 690 y acumula 100 puntos mas "CONFIRMED"
      await createNotification({
        registeredCustomerId: req.body.registeredCustomerId,
        message: "Has subido tu foto a Redes Sociales y la hemos validado."
      });
      await pointsModule.savePointOfMovement(true, req.body.registeredCustomerId, POINTS.UPLOAD_IMA_RED_SOCIAL);
    }
    const place = await models.Place.findByPk(req.body.placeId, {
      attributes: ['planId', 'timezone', 'name']
    });

    if (
      costumer.email && costumer.validEmail && costumer.emailComplaints < 3
      && costumer.subscribedEmails
    ) {
      const isUpdated = reservation.status != 'CANCELLED';
      emailModule.reservationEmail(reservation.id, isUpdated); // correo al cliente de que se actualizó su reserva
    }
    if (reservation.notificationSms && process.env.NODE_ENV === 'production') {
      sendSMS(reservation, req.polyglot, place.name, costumer);
    }

    await models.Reservation.update(reservation.dataValues, {where: {id: req.body.id}});

    seendMessageRegisteredCustomerStatus(reservation, place);

    const notificationType = newStatus === 'CANCELLED'
      ? TYPE_NOTIFICATION_BY_RESERVATION.CAN_RES
      : TYPE_NOTIFICATION_BY_RESERVATION.UPD_RES;

    Notifications.dispatchReservation(notificationType, reservation.id)
      .catch(e => HandleError(null, null, new InternalProcessError(e, {
        reservation: reservation.toJSON()
      })));

    return res.json(reservation);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/updateTime', async (req, res) => {
  try {
    const response = await helpGoogle.updateReservation(req.body);
    return res.json(response);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/******************************** DELETE *******************************/

router.delete('/:id', async (req, res) => {
  try {
    await models.Reservation.destroy({where: {id: req.params.id}});
    return res.json({response: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
