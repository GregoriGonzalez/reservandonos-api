const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const encryption = require('../helpers/encryption');
const {emailModule} = require('../helpers/email.helper');
const {divide, sum} = require("../helpers/calculate.helper");
const {HandleError} = require("../helpers/error.helper");
const {checkReservationsToRegisterCustomerByReference, savePointOfMovement} = require("../helpers/points");
const {POINTS} = require("../helpers/constants.helper");

router.get('/', async (req, res) => {
  try {
    let order;
    switch (req.query.sortBy) {
      case 'fullname':
        order = [[['reservation', 'costumer'], 'fullname', req.query.sortDir]];
        break;
      case 'phone':
        order = [[['reservation', 'costumer'], 'phone', req.query.sortDir]];
        break;
      case 'email':
        order = [[['reservation', 'costumer'], 'email', req.query.sortDir]];
        break;
      case 'date':
        order = [['reservation', 'date', req.query.sortDir], ['reservation', 'start', req.query.sortDir]];
        break;
      default:
        order = [[req.query.sortBy, req.query.sortDir]];
        break;
    }
    const reviews = await models.Review.findAndCountAll({
      attributes: ['id', 'score', 'comment'],
      include: [
        {
          model: models.Reservation,
          as: 'reservation',
          attributes: ['date', 'start'],
          required: true,
          include: [
            {
              model: models.Costumer,
              as: 'costumer',
              required: true,
              attributes: ['fullname', 'countryCode', 'phone', 'email'],
              where: {
                [Op.or]: [
                  {fullname: {[Op.like]: `%${req.query.term}%`}},
                  {email: {[Op.like]: `%${req.query.term}%`}},
                  {phone: {[Op.like]: `%${req.query.term}%`}}
                ]
              }
            }
          ]
        },
        {
          as: 'place',
          model: models.Place,
          attributes: ['id', 'name']
        }
      ],
      where: {
        placeId: req.user.placeId
      },
      order: order,
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(reviews);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/sends', async (req, res) => {
  try {
    return res.json(await emailModule.reviewEmail(false));
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/stars/:placeId', async (req, res) => {
  try {
    const {placeId} = req.params;
    const result = await models.Review.findAll({
      attributes: [
        'score', [models.Sequelize.fn('count', models.sequelize.col('score')), 'total'], [models.Sequelize.fn('sum', models.sequelize.col('score')), 'stars']
      ],
      where: {placeId},
      group: 'score',
      raw: true
    });

    let totalStars = 0;
    let totalOpinions = 0;
    for (const row of result) {
      totalStars = await sum(1, totalStars, row.stars);
      totalOpinions = await sum(0, totalOpinions, row.total);
    }
    const averageStars = await divide(1, totalStars, totalOpinions);

    return res.json({
      error: false,
      stars: averageStars,
      totalStars,
      totalOpinions,
      resume: result
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:placeId/all', async (req, res) => {
  try {
    const placeId = req.params.placeId;
    const place = await models.Place.findOne({where: {id: placeId}, attributes: ['name']});
    const reviews = await models.Review.findAndCountAll({
      where: {placeId},
      attributes: ['score', 'comment', 'createdAt'],
      include: {
        model: models.Reservation,
        as: 'reservation',
        attributes: ['date'],
        include: {model: models.Costumer, as: 'costumer', attributes: ['fullname']}
      }
    });
    return res.json({place: place, reviews: reviews});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res) => {
  try {
    await models.Review.create(req.body.params ? req.body.params : req.body);
    const reservationId = req.body.params ? req.body?.params?.reservationId : req.body.reservationId;
    await models.Reservation.update({reviewed: true}, {where: {id: reservationId}});
    const reservation = await models.Reservation.findByPk(reservationId, {
      attributes: ['costumerId'],
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: ['id', 'email', 'fullname']
      }]
    });

    const customer = await checkReservationsToRegisterCustomerByReference(reservation.costumer?.email);

    if (customer) {
      await savePointOfMovement(true, customer.customer.id, POINTS.REVIEW);
    }
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/:data/reviewed', async (req, res) => {
  try {
    const data = JSON.parse(encryption.decrypt(req.params.data));
    const reservation = await models.Reservation.findByPk(data.reservationId, {
      attributes: ['id', 'placeId', 'reviewed']
    });
    if (reservation.reviewed) {
      return res.json({
        message: 'This reservation already has a review!'
      });
    }
    reservation.reviewed = true;
    await reservation.save();
    const review = await models.Review.create({
      score: req.body.score,
      comment: req.body.comment,
      reservationId: reservation.id,
      placeId: reservation.placeId
    });
    return res.json(review);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
