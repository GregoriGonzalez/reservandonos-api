const express = require('express');
const router = express.Router();
const moment = require('moment');
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");
const Redis = require("../redis");
const Op = models.Sequelize.Op;

router.get('/', async (req, res) => {
  try {
    const data = await Redis.getRegister(`sellers`);
    if (data) {
      console.log('REDIS-sellers');
      return res.json(data);
    }
    const sellers = await models.Seller.findAll();
    Redis.setRegister(sellers, `sellers`);
    return res.json(sellers);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
