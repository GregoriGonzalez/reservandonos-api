const express = require('express');
const moment = require('moment');
const router = express.Router();
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");
const Op = models.Sequelize.Op;
const serviceHelper = require('../helpers/services.helper');

async function availableCheck(services, date, start, end, persons) {
  const response = await models.ServiceZone.findAll({
    where: {serviceId: services, layoutId: {[Op.not]: null}},
    attributes: ['smokeArea', 'layoutId', 'zoneId', 'serviceId'],
    include: [
      {
        model: models.Layout, as: 'layout', attributes: ['id'],
        include: [
          {
            model: models.Table,
            as: 'tables',
            where: {onlineSales: true},
            attributes: ['id', 'number', 'onlineSales', 'minCapacity', 'maxCapacity', 'tableTypeId'],
            include: [
              {
                model: models.Reservation,
                as: 'reservations',
                required: false,
                attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'tableId'],
                where: {
                  date: date,
                  serviceId: services,
                  [Op.or]: [{start: {[Op.lte]: start}, end: {[Op.gte]: start}}, {
                    start: {[Op.lte]: end},
                    end: {[Op.gte]: end}
                  }]
                }
              }
            ]
          },
          {
            model: models.Group, as: 'groups', attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity'],
            include: [
              {
                model: models.Reservation,
                as: 'reservations',
                required: false,
                attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'groupId'],
                where: {
                  date: date,
                  serviceId: services,
                  [Op.or]: [{start: {[Op.lte]: start}, end: {[Op.gte]: start}}, {
                    start: {[Op.lte]: end},
                    end: {[Op.gte]: end}
                  }]
                }
              }
            ]
          }
        ]
      },
    ]
  });
  let tables = [];
  for (var item of response) {
    if (item.layout) {
      for (var table of item.layout.tables) {
        console.log('--------', persons, '<=', table.maxCapacity);
        if (table.reservations.length == 0 && persons <= table.maxCapacity) {
          tables.push({
            id: table.id,
            number: table.number,
            maxCapacity: table.maxCapacity,
            zoneId: item.zoneId,
            serviceId: item.serviceId,
            isGroup: false
          });
        }
      }
    }
  }
  return tables;
}

async function findSpecificReservations(placeId, reservationId, date, start, end) {
  let whereReservation;
  whereReservation = {
    placeId: placeId,
    date: date,
    status: {[Op.notIn]: ['NO_SHOW', 'CANCELLED', 'WAIT_LIST', 'WAIT_LIST_LOCAL', 'GONE']},
    [Op.or]: {
      start: {
        [Op.gt]: start,
        [Op.lt]: end
      },
      end: {
        [Op.gt]: start,
        [Op.lt]: end
      },
      [Op.and]: {
        start: {[Op.lte]: start},
        end: {[Op.gt]: start}
      }
    },
  }
  if (reservationId) {
    whereReservation['id'] = {[Op.ne]: reservationId};
  }
  return models.Reservation.findAll({
    include: [
      {
        model: models.Table,
        as: 'table',
        attributes: ['id', 'deletedAt'],
        where: {deletedAt: {[Op.is]: null}}, // ¿esto no es necesario o si?,
        required: false,
      },
      {
        model: models.Group,
        as: 'group',
        attributes: ['id'],
        required: false,
        // where: { deletedAt: { [Op.is]: null } },
        include: [{
          model: models.Table,
          as: 'tables',
          attributes: ['id', 'deletedAt'],
          // where: { deletedAt: { [Op.is]: null } }, // ??
        }]
      }
    ],
    where: whereReservation
  });
}

async function findSpecificServiceZones(serviceId, zoneId, smokeArea, reservedTablesIds, peopleCount, tableTypeId, tableHeightId, onlyOnlineSales, reservedGroupsIds) {
  const where = {
    serviceId,
    layoutId: {
      [Op.not]: null
    }
  };
  if (zoneId && !['null', 'undefined'].includes(zoneId)) {
    where.zoneId = zoneId;
  }
  if (smokeArea && !['null', 'undefined'].includes(smokeArea)) {
    where.smokeArea = smokeArea === 'true';
  }
  let tableWhere = {
    id: {
      [Op.notIn]: reservedTablesIds
    },
    maxCapacity: {
      [Op.gte]: peopleCount
    },
    minCapacity: {
      [Op.lte]: peopleCount
    }
  };
  const groupWhere = {
    id: {
      [Op.notIn]: reservedGroupsIds
    },
    maxCapacity: {
      [Op.gte]: peopleCount
    },
  }
  if (onlyOnlineSales) {
    tableWhere['onlineSales'] = true;
    groupWhere['onlineSales'] = true;
  }
  if (tableTypeId && tableTypeId != '0') {
    tableWhere.tableTypeId = tableTypeId;
  }
  if (tableHeightId && tableHeightId != '0') {
    tableWhere.tableHeightId = tableHeightId;
  }
  return await models.ServiceZone.findAll({
    where: where,
    attributes: ['smokeArea', 'zoneId', 'layoutId'],
    include: [
      {
        model: models.Zone,
        as: 'zone',
        attributes: ['id', 'name']
      },
      {
        model: models.Layout,
        as: 'layout',
        attributes: ['id'],
        include: [
          {
            model: models.Table,
            as: 'tables',
            attributes: ['id', 'number', 'minCapacity', 'maxCapacity'],
            where: tableWhere,
            required: false
          },
          {
            model: models.Group,
            as: 'groups',
            attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity'],
            include: [{
              model: models.Table,
              as: 'tables',
              attributes: ['id', 'tableTypeId', 'tableHeightId']
            }],
            where: groupWhere,
            required: false
          }
        ]
      }
    ]
  });
}

router.get('/', async (req, res) => {
  try {
    const result = await models.Service.findAndCountAll({
      where: {
        placeId: req.user.placeId,
        name: {
          [Op.like]: `%${req.query.term}%`
        }
      },
      order: [
        [req.query.sortBy, req.query.sortDir]
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/get-all/:date', async (req, res) => {
  try {
    const placeId = req.user.placeId;
    const weekday = moment(req.params.date).format('dddd').toLowerCase();
    const services = await models.Service.findAll({ // TODO hay que separar la estructura del restaurante (servicios, zonas y layouts) de las reservas :/
      where: {placeId, weekdays: {[Op.like]: `%${weekday}%`}},
      attributes: ['id', 'name', 'start', 'end'],
      include: [
        {
          model: models.Zone, as: 'zones', attributes: ['id', 'name', 'coordX', 'coordY'], required: true, through: {
            model: models.ServiceZone,
            as: 'ServiceZone',
            where: {layoutId: {[Op.not]: null}},
            attributes: ['smokeArea', 'layoutId']
          }, include: [
            {
              model: models.Layout, as: 'layouts', attributes: ['id'], include: [
                {
                  model: models.Table,
                  as: 'tables',
                  attributes: ['id', 'number', 'onlineSales', 'minCapacity', 'maxCapacity', 'coordX', 'coordY', 'tableHeightId', 'tableTypeId', 'isInclined', 'isVertical'],
                  include: [
                    {
                      model: models.Reservation,
                      as: 'reservations',
                      required: false,
                      attributes: ['id', 'datetime', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin'],
                      where: {date: req.params.date, serviceId: {[Op.col]: 'Service.id'}},
                      include: [
                        {model: models.Costumer, as: 'costumer', attributes: ['fullname', 'email', 'phone']}
                      ]
                    }
                  ]
                },
                {
                  model: models.Group,
                  as: 'groups',
                  attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity'],
                  include: [
                    {
                      model: models.Reservation,
                      as: 'reservations',
                      required: false,
                      attributes: ['id', 'datetime', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin'],
                      where: {date: req.params.date, serviceId: {[Op.col]: 'Service.id'}},
                      include: [
                        {model: models.Costumer, as: 'costumer', attributes: ['fullname', 'email', 'phone']}
                      ]
                    }
                  ]
                }
              ]
            },
          ]
        },
        // { model: models.Reservation, as: 'reservations', where: { date: req.params.date, status: { [Op.or]: ['WAIT_LIST', 'WAIT_LIST_LOCAL', 'GOOGLE'] } }, attributes: ['id', 'datetime', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId'], include: [
        //   { model: models.Costumer, as: 'costumer', attributes: ['fullname', 'email', 'phone'] }
        // ], required: false },
      ],
      order: [['name', 'asc'], ['zones', 'name', 'asc'], ['zones', 'layouts', 'tables', 'number', 'asc']]
    });
    const reservationsWithIncompleteData = await models.Reservation.findAll({
      where: {
        placeId,
        date: req.params.date,
        [Op.or]: {
          serviceId: null,
          zoneId: null,
          [Op.and]: {isGroup: 0, tableId: null},
          [Op.and]: {isGroup: 1, groupId: null}
        }
      },
      attributes: ['id', 'datetime', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin'],
      include: [{model: models.Costumer, as: 'costumer', attributes: ['fullname', 'email', 'phone']}]
    });
    const servicesAndIncompleteReservations = {
      services,
      reservations: reservationsWithIncompleteData
    }
    return res.json(servicesAndIncompleteReservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-date/:date', async (req, res) => {
  try {
    const weekday = moment(req.params.date).format('dddd').toLowerCase();
    let services = await models.Service.findAll({
      include: ['zones'],
      where: {
        placeId: req.user.placeId,
        [Op.or]: [
          {
            date: req.params.date
          },
          {
            weekdays: {
              [Op.like]: `%${weekday}%`
            }
          }
        ]
      },
      order: [['start', 'asc']],
    });
    const noRepeatServices = services.filter(s => s.repetition === 'NEVER');
    if (noRepeatServices.length > 0) {
      const repeatWeeklyServices = services.filter(s => s.repetition === 'WEEKLY');
      if (repeatWeeklyServices.length > 0) {
        const noCrashRepeatWeeklyServices = repeatWeeklyServices.filter(rws => {
          let crash = false;
          noRepeatServices.forEach(nrs => {
            if ((rws.start >= nrs.start && rws.start <= nrs.end)
              || (rws.end >= nrs.start && rws.end <= nrs.end)
              || (rws.start <= nrs.start && rws.end >= nrs.end)
            ) {
              crash = true;
            }
          });
          return !crash;
        });
        services = [...noRepeatServices, ...noCrashRepeatWeeklyServices].sort((a, b) => a.start - b.start);
      }
    }
    return res.json(services);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const result = await models.Service.findOne({
      include: ['zones'],
      where: {
        id: req.params.id,
        placeId: req.user.placeId
      },
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/full/:serviceId', async (req, res) => {
  try {
    const result = await models.ServiceZone.findAll({
      include: ['zone', {
        model: models.Layout,
        as: 'layout',
        paranoid: false,
        include: [{
          model: models.Table,
          as: 'tables',
          separate: true,
          order: [['number', 'asc']]
        }]
      }],
      where: {
        serviceId: req.params.serviceId,
        layoutId: {
          [Op.not]: null
        }
        // placeId: req.user.placeId
      },
      order: [[{model: models.Zone, as: 'zone'}, 'name', 'asc']]
    });
    // TODO ¿se podrá hacer en el query?
    const noDelLayout = result.filter(sz => sz.layout.deletedAt == null);
    return res.json(noDelLayout);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:serviceId/zones', async (req, res) => {
  try {
    const result = await models.ServiceZone.findAll({
      attributes: ['smokeArea', 'layoutId'],
      include: [
        {
          model: models.Zone,
          as: 'zone',
          attributes: ['id', 'name']
        },
        {
          model: models.Layout,
          as: 'layout',
          attributes: ['deletedAt'],
          paranoid: false
        }
      ],
      where: {
        serviceId: req.params.serviceId,
        layoutId: {
          [Op.not]: null
        },
        // placeId: req.user.placeId
      },
      order: [['zone', 'name', 'asc']]
    });
    const sZNoNullLayout = result.filter(sz => sz.layout.deletedAt == null);
    return res.json(sZNoNullLayout);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:placeId/suggestions', async (req, res) => {
  try {
    // firstMinute
    let services = [0];
    if (req.query.isServices) {
      const serviceZones = await models.Service.findAll({
        where: {placeId: req.params.placeId},
        attributes: ['id']
      });
      if (serviceZones) {
        services = [];
        for (var item of serviceZones) {
          services.push(item.id);
        }
      }
    }
    let availables = [];
    let day = [];
    let diff = Number(req.query.start) - Number(req.query.firstMinute);
    let initial = diff > 0 && diff <= 50 ? Number(req.query.firstMinute) : Number(req.query.start);
    let end = diff > 0 && diff <= 50 ? Number(req.query.end) - Number(req.query.start) + initial : Number(req.query.end);
    let search = diff > 0 && diff <= 50 ? 20 : 14;
    let interalWidget = req.query.intervalWidget ? req.query.intervalWidget : 1;
    for (let i = 0; i < search; i++) {
      let interval = ((15 * interalWidget) * i);
      if (req.query.minute > initial + interval) {
        const placeBlock = await models.PlaceLock.count({
          where: {
            placeId: req.params.placeId, [Op.or]: [
              {date: req.query.date},
              {
                [Op.and]: [
                  {date: {[Op.lte]: req.query.date}},
                  {dateEnd: {[Op.gte]: req.query.date}}
                ]
              }
            ], start: {[Op.lte]: initial + interval}, end: {[Op.gte]: initial + interval}
          }
        });
        if (!placeBlock) {
          let tables = [];
          if (req.query.isServices) {
            tables = await availableCheck(services, req.query.date, initial + interval, end + interval, req.query.persons)
          }
          if (tables.length > 0 || !req.query.isServices) {
            day.push({
              date: req.query.date,
              start: initial + interval,
              persons: req.query.persons,
              day: req.query.date
            });
          }
        }
      }
    }
    availables.push({date: req.query.date, hours: day, isToday: true});
    for (let i = 1; i < 5; i++) {
      let date = moment(req.query.date).add(i, 'day').format('YYYY-MM-DD');
      const placeBlock = await models.PlaceLock.count({
        where: {
          placeId: req.params.placeId, [Op.or]: [
            {date: date},
            {
              [Op.and]: [
                {date: {[Op.lte]: date}},
                {dateEnd: {[Op.gte]: date}}
              ]
            }
          ], start: {[Op.lte]: req.query.start}, end: {[Op.gte]: req.query.start}
        }
      });
      if (!placeBlock) {
        let tables = [];
        if (req.query.isServices) {
          tables = await availableCheck(services, date, Number(req.query.start), Number(req.query.end) + 60, req.query.persons)
        }
        if (tables.length > 0 || !req.query.isServices) {
          availables.push({
            date: date,
            start: Number(req.query.start),
            persons: req.query.persons,
            day: date,
            isToday: false
          });
        }
      }
    }
    return res.json({suggestions: availables});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:serviceId/smart-tables', async (req, res, next) => {
  // revisar bloqueos
  // buscar servicio y traer sus layouts
  // buscar las mesas y grupos de esos layouts
  // filtrar segun la zona seleccionada
  // filtrar mesas segun numero de personas (minCapacity/maxCapacity)
  // filtrar segun reservaciones en esa fecha/hora
  // filtrar segun duracion que no se traslapen reservaciones
  // excluir de las mesas y grupos de mesas las que coincidan con las mesas de las reservaciones
  const hosts = [
    // 'http://reservandonos.com', // si redirecciona a https
    // 'https://reservandonos.com',
    'http://sistema.reservandonos.com', // TODO redireccionar siempre a https
    'https://sistema.reservandonos.com',
    'http://bookercontrol.com',
    'https://bookercontrol.com',
    'http://testing.bookercontrol.com/',
    'https://testing.bookercontrol.com/',
    'http://localhost:4200',
  ];
  const originDomain = req.get('origin');
  try {
    if (!hosts.includes(originDomain)) {
      console.log(req.query.placeId, req.query.date, req.query.start, req.query.end)
      const placeBlock = await models.PlaceLock.count({
        where: {
          placeId: req.query.placeId, [Op.or]: [
            {date: req.query.date},
            {
              [Op.and]: [
                {date: {[Op.lte]: req.query.date}},
                {dateEnd: {[Op.gte]: req.query.date}}
              ]
            }
          ], start: {[Op.lte]: req.query.start}, end: {[Op.gte]: req.query.start}
        }
      })
      if (placeBlock) {
        return res.json([]);
      }
    }
    const resData = [req.user.placeId, req.query.reservationId, req.query.date, req.query.start, req.query.end];
    const reservations = await findSpecificReservations(...resData);
    const reservedTablesIds = [];
    const reservedGroupsIds = [];
    let tables;
    reservations.forEach(reservation => {
      const r = reservation.dataValues;
      // const tables = !r.isGroup ? (r.table && r.table['id'] ? [r.table.id] : []) : r.group.tables.map(t => t.id);
      if (r.isGroup) {
        reservedGroupsIds.push(r.groupId);
        tables = r.group.tables.map(t => t.id);
      } else {
        tables = r.table && r.table['id'] ? [r.table.id] : [];
      }
      if (tables.length > 0) {
        reservedTablesIds.push(...tables);
      }
    });
    const onlyOnlineSales = req.query.fromWidget == 'true' ? true : false;
    const serZonesData = [req.params.serviceId, req.query.zoneId, req.query.smokeArea, reservedTablesIds, req.query.peopleCount, req.query.tableTypeId, req.query.tableHeightId, onlyOnlineSales, reservedGroupsIds];
    const serviceZones = await findSpecificServiceZones(...serZonesData);
    const sZNoNullLayout = serviceZones.filter(sz => sz.layout != null);
    const zones = sZNoNullLayout.filter(z => z.layout.tables.length || z.layout.groups.length).map(z => ({
      id: z.zone.id,
      name: z.zone.name,
      smokeArea: z.smokeArea,
      tableGroups: [
        ...z.layout.tables.map(t => ({
          isGroup: false,
          id: t.id,
          numbers: t.number.toString(),
          minCapacity: t.minCapacity,
          maxCapacity: t.maxCapacity,
          zoneName: z.zone.name,
          zoneId: z.zone.id,
          smokeArea: z.smokeArea
        })),
        ...z.layout.groups.filter(g => g.tables.find(t => {
          if (req.query.tableTypeId && +req.query.tableTypeId !== 0 && t.tableTypeId !== +req.query.tableTypeId) {
            return true;
          }
          if (req.query.tableHeightId && +req.query.tableHeightId !== 0 && t.tableHeightId !== +req.query.tableHeightId) {
            return true;
          }
          if (reservedTablesIds.includes(t.id)) {
            return true;
          }
          return false;
        }) ? false : true)
          .map(g => ({
            isGroup: true,
            id: g.id,
            numbers: g.numbers,
            minCapacity: g.minCapacity,
            maxCapacity: g.maxCapacity,
            zoneName: z.zone.name,
            zoneId: z.zone.id,
            smokeArea: z.smokeArea
          }))
      ]
    }))
      .filter(z => z.tableGroups.length > 0)
      .sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
    const allTableGroups = [];
    zones.forEach(z => {
      allTableGroups.push(...z.tableGroups);
    });
    recomendedTableGroups = allTableGroups
      .sort(//(a, b) => a.maxCapacity - b.maxCapacity)
        (a, b) => {
          if (a.maxCapacity < b.maxCapacity) {
            return -1;
          } else if (a.maxCapacity > b.maxCapacity) {
            return 1;
          }

          if (a.numbers.split(',').length < b.numbers.split(',').length) {
            return -1;
          } else if (a.numbers.split(',').length > b.numbers.split(',').length) {
            return 1;
          }

          return a.numbers < b.numbers ? -1 : a.numbers > b.numbers ? 1 : 0;
        })
      .slice(0, 9);
    const recomendedZone = {id: 0, name: 'Recomendadas', tableGroups: recomendedTableGroups};
    return res.json([recomendedZone, ...zones]);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/step1/:id', async (req, res) => {
  try {
    const id = req.params.id;
    if (id) {
      const service = await models.Service.findByPk(id, {
        attributes: ['name'],
        include: [
          {
            model: models.Zone, as: 'zones', attributes: ['id', 'name'], required: true, through: {
              model: models.ServiceZone,
              as: 'ServiceZone',
              where: {layoutId: {[Op.not]: null}},
              attributes: ['smokeArea', 'layoutId']
            }, include: [
              {
                model: models.Layout, as: 'layouts', attributes: ['id'], include: [
                  {model: models.Table, as: 'tables', attributes: ['id', 'number', 'minCapacity', 'maxCapacity']},
                  {model: models.Group, as: 'groups', attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity']}
                ]
              },
            ]
          },
        ], order: [['zones', 'name', 'asc']]
      });
      // TODO encontrar por que está regresando también ServiceZone dentro de zones...
      return res.json(service);
    } else {
      return res.sendStatus(400);
    }
  } catch (error) {
    HandleError(req, res, e);
  }
})

router.get('/step3/:date/:serviceId/:number/:isGroup/:peopleCount', async (req, res) => {
  try {
    const date = req.params.date;
    const weekDayName = moment(date).format('dddd');
    const serviceId = req.params.serviceId;
    const service = await models.Service.findByPk(serviceId, {attributes: ['weekdays', 'start', 'end', 'placeId']});
    if (service.weekdays.includes(weekDayName.toLowerCase())) {
      let where = {date, status: {[Op.notIn]: ['NO_SHOW', 'CANCELLED', 'WAIT_LIST', 'WAIT_LIST_LOCAL', 'GONE']}};
      if (req.params.isGroup == 1) {
        const group = await models.Group.findOne({where: {id: req.params.number}, attributes: ['numbers', 'layoutId']});
        if (group) { // TODO usar groupTables
          const tablesNumbers = group.numbers.split(",");
          const tables = await models.Table.findAll({
            where: {layoutId: group.layoutId, number: tablesNumbers},
            attributes: ['id']
          });
          const tablesIds = tables.map(table => table.id);
          const groupWhere = {[Op.or]: {tableId: {[Op.or]: tablesIds}, groupId: req.params.number}};
          where = {...where, ...groupWhere};
        }
      } else {
        // TODO existe groupTables pero no la vi
        const table = await models.Table.findOne({where: {id: req.params.number}, attributes: ['number', 'layoutId']});
        if (table) {
          const sameLayoutGroups = await models.Group.findAll({
            where: {layoutId: table.layoutId},
            attributes: ['id', 'numbers']
          });
          if (sameLayoutGroups) {
            const groupsWithWantedTable = sameLayoutGroups.filter(group => group.numbers.split(",").includes(table.number.toString()));
            if (groupsWithWantedTable && groupsWithWantedTable.length) {
              const groupsIds = groupsWithWantedTable.map(group => group.id);
              console.log(groupsIds);
              const groupsWhere = {[Op.or]: {groupId: {[Op.or]: groupsIds}, tableId: req.params.number}};
              where = {...where, ...groupsWhere};
            } else {
              where['tableId'] = req.params.number;
            }
          } else {
            where['tableId'] = req.params.number;
          }
        }
      }
      console.log('where', where)
      const reservations = await models.Reservation.findAll({
        where,
        attributes: ['start', 'end']
      });

      const stayTime = await models.StayTime.findOne({  // TODO esto puede regresar null si no está registrado en general->tiempo de estadia
        where: {placeId: service.placeId, peopleCount: req.params.peopleCount},
        attributes: ['time']
      });
      const time = stayTime ? stayTime.time : 350;
      return res.json({service: {start: service.start, end: service.end}, reservations, stayTime: time});
    } else {
      console.log('mal día')
      return res.status(400).send('wrong-date');
    }
  } catch (error) {
    HandleError(req, res, e);
  }
})

router.post('/', async (req, res) => {
  try {
    const layouts = await models.Layout.findAll({
      attributes: ['tablesCount', 'maxCapacity'],
      where: {
        placeId: req.user.placeId,
        id: {
          [Op.in]: req.body.zones.map(z => z.layoutId).filter(id => id)
        }
      }
    });
    req.body.placeId = req.user.placeId;
    if (layouts.length > 0) {
      req.body.tablesCount = layouts.map(l => l.tablesCount).reduce((a, b) => a + b);
      req.body.maxCapacity = layouts.map(l => l.maxCapacity).reduce((a, b) => a + b);
    }
    req.body.date = req.body.repetition === "NEVER" ? req.body.date : moment();

    const service = await models.Service.create(req.body);
    const zones = await models.Zone.findAll({
      where: {
        placeId: req.user.placeId,
        id: {
          [Op.in]: req.body.zones.map(z => z.zoneId)
        }
      }
    });
    const serviceZones = zones.map(z => {
      z.ServiceZone = req.body.zones.find(sz => sz.zoneId === z.id);
      return z;
    });
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    service.setZones(serviceZones);
    return res.json(service);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    const layouts = await models.Layout.findAll({
      attributes: ['tablesCount', 'maxCapacity'],
      where: {
        placeId: req.user.placeId,
        id: {
          [Op.in]: req.body.zones.map(z => z.layoutId).filter(id => id)
        }
      }
    });
    let service = await models.Service.findOne({
      where: {
        id: req.body.id,
        placeId: req.user.placeId
      },
    });
    req.body.date = req.body.repetition === "NEVER" ? req.body.date : moment();
    service = Object.assign(service, req.body);
    if (layouts.length > 0) {
      service.tablesCount = layouts.map(l => l.tablesCount).reduce((a, b) => a + b);
      service.maxCapacity = layouts.map(l => l.maxCapacity).reduce((a, b) => a + b);
    }
    await service.save();
    const zones = await models.Zone.findAll({
      where: {
        placeId: req.user.placeId,
        id: {
          [Op.in]: req.body.zones.map(z => z.zoneId)
        }
      }
    });
    const serviceZones = zones.map(z => {
      z.ServiceZone = req.body.zones.find(sz => sz.zoneId === z.id);
      return z;
    });
    service.setZones(serviceZones);
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(service);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    await models.Service.destroy({
      where: {
        id: req.params.id
      }
    });
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json({response: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Gets the number of future reservations associated with a service
 */
router.get('/:id/total-reservations', async (req, res) => {
  try {
    const {id} = req.params;
    const status = ['NO_SHOW', 'CANCELLED', 'GONE'];
    const numberOfReservations = await models.Reservation.count({
      where: {
        serviceId: id,
        status: {
          [Op.notIn]: status
        },
        date: {[Op.gt]: moment().startOf('month').subtract(1, 'month').format('YYYY-MM-DD')},
      }
    });
    return res.json(numberOfReservations);
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
