const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const {emailModule} = require('../helpers/email.helper');
const awsModule = require('../helpers/aws');
const env = require('../config/env');
const payment = require('../helpers/conekta');
const {divide} = require("../helpers/calculate.helper");
const {TYPE_CHARGES, ROLES} = require("../helpers/constants.helper");
const ElectronicInvoicing = require("../helpers/electronicInvoicing.helper");
const Facturapi = require("../helpers/facturapi.helper");
const {HandleError, InternalProcessError, AppError} = require("../helpers/error.helper");
const Redis = require("./../redis");

/********************************** GET *************************************/

router.get('/payment-gateway/public-key', async (req, res) => {
  try {
    const place = await models.Place.findByPk(req.user.placeId, {
      attributes: ['paymentGatewayPublicKey'],
      include: ['openingDays', 'stayTimes']
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/check-tax-record/:taxRecord', async (req, res) => {
  // TODO actualmente check RFC de mexico
  try {
    // const {taxRecord} = req.params;
    // const {registro: {is_valid: isValid}} = await Facturapi.fetchDataTaxRecord(taxRecord);
    return res.json({error: false, isValid: true});
  } catch (e) {
    HandleError(req, res, new AppError(e.message, AppError.TYPE_ERROR.TAX_INVALID, 400));
  }
});

router.get('/email-is-validated/:email', async (req, res) => {
  try {
    const data = await awsModule.checkIfEmailWasValidated(req.params.email);
    return res.json(data);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id/public-key', async (req, res) => {
  try {
    const place = await models.Place.findByPk(req.params.id, {
      attributes: ['paymentGatewayPublicKey']
    })
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id/rsv', async (req, res) => {
  try {
    const placeId = (!['undefined', 'null'].includes(req.params.id) ? req.params.id : req.user.placeId) || req.user.placeId;
    const place = await models.Place.findByPk(placeId, {
      attributes: ['name', 'id'],
      include: ['sponsorsPlaces']
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/groupBusiness/:id', async (req, res) => {
  try {
    const places = await models.GroupsBusinessPlaces.findAll({
      where: {
        groupsBusinessId: req.params.id,
        isActive: 1
      },
      include: {
        attributes: ['id', 'name'],
        model: models.Place,
        as: 'place',
        where: {
          isActive: 1
        }
      }
    });

    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id/widget-v1', async (req, res) => {
  try {
    const placeId = (!['undefined', 'null'].includes(req.params.id) ? req.params.id : req.user.placeId) || req.user.placeId;
    const place = await models.Place.findByPk(placeId, {
      attributes: {
        include: ['planId'],
        exclude: ['paymentGatewayPrivateKey']
      },
      include: ['openingDays', 'stayTimes', 'placeLock', 'sponsorsPlaces', 'categories', 'typeFood', 'workSchedule']
    });

    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id/widget', async (req, res) => {
  try {
    const placeId = (!['undefined', 'null'].includes(req.params.id) ? req.params.id : req.user.placeId) || req.user.placeId;
    const place = await models.Place.findByPk(placeId, {
      attributes: {
        include: ['planId'],
        exclude: ['paymentGatewayPrivateKey']
      },
      include: ['openingDays', 'stayTimes', 'placeLock', 'sponsorsPlaces', 'groupsBusinessPlaces', 'categories', 'typeFood', 'workSchedule']
    });
    if (place && place.groupsBusinessPlaces.length > 0) {
      const companies = await models.GroupsBusinessPlaces.findAll({
        where: {
          groupsBusinessId: place.groupsBusinessPlaces[0].groupsBusinessId,
          isActive: 1
        },
        include: {
          model: models.Place,
          as: 'place',
          include: ['openingDays', 'stayTimes', 'placeLock', 'sponsorsPlaces']
        }
      });
      if (companies.length > 1) {
        return res.json(companies);
      } else {
        return res.json(place);
      }
    } else {
      return res.json(place);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/stay-times', async (req, res) => {
  try {
    const stayTimes = await models.StayTime.findAll({
      where: {
        placeId: req.user.placeId
      }
    });
    return res.json(stayTimes);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-name', async (req, res) => {
  try {
    const places = await models.Place.findAll({
      attributes: {include: ['planId']},
      where: {
        name: {
          [Op.like]: `%${req.query.name}%`
        }
      },
      paranoid: false,
      limit: 10
    });
    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/by-name-sponsor', async (req, res) => {
  try {
    let where = {};
    if (req.query.cities && req.query.cities != '') {
      city = (req.query.cities).split(',');
      where.cityId = {[Op.in]: city};
    }
    where.name = {
      [Op.like]: `%${req.query.name}%`
    };
    const places = await models.Place.findAll({
      attributes: ['name', 'id', 'deletedAt'],
      where: where,
      paranoid: false,
      limit: 10
    });
    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/timezones', async (req, res) => {
  try {
    const data = await Redis.getRegister(`timezones`);
    if (data) {
      console.log('REDIS-timeZones');
      return res.json(data);
    }
    const timezones = await models.Timezone.findAll({
      attributes: ['name', 'code']
    });

    if (timezones) {
      Redis.setRegister(timezones, `timezones`);
    }
    return res.json(timezones);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/categories', async (req, res) => {
  try {
    const data = await Redis.getRegister(`categories`);
    if (data) {
      console.log('REDIS-categories');
      return res.json(data);
    }
    const categories = await models.Categories.findAll({
      attributes: ['id', 'type']
    });

    if (categories) {
      Redis.setRegister(categories, `categories`);
    }
    return res.json(categories);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/typeCharge', async (req, res) => {
  try {
    const types = await models.TypeCharge.findAll({
      order: [['value', 'asc']]
    });
    return res.json(types);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/typeFood', async (req, res) => {
  try {
    const data = await Redis.getRegister(`typeFood`);
    if (data) {
      console.log('REDIS-typeFood');
      return res.json(data);
    }
    const types = await models.TypeFood.findAll();

    if (types) {
      Redis.setRegister(types, `typeFood`);
    }
    return res.json(types);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/billingFrequency', async (req, res) => {
  try {
    const data = await Redis.getRegister(`billingFrequency`);
    if (data) {
      console.log('REDIS-billingFrequency');
      return res.json(data);
    }
    const billingFrequency = await models.BillingFrequency.findAll();
    if (billingFrequency) {
      Redis.setRegister(billingFrequency, `billingFrequency`);
    }
    return res.json(billingFrequency);
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/:id', async (req, res) => {
  try {
    const placeId = (!['undefined', 'null'].includes(req.params.id) ? req.params.id : req.user.placeId) || req.user.placeId;
    const place = await models.Place.findByPk(placeId, {
      attributes: {
        include: ['planId'],
        exclude: ['paymentGatewayPrivateKey']
      },
      include: ['openingDays', 'stayTimes']
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/', async (req, res) => {
  try {
    const place = await models.Place.findByPk(req.user.placeId ?? req.query.placeId, {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAd', 'typeChargeId']
      },
      include: [
        'openingDays',
        'stayTimes',
        'typeCharges',
        'categories',
        'workSchedule'
      ]
    });
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});
/********************************** POST *************************************/

router.post('/', async (req, res) => {
  try {
    if (req.file) {
      const folder = `place/${req.user.placeId}/logo`;
      const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
      req.body.logoImage = `${env.awsBucketUrl}/${uploadedFileUrl}`;
    }

    if (req.body.paymentTokenId) {
      const costumer = {
        fullname: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        countryCode: '+52'
      };
      const paymentCostumer = await payment.createCustomer(costumer, req.body.paymentTokenId, env.paymentGatewayPrivateKey);
      req.body.paymentCustomerId = paymentCostumer.id;
      req.body.paymentCardId = paymentCostumer.default_payment_source_id;

      if (req.body.planId !== 1) {
        const subscription = await payment.subscription.create(req.body.paymentCustomerId, req.body.planId, req.body.paymentCardId);
        req.body.paymentSubscriptionId = subscription.id;
      }
    }

    if (req.body.advancePaymentPolicyEnable == null) {
      req.body.advancePaymentPolicyEnable = false;
      req.body.advancePaymentPolicyFeePerPerson = 0;
      req.body.advancePaymentPolicyNumberPeople = 20;
    }

    const place = await models.Place.create({...req.body, taxToApply: await divide(2, req.body.taxToApply, 100)});

    req.user.placeId = place.id;
    emailModule.verifyEmailIdentity(place.email)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));

    emailModule.recurrentCardEmail(place.id)
      .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));

    const openingDays = req.body.openingDays.map(d => ({...d, placeId: req.user.placeId}));
    await models.OpeningDay.bulkCreate(openingDays);

    const workShedules = req.body.workSchedule.map(d => ({...d, placeId: req.user.placeId}));
    await models.WorkSchedule.bulkCreate(workShedules);

    const stayTimes = req.body.stayTimes.map(t => ({...t, placeId: req.user.placeId}));
    await models.StayTime.bulkCreate(stayTimes);

    const categories = req.body.categories?.map(c => ({placeId: req.user.placeId, categoryId: c}));
    await models.PlacesCategories.bulkCreate(categories);

    if (req.body.typeCharges.general) {
      await models.PlacesTypeCharge.create({
        placeId: place.id,
        typeChargeId: req.body.typeCharges.general
      });
    }

    if (req.body.typeCharges.google) {
      await models.PlacesTypeCharge.create({
        placeId: place.id,
        typeChargeId: req.body.typeCharges.google
      });
    }

    // ElectronicInvoicing.setCustomerInProvider(place.id)
    // .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));;

    return res.json({success: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/********************************** PUT *************************************/

router.put('/online/:id', async (req, res) => {
  try {
    const place = await models.Place.findByPk(req.user.placeId);
    place.onlineSales = req.body.onLineSales;
    await place.save();
    return res.json(place.dataValues);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/payment-method', async (req, res) => {
  try {
    const place = await models.Place.findByPk(req.user.placeId);
    if (place.paymentCustomerId) {
      // add payment method
      const paymentSource = await payment.createPaymentSource(place.paymentCustomerId, req.body.paymentTokenId, env.paymentGatewayPrivateKey);
      // set default payment source
      await payment.setDefaultPaymentSource(place.paymentCustomerId, paymentSource.id, env.paymentGatewayPrivateKey);
      place.paymentCardId = paymentSource.id;
    } else {
      // create conekta client
      const costumer = {
        fullname: place.name,
        email: place.email,
        phone: place.phone,
        countryCode: '+52'
      };
      const paymentCostumer = await payment.createCustomer(costumer, req.body.paymentTokenId, env.paymentGatewayPrivateKey);
      place.paymentCustomerId = paymentCostumer.id;
      place.paymentCardId = paymentCostumer.default_payment_source_id;
    }

    if (place.paymentSubscriptionId) {
      // update subscription card
      const subscription = await payment.subscription.update(place.paymentCustomerId, place.planId, place.paymentCardId);
      place.paymentSubscriptionId = subscription.id;
    }
    await place.save();
    return res.json(place);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/opening-days', async (req, res) => {
  try {
    const placeId = req.user.placeId;
    const {openingDays} = req.body;

    await models.OpeningDay.destroy({where: {placeId}});
    await models.OpeningDay.bulkCreate(openingDays.map(openingDay => ({...openingDay, placeId})));
    return res.json({error: false, message: "Opening Days Updated Successful"});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/working-hours', async (req, res) => {
  try {
    const placeId = req.user.placeId;
    const {workSchedules} = req.body;

    await models.WorkSchedule.destroy({where: {placeId}});
    await models.WorkSchedule.bulkCreate(workSchedules.map(workSchedule => ({...workSchedule, placeId})));
    return res.json({error: false, message: "Work Hours Updated Successful"});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    const {typeCharges, categories, workSchedule: workSchedules, openingDays, stayTimes} = req.body;
    delete req.body.typeCharges;
    delete req.body.categories;
    delete req.body.workSchedule;
    delete req.body.openingDays;
    delete req.body.stayTimes;

    if (req.file) {
      const folder = `place/${req.user.placeId}/logo`;
      const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
      req.body.logoImage = `${env.awsBucketUrl}/${uploadedFileUrl}`;
    }
    const place = await models.Place.findByPk(req.user.placeId, {
      attributes: {include: ['planId']},
      include: [{
        model: models.TypeCharge,
        as: 'typeCharges',
        attributes: ['id', 'description', 'type', 'value', 'origin'],
        // through: {
        //   where: {end: null}
        // }
      }]
    });
    if (place.planId !== req.body.planId) {
      place.planId = req.body.planId;
      if (place.planId === 1 && place.paymentSubscriptionId) {
        // plan gratis, cancelar suscripcion
        const subscription = await payment.subscription.cancel(place.paymentCustomerId);
        place.paymentSubscriptionId = null;
      } else if (!place.paymentSubscriptionId) {
        // crear suscripcion
        if (place.paymentCardId) {
          const subscription = await payment.subscription.create(place.paymentCustomerId, place.planId, place.paymentCardId);
          place.paymentSubscriptionId = subscription.id;
        }
      } else {
        // actualizar suscripcion
        const subscription = await payment.subscription.update(place.paymentCustomerId, place.planId, place.paymentCardId);
        place.paymentSubscriptionId = subscription.id;
      }
    }
    Object.assign(place, {
      ...req.body,
      taxToApply: (req.body.taxToApply > 1)
        ? await divide(2, req.body.taxToApply, 100)
        : req.body.taxToApply
    });
    await place.save();

    await models.OpeningDay.destroy({where: {placeId: req.user.placeId}});
    await models.OpeningDay.bulkCreate(openingDays.map(d => ({...d, placeId: req.user.placeId})));

    await models.WorkSchedule.destroy({where: {placeId: req.user.placeId}});
    await models.WorkSchedule.bulkCreate(workSchedules.map(d => ({...d, placeId: req.user.placeId})));

    await models.StayTime.destroy({where: {placeId: req.user.placeId}});
    await models.StayTime.bulkCreate(stayTimes.map(t => ({...t, placeId: req.user.placeId})));

    if (req.user.role === ROLES.ADMIN) {
      const general = place.typeCharges.find(tc => tc.origin === TYPE_CHARGES.WEBSITE && tc.PlacesTypeCharge.end === null);
      if (typeCharges?.general) {
        if (general) {
          if (typeCharges.general !== general.id) {
            await general.PlacesTypeCharge.update({end: models.Sequelize.fn('now')});
            await models.PlacesTypeCharge.create({
              placeId: place.id,
              typeChargeId: typeCharges.general
            });
          }
        } else {
          await models.PlacesTypeCharge.create({
            placeId: place.id,
            typeChargeId: typeCharges.general
          });
        }
      } else if (general) {
        await general.PlacesTypeCharge.update({end: models.Sequelize.fn('now')});
      }

      const google = place.typeCharges.find(tc => tc.origin === TYPE_CHARGES.GOOGLE && tc.PlacesTypeCharge.end === null);
      if (typeCharges?.google) {
        if (google) {
          if (typeCharges.google !== google.id) {
            await google.PlacesTypeCharge.update({end: models.Sequelize.fn('now')});
            await models.PlacesTypeCharge.create({
              placeId: place.id,
              typeChargeId: typeCharges.google
            });
          }
        } else {
          await models.PlacesTypeCharge.create({
            placeId: place.id,
            typeChargeId: typeCharges.google
          });
        }
      } else if (google) {
        await google.PlacesTypeCharge.update({end: models.Sequelize.fn('now')});
      }

      await models.PlacesCategories.destroy({where: {placeId: place.id}});
      await models.PlacesCategories.bulkCreate(categories?.map(c => ({placeId: place.id, categoryId: c})));

      // ElectronicInvoicing.setCustomerInProvider(place.id)
      //  .catch(e => HandleError(req, res, new InternalProcessError(e, {place: place.dataValues})));
    }
    return res.json({success: true});
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
