const express = require('express');
const router = express.Router();
const models = require('../models');
const {HandleError} = require("../helpers/error.helper");

router.get('/:short', async (req, res) => {
  try {
    const short = req.params.short;
    const s = await models.Shorter.findOne({where: {short}, attributes: ['long', 'type']});
    if (s) {
      let url = "";
      if (s.type == "ADD_CARD") {
        url = `/reservations/${s.long}/payment`;
      } else if (s.type == "RE_CONFIRM") {
        url = `/reservations/${s.long}/RE_CONFIRMED`; // TODO ¿cómo cancelar?
      } else { // review
        url = `https://reservandonos.com/encuesta-satisfaccion?reserva=${s.long}`;
      }
      return res.json(url);
    } else {
      return res.sendStatus(404);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});
module.exports = router;
