const express = require('express');
const router = express.Router();
const models = require('../models');
const sequelize = require("sequelize");
const {HandleError} = require("../helpers/error.helper");
const {SPONSOR_KEYS, SPONSOR_PLACE_TYPE} = require("../helpers/constants.helper");
const Op = models.Sequelize.Op;

/**
 * calcula la cantidad de lugares asociadosa un sponsor de acuerdo al tipo
 */
router.get('/total-places', async (req, res) => {
  try {
    let where = {};
    if (req.query.term) {
      where = {
        name: {
          [Op.like]: `%${req.query.term}%`
        }
      };
    }
    const sponsors = await models.Sponsor.findAll({
      attributes: ['id', 'name'],
      include: [
        {
          attributes: ['type', [sequelize.fn('COUNT', sequelize.col('sponsor.type')), 'total']],
          model: models.SponsorPlaces,
          as: 'sponsor',
          required: false
        },
      ],
      where: where,
      group: ['sponsor.sponsorId', 'sponsor.type'],
      order: [
        ['name', req.query.sortDir]
      ]
    });
    return res.json(sponsors);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Obtiene todos los lugares asociado a un sponsor de acuerdo al tipo
 */
router.get('/:sponsorId/places', async (req, res) => {
  try {
    const sponsorId = req.params.sponsorId;
    const {
      sponsorType = SPONSOR_PLACE_TYPE.COMMISSION,
      sortBy = 'name',
      sortDir = 'asc',
      pageIndex = '0',
      pageSize = '10',
      term = ''
    } = req.query;

    const places = await models.Place.findAndCountAll({
      attributes: ['id', 'name', 'logoImage'],
      include: [
        {
          attributes: ['id', 'type', 'isActive', 'value', 'urlBase'],
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          required: true,
          where: {
            type: sponsorType,
            sponsorId: sponsorId
          }
        }
      ],
      where: {
        name: {
          [Op.like]: `%${term}%`
        }
      },
      order: [
        [sortBy, sortDir]
      ],
      offset: (+pageIndex * +pageSize) || 0,
      limit: +pageSize || 10,
    });
    return res.json(places);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * Consulta por nombre si un lugar esta disponible para agregar a un lugar
 */
router.get('/:sponsorId/places/to/add', async (req, res) => {
  try {
    const sponsorId = req.params.sponsorId;
    const {
      sponsorType = SPONSOR_PLACE_TYPE.COMMISSION,
      term = ''
    } = req.query;
    const result = await models.Place.findAndCountAll({
      attributes: ['id', 'name'],
      where: {
        name: {
          [Op.like]: `%${term}%`
        },
        id: {
          [Op.notIn]: sequelize.literal(
            `(SELECT SP.placeId as id FROM SponsorPlaces SP WHERE SP.sponsorId = ${sponsorId}  AND SP.type = '${sponsorType}' AND SP.deletedAt is null)`, 'a'
          )
        }
      },
      limit: 10,
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * POST - Agregar un place a un sponsor
 */
router.post('/:sponsorId/place/:placeId', async (req, res) => {
  try {
    const {sponsorId, placeId} = req.params;
    const {type, value, isActive = true, baseUrl: urlBase} = req.body;
    let sponsorPlace = await models.SponsorPlaces.findOne({
      where: {
        sponsorId,
        placeId,
        type
      }
    });
    if (sponsorPlace) {
      await sponsorPlace.update({value, type, deletedAt: null});
    } else {
      sponsorPlace = await models.SponsorPlaces.create({
        type, urlBase, isActive, value, placeId, sponsorId
      });
    }
    return res.json({
      message: {
        es: 'Agregado correctamente',
        en: 'Success added'
      },
      created: sponsorPlace
    });
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * PUT - Modificar value o baseUrl del lugar asociado al sponsor
 */
router.put('/place/:sponsorPlacesId', async (req, res) => {
  try {
    const {sponsorPlacesId} = req.params;
    const {value, isActive, baseUrl: urlBase} = req.body;
    const sponsorPlace = await models.SponsorPlaces.findByPk(sponsorPlacesId);
    if (sponsorPlace) {
      const updated = await sponsorPlace.update({value, isActive, urlBase});
      return res.json({
        message: {
          es: 'Actualizado correctamente',
          en: 'Success updated'
        },
        updated
      });
    } else {
      return res.status(406).json({
        message: {
          es: 'Informacion no encontrada',
          en: 'Information not found'
        }
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * DELETE - Eliminar el lugar de un sponsor
 */
router.delete('/place/:sponsorPlacesId', async (req, res) => {
  try {
    const {sponsorPlacesId} = req.params;
    const sponsorPlace = await models.SponsorPlaces.findByPk(sponsorPlacesId);
    if (sponsorPlace) {
      const deleted = await sponsorPlace.destroy();
      return res.json({
        message: {
          es: 'Eliminado correctamente',
          en: 'Success deleted'
        },
        deleted
      });
    } else {
      return res.status(406).json({
        message: {
          es: 'Informacion no encontrada',
          en: 'Information not found'
        }
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
