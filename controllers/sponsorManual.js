const express = require('express');
const router = express.Router();
const models = require('../models');
const sequelize = require("sequelize");
const {HandleError} = require("../helpers/error.helper");
const {sum, minus} = require("../helpers/calculate.helper");
const Op = models.Sequelize.Op;

/**
 * GET - Obtiene sponsors cargados manualmente
 */
router.get('/:sponsorId/place/:placeId', async (req, res) => {
  try {
    const {sponsorId, placeId} = req.params;
    const sponsorplace = await models.SponsorPlaceManual.findAll({
      attributes: [
        'id', 'date', 'value'
      ],
      where: {
        sponsorId,
        placeId
      }
    });

    const sum = await models.SponsorPlaceManual.findAll({
      attributes: [
        [sequelize.fn('SUM', sequelize.col('value')), 'total']
      ],
      where: {
        sponsorId,
        placeId
      }
    });

    return res.json({sponsorplace, sum});
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * GET - Return Resume Load Manual
 */
router.get('/:sponsorId/resume', async (req, res) => {
  try {
    const {sponsorId} = req.params;
    const resume = await models.SponsorPlaceManual.findAll({
      attributes: [
        'placeId',
        [sequelize.literal('LEFT(date, 7)'), 'date'],
        [sequelize.literal('SUM(IF(value > 0, value, 0))'), 'add'],
        [sequelize.literal('SUM(IF(value < 0, value, 0))'), 'subtract']
      ],
      include: [
        {
          attributes: ['name', 'typeAccount'],
          model: models.Place,
          as: 'place'
        }
      ],
      where: {
        sponsorId
      },
      group: ['placeId', [sequelize.literal('LEFT(date, 7)')]],
      order: [[sequelize.literal('LEFT(date, 7)')], 'placeId']
    });

    let headers = ['name'];
    const obj = {id: '', name: ''};
    for (const item of resume) {
      if (!headers.includes(item.date)) {
        headers.push(item.date);
        obj[item.date] = '';
      }
    }

    const data = [];
    for (let item of resume) {
      const place = data.filter(elem => elem?.id === item.dataValues.placeId);
      if (place.length === 0) {
        const obj = {};
        obj[item.date] = Number(item.dataValues.subtract);
        data.push({
          id: item.dataValues.placeId,
          name: item?.place?.name,
          add: Number(item.dataValues.add),
          remain: Number(item.dataValues.subtract),
          sponsorsPlaces: [{sponsorId}],
          ...obj
        });
      } else {
        for (const elem of data) {
          if (elem?.id === item.dataValues.placeId) {
            elem[item.dataValues.date] = Number(item.dataValues.subtract);
            elem.add = item.dataValues.add ? elem.add + Number(item.dataValues.add) : elem.add;
            elem.remain = item.dataValues.subtract ? elem.remain + Number(item.dataValues.subtract) : elem.remain;
          }
        }
      }
    }

    for (const datum of data) {
      datum.total = datum.add + datum.remain;
    }

    headers = headers.concat(['add', 'total']);

    return res.json({headers, data});
  }  catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * POST - Carga de sponsor manual
 */
router.post('/load-sponsor', async (req, res) => {
  try {
    const {value, date, placeId, sponsorId} = req.body;
    const sponsorPlace = await models.SponsorPlaceManual.create({
      value, date, placeId, sponsorId
    });
    return res.json(sponsorPlace);
  } catch (e) {
    HandleError(req, res, e);
  }
});

/**
 * DELETE - Eliminar Registro.
 */
router.delete('/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const sponsorPlaceManual = await models.SponsorPlaceManual.findByPk(id);
    if (sponsorPlaceManual) {
      const deleted = await sponsorPlaceManual.destroy();
      return res.json({
        message: {
          es: 'Eliminado correctamente',
          en: 'Success deleted'
        },
        deleted
      });
    } else {
      return res.status(406).json({
        message: {
          es: 'Informacion no encontrada',
          en: 'Information not found'
        }
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});
module.exports = router;
