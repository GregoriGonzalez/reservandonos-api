const express = require('express');
const router = express.Router();
const models = require('../models');
const payment = require('../helpers/conekta');
const moment = require('moment');
const {HandleError} = require("../helpers/error.helper");
const Op = models.Sequelize.Op;

// Verificación que el servidor está activo
router.post('/create/', async (req, res) => {
  try {
    await payment.createCustomer(res.body.costumer, "", req.body.privateKey);
    return res.json({success: 'success'});
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
