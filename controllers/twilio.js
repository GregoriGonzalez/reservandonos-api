const express = require('express');
const {HandleError} = require("../helpers/error.helper");
const router = express.Router();
const MessagingResponse = require('twilio').twiml.MessagingResponse;

router.post('/', async (req, res) => {
  try {
    const twiml = new MessagingResponse();
    twiml.message('The Robots are coming! Head for the hills!');
    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end(twiml.toString());
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
