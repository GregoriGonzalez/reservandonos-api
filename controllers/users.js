const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const models = require('../models');
const Op = models.Sequelize.Op;
const sequelize = models.sequelize;
const env = require('../config/env');
const auth = require('../middlewares/auth');
const authController = require('./auth');
const awsModule = require('../helpers/aws');
const {decrypt} = require('../helpers/encryption');
const {emailModule} = require('../helpers/email.helper');
const {Capitalize} = require("../helpers/string.helper");
const {ROLES} = require("../helpers/constants.helper");
const {HandleError} = require("../helpers/error.helper");

const randomPassword = length => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('');
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters[Math.floor(Math.random() * charactersLength)];
  }
  return result;
}

router.get('/', auth.hasRole(ROLES.ADMIN, ROLES.MANAGER, ROLES.WAITLIST, ROLES.FASTFOOD), async (req, res) => {
  try {
    let where = {};
    let options = {
      attributes: {
        exclude: ['password', 'acceptTerms']
      },
      order: [
        [req.query.sortBy, req.query.sortDir]
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10,
    };
    if (req.query.term) {
      where = {
        [Op.or]: [
          {fullname: {[Op.like]: `%${req.query.term}%`}},
          {email: {[Op.like]: `%${req.query.term}%`}},
          {phone: {[Op.like]: `%${req.query.term}%`}}
        ]
      };
    }
    if (req.user.placeId) {
      options.include = [
        {
          model: models.Place,
          as: 'places',
          required: true,
          through: {
            where: {
              placeId: req.user.placeId
            }
          },
          attributes: ['id']
        }
      ]
    } else if (req.query.partnerId) {
      where.partnerId = req.query.partnerId;
    } else if (['ADMIN'].includes(req.user.role)) {
      where.role = {[Op.or]: [req.query.roles]};
    }

    const results = await models.User.findAndCountAll({
      where: where,
      ...options
    });
    return res.json(results);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/self', async (req, res) => {
  try {
    const user = await models.User.findByPk(req.user.id, {
      attributes: {
        exclude: ['password']
      }
    });
    return res.json(user);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/is-it-available', auth.hasRole(ROLES.ADMIN, ROLES.MANAGER), async (req, res) => {
  try {
    const {placeId} = req.user;
    const body = {userExists: false};
    const user = await models.User.getByEmailAndPlaceId(req.query.email, placeId, {
      attributes: ['id', 'email', 'fullname', 'address', 'phone', 'genre', 'profileImage', 'role'],
    });
    if (user) {
      body.userExists = true;
      if (placeId) {
        if (['MANAGER', 'HOSTESS', 'RECEPCIONIST', 'CASHIER', 'FASTFOOD'].includes(user.dataValues.role)) {
          body.user = await user.populateNotification();
          body.existsInPlace = user.usersPlaces.length;
        } else {
          body.restricted = true;
        }
      }
    }
    return res.json(body);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const where = {
      id: req.params.id,
    };
    const include = [
      {
        model: models.Place,
        as: 'places',
        required: true,
        through: {
          where: {
            placeId: req.user.placeId
          }
        },
        attributes: ['id']
      }
    ];
    const User = (req.user.placeId)
      ? await models.User.findOne({
        attributes: {
          exclude: ['password']
        },
        where: where,
        include: include
      })
      : await models.User.findByPk(req.params.id);
    if (User) {
      if (User.dataValues.places && User.dataValues.places.length) {
        User.dataValues.notificationEmail = User.dataValues.places[0].UsersPlaces.notificationEmail;
        User.dataValues.notificationWhatsApp = User.dataValues.places[0].UsersPlaces.notificationWhatsApp;
      }
      return res.json(User);
    } else {
      return res.status(400).json('Usuario no encontrado');
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/owner/place/:id', async (req, res) => {
  try {
    const owner = await models.User.findByPk(req.params.id);
    if (owner) {
      return res.json(owner);
    } else {
      return res.status(400).json('Usuario no encontrado');
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', auth.hasRole(ROLES.ADMIN, ROLES.MANAGER), async (req, res) => {
  let transaction;
  try {
    const {placeId} = req.user;
    let {body} = req;
    const {notificationEmail, notificationWhatsApp} = req.body;
    const allowed_roles = {
      ADMIN: [ROLES.ADMIN, ROLES.TELEMARKETING, ROLES.SPONSOR],
      MANAGER: [ROLES.MANAGER, ROLES.HOSTESS, ROLES.RECEPCIONIST, ROLES.CASHIER, ROLES.FASTFOOD]
    };
    if (placeId) {
      if (!allowed_roles.MANAGER.includes(body.role)) {
        return res.status(406).json('Rol inválido');
      }
    } else {
      if (!allowed_roles.ADMIN.includes(body.role)) {
        return res.status(406).json('Rol inválido');
      }
    }

    const userWithEmail = await models.User.findOne({where: {email: body.email}});
    if (userWithEmail) {
      return res.status(406).json('Email no disponible');
    }

    body = {
      ...req.body,
      email: req.body.email.toLowerCase(),
      fullname: Capitalize(req.body.fullname),
      password: await bcrypt.hash(req.body.password || randomPassword(32), 10),
      verified: true,
      acceptTerms: true
    }
    delete body.placeId;
    delete body.notificationEmail;
    delete body.notificationWhatsApp;

    transaction = await sequelize.transaction();
    const userCreated = await models.User.create(body, {transaction});
    if (placeId) {
      await models.UsersPlaces.create({
        userId: userCreated.id,
        placeId,
        notificationEmail,
        notificationWhatsApp
      }, {transaction});
    }
    if (req.file) {
      const folder = `user/${userCreated.id}`;
      const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
      userCreated.profileImage = `${env.awsBucketUrl}/${uploadedFileUrl}`;
      await userCreated.save({transaction});
    }
    await transaction.commit();
    return res.json({
      error: false,
      message: 'Usuario Creado',
      data: {...userCreated.dataValues, notificationEmail, notificationWhatsApp}
    });
  } catch (e) {
    if (transaction) {
      await transaction.rollback();
    }
    HandleError(req, res, e);
  }
});

router.post('/register-partner', async (req, res) => {
  try {
    const user = req.body.user;
    user.password = await bcrypt.hash(user.password, 10)
    const placeId = req.body.placeId;
    const newUser = await models.User.create(user);
    await newUser.addPlaces([placeId])
    return res.status(201).send({message: 'Nuevo usuario creado exitosamente.'}) // TODO traducir
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', auth.hasRole(ROLES.ADMIN, ROLES.MANAGER), async (req, res) => {
  try {
    const {
      id, password, fullname, genre, address,
      phone, email, notificationEmail, notificationWhatsApp, role
    } = req.body;
    const placeId = (req.user.role === ROLES.ADMIN) ? req.body.placeId : req.user.placeId;
    const User = await models.User.findByPk(id);
    if (User) {
      if (email && email.toLowerCase() !== User.dataValues.email.toLowerCase()) {
        const emailIsNotAvailable = await models.User.count({
          where: {
            email: req.body.email
          }
        });
        if (emailIsNotAvailable)
          return res.status(406).send('Ya existe un usuario con el mismo correo.');
      }
      if (req.file) {
        const folder = `user/${id}`;
        const uploadedFileUrl = await awsModule.uploadFile(folder, req.file)
        req.body.profileImage = `${env.awsBucketUrl}/${uploadedFileUrl}`;
      }
      await User.update({
        fullname: Capitalize(fullname),
        genre: genre,
        address: address,
        phone: phone,
        email: email ? email.toLowerCase(): User.email,
        role: role,
        password: (password === '********') ? User.dataValues.password : bcrypt.hashSync(password, 10),
        profileImage: (req.body.profileImage) ? req.body.profileImage : User.dataValues.profileImage
      });
      if (placeId) {
        const usersPlaces = await models.UsersPlaces.findOne({
          where: {
            userId: User.dataValues.id,
            placeId
          }
        });
        if (usersPlaces) {
          await usersPlaces.update({notificationEmail, notificationWhatsApp});
        } else {
          await models.UsersPlaces.create({
            userId: id,
            placeId,
            notificationEmail,
            notificationWhatsApp
          });
        }
      }
      return res.json({
        error: false,
        message: 'Usuario actualizado',
        data: {...User.dataValues, notificationEmail, notificationWhatsApp}
      });
    } else {
      return res.status(400).json('Usuario no encontrado');
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/validate-partner-email/', async (req, res) => {
  try {
    const email = decrypt(req.body.key);
    const user = await models.User.update({verified: true}, {where: {email}});
    if (user) {
      return res.status(202).send({message: 'Correo validado exitosamente.'}); // TODO traducir
    } else {
      return res.status(400).send({message: 'Not found'}); // ¿realmente el error es solamante por not found?
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/password', async (req, res) => {
  try {
    const user = await models.User.findByPk(req.user.id);
    const match = await bcrypt.compare(req.body.currentPassword, user.password);
    if (match) {
      user.password = await bcrypt.hash(req.body.newPassword, 10);
      await user.save();
      return res.json({success: true});
    } else {
      return res.status(422).json({
        type: 'invalid-password'
      });
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/dark-mode', async (req, res) => {
  try {
    const id = req.user.id;
    const darkMode = req.body.darkMode;
    await models.User.update({darkMode}, {where: {id}});
    const updatedUser = await models.User.findOne({
      where: {id},
      include: [{model: models.Place, as: 'place', attribues: ['id', 'logoImage', 'theme']}]
    });
    const token = await authController.generateToken(updatedUser, true);
    return res.json({token});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/lang', async (req, res) => {
  try {
    const id = req.user.id;
    const langCode = req.params.code;
    await models.User.update({langCode}, {where: {id}});
    return res.json('OK');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/token-fcm', async(req, res) => {
  try {
   const {token = null} = req.body;
    await models.User.update({tokenFCM: token}, {where: {id: req.user.id}});
    return res.json({error: false, message: 'Token FCM updated'});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/:userId', auth.hasRole(ROLES.ADMIN, ROLES.MANAGER), async (req, res) => {
  try {
    const {placeId} = req.user;
    if (!placeId) {
      return res.status(400).json('placeId es requerido');
    }
    const id = req.params.userId;
    const User = await models.User.findByPk(id);
    if (User) {
      if (placeId !== null) {
        const wasDeleted = await User.removePlaces([placeId]);
        if (wasDeleted) {
          return res.json('Usuario eliminado del lugar');
        } else {
          return res.status(500).json('No pudo ser eliminado el usuario del lugar');
        }
      }
      // TODO eliminar usuarios con placeId nulo, eliminacion logica de la tabla user
    } else {
      return res.status(400).json('Usuario no encontrado');
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
