const express = require('express');
const router = express.Router();
const models = require('../models');
const moment = require("moment-timezone");
const {moduleDialog360} = require('../helpers/dialog360');
const {moduleWhatsApp} = require('../helpers/toolsSendWhatsApp');
const {statusReservation, confirmedReservation, cancelledReservation} = require('../helpers/reservation.helper');
const ElectronicInvoicing = require("../helpers/electronicInvoicing.helper");
const Notifications = require('../helpers/notifications.helper');
const {emailModule: Emails} = require("../helpers/email.helper");
const {savePayment: saveStripePayment, attachCustomerToPaymentIntent} = require("../helpers/stripe.helper");
const {savePayPalPayment} = require("../helpers/paypal.helper");
const {
  PAYMENTS_GATEWAY, BILLINGS_STATUS, RESERVATION_STATUS, PAYMENTS_TYPE, STRIPE_STATUS_ORDER,
  PAYMENTS_STATUS, TEMPLATES_WHATSAPP
} = require("../helpers/constants.helper");
const {HandleError, InternalProcessError} = require("../helpers/error.helper");

/**
 * Webhook diagol360 whatsApp
 */
router.post('/configs/webhook', async (req, res) => {
  try {
    res.status(200).json();
    const body = req.body;
    let id = null, numberPhone = null;

    if (body?.contacts && body?.messages && body?.messages.length > 0 && body?.messages[0]?.text) {
      id = body?.statuses?.length > 0 && body?.statuses[0]?.conversation?.id ? body?.statuses[0]?.conversation?.id : null;
      numberPhone = body?.messages[0]?.from;
      if (body?.messages[0]?.text?.body.toUpperCase() === 'CONCIERGE') {
        await moduleDialog360.sendAutoReply(TEMPLATES_WHATSAPP.PARTNER.AUTO_REPLY, body?.messages[0]?.from);
      } else {
        await moduleDialog360.sendAutoReply(TEMPLATES_WHATSAPP.CUSTOMER.AUTO_REPLY, body?.messages[0]?.from);
      }
    }

    if (body?.contacts && body?.messages && body?.messages.length > 0 && body?.messages[0]?.type == 'button' && body?.messages[0]?.button?.payload) {
      numberPhone = body?.messages[0]?.from;
      // "reservation-CANCELLED/CONFIRMED-reservationId-CODE-User/phone-force-customer/partner"
      let payload = body?.messages[0]?.button?.payload.split('-');
      if (payload[0] === 'reservation') {
        const data = await statusReservation(payload[2]);
        if (!data.oldReservation) {
          data.reservation.hour = data.reservation?.start ? moment(data.reservation.date).add(data.reservation.start, 'minutes').format('H:mm') : '';
          if (data.reservation.status === RESERVATION_STATUS.PENDING || payload[5] === 'true') {
            if (payload[1] === RESERVATION_STATUS.CONFIRMED || payload[1] === RESERVATION_STATUS.RE_CONFIRMED) {
              data.reservation.status = payload[1];
              await confirmedReservation(data.reservation, payload[6] === 'customer' ? true : false, numberPhone, Emails);
              if (payload[6] !== 'customer') {
                await moduleDialog360.sendMessageIndividual(numberPhone,
                  `La reservación en ${data.reservation.place.name} que esta a nombre de *${data.reservation?.costumer?.fullname}* ` +
                  `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour} ha sido _*CONFIRMADA*_ con éxito.`
                );
              } else if (payload[6] === 'customer' && payload[1] === RESERVATION_STATUS.RE_CONFIRMED) {
                await moduleDialog360.sendMessageIndividual(numberPhone,
                  `✋🏻 Hola *${data.reservation?.costumer?.fullname}*, gracias por 🟢 *_CONFIRMAR_* 🟢 tu asistencia en *${data.reservation.place.name.toUpperCase()}* ` +
                  `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour}.`
                );
              }
            }

            if (payload[1] === RESERVATION_STATUS.CANCELLED) {
              data.reservation.status = RESERVATION_STATUS.CANCELLED;
              await cancelledReservation(data.reservation, payload[6] === 'customer' ? true : false, Emails, moduleWhatsApp);

              if (payload[6] !== 'customer') {
                await moduleDialog360.sendMessageIndividual(numberPhone,
                  `La reservación en ${data.reservation.place.name} que esta a nombre de *${data.reservation?.costumer?.fullname}* ` +
                  `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour} ha sido _*CANCELADA*_ con éxito.`
                );
              }
            }
          } else {
            if (payload[1] === RESERVATION_STATUS.CANCELLED) {
              if (data.reservation.status === RESERVATION_STATUS.CANCELLED) {
                await moduleDialog360.sendMessageIndividual(numberPhone,
                  `La reservación en ${data.reservation.place.name} que esta a nombre de *${data.reservation?.costumer?.fullname}* ` +
                  `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour} ya ha sido _*CANCELADA*_ previamente.`
                );
              }

              if ([RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED].includes(data.reservation.status)) {
                await moduleWhatsApp.sendForcePartner(data.reservation, numberPhone, payload[3], true);
              }
            }

            if (payload[1] === RESERVATION_STATUS.CONFIRMED) {
              if ([RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED].includes(data.reservation.status)) {
                await moduleDialog360.sendMessageIndividual(numberPhone,
                  `La reservación en ${data.reservation.place.name} que esta a nombre de *${data.reservation?.costumer?.fullname}* ` +
                  `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour} ya ha sido _*CONFIRMADA*_ previamente.`
                );
              }

              if (data.reservation.status === RESERVATION_STATUS.CANCELLED) {
                await moduleWhatsApp.sendForcePartner(data.reservation, numberPhone, payload[3], false);
              }
            }


            if ([RESERVATION_STATUS.SEATED, RESERVATION_STATUS.GONE].includes(data.reservation.status)) {
              await moduleDialog360.sendMessageIndividual(numberPhone,
                `La reservación en ${data.reservation.place.name} que esta a nombre de *${data.reservation?.costumer?.fullname}* ` +
                `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour} ya ha sido _*PROCESADA*_ por lo ` +
                `cual no podemos cambiar el estado de la reservación.`
              );
            }
          }
        } else {
          data.reservation.hour = data.reservation?.start ? moment(data.reservation.date).add(data.reservation.start, 'minutes').format('H:mm') : '';
          await moduleDialog360.sendMessageIndividual(numberPhone,
            `La reservación en ${data.reservation.place.name} que esta a nombre de *${data.reservation?.costumer?.fullname}* ` +
            `para el día ${moment(data.reservation.date).format('DD-MM-YYYY')} a las ${data.reservation.hour} ya ha pasado de fecha, por lo tanto no es ` +
            `posible modificar su estado.`
          );
        }
      }
    }

    models.WebHookWhatsApp.create({description: JSON.stringify(req.body), conversationId: id, numberPhone: numberPhone});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/webhook/stripe', async (req, res) => {
  try {
    res.status(200).json();
    const {type: eventType, data, object} = req.body;
    const {paymentGateway, paymentType, billingId = null} = data.object.metadata;
    const transactionId = data.object.id;
    if (object === 'event') {
      if (paymentType === PAYMENTS_TYPE.BILLING && billingId !== null) {
        const billing = await models.Billings.findByPk(billingId);
        const payment = await models.Payments.findOne({where: {transactionId, billingId}});
        if (paymentGateway === PAYMENTS_GATEWAY.STRIPE) {
          if (eventType === 'payment_intent.succeeded') {
            await attachCustomerToPaymentIntent(transactionId, billing.placeId);
            const paymentRegistered = await saveStripePayment(PAYMENTS_TYPE.BILLING, paymentGateway, transactionId, billing.placeId, billing.id);
            billing.status = BILLINGS_STATUS.CONFIRMED;
            await billing.save();
            ElectronicInvoicing.generate(billing.placeId, paymentRegistered)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
            Emails.dispatchPaymentByBillingConfirmed(paymentRegistered.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
            Notifications.dispatchPayment(paymentRegistered.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
          } else if (eventType === 'payment_intent.payment_failed') {
            const paymentRegistered = await saveStripePayment(PAYMENTS_TYPE.BILLING, paymentGateway, transactionId, billing.placeId, billing.id);
            if (paymentRegistered.statusPayment === PAYMENTS_STATUS.PROCESS) {
              paymentRegistered.statusPayment = PAYMENTS_STATUS.DECLINED;
              await paymentRegistered.save();
            }
            billing.status = BILLINGS_STATUS.PENDING;
            await billing.save();
            Emails.dispatchPaymentByBillingReject(paymentRegistered.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
          }
        } else if (paymentGateway === PAYMENTS_GATEWAY.OXXO) {
          if (eventType === 'payment_intent.requires_action') {
            await attachCustomerToPaymentIntent(transactionId, billing.placeId);
            const paymentRegistered = await saveStripePayment(PAYMENTS_TYPE.BILLING, paymentGateway, transactionId, billing.placeId, billing.id);
            billing.status = BILLINGS_STATUS.VERIFIED;
            await billing.save();
            Emails.dispatchLinkValetOfOxxo(paymentRegistered.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
          } else if (eventType === 'payment_intent.succeeded' && payment.statusPayment === PAYMENTS_STATUS.PROCESS) {
            payment.statusPaymentGateway = STRIPE_STATUS_ORDER.SUCCEEDED;
            payment.statusPayment = PAYMENTS_STATUS.APPROVED;
            await payment.save();
            billing.status = BILLINGS_STATUS.CONFIRMED;
            await billing.save();
            ElectronicInvoicing.generate(billing.placeId, payment)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
            Emails.dispatchPaymentByBillingConfirmed(payment.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
            Notifications.dispatchPayment(payment.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: payment.dataValues})));
          } else if (eventType === 'payment_intent.payment_failed' && payment.statusPayment === PAYMENTS_STATUS.PROCESS) {
            payment.statusPaymentGateway = STRIPE_STATUS_ORDER.CANCELED;
            payment.statusPayment = PAYMENTS_STATUS.DECLINED;
            await payment.save();
            billing.status = BILLINGS_STATUS.PENDING;
            await billing.save();
            Emails.dispatchPaymentByBillingReject(payment.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment})));
            Notifications.dispatchPayment(payment.id)
              .catch(e => HandleError(req, res, new InternalProcessError(e, {payment})));
          }
        }
      }
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/webhook/paypal', async (req, res) => {
  try {
    res.status(200).json();
    const {event_type: eventType, resource} = req.body;
    if (eventType === 'PAYMENT.CAPTURE.COMPLETED') {
      const orderId = resource.supplementary_data.related_ids.order_id;
      const {billingId, paymentType} = JSON.parse(resource.custom_id);
      if (paymentType === PAYMENTS_TYPE.BILLING) {
        const billing = await models.Billings.findByPk(billingId);
        const paymentRegistered = await savePayPalPayment(PAYMENTS_TYPE.BILLING, orderId, billing.placeId, billing.id);
        billing.status = BILLINGS_STATUS.CONFIRMED;
        await billing.save();
        ElectronicInvoicing.generate(billing.placeId, paymentRegistered)
          .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
        Emails.dispatchPaymentByBillingConfirmed(paymentRegistered.id)
          .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
        Notifications.dispatchPayment(paymentRegistered.id)
          .catch(e => HandleError(req, res, new InternalProcessError(e, {payment: paymentRegistered.dataValues})));
      }
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

module.exports = router;
