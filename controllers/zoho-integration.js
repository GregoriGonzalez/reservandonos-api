const express = require('express');
const router = express.Router();
const models = require('../models');
const moment = require('moment');
const timezone = require('moment-timezone');
const {HandleError} = require("../helpers/error.helper");
const Op = models.Sequelize.Op;
const sequelize = models.Sequelize;

router.get('/v1/places', async (req, res) => {
  try {
    const result = await models.Place.findAll({
      include: [
        {
          model: models.OpeningDay,
          as: 'openingDays',
          attributes: ['weekday', 'start', 'end', 'start2', 'end2', 'smokeArea']
        },
        {model: models.Plan, as: 'plan', attributes: ['id', 'name']},
        {model: models.TypeFood, as: 'typeFood'}
      ]
    })
    res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/v1/places/:id', async (req, res) => {
  try {
    const result = await models.Place.findAll({
      include: [
        {
          model: models.OpeningDay,
          as: 'openingDays',
          attributes: ['weekday', 'start', 'end', 'start2', 'end2', 'smokeArea']
        },
        {model: models.Plan, as: 'plan', attributes: ['id', 'name']},
        {model: models.TypeFood, as: 'typeFood'}
      ],
      where: {
        id: req.params.id
      }
    });
    return res.status(200).json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/v1/customers', async (req, res) => {
  try {
    const result = await models.Costumer.findAndCountAll({
      attributes: [sequelize.where(sequelize.fn('DISTINCT', sequelize.col('email')), 'email'), 'email', 'id', 'fullname', 'countryCode', 'phone', 'notes', 'tags'],
      where: {
        [Op.and]: [
          {email: {[Op.not]: null}},
          {email: {[Op.ne]: ''}},
          {validEmail: 1},
          sequelize.where(sequelize.fn('CHAR_LENGTH', sequelize.col('email')), {[Op.gte]: 5})
        ]
      },
      offset: Number(req.query.start) || 0,
      limit: Number(req.query.end) || 100
    })
    return res.status(200).json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/v1/reservations', async (req, res) => {
  try {
    if (req.query.dateStart && req.query.dateEnd && (req.query.dateStart <= req.query.dateEnd)) {
      const reservations = await models.Reservation.findAll({
        include: [
          {
            model: models.Place,
            as: 'place',
            attributes: ['name'],
            paranoid: false
          }
        ],
        where: {
          date: {
            [Op.gte]: moment(req.query.dateStart).startOf('day'),
            [Op.lte]: moment(req.query.dateEnd).endOf('day')
          },
          [Op.or]: [
            {partnerId: 1},
            {origin: 'GOOGLE'}
          ]
        }
      })
      return res.status(200).json(reservations);
    } else {
      if (!req.query.dateStart) {
        return res.status(422).json({code: 422, msg: 'dateStart field is missing'});
      } else if (!req.query.dateEnd) {
        return res.status(422).json({code: 422, msg: 'dateEnd field is missing'});
      } else if (req.query.dateStart > req.query.dateEnd) {
        return res.status(422).json({code: 422, msg: 'the start date must be less than the end date'});
      }
    }
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/v1/locations', async (req, res) => {
  try {
    const result = await models.Place.findAll({
      attributes: ['id', 'name'],
      include: [
        {model: models.City, as: 'cities', attributes: ['name', 'id']},
        {model: models.State, as: 'states', attributes: ['name', 'id']}
      ]
    });
    return res.status(200).json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/v1/locations/:placeId', async (req, res) => {
  try {
    const result = await models.Place.findAll({
      attributes: ['id', 'name'],
      include: [
        {model: models.City, as: 'cities', attributes: ['name', 'id']},
        {model: models.State, as: 'states', attributes: ['name', 'id']}
      ],
      where: {
        id: req.params.placeId
      }
    });
    return res.status(200).json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});


module.exports = router;
