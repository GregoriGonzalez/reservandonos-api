const express = require('express');
const router = express.Router();
const models = require('../models');
const Op = models.Sequelize.Op;
const moment = require('moment');
const Redis = require('./../redis');
const {HandleError} = require("../helpers/error.helper");
const serviceHelper = require("../helpers/services.helper");
const {RESERVATION_STATUS} = require("../helpers/constants.helper")

router.get('/', async (req, res) => {
  try {
    const result = await models.Zone.findAndCountAll({
      where: {
        placeId: req.user.placeId,
        name: {
          [Op.like]: `%${req.query.term}%`
        }
      },
      order: [
        [req.query.sortBy, req.query.sortDir]
      ],
      offset: (+req.query.pageIndex * +req.query.pageSize) || 0,
      limit: +req.query.pageSize || 10
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/reservations/:placeId/:date/:serviceId', async (req, res) => {
  const reservations = await models.Reservation.findAll({
    attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'tableId', 'groupId', 'placeId'],
    where: {date: req.params.date, placeId: req.params.placeId, serviceId: req.params.serviceId},
    include: [
      {
        model: models.Costumer,
        as: 'costumer',
        attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
      },
      {
        model: models.ReservationSponsors,
        as: 'reservationSponsor',
        attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
      }
    ]
  });

  const reservationsWithIncompleteData = await models.Reservation.findAll({
    where: {
      placeId: req.params.placeId, date: req.params.date, [Op.or]: {
        serviceId: null,
        zoneId: null,
        [Op.and]: {tableId: null, groupId: null},
        start: null
      }
    },
    attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'createdAt'],
    include: [{
      model: models.Costumer,
      as: 'costumer',
      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
    }]
  });

  return res.json({reservations, reservationsWithIncompleteData});
});

router.get('/map/:placeId/:date/:serviceId', async (req, res) => {
  try {
    const data = await Redis.getRegister(`zone-${req.params.serviceId}-${req.params.placeId}`);
    if (data) {
      console.log('REDIS-ZoneByPlaceId');
      return res.json(data);
    }

    const serviceZones = await models.ServiceZone.findAll({
      where: {serviceId: req.params.serviceId, layoutId: {[Op.not]: null}},
      attributes: ['smokeArea', 'layoutId', 'zoneId'],
      include: [
        {model: models.Zone, as: 'zone', attributes: ['name', 'coordX', 'coordY']},
        {
          model: models.Layout, as: 'layout', attributes: ['id'],
          include: [
            {
              model: models.Table,
              as: 'tables',
              attributes: ['id', 'number', 'onlineSales', 'minCapacity', 'maxCapacity', 'coordX', 'coordY', 'tableHeightId', 'tableTypeId', 'isInclined', 'isVertical'],
            },
            {
              model: models.Group,
              as: 'groups', attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity']
            }
          ]
        },
      ]
    });

    if (serviceZones) {
      Redis.setRegister({serviceZones}, `zone-${req.params.serviceId}-${req.params.placeId}`);
    }

    return res.json({serviceZones});
  } catch (e) {
    HandleError(req, res, e);
  }
});


router.get('/:placeId/:date/:serviceId', async (req, res) => {
  try {
    const serviceZones = await models.ServiceZone.findAll({
      where: {serviceId: req.params.serviceId, layoutId: {[Op.not]: null}},
      attributes: ['smokeArea', 'layoutId', 'zoneId'],
      include: [
        {model: models.Zone, as: 'zone', attributes: ['name', 'coordX', 'coordY']},
        {
          model: models.Layout, as: 'layout', attributes: ['id'],
          include: [
            {
              model: models.Table,
              as: 'tables',
              attributes: ['id', 'number', 'onlineSales', 'minCapacity', 'maxCapacity', 'coordX', 'coordY', 'tableHeightId', 'tableTypeId', 'isInclined', 'isVertical'],
              include: [
                {
                  model: models.Reservation,
                  as: 'reservations',
                  required: false,
                  attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'tableId', 'placeId'],
                  where: {date: req.params.date, serviceId: req.params.serviceId, placeId: req.params.placeId},
                  include: [
                    {
                      model: models.Costumer,
                      as: 'costumer',
                      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
                    },
                    {
                      model: models.ReservationSponsors,
                      as: 'reservationSponsor',
                      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
                    }
                  ]
                }
              ]
            },
            {
              model: models.Group, as: 'groups', attributes: ['id', 'numbers', 'minCapacity', 'maxCapacity'], include: [
                {
                  model: models.Reservation,
                  as: 'reservations',
                  required: false,
                  attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'groupId'],
                  where: {date: req.params.date, serviceId: req.params.serviceId, placeId: req.params.placeId},
                  include: [
                    {
                      model: models.Costumer,
                      as: 'costumer',
                      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
                    },
                    {
                      model: models.ReservationSponsors,
                      as: 'reservationSponsor',
                      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
                    }
                  ]
                }
              ]
            }
          ]
        },
      ]
    });
    const reservationsWithIncompleteData = await models.Reservation.findAll({
      where: {
        placeId: req.params.placeId, date: req.params.date, [Op.or]: {
          serviceId: null,
          zoneId: null,
          [Op.and]: {tableId: null, groupId: null},
          start: null
        }
      },
      attributes: ['id', 'datetime', 'date', 'start', 'end', 'duration', 'status', 'peopleCount', 'isGroup', 'type', 'advancePaymentId', 'origin', 'peopleSeating', 'serviceId', 'zoneId', 'smokeArea', 'notes', 'notificationSms', 'notificationWhatsapp', 'partnerId', 'createdAt'],
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
      }]
    });
    return res.json({serviceZones, reservationsWithIncompleteData});
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/all/layouts', async (req, res) => {
  try {
    const result = await models.Zone.findAll({
      attributes: ['id', 'name'],
      include: [{
        model: models.Layout,
        as: 'layouts',
        attributes: ['id', 'name']
      }],
      where: {
        placeId: req.user.placeId
      }
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.get('/zones-layouts-tables', async (req, res) => {
  try {
    const placeId = req.user.placeId;
    const zones = await models.Zone.findAll({
      where: {placeId},
      attributes: ['id', 'name', 'coordX', 'coordY'],
      include: [{
        model: models.Layout,
        as: 'layouts',
        attributes: ['id', 'name'],
        include: [{
          model: models.Table,
          as: 'tables',
          attributes: ['id', 'number', 'isVertical', 'maxCapacity', 'coordX', 'coordY', 'tableHeightId', 'tableTypeId', 'isInclined', 'onlineSales']
        }],
        required: false
      }],
      required: false
    });
    const zoomLevel = await models.Place.findOne({where: {id: placeId}, attributes: ['zoom']});
    return res.json({zones, zoomLevel});
  } catch (e) {
    HandleError(req, res, e);
  }
})

router.get('/:id', async (req, res) => {
  try {
    const result = await models.Zone.findOne({
      where: {
        id: req.params.id,
        placeId: req.user.placeId
      },
    });
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.post('/', async (req, res) => {
  try {
    req.body.placeId = req.user.placeId;
    const result = await models.Zone.create(req.body);
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/', async (req, res) => {
  try {
    let result = await models.Zone.findOne({
      where: {
        id: req.body.id,
        placeId: req.user.placeId
      },
    });
    result = Object.assign(result, req.body);
    await result.save();
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json(result);
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.put('/update-zones-position', async (req, res) => {
  try {
    const zones = req.body;
    for (const zone of zones) {
      if (zone.newX || zone.newY) {
        const coordX = zone.newX ? zone.newX : zone.coordX;
        const coordY = zone.newY ? zone.newY : zone.coordY;
        await models.Zone.update({coordX, coordY}, {where: {id: zone.id}});
      }
    }
    serviceHelper.deleteServiceByPlaceId(req.user.placeId);
    return res.json('ok');
  } catch (e) {
    HandleError(req, res, e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const resCount = await models.Reservation.findAndCountAll({
      where: {
        zoneId: id,
        date: {[Op.gt]: moment().startOf('month').subtract(1, 'month').format('YYYY-MM-DD')},
        status: {[Op.in]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.SEATED]},
        placeId: req.user.placeId
      },
    });
    if (resCount.count > 0) {
      return res.status(403).json(resCount.rows);
    } else {
      const del = await models.Zone.destroy({where: {id}});
      await models.ServiceZone.destroy({where: {zoneId: id}});
      serviceHelper.deleteServiceByPlaceId(req.user.placeId);
      return res.json(del);
    }
  } catch (e) {
    HandleError(req, res, e);
  }
})

module.exports = router;
