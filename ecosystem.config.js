module.exports = {
  apps: [
    {
      name: "instance_cron",
      script: "./server.js",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      env: {
        "NODE_ENV": "production",
        "PORT": "3100",
        "CRON": "true"
      }
    },
    {
      name: "instance_google_webhook",
      script: "./server.js",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      env: {
        "NODE_ENV": "production",
        "PORT": "3000",
        "CRON": "false"
      }
    },
    {
      name: "instance_one",
      script: "./server.js",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      env: {
        "NODE_ENV": "production",
        "PORT": "3001",
        "CRON": "false"
      }
    },
    {
      name: "instance_two",
      script: "./server.js",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      env: {
        "NODE_ENV": "production",
        "PORT": "3002",
        "CRON": "false"
      }
    }
  ]
}
