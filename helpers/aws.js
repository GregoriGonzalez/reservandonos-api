const uuidv4 = require('uuid/v4');
const AWS = require('aws-sdk');
const env = require('../config/env');

AWS.config.accessKeyId = env.awsAccessKeyId;
AWS.config.secretAccessKey = env.awsSecretAccessKey;
AWS.config.region = env.awsRegion;

module.exports = {
  uploadFile: (folder, file) => {
    return new Promise((resolve, reject) => {
      const S3 = new AWS.S3();
      const fileNameParts = file.originalname.split('.');
      const ext = fileNameParts[fileNameParts.length - 1];
      const fileKey = `${folder}/${uuidv4()}.${ext}`;
      const params = {
        Bucket: env.awsBucket,
        Key: fileKey,
        ACL: 'public-read',
        ContentType: file.mimetype,
        Body: file.buffer
      };

      S3.putObject(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(fileKey);
        }
      });
    });
  },
  checkIfEmailWasValidated: email => {
    return new Promise((resolve, reject) => {
      const ses = new AWS.SESV2();
      const params = {
        EmailIdentity: email
      };
      try {
        ses.getEmailIdentity(params, function(err, data) {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        })
      } catch (error) {
        console.error(error)
        reject(error);
      }
    })
  }
};