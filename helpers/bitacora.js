const models = require('../models');
const moment = require('moment');
const {logging} = require("googleapis/build/src/apis/logging");
const Op = models.Sequelize.Op;

const moduleBitacora = {};

moduleBitacora.createBitacora = (operation, description, tableName, tableId, userId, placeId) => {
  models.Bitacora.create({
    operation, description, tableName, tableId, userId, placeId
  });
};

module.exports = {
  moduleBitacora
};
