const _ = require('lodash');
const models = require('../models');
const moment = require("moment/moment");
const {RESERVATION_ORIGIN, RESERVATION_STATUS} = require("./constants.helper");

/************************* Arithmetic Operations ****************************/

const sum = async (numberDecimal = 2, firstValue, ...numbers) => {
  let result = await number(2, firstValue);
  for (const num of numbers) {
    result = _.add(result, await number(2, num));
  }
  return _.ceil(result, numberDecimal);
};

const minus = async (numberDecimal = 2, firstValue, ...numbers) => {
  let result = await number(2, firstValue);
  for (const num of numbers) {
    result = _.subtract(result, await number(2, num));
  }
  return _.ceil(result, numberDecimal);
};

const multiply = async (numberDecimal = 2, firstValue, ...numbers) => {
  let result = await number(2, firstValue);
  for (const num of numbers) {
    result = _.multiply(result, await number(2, num));
  }
  return _.ceil(result, numberDecimal);
};

const divide = async (numberDecimal = 2, firstValue, ...numbers) => {
  let result = await number(2, firstValue);
  for (const num of numbers) {
    result = _.divide(result, await number(2, num));
  }
  return _.ceil(result, numberDecimal);
};

const number = async (numberDecimal = 2, value) => {
  if (!isNaN(Number(value))) {
    return _.ceil(value, numberDecimal);
  }
  throw new Error(`${value} is not number`);
};

/************************ Reservation Calculations ***************************/

const getChargeApplyToTheReservation = async (reservation, typeCharges = []) => {
  const origin = reservation.origin === RESERVATION_ORIGIN.GOOGLE ? RESERVATION_ORIGIN.GOOGLE : RESERVATION_ORIGIN.WEBSITE;
  if (typeCharges.length === 1) {
    return typeCharges[0];
  } else {
    const typeChargeThatApplyByDate = typeCharges?.filter(value => {
      const start = moment(value.PlacesTypeCharge?.start);
      const end = value.PlacesTypeCharge?.end ? moment(value.PlacesTypeCharge?.end) : moment();
      return start.isSameOrBefore(reservation.date) && end.isSameOrAfter(reservation.date);
    });
    return typeChargeThatApplyByDate.length === 1
      ? typeChargeThatApplyByDate[0]
      : typeChargeThatApplyByDate.find(value => value.origin === origin);
  }
}

const getCommissionByChargeOfReservation = async (reservation, typeCharges = [], fixedCharge = 0) => {
  if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status) && typeCharges.length > 0 && reservation.partnerId === 1) {
    if (reservation.invoiceAmount > 0) {
      return reservation.invoiceAmount;
    } else {
      const typeChargeSelected = await getChargeApplyToTheReservation(reservation, typeCharges);
      if (typeChargeSelected?.type === '%') {
        return await number(2, fixedCharge);
      } else if (typeChargeSelected?.type === '$') {
        return await multiply(2, reservation.peopleSeating > 0 ? reservation.peopleSeating : reservation.peopleCount, typeChargeSelected.value);
      }
    }
  }
  return 0;
}

const getCommissionBySponsorOfTheReservation = async (reservation) => {
  if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status) && reservation.partnerId === 1) {
    let commission = 0;
    for (const sponsor of (reservation.reservationSponsor ?? [])) {
      commission = await sum(2, commission, sponsor.totalCommission);
    }
    return commission;
  }
  return 0;
}

const invoiceByReservation = async (reservation, typeCharges = [], place, breakDown = false) => {
  if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status) && typeCharges.length > 0 && reservation.partnerId === 1) {
    const amount = await getCommissionByChargeOfReservation(reservation, typeCharges, place.fixedCharge);
    const commission = await getCommissionBySponsorOfTheReservation(reservation);
    return breakDown ? {bySponsor: commission, byCharge: amount} : await sum(2, amount, commission);
  } else {
    return breakDown ? {bySponsor: 0, byCharge: 0} : 0;
  }
};

const invoiceOfThePlaceForItsBookings = async (place, reservations = [], typeCharges = [], billing) => {
  const numberMonths = moment(billing?.dateEnd).diff(moment(billing?.dateStart), 'months', false) + 1;
  const commissionByCharges = {
    completed: (billing?.fixedChargeApplied * numberMonths) || 0
  };

  if (typeCharges.length === 0) {
    return {commissionByCharges};
  }
  for (const reservation of reservations) {
    if (billing.id === reservation.billingId) {
      commissionByCharges.completed = await sum(
        2,
        commissionByCharges.completed,
        await invoiceByReservation(reservation, typeCharges, place)
      );
    }
  }

  if (place?.hasElectronicBillingEnabled) {
    commissionByCharges.completed = await sum(2, commissionByCharges.completed, commissionByCharges.completed * billing.appliedTax)
  }
  return {commissionByCharges};
};

/**
 * @param reservations: models.Reservation[]
 * @param keySponsor: SPONSOR_KEYS
 * @returns {Promise<{quantity: number, commission: number}>}
 */
const unitsDeliveredBySponsor = async (reservations = [], keySponsor) => {
  const sponsor = await models.Sponsor.findOne({where: {key: keySponsor}});
  const data = {
    commission: 0,
    quantity: 0
  };
  if (sponsor) {
    for (const reservation of reservations) {
      if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED].includes(reservation.status) && reservation.partnerId === 1) {
        for (const rs of (reservation.reservationSponsor ?? [])) {
          if (rs.sponsorId === sponsor.id) {
            data.commission = await sum(2, data.commission, rs.totalCommission);
            data.quantity = await sum(2, data.quantity, rs.quantity);
          }
        }
      }
    }
  }
  return data;
};

module.exports = {
  sum,
  minus,
  multiply,
  divide,
  number,
  invoiceByReservation,
  invoiceOfThePlaceForItsBookings,
  unitsDeliveredBySponsor,
  getChargeApplyToTheReservation,
  getCommissionByChargeOfReservation,
  getCommissionBySponsorOfTheReservation
}
