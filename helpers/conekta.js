const conekta = require('conekta');
conekta.api_version = '2.0.0';
const env = require('../config/env');

const conektaModule = {
  createCustomer: (costumer, tokenId, privateKey) => {
    return new Promise((resolve, reject) => {
      conekta.api_key = privateKey;
      conekta.Customer.create({
        name: costumer.fullname,
        email: costumer.email,
        phone: `${costumer.countryCode}${costumer.phone}`,
        payment_sources: [{
          type: 'card',
          token_id: tokenId
        }]
      })
        .then(customer => resolve(customer.toObject()))
        .catch(err => reject(err));
    });
  },
  createPaymentSource: (costumerId, tokenId, privateKey) => {
    return new Promise((resolve, reject) => {
      conekta.api_key = privateKey;
      conekta.Customer.find(costumerId).then(customer => {
        customer.createPaymentSource({
          type: "card",
          token_id: tokenId
        })
          .then(paymentSource => resolve(paymentSource))
          .catch(err => reject(err));
      }).catch(err => reject(err));
    });
  },
  setDefaultPaymentSource: (custumerId, cardId, privateKey) => {
    return new Promise(async (resolve, reject) => {
      try {
        conekta.api_key = privateKey;
        const customer = await conekta.Customer.find(custumerId);
        await customer.update({
          default_payment_source_id: cardId
        });
        resolve(customer.toObject());
      } catch (err) {
        reject(err);
      }
    });
  },
  createOrder: (costumerId, cardId, privateKey, reservationId, cancellationFeePerPerson, peopleCount) => {
    return new Promise((resolve, reject) => {
      conekta.api_key = privateKey;
      conekta.Order.create({
        currency: 'MXN',
        customer_info: {
          customer_id: costumerId
        },
        line_items: [{
          name: 'Cargo por cancelación por persona',
          unit_price: cancellationFeePerPerson * 100,
          quantity: peopleCount
        }],
        metadata: {
          description: 'Política de cancelación de reservación',
          reference: reservationId
        },
        charges: [{
          payment_method: {
            type: 'card',
            payment_source_id: cardId
          }
        }]
      })
        .then(order => resolve(order.toObject()))
        .catch(err => reject(err));
    });
  },
  createUniqueCharge: (privateKey, paymentTokenId, amount, customerName, customerEmail, customerPhone) => {
    return new Promise((resolve, reject) => {
      conekta.api_key = privateKey;
      conekta.Order.create({
        currency: 'MXN',
        customer_info: {
          name: customerName,
          email: customerEmail,
          phone: customerPhone
        },
        line_items: [{
          name: 'Tarjeta de regalo',
          unit_price: amount * 100,
          quantity: 1
        }],
        metadata: {
          description: `Tarjeta de regalo para ${customerName}`,
          // reference: `gc-${}`
        },
        charges: [{
          payment_method: {
            type: 'card',
            token_id: paymentTokenId
          }
        }]
      })
        .then(order => resolve(order.toObject()))
        .catch(err => reject(err));
    });
  },
  createAdvanceCharge: (privateKey, cardId, amount, customerId) => {
    return new Promise(async (resolve, reject) => {
      try {
        conekta.api_key = privateKey;
        const order = await conekta.Order.create({
          currency: 'MXN',
          line_items: [{
            name: "Adelanto para reserva",
            unit_price: amount * 100,
            quantity: 1
          }],
          metadata: {
            description: `Adelanto para reserva`,
          },
          customer_info: {
            customer_id: customerId
          },
          charges: [{
            payment_method: {
              type: 'card',
              payment_source_id: cardId
            }
          }]
        });
        resolve(order.toObject());
      } catch (err) {
        reject(err);
      }
    });
  },

  plan: {
    get: (id) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = env.paymentGatewayPrivateKey;
        conekta.Plan.find(id, (err, plan) => {
          if (err && err.http_code != 404) return reject(err);
          return resolve(plan.toObject());
        });
      });
    },
    create: (planData) => {
      return new Promise((resolve, reject) => {
        conekta.Plan.create(planData, (err, res) => {
          if (err) return reject(err);
          return resolve(res.toObject());
        })
      });
    },
    update: (planData) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = env.paymentGatewayPrivateKey;
        conekta.Plan.find(planData.id, (err, plan) => {
          if (err) {
            return reject(err);
          }
          plan.update({
            name: planData.name,
            amount: (+planData.monthlyPlanPrice * 100)
          }, (err, res) => {
            if (err) {
              return reject(err);
            } else {
              return resolve(res.toObject());
            }
          });
        });
      });
    }
  },
  subscription: {
    create: (paymentCustomerId, planId, paymentCardId) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = env.paymentGatewayPrivateKey;
        conekta.Customer.find(paymentCustomerId, (err, customer) => {
          if (err) return reject(err);
          customer.createSubscription({
            plan: planId,
            card: paymentCardId
          }, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
          });
        });
      });
    },
    update: (paymentCustomerId, planId, paymentCardId) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = env.paymentGatewayPrivateKey;
        conekta.Customer.find(paymentCustomerId, (err, customer) => {
          if (err) return reject(err);
          customer.subscription.update({
            plan: planId,
            card: paymentCardId
          }, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
          });
        });
      });
    },
    cancel: (paymentCustomerId) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = env.paymentGatewayPrivateKey;
        conekta.Customer.find(paymentCustomerId, (err, customer) => {
          if (err) return reject(err);
          customer.subscription.cancel((err, res) => {
            if (err) return reject(err);
            return resolve(res);
          });
        });
      });
    }
  },
  customer: {
    get: (paymentCustomerId, privateKey) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = privateKey;
        conekta.Customer.find(paymentCustomerId, (err, customer) => {
          if (err && err.http_code != 404) return reject(err);
          // return resolve(customer.toObject());
          return resolve(customer);
        });
      });
    },
    create: (customerData) => {
      return new Promise((res, rej) => {
        conekta.api_key = env.paymentGatewayPrivateKey;
        conekta.Customer.create(customerData, (err, customer) => {
          if (err) return rej(err);
          return res(customer.toObject());
        })
      })
    },
    deleteCard: (customer, i) => {
      return new Promise((resolve, reject) => {
        const card = customer.payment_sources.get(i);
        console.log('delete -->', card)
        card.delete((err, res) => {
          if (err) return reject(err);
          return resolve(res);
        });
      });
    }
  },
  order: {
    get: (orderId, privateKey) => {
      return new Promise((resolve, reject) => {
        conekta.api_key = privateKey;
        conekta.Order.find(orderId, (err, order) => {
          if (err) return reject(err);
          return resolve(order.toObject());
        })
      });
    }
  }
}

module.exports = conektaModule;
