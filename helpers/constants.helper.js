const PAYMENTS_TYPE = {
  BILLING: 'BILLING'
};

const SPONSOR_PLACE_TYPE = {
  COMMISSION: 'COMMISSION'
};

const PAYMENTS_GATEWAY = {
  PAYPAL: 'PayPal',
  STRIPE: 'Stripe',
  EXTERNAL: 'External',
  CASH: 'Cash',
  OXXO: 'Oxxo'
};

const RESERVATION_STATUS = {
  NO_SHOW: 'NO_SHOW',
  CANCELLED: 'CANCELLED',
  PENDING: 'PENDING',
  CONFIRMED: 'CONFIRMED',
  RE_CONFIRMED: 'RE_CONFIRMED',
  ARRIVED: 'ARRIVED',
  SEATED: 'SEATED',
  GONE: 'GONE',
  WAIT_LIST: 'WAIT_LIST',
  WAIT_LIST_LOCAL: 'WAIT_LIST_LOCAL'
};

const RESERVATION_ORIGIN = {
  WEBSITE: 'WEBSITE',
  PHONE: 'PHONE',
  WALK_IN: 'WALK-IN',
  WHATSAPP: 'WHATSAPP',
  GOOGLE: 'GOOGLE',
  APP: 'APP',
  FACEBOOK: 'FACEBOOK',
  TWITTER: 'TWITTER',
  INSTAGRAM: 'INSTAGRAM'
};

const BILLINGS_STATUS = {
  CONFIRMED: 'CONFIRMED',
  VERIFIED: 'VERIFIED',
  LATE: 'LATE',
  PENDING: 'PENDING'
};

const BILLINGS_LABELS = {
  AWAITING_INVOICE: 'AWAITING_INVOICE',
  NEED_TO_UPDATE: 'NEED_TO_UPDATE',
  PENDING_PAYMENT: 'PENDING_PAYMENT',
  PAY_PER_DAY: 'PAY_PER_DAY',
  CLOSURE_NEW: 'CLOSURE_NEW'
};

const PAYMENTS_STATUS = {
  APPROVED: 'APPROVED',
  PROCESS: 'IN_PROCESS',
  DECLINED: 'DECLINED'
};

const PAYPAL_STATUS_ORDER = {
  CREATED: 'CREATED',
  APPROVED: 'APPROVED',
  VOIDED: 'VOIDED',
  COMPLETED: 'COMPLETED',
  PAYER_ACTION_REQUIRED: 'PAYER_ACTION_REQUIRED',
  SAVED: 'SAVED'
};

const STRIPE_STATUS_ORDER = {
  REQUIRES_PAYMENT_METHOD: 'REQUIRES_PAYMENT_METHOD',
  REQUIRES_CONFIRMATION: 'REQUIRES_CONFIRMATION',
  REQUIRES_ACTION: 'REQUIRES_ACTION',
  REQUIRES_SOURCE: 'REQUIRES_SOURCE',
  PROCESSING: 'PROCESSING',
  SUCCEEDED: 'SUCCEEDED',
  CANCELED: 'CANCELED'
};

const COUNTRIES = {
  MEXICO: 'Mexico'
};

const ROLES = {
  ADMIN: 'ADMIN',
  OWNER: 'OWNER',
  MANAGER: 'MANAGER',
  HOSTESS: 'HOSTESS',
  TELEMARKETING: 'TELEMARKETING',
  RECEPCIONIST: 'RECEPCIONIST',
  CASHIER: 'CASHIER',
  PARTNER: 'PARTNER',
  WAITLIST: 'WAITLIST',
  FASTFOOD: 'FASTFOOD',
  SPONSOR: 'SPONSOR'
};

const FOLDERS = {
  PROMOTIONS: 'place/{{placeId}}/promotion'
};

const CARD_TYPE = {
  CREDIT: 'CREDIT',
  DEBIT: 'DEBIT',
  PREPAID: 'PREPAID',
  UNKNOWN: 'UNKNOWN'
};

const TYPE_CHARGES = {
  WEBSITE: 'WEBSITE',
  GOOGLE: 'GOOGLE'
};

const SPONSOR_KEYS = {
  BOTTLE_COCA_COLA_WITHOUT_SUGAR: 'BOTTLE_COCA_COLA_WITHOUT_SUGAR'
};

const TEMPLATES_WHATSAPP = {
  PARTNER: {
    NEW: 'rsv_template_partner_new_reservation_v2',
    FORCE_CONFIRMED: 'rsv_template_partner_force_confirmed_reservation_v1',
    FORCE_CANCELLED: 'rsv_template_partner_force_cancelled_reservation_v1',
    UPDATE: 'rsv_template_partner_update_reservation_v1',
    CANCELLED: 'rsv_template_partner_cancel_reservation_v1',
    AUTO_REPLY: 'template_auto_reply_partner_v2'
  },
  CUSTOMER: {
    CONFIRMED: 'rsv_template_confirmed_customer_v2',
    CONFIRMED_SPONSOR: 'rsv_template_confirmed_customer_sponsor_v2',
    CANCELLED: 'rsv_template_cancelled_customer_v5',
    CANCELLED_SPONSOR: 'rsv_template_cancelled_customer_sponsor_v5',
    CANCELLED_FROM_CUSTOMER: 'rsv_template_cancelled_from_custumer_v1',
    CANCELLED_FROM_CUSTOMER_SPONSOR: 'rsv_template_cancelled_from_custumer_sponsor_v1',
    PENDIENTE: 'rsv_template_customer_reservation_process_v2',
    PENDIENTE_TW: 'rsv_template_customer_reservation_process_tw_v2',
    PENDIENTE_SPONSOR: 'rsv_template_customer_reservation_sponsor_process_v2', // rsv_template_customer_reservation_sponsor_process_v1
    PENDIENTE_SPONSOR_TW: 'rsv_template_customer_reservation_sponsor_process_tw_v2',
    PENDIENTE_WORK_SCHEDULES: 'rsv_template_customer_reservation_process_schedules_v2',
    PENDIENTE_WORK_SCHEDULES_TW: 'rsv_template_customer_reservation_process_schedules_tw_v2',
    PENDIENTE_WORK_SCHEDULES_SPONSOR: 'rsv_template_customer_reservation_sponsor_schedules_process_v2',
    PENDIENTE_WORK_SCHEDULES_SPONSOR_TW: 'rsv_template_customer_reservation_sponsor_schedules_process_tw_v1',
    REMEMBER_SPONSOR: 'rsv_template_reconfirmed_client_sponsor_v1',
    REMEMBER: 'rsv_template_reconfirmed_client_v1',
    REMEMBER_LATEST: 'rsv_template_reconfirmed_customer_latest_v2',
    AUTO_REPLY: 'template_auto_reply_v3',
    FRIEND: 'template_rsv_friend_v1',
    COUPON: 'template_coupon_v2'
  }
};


const TYPE_NOTIFICATION_BY_RESERVATION = {
  CAN_RES: 'CAN_RES',
  NEW_RES: 'NEW_RES',
  UPD_RES: 'UPD_RES',
};
const TYPE_NOTIFICATION_BY_BILLING = {
  LATE_CUT: 'LATE_CUT',
  NEW_CUT: 'NEW_CUT',
};
const TYPE_NOTIFICATION_BY_PAYMENTS = {
  PAID_CUT: 'PAID_CUT',
  REJECT_CUT: 'REJECT_CUT',
};
const TYPE_NOTIFICATION_BY_GIFT = {
  NEW_GIFT: 'NEW_GIFT'
};
const TYPE_NOTIFICATION_BY_COUPON = {
  NEW_COUPON: 'NEW_COUPON',
  REDEEMED_COUPON: 'REDEEMED_COUPON',
};
const TYPE_NOTIFICATION_BY_DATA = {
  ERROR_DATA: 'ERROR_DATA',
};
const TYPE_NOTIFICATION_BY_WAIT_LIST = {
  NEW_PEN: 'NEW_PEN',
}

const ACCOUNT_TYPE = {
  BOOKER: 'booker',
  PARTNER: 'rsv'
}

const NOTIFICATION_TYPE = {
  ...TYPE_NOTIFICATION_BY_RESERVATION,
  ...TYPE_NOTIFICATION_BY_BILLING,
  ...TYPE_NOTIFICATION_BY_PAYMENTS,
  ...TYPE_NOTIFICATION_BY_GIFT,
  ...TYPE_NOTIFICATION_BY_COUPON,
  ...TYPE_NOTIFICATION_BY_DATA,
  ...TYPE_NOTIFICATION_BY_WAIT_LIST
}

const ZOHO_STATUS_RESPONSE = {
  INVALID_TOKEN: 'INVALID_TOKEN'
}

const CANCELED_BY = {
  CUSTOMER: 'CUSTOMER',
  PARTNER: 'PARTNER',
  CALLCENTER: 'CALL-CENTER',
  NONE: 'NONE'
}

const GROUP_WHATSAPP_TELEGRAM = {
  MEXICO_CITY: {
    WHATSAPP: 'https://chat.whatsapp.com/JxwsRwPfS0lC0zwYf0o4t5',
    TELEGRAM: 'https://t.me/reservandonoscdmx',
    NAME: 'Ciudad de México'
  },
  CANCUN: {
    WHATSAPP: 'https://chat.whatsapp.com/EFZEDj9zVFLGyj2tN4WaET',
    TELEGRAM: 'https://t.me/+gWoREYsd4CI2MjBh',
    NAME: 'Cancún'
  },
  TULUM: {
    WHATSAPP: 'https://chat.whatsapp.com/Is3qSUZ7Zev1BLxQDJbXHs',
    TELEGRAM: 'https://t.me/+RzbMwjHbc20wNDc5',
    NAME: 'Tulum'
  },
  PCR: {
    WHATSAPP: 'https://chat.whatsapp.com/KuI8MPuQEvKC51VM4XCB1N',
    TELEGRAM: 'https://t.me/+N7nsT_-SWksxNzEx',
    NAME: 'Playa del Carmen'
  }
}

const ERROR_CODE_APP = {
  USER_WITH_NO_ASSOCIATED_PLACES: 'USER_WITH_NO_ASSOCIATED_PLACES',
  ACCESS_CODE_INVALID: 'ACCESS_CODE_INVALID',
}

const POINTS = {
  FIRST_RESERVE_WITH_REFERENCE: 150,
  NEW_USER: 50,
  UPLOAD_IMA_RED_SOCIAL: 50,
  REVIEW: 15,
  IMAGE_REVIEW: 10,
  EFFECTIVE_RESERVE: 50,
  EFFECTIVE_RESERVE_PLACE_SPECIAL: 100
}

module.exports = {
  ERROR_CODE_APP,
  NOTIFICATION_TYPE,
  TYPE_NOTIFICATION_BY_RESERVATION,
  TYPE_NOTIFICATION_BY_BILLING,
  TYPE_NOTIFICATION_BY_PAYMENTS,
  TYPE_NOTIFICATION_BY_GIFT,
  TYPE_NOTIFICATION_BY_COUPON,
  TYPE_NOTIFICATION_BY_DATA,
  TYPE_NOTIFICATION_BY_WAIT_LIST,
  TYPE_CHARGES,
  COUNTRIES,
  ROLES,
  PAYMENTS_TYPE,
  PAYMENTS_GATEWAY,
  RESERVATION_STATUS,
  RESERVATION_ORIGIN,
  BILLINGS_STATUS,
  BILLINGS_LABELS,
  PAYMENTS_STATUS,
  STRIPE_STATUS_ORDER,
  PAYPAL_STATUS_ORDER,
  SPONSOR_PLACE_TYPE,
  FOLDERS,
  CARD_TYPE,
  SPONSOR_KEYS,
  TEMPLATES_WHATSAPP,
  ZOHO_STATUS_RESPONSE,
  CANCELED_BY,
  GROUP_WHATSAPP_TELEGRAM,
  ACCOUNT_TYPE,
  POINTS
};
