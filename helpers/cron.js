const CronJob = require('cron').CronJob;
const models = require('../models');
const helpGoogle = require('./googleReserve');
const CutsModule = require('./cuts.helper');
const {emailModule: EmailModule} = require('./email.helper');
const {sendNotificationCustomers} = require('./notificationsCustomer');
const {moduleWhatsApp} = require('./toolsSendWhatsApp');
const Op = models.Sequelize.Op;
const moment = require('moment');
const {HandleError, InternalProcessError} = require("./error.helper");

module.exports.init = async () => {
  console.log('*****************************');
  console.log(' NODE ENV ', process.env.NODE_ENV);
  console.log('*****************************');
  if (process.env.NODE_ENV === 'production') {
    dailyMaintenance();
    loadFilesGoogleReserves();
    sendEmails();
    sendWhatsAppClientRemember();
    generateCuts();
    // generateAdvertency();
    sendEmailsCustomer();
  }
};

const sendEmailsCustomer = () => {
  const cron = new CronJob('0 */15 * * * *', () => {
    sendNotificationCustomers()
  }, null, true);
};

const loadFilesGoogleReserves = () => {
  // CronJob(cronTime, onTick, onComplete, start, timezone, context, runOnInit, unrefTimeout)
  // Seconds: 0-59
  // Minutes: 0-59
  // Hours: 0-23
  // Day of Month: 1-31
  // Months: 0-11 (Jan-Dec)
  // Day of Week: 0-6 (Sun-Sat)

  // Carga feeds de google myBusiness todos los días a las 01:20 am hora Mexico
  let cronJob = new CronJob('0 20 6 * * *', () => {
    let place = 'https://reservandonos.com/wp-json/webinfinitech/v1/get-data-places';
    let meta = 'https://reservandonos.com/wp-json/webinfinitech/v1/get-data-places-metas';
    let arrayPromise = [
      helpGoogle.preparedInfoBooker(),
      helpGoogle.preparedInfoWordpress(place),
      helpGoogle.preparedInfoWordpress(meta)
    ];

    Promise.all(arrayPromise).then(data => {
      console.log('promise all');
      let outputs = helpGoogle.validatePlaces(data[0], data[1], data[2], true);
    }, err => {
      console.log('err All Promises loadFilesGoogleReserves', err);
    });
  }, null, true);
}

const sendEmails = () => {
  const cron = new CronJob('0 */15 * * * *', () => {
    EmailModule.reviewEmail(true); // TODO tener la página de review dentro de bookercontrol.com
    EmailModule.reconfirmEmail(true);
  }, null, true);
};

const sendWhatsAppClientRemember = () => {
  const cron = new CronJob('0 */15 * * * *', () => {
    moduleWhatsApp.sendWhatsAppClientRemember(180, 'three');
    moduleWhatsApp.sendWhatsAppClientRemember(30, 'latest');
  }, null, true);
};

const dailyMaintenance = _ => new CronJob('0 0 8 * * *', async _ => { // correr diario, a las 8 am
                                                                      // console.log('deactivate places')
  const date = new Date();
  const todayDate = date.getDate();
  const todayMonth = date.getMonth() + 1;
  // TODO encontrar un query más eficiente
  const places = await models.Place.findAll({ // esto trae los lugares y si tiene, su pago del mes
    where: {isActive: true, payDay: {[Op.lt]: todayDate}},
    attributes: ['id', 'payDay', 'extraTimeDate'],
    include: [{
      model: models.PlacePayment,
      as: 'placePayment',
      limit: 1,
      order: [['id', 'DESC']],
      attributes: ['placeId', 'createdAt'],
      where: models.sequelize.where(models.sequelize.fn('MONTH', models.sequelize.col('createdAt')), todayMonth)
    }]
  });
  // for (const place of places) { // comentado temporalmente
  //   if (!place.placePayment.length) {
  //     const deactivateDay = place.payDay + place.extraTimeDate;
  //     if (deactivateDay < todayDate) {
  //       await models.Place.update({ isActive: 0 }, { where: { id: place.id } });
  //       EmailModule.placeChangeStatus(place.id);
  //     } else if (deactivateDay == todayDate) {
  //       EmailModule.willBeDeactivated(place.id);
  //     }
  //   }
  // }
  // delete shorted urls
  try {
    await models.Shorter.destroy({where: {delete_at: {[Op.lt]: date}}});
  } catch (error) {
    console.error(error);
  }
});

// Comprueba creaciones de cortes cada día a las 9 hora México
const generateCuts = () => {
  const cron = new CronJob('0 0 14 * * *', () => {
    CutsModule.generateCuts(moment().format('YYYY-MM-DD'))
      .catch(e => HandleError(null, null, new InternalProcessError(e)))
  }, null, true);
};

// Comprueba creaciones de cortes cada día a las 10 hora México
const generateAdvertency = () => {
  const cron = new CronJob('0 0 15 * * *', () => {
    CutsModule.updateRsvCutsLates();
  }, null, true);
};

// cada 15 mins, 3hrs antes de la reservacion
// const sendReconfirmEmails = async () => {
//   console.log('---------------->cron reconfirm<------------------')
//   const cron = new CronJob('0 */15 * * * *', () => {
//     EmailModule.reconfirmEmail();
//   }, null, true);
// };

// a las 6 am de la zona horaria
// const sendReconfirmEmails = async () => {
//   const timezones = await models.Timezone.findAll({attributes: ['code']});
//   timezones.forEach(tz => {
//     const cron = new CronJob('0 0 6 * * *', () => {
//       EmailModule.reconfirmEmail(tz.code);
//     }, null, true, tz.code);
//   });
// };
