const models = require('../models');
const Op = models.Sequelize.Op;
const moment = require('moment');
const {emailModule: Emails} = require('./email.helper');
const {
  RESERVATION_STATUS,
  BILLINGS_STATUS,
  PAYMENTS_STATUS,
  PAYMENTS_GATEWAY,
  PAYMENTS_TYPE,
  TYPE_NOTIFICATION_BY_BILLING
} = require("./constants.helper");
const ElectronicInvoicing = require('./electronicInvoicing.helper');
const {
  getOrder, getPaymentStatus, retrieveCustomer, createPaymentIntent, savePayment, getCustomer
} = require("./stripe.helper");
const Calculate = require("./calculate.helper");
const Notifications = require("./notifications.helper");
const {HandleError, InternalProcessError} = require("./error.helper");

const CutsModule = {};

/**
 * @param {String} dateStart
 * @param {String} dateEnd
 * @param {int} billingId
 * @param {int} placeId
 */
const updateRsv = async (dateStart, dateEnd, billingId, placeId) => {
  let rsv = await models.Reservation.findAll({
    attributes: ['id', 'peopleCount', 'peopleSeating', 'invoiceAmount', 'date', 'billingId', 'status'], include: [{
      model: models.Place, as: 'place', attributes: ['fixedCharge'], include: [{
        model: models.TypeCharge, as: 'typeCharges', attributes: ['description', 'type', 'value', 'id']
      }]
    }], where: {
      [Op.or]: [{billingId: billingId}, {
        [Op.and]: {
          date: {
            [Op.gte]: dateStart, [Op.lte]: dateEnd
          }, partnerId: {
            [Op.in]: [1, null]
          }, placeId: placeId
        }
      }]
    }
  });
  for (let item of rsv) {
    if ([RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED, RESERVATION_STATUS.RE_CONFIRMED, RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.PENDING].includes(item.status)) {
      if (!item.invoiceAmount || item.invoiceAmount === 0) {
        item.peopleSeating = item.peopleSeating != 0 ? item.peopleSeating : item.peopleCount;
        if (item.place.typeCharge.type === '%') {
          item.invoiceAmount = item.place.fixedCharge;
        } else {
          item.invoiceAmount = item.peopleSeating * item.place.typeCharge.value;
        }
        await item.update({
          peopleSeating: item.peopleSeating, invoiceAmount: item.invoiceAmount, status: RESERVATION_STATUS.SEATED
        });
      }
    }
  }
  return rsv;
}

CutsModule.updateRsvCutsLates = async () => {
  const now = moment().subtract(5, 'days');
  now.format('YYYY-MM-DD');

  let billings = await models.Billings.findAll({
    attributes: ['id', 'dateStart', 'dateEnd', 'status', 'placeId', 'createdAt'], where: {
      status: 'PENDING', dateEnd: {
        [Op.lte]: now.format('YYYY-MM-DD')
      }
    }, include: [{
      model: models.Place,
      as: 'place',
      attributes: ['name', 'id'],
      include: [{model: models.User, as: 'usersOfPlace', attributes: ['id', 'fullname', 'email'], required: true}]
    }]
  });

  for (let item of billings) {
    await item.update({status: 'LATE'});
    await updateRsv(item.dateStart, item.dateEnd, item.id, item.placeId);

    Notifications.dispatchBilling(TYPE_NOTIFICATION_BY_BILLING.LATE_CUT, item.id)
      .catch(e => HandleError(null, null, new InternalProcessError(e)));

  }
}

/**
 * @param {String} date
 * @param placeIds array de id para crear a sitios especificos
 */
CutsModule.generateCuts = async (date, placeIds = []) => {
  try {
    const day = moment(date).format('DD');
    if (['01', '16'].includes(day)) {
      const options = {
        attributes: ['id', 'name', 'taxToApply', 'monthlyPlanPrice'], include: [{
          model: models.BillingFrequency,
          as: 'billingFrequency',
          required: true,
          attributes: ['id', 'description', 'frequency'],
          where: {
            frequency: {
              [Op.in]: day === '01' ? [15, 30] : [15]
            }
          }
        }, {
          model: models.User,
          as: 'usersOfPlace',
          attributes: ['id', 'fullname', 'email'],
          required: false,
          through: {
            attributes: ['placeId', 'userId', 'notificationEmail', 'notificationWhatsApp']
          }
        }], where: {
          billingFrequencyId: {[Op.ne]: null},
          isActive: 1
        }, order: [['id', 'ASC']]
      };
      if (placeIds.length > 0) {
        options.where.id = {
          [Op.in]: placeIds
        }
      }
      const places = await models.Place.findAll(options);
      for (const place of places) {
        let dateStart = `${moment(date).subtract(1, 'day').startOf('month').format('YYYY-MM-DD')}`;
        const dateEnd = moment(date).subtract(1, 'day').format('YYYY-MM-DD');
        if (place.billingFrequency.frequency === 15 && day === '01') {
          dateStart = `${moment(date).subtract(1, 'day').format('YYYY-MM')}-16`;
        }

        const placeHasReservations = await models.Reservation.count({
          where: {
            date: {
              [Op.and]: {
                [Op.gte]: dateStart, [Op.lte]: dateEnd,
              }
            }, placeId: place.id, partnerId: 1
          }
        });
        if (placeHasReservations) {
          const billingCreated = await models.Billings.create({
            placeId: place.id,
            status: BILLINGS_STATUS.PENDING,
            dateStart,
            dateEnd,
            paymentUrl: null,
            appliedTax: place.taxToApply,
            fixedChargeApplied: place.monthlyPlanPrice
          });
          if (billingCreated) {
            await models.Reservation.update({billingId: billingCreated.id}, {
              where: {
                date: {
                  [Op.and]: {
                    [Op.gte]: billingCreated.dateStart, [Op.lte]: billingCreated.dateEnd,
                  }
                }, placeId: billingCreated.placeId, partnerId: 1
              }
            });

            Notifications.dispatchBilling(TYPE_NOTIFICATION_BY_BILLING.NEW_CUT, billingCreated.id)
              .catch(e => HandleError(null, null, new InternalProcessError(e, {})));

          }
        }
      }
    }
  } catch (e) {
    HandleError(null, null, new InternalProcessError(e, {date, placeIds}))
  }
}

/**
 * Comprobamos los cortes en status VERIFIED
 * que tengan pagos en IN_PROCESS
 */
CutsModule.checkCutPayments = async () => {
  const billings = await models.Billings.findAll({
    where: {
      status: BILLINGS_STATUS.VERIFIED
    },
    include: [
      {
        model: models.Payments,
        as: 'payment',
        required: true,
        where: {
          statusPayment: PAYMENTS_STATUS.PROCESS,
          paymentGateway: {
            [Op.in]: [PAYMENTS_GATEWAY.OXXO]
          }
        }
      }
    ]
  });
  for (const billing of billings) {
    const payment = billing.payment;
    const data = await getOrder(payment.transactionId);
    payment.statusPaymentGateway = data.status.toUpperCase();
    payment.statusPayment = await getPaymentStatus(PAYMENTS_GATEWAY.STRIPE, data.status);
    payment.save();

    if (payment.statusPayment === PAYMENTS_STATUS.APPROVED) {
      billing.status = BILLINGS_STATUS.CONFIRMED;
      await billing.save();
      ElectronicInvoicing.generate(billing.placeId, payment)
        .catch(e => HandleError(null, null, new InternalProcessError(e, {payment})));
      Emails.dispatchPaymentByBillingConfirmed(payment.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e, {payment})));
      Notifications.dispatchPayment(payment.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e, {payment})));
    } else if (payment.statusPayment === PAYMENTS_STATUS.PROCESS) {
      billing.status = BILLINGS_STATUS.VERIFIED;
      await billing.save();
    } else if (payment.statusPayment === PAYMENTS_STATUS.DECLINED) {
      billing.status = BILLINGS_STATUS.PENDING;
      await billing.save();
      Emails.dispatchPaymentByBillingReject(payment.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e, {payment})));
      Notifications.dispatchPayment(payment.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e, {payment})));
    }
  }
};

/**
 * Intentamos hacer el cobro automatico de un corte
 */
CutsModule.collectCutAutomatically = async (placeIds = []) => {
  const options = {
    where: {
      stripeCustomerId: {
        [Op.not]: null
      },
      stripeCollectionAuto: true,
      stripeHasDefaultCard: true
    },
    include: [
      {
        model: models.Billings,
        as: 'billings',
        required: true,
        where: {
          status: {
            [Op.in]: [BILLINGS_STATUS.PENDING, BILLINGS_STATUS.LATE]
          }
        },
        include: [
          {
            model: models.Reservation,
            as: 'reservation',
            required: true,
            where: {
              partnerId: 1,
              status: {
                [Op.in]: [RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED]
              }
            },
            include: [
              {
                model: models.ReservationSponsors,
                as: 'reservationSponsor',
                required: false
              }
            ]
          }
        ]
      },
      {
        model: models.TypeCharge,
        as: 'typeCharges',
        required: false
      },
      {
        model: models.SponsorPlaces,
        as: 'sponsorsPlaces',
        where: {
          type: 'COMMISSION'
        },
        required: false
      }
    ]
  };
  if (placeIds.length) {
    options.where.id = {[Op.in]: placeIds};
  }
  const places = await models.Place.findAll(options);
  for (const place of places) {
    const customer = await retrieveCustomer(place.stripeCustomerId);
    const paymentMethodId = customer.invoice_settings?.default_payment_method;
    if (paymentMethodId) {
      const billings = place.billings;
      const typeCharges = place.typeCharges;
      for (const billing of billings) {
        const reservations = billing?.reservation || [];
        const totalConfirmedOrReconfirmedReservations = await models.Reservation.count({
          where: {
            billingId: billing.id,
            partnerId: 1,
            status: {[Op.in]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED]}
          }
        });
        if (totalConfirmedOrReconfirmedReservations === 0) {
          const {commissionByCharges: {completed}} = await Calculate.invoiceOfThePlaceForItsBookings(place, reservations, typeCharges, billing);
          if (completed > 0) {
            const tax = await Calculate.sum(2, place.taxToApply, 1);
            const amount = place.hasElectronicBillingEnabled ? await Calculate.multiply(2, completed, tax, 100) : await Calculate.multiply(2, completed, 100);
            try {
              await createPaymentIntent({
                payment_method_types: ['card'],
                payment_method: paymentMethodId,
                amount,
                capture_method: 'automatic',
                currency: 'MXN',
                confirm: true,
                customer: place.stripeCustomerId,
                description: `${place.name} - Cobro Automático - Corte: ${billing.dateStart} / ${billing.dateEnd}`,
                off_session: true,
                metadata: {
                  billingId: billing.id, paymentGateway: PAYMENTS_GATEWAY.STRIPE, paymentType: PAYMENTS_TYPE.BILLING
                }
              });
            } catch (e) {
              HandleError(null, null, new InternalProcessError(e, {place: place.id, billing: billing.id, amount}))
            }
          }
        }
      }
    }
  }
};

CutsModule.getStatusByStatusPayment = (paymentStatus) => {
  if (paymentStatus === PAYMENTS_STATUS.APPROVED) {
    return BILLINGS_STATUS.CONFIRMED;
  } else if (paymentStatus === PAYMENTS_STATUS.PROCESS) {
    return BILLINGS_STATUS.VERIFIED;
  } else if (paymentStatus === PAYMENTS_STATUS.DECLINED) {
    return BILLINGS_STATUS.PENDING;
  }
}

module.exports = CutsModule;
