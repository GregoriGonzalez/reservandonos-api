const env = require('../config/env');
const authkey = env.dialog360ApiKey;
const axios = require('axios');

const urlBase = "https://waba.360dialog.io/v1/"

const moduleDialog360 = {}


conection = () => {
  return axios.create({
    baseURL: urlBase,
    timeout: 60000,
    headers: {
      'D360-Api-Key': authkey,
      'Content-Type': 'application/json'
    }
  })
}

moduleDialog360.verifyNumberPhone = (number) => {
  const myRe = /^\+\d{7,15}$/;
  return myRe.exec(number);
}

moduleDialog360.setWebhook = () => {
  let payload =  { "url": "https://your-webhook-adress" }
  let url = `${urlBase}configs/webhook`
}

moduleDialog360.health = async () => {
  let url = `health`;
  return await requestGet(url);
}

moduleDialog360.getNumberPhone = async () => {
  let url = `configs/phone_number`;
  return await requestGet(url);
}

moduleDialog360.sendMessageIndividual = async (phone, message) => {
    let url = 'messages';
    let body = {
        "preview_url": false,
        "recipient_type": "individual",
        "to": phone,
        "type": "text",
        "text": {
            "body": message
        }
    }
    return await requestPost(url, body);
}

moduleDialog360.sendAutoReply = async (templateName, to) => {
    let url = 'messages';
    let body = {
        "to": to,
        "type": "template",
        "template": {
            "namespace": "93cd6037_0242_428f_8074_def7822f7e77",
            "name": templateName,
            "language": {
                "policy": "deterministic",
                "code": "es_MX"
            }
        }
    };

    return await requestPost(url, body);
}

moduleDialog360.sendTemplateWhatsAppDefault = async (templateName, params, to) => {
  let url = `messages`;
  let body = {
      "to": to,
      "type": "template",
      "template": {
          "namespace": "93cd6037_0242_428f_8074_def7822f7e77",
          "name": templateName,
          "language": {
              "policy": "deterministic",
              "code": "es_MX"
          },
          "components": [
              {
                  "type" : "body",
                  "parameters": params
              }
          ]
      }
  };

  return await requestPost(url, body);
}

moduleDialog360.sendTemplateWhatpAppOneButton = async (templateName, params, to, code) => {
  let url = `messages`;
  let body = {
    "to": to,
    "type": "template",
    "template": {
      "namespace": "93cd6037_0242_428f_8074_def7822f7e77",
      "name": templateName, // "template_rsv_confirmation_v9",
      "language": {
        "policy": "deterministic",
        "code": "es_MX"
      },
      "components": [
        {
          "type" : "body",
          "parameters": params
        },
        {
          "type": "button",
          "sub_type" : "url",
          "index": "0",
          "parameters": [
            {
              "type": "text",
              "text": code
            }
          ]
        }
      ]
    }
  };

  return await requestPost(url, body);
}

moduleDialog360.sendTemplateWhatsAppIterativoOneButton = async (templateName, params, to, payload) => {
    let url = 'messages';
    let body = {
        "to": to,
        "type": "template",
        "template": {
            "namespace": "93cd6037_0242_428f_8074_def7822f7e77",
            "name": templateName,
            "language": {
                "policy": "deterministic",
                "code": "es_MX"
            },
            "components": [
                {
                    "type": "body",
                    "parameters": params
                },
                {
                    "type": "button",
                    "sub_type": "quick_reply",
                    "index": 0,
                    "parameters": [
                        {
                            "type": "payload",
                            "payload": payload// "reservation-CANCELLED-3334-DFWTS-500"
                        }
                    ]
                }
            ]
        }
    }

    return await requestPost(url, body);
}

moduleDialog360.sendTemplateWhatsAppIterativoTwoButtons = async (templateName, params, to, payloadOne, payloadTwo) => {
    let url = 'messages';
    let body = {
        "to": to,
        "type": "template",
        "template": {
            "namespace": "93cd6037_0242_428f_8074_def7822f7e77",
            "name": templateName,
            "language": {
                "policy": "deterministic",
                "code": "es_MX"
            },
            "components": [
                {
                    "type": "body",
                    "parameters": params
                },
                {
                    "type": "button",
                    "sub_type": "quick_reply",
                    "index": 0,
                    "parameters": [
                        {
                            "type": "payload",
                            "payload": payloadOne// "reservation-CANCELLED-3334-DFWTS-500"
                        }
                    ]
                },
                {
                    "type": "button",
                    "sub_type": "quick_reply",
                    "index": 1,
                    "parameters": [
                        {
                            "type": "payload",
                            "payload": payloadTwo // "reservation-CONFIRMED-3334-DFWTS-500"
                        }
                    ]
                }
            ]
        }
    }

    return await requestPost(url, body);
}

moduleDialog360.sendTemplateWhatsAppOneButton = async (templateName, params, to, code) => {
  let url = `messages`;
  let body = {
      "to": to,
      "type": "template",
      "template": {
          "namespace": "93cd6037_0242_428f_8074_def7822f7e77",
          "name": templateName, // "template_rsv_confirmation_v9",
          "language": {
              "policy": "deterministic",
              "code": "es_MX"
          },
          "components": [
              {
                  "type" : "body",
                  "parameters": params
              },
             {
                  "type": "button",
                  "sub_type" : "url",
                  "index": "0",
                  "parameters": [
                      {
                          "type": "text",
                          "text": code
                      }
                  ]
              }
          ]
      }
  };

  return await requestPost(url, body);
}

moduleDialog360.checkContacts = async (contacts) => {
  let url = `contacts`;
  let body = {
    "blocking": "wait",
    "contacts": contacts,
    "force_check": true
  };

  return await requestPost(url, body);
}

requestPost = (url, payload) => {
  return new Promise((resolve, reject) => {
    conection().post(url, payload)
    .then((response) => {
      resolve(response['data']);
    })
    .catch((error) => {
      reject(error);
    });
  })
};


requestGet = (url) => {
  return new Promise((resolve, reject) => {
    conection().get(url)
    .then(function (response) {
      resolve(response['data']);
  })
  .catch(function (error) {
    console.log('Err');
    reject(error);
  })
  })
};

module.exports = {
  moduleDialog360
}
