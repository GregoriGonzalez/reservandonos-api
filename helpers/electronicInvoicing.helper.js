const models = require("../models");
const {COUNTRIES, PAYMENTS_STATUS} = require("./constants.helper");
const Facturiapi = require('./facturapi.helper');
const {AppError} = require("./error.helper");

const factory = async (placeId) => {
  let result = {place: null, invoicing: null};
  const place = await models.Place.scope('withApplicationSettings').findByPk(placeId);
  if (place) {
    const {states: {country}} = place;
    const {applicationSetting} = country;
    if (applicationSetting.enableElectronicInvoicing) {
      switch (country.name) {
        case COUNTRIES.MEXICO:
          result = {place, invoicing: Facturiapi};
          break;
        default:
          result = {place, invoicing: null};
          break;
      }
    }
    return result;
  } else {
    return Promise.reject(new AppError('Place not found', AppError.TYPE_ERROR.RESOURCE_NOT_FOUND));
  }
}

const generateElectronicInvoicing = async (placeId, payment) => {
  const {place, invoicing} = await factory(placeId);
  if (invoicing && place?.hasElectronicBillingEnabled && payment.statusPayment === PAYMENTS_STATUS.APPROVED) {
    await invoicing.generate(place, payment);
  }
};

const downloadElectronicInvoicing = async (placeId, electronicInvoiceId, format) => {
  const {invoicing} = await factory(placeId);
  if (invoicing && electronicInvoiceId) {
    return await invoicing.download(electronicInvoiceId, format);
  }
};

const setCustomerInProvider = async (placeId) => {
  const {place, invoicing} = await factory(placeId);
  if (place && invoicing) {
    await invoicing.setCustomer(place);
  }
};

module.exports = {
  setCustomerInProvider,
  generate: generateElectronicInvoicing,
  download: downloadElectronicInvoicing
}
