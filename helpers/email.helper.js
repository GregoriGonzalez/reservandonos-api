const models = require('../models');
const Op = models.Sequelize.Op;
const moment = require('moment-timezone');
const AWS = require('aws-sdk');
const env = require('../config/env');
const encryption = require('./encryption');
const smsModule = require('./sms');
const ics = require('ics');
const {writeFile, unlink} = require('fs')
const jwt = require('jsonwebtoken');
const {moduleWhatsApp} = require('./toolsSendWhatsApp');
const {RESERVATION_STATUS, CARD_TYPE, PAYMENTS_STATUS, PAYMENTS_GATEWAY} = require('./constants.helper');
const nodemailer = require('nodemailer');
const utf8 = require('utf8');
const {schedulesPlaces} = require("../helpers/reservation.helper");
const {reservationConfirmedString} = require('../templates/emails/stringTemplates');
const {
  invitationRsvPartners,
  confirmEmail,
  newReservation,
  reservationConfirm,
  reservationCancelled,
  reservationConfirmManager,
  reservationCancelledManager,
  cutNotification,
  invoiceNotification,
  notificationPendingPayment,
  reservandonosReConfirmedReservationFromClient,
  resetPassword,
  resetPasswordSuccess
} = require('../templates/emails/reservandonos/base');
const i18n = require('../i18n/i18n').messages;

const {
  templateInProcessClient,
  templateRsvConfirmed,
  templateRsvCancelledPlace,
  templateRsvCancelledCustomers
} = require('../templates/emails/reservandonos/base-v1');
const path = require("path");
const fs = require("fs-extra");
const handlebar = require("handlebars");
const {HandleError, AppError, InternalProcessError} = require("./error.helper");
const PDF = require("./pdf.helper");

AWS.config.accessKeyId = env.awsAccessKeyId;
AWS.config.secretAccessKey = env.awsSecretAccessKey;
AWS.config.region = env.awsRegion;

const transporter = nodemailer.createTransport({SES: new AWS.SES()});

const sendTemplatedEmail = (emailFrom, emailTo, templateName, templateData) => {
  try {
    const SES = new AWS.SES();
    const params = {
      Source: utf8.encode(emailFrom),
      Template: templateName,
      ConfigurationSetName: 'ReservationsConfigSet',
      Destination: {
        ToAddresses: [emailTo]
      },
      TemplateData: templateData
    };
    SES.sendTemplatedEmail(params, (err, data) => {});
  } catch (e) {
    console.error(e.message, {emailFrom, emailTo, templateName, templateData});
  }
};

const sendBulkTemplatedEmail = (params) => {
  const SES = new AWS.SES();
  SES.sendBulkTemplatedEmail(params, (err, data) => {
    if (err) {
      console.error(err, err.stack);
    } else {
      console.log(data);
    }
  });
};

const getEmailTemplateName = (reservationStatus, planId, lang = 'es') => {
  if (planId === 1) {
    switch (reservationStatus) {
      case 'CANCELLED':
        return lang == 'es' ? 'ReservationOfflineCancelled' : 'ReservationOfflineCancelledEn';
      case 'PENDING':
        return lang == 'es' ? 'ReservationOfflinePending' : 'ReservationOfflinePendingEn';
      case 'CONFIRMED':
        return lang == 'es' ? 'ReservationOfflineConfirmed' : 'ReservationOfflineConfirmedEn';
    }
  } else {
    switch (reservationStatus) {
      case 'PENDING':
        return lang == 'es' ? 'ReservationOfflinePending' : 'ReservationOfflinePendingEn'; //'ReservationPayment2'; // TODO ¿por qué estaba el de payment ahí?
      case 'CONFIRMED':
        return lang == 'es' ? 'ReservationConfirmed2' : 'ReservationConfirmed2En';//'ReservationOnlineConfirmed';
      case 'RE_CONFIRMED':
        return lang == 'es' ? 'ReservationConfirmed2' : 'ReservationConfirmed2En'; // TODO no es el template correcto
      case 'WAIT_LIST':
        return 'WaitList';
      case 'CANCELLED':
        return lang == 'es' ? 'ReservationOnlineCancelled' : 'ReservationOnlineCancelledEn';
      case 'NO_SHOW':
        return lang == 'es' ? 'ReservationOnlineCancelled' : 'ReservationOnlineCancelledEn';
    }
  }
};

const themes = [
  {className: '', name: 'Default', font: 'Omnes', primary: '#eb3f58', accent: '#eb3f58'},
  {className: 'orange-blue', name: 'Fishers', font: 'Avenir', primary: '#264abc', accent: '#fe8300'},
  {className: 'brown-gray', name: 'Simons', primary: '#eb3f58'},
  {className: 'red-brown', name: 'Decrab', primary: '#eb3f58'},
  {className: 'yellow-red', name: 'Don Capitán', primary: '#eb3f58'},
  {className: 'black-gold', name: 'Península', primary: '#3d3835'},
  {className: 'black-white', name: 'La Leche', primary: '#000000'},
];

const primaryThemeColor = (className) => {
  const selectedTheme = themes.find(theme => theme.className == className);
  const primary = selectedTheme && selectedTheme.primary ? selectedTheme.primary : "#eb3f58";
  return primary;
}

const fillBulkEmail = (reservations, template) => {
  return {
    Source: reservations[0].partnerId ? `${reservations[0].partner.name}<${reservations[0].partner.email}>` : `${reservations[0].place.name}<${reservations[0].place.email}>`,
    Template: template,
    ConfigurationSetName: 'ReservationsConfigSet',
    Destinations: reservations.map(r => ({
      Destination: {
        ToAddresses: [r.costumer.email]
      },
      ReplacementTemplateData: JSON.stringify({
        reservationId: r.id,
        website: r.place.website ? r.place.website : null,
        address: r.place.address,
        logoImage: r.place.planId != 1 ? r.place.logoImage : (r.partner ? r.partner.logoImage : r.place.logoImage), // esto es temporal, en lo que fisher's actualiza su app
        primaryColor: r.place.secondaryColor,//primaryThemeColor(r.place.theme),
        name: r.costumer.fullname,
        place: r.place.name,
        date: moment(r.date).locale('es').format('dddd, D [de] MMMM [de] YYYY'),
        time: moment(r.date).add(r.start, 'minutes').format('H:mm'),
        peopleCount: r.peopleCount,
        smokeArea: r.smokeArea === null ? 'Cualquiera' : r.smokeArea ? 'Fumar' : 'No Fumar',
        partnerReservandonos: r.partnerId == 1 ? true : false,
        phone: r.partner ? r.partner.phone : r.place.phone,
        notes: r.notes ? r.notes : '',
        zone: r.zone ? r.zone.name : '',
        tableType: r.table && r.table.tableType ? r.table.tableType.name : '',
        tableHeight: r.table && r.table.tableHeight ? r.table.tableHeight.name : '',
        urlData: encryption.encrypt(JSON.stringify({
          reservationId: r.id,
          placeId: r.placeId,
          name: r.costumer.fullname,
          email: r.costumer.email
        }))
      })
    })),
    DefaultTemplateData: '{}'
  };
};

const cancelledPlace = (place, reservation, customer, cancelURL, sponsor, urlMenu) => {
  const body = templateRsvCancelledPlace(place, reservation, customer, cancelURL, sponsor, urlMenu);

  transporter.sendMail({
    from: `Reservándonos <hola@reservandonos.com>`,
    to: customer.email,
    subject: `¡No hay disponibilidad para este horario en ${place.name}!. Reservación (${reservation.id})`,
    text: `¡Lo sentimos!`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
};

const cancelledCustomer = (place, reservation, customer, cancelURL, sponsor, urlMenu) => {
  const body = templateRsvCancelledCustomers(place, reservation, customer, cancelURL, sponsor, urlMenu);

  transporter.sendMail({
    from: `Reservándonos <hola@reservandonos.com>`,
    to: customer.email,
    subject: `Tu reserva en ${place.name} se ha cancelado con éxito`,
    text: `Haz click en el link para confirmar.`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
};

const confirmed = (place, reservation, customer, cancelURL, sponsor, urlMenu) => {
  const body = templateRsvConfirmed(place, reservation, customer, cancelURL, sponsor, urlMenu);

  transporter.sendMail({
    from: `Reservándonos <hola@reservandonos.com>`,
    to: customer.email,
    subject: `Reserva en ${place.name} ha sido Confirmada (${reservation.id})`,
    text: `En hora buena.`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
};

const inProcess = (place, reservation, customer, cancelURL, sponsor, urlMenu, objTemplate) => {
  const body = templateInProcessClient(place, reservation, customer, cancelURL, sponsor, urlMenu, objTemplate);

  transporter.sendMail({
    from: `Reservándonos <hola@reservandonos.com>`,
    to: customer.email,
    subject: `Tu reserva en ${place.name} esta en proceso (${reservation.id})`,
    text: `Te confirmaremos tu reserva en breve.`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
};

const formatEmailData = (reservation, menuCoca = false, productSponsor = false, banner = 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/banner.jpg') => {
  let newStatus = 'modificada';
  if (reservation.status == 'CONFIRMED') {
    newStatus = 'confirmada';
  } else if (reservation.status == 'RE_CONFIRMED') {
    newStatus = 'reconfirmada';
  }
  return {
    reservationId: reservation.id,
    logoImage: reservation.partner ? reservation.partner.logoImage : reservation.place.logoImage, //reservation.place.logoImage,
    name: reservation.costumer.fullname,
    primaryColor: reservation.place.secondaryColor,//primaryThemeColor(reservation.place.theme),
    place: reservation.place.name,
    date: moment(reservation.date).locale('es').format('dddd, D [de] MMMM [de] YYYY'),
    time: reservation.start ? moment(reservation.date).add(reservation.start, 'minutes').format('H:mm') : '',
    peopleCount: reservation.peopleCount,
    smokeArea: reservation.smokeArea === null ? 'Cualquiera' : reservation.smokeArea ? 'Fumar' : 'No Fumar',
    phone: reservation.place.phone, //reservation.partner ? reservation.partner.phone : reservation.place.phone,
    notes: reservation.notes ? reservation.notes : '',
    zone: reservation.zone ? reservation.zone.name : 'Cualquiera',
    tableType: reservation.table && reservation.table.tableType ? reservation.table.tableType.name : 'Cualquiera',
    tableHeight: reservation.table && reservation.table.tableHeight ? reservation.table.tableHeight.name : 'Cualquiera',
    urlData: encryption.encrypt(JSON.stringify({reservationId: reservation.id})),
    address: reservation.place.address ? reservation.place.address : '',
    website: reservation.place.website ? reservation.place.website : '',
    facebook: reservation.place.facebook,
    instagram: reservation.place.instagram,
    twitter: reservation.place.twitter,
    newStatus,
    unsubscribeRes: `${env.dns}/#/email/${jwt.sign({reservationId: reservation.id}, env.jwtSecret, {expiresIn: '1w'})}`,
    unsubscribePlace: `${env.dns}/#/email/${jwt.sign({customerId: reservation.costumer.id}, env.jwtSecret, {expiresIn: '1w'})}`,
    bannerLinkTo: reservation.partnerId == 1 ? 'https://alaorden.coca-cola.com.mx/cuponeras-digitales.php' : null,
    bannerImageUrl: reservation.partnerId == 1 ? banner : null,
    primaryColor: reservation.place.primaryColor || '#F23D60',
    sponsorCocaUrl: reservation.partnerId == 1 && menuCoca ? menuCoca : null,
    productSponsor: reservation.partnerId == 1 && productSponsor ? true : null
  };
};


const emailModule = {};

/**
 * Send email with valet pdf to client
 * @returns {Promise<void>}
 */
emailModule.sendCoupon = async (couponId) => {
  try {
      const coupon = await models.Coupons.getDetails(couponId);
      if (coupon) {
        const {promotion} = coupon;
        const {place} = promotion;
        const {email} = coupon.getClient();
        const html = await PDF.buildTemplate('coupon', {
          place: {
            name: place.name,
            address: place.address,
            logo: place.logoImage,
          },
          promotion: {
            start: moment(promotion.start).format('DD-MM-YYYY'),
            end: moment(promotion.end).format('DD-MM-YYYY'),
            logo: promotion.imageUrl,
            title: promotion.title
          },
          coupon: {
            code: coupon.code
          }
        });
        await transporter.sendMail({
          from: `Nuevo cupón  <hola@reservandonos.com>`,
          to: email,
          subject: `Código de cupón ${coupon.code}`,
          text: `Presenta el código para redimir la promoción`,
          html,
        });
      }
  } catch (e) {
    HandleError(null, null, new InternalProcessError(e))
  }
}

emailModule.recurrentCardEmail = async placeId => {
  if (!isNaN(placeId)) {
    placeId = placeId.toString();
  }
  const bookerControlLogo = "https://s3-us-west-2.amazonaws.com/sistema.reservandonos.com/assets/images/logo.png";
  const urlData = encryption.encrypt(placeId);
  const place = await models.Place.findByPk(placeId, {attributes: ['monthlyPlanPrice', 'paymentEmail']});
  const monthlyPlanPrice = place.monthlyPlanPrice;
  const phone = "+52 1 55 7500 1302";
  const tags = {bookerControlLogo, urlData, monthlyPlanPrice, phone};
  const emailFrom = "hola@bookercontrol.com";
  const emailTo = place.paymentEmail;
  sendTemplatedEmail(emailFrom, emailTo, 'CardForRecurrent', JSON.stringify(tags));
}

emailModule.reservationEmail = async (reservationId, isUpdated = false, isClient = false) => {
  const reservation = await models.Reservation.findByPk(reservationId, {
    include: ['costumer', 'zone', 'partner',
      {
        model: models.Place,
        as: 'place',
        attributes: {include: ['planId', 'langEmail', 'name', 'address', 'phone', 'id', 'stateId', 'colonyId']},
        include: ['sponsorsPlaces']
      },
      {
        model: models.Table,
        as: 'table',
        include: ['tableType', 'tableHeight']
      }
    ]
  });

  if (reservation) {
    let menuCoca = undefined;
    let productSponsor = undefined;
    let banner = 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/banner.jpg';
    let exclusivePlaces = [674, 1600, 188, 1253, 320, 319, 1654, 1537, 1538, 1539]; // No enviar mensajes con informacion de sponsor a clientes.

    if (!exclusivePlaces.includes(reservation.place.id)) {
      for (let item of reservation.place.sponsorsPlaces) {
        if (item.sponsorId == 1 && item.isActive) {
          menuCoca = `${item.urlBase}/${item.placeId}.pdf`;
        }

        if (item.sponsorId == 2 && item.isActive && [RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.PENDING, RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED].includes(reservation.status)) { // Sponsor-CocaCola
          productSponsor = true;
          banner = 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/Banner-Web-Coca-Cola.png';
        }
      }
    }


    const emailData = formatEmailData(reservation, menuCoca, productSponsor, banner);
    const emailFrom = `${reservation.place.name}<${reservation.place.email}>`;
    if (reservation.notificationEmail && (reservation.costumer.email && reservation.costumer.validEmail && reservation.costumer.emailComplaints < 3 && reservation.costumer.subscribedEmails) || reservation.partnerId == 1) {
      const templateName = isUpdated ? 'ReservationUpdated' : getEmailTemplateName(reservation.status, reservation.place.planId, reservation.place.langEmail);
      const templateData = JSON.stringify(emailData);

      // *** Envio de Email a Cliente *** //
      if (reservation['partnerId'] == 1 && reservation.place.langEmail == 'es') {
        reservation.hour = emailData.time;
        let chars = [..."ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
        const short = [...Array(6)].map(i => chars[Math.random() * chars.length | 0]).join``;
        const long = encryption.encrypt(JSON.stringify({reservationId: reservation.id}));
        const twoWeeksLater = moment(reservation.date).add(1, 'days');

        const cancelUrl = `${env.dns}/#/rsv/reservation-client-cancelled/${long}`;

        if (reservation.status == RESERVATION_STATUS.PENDING) {
          await models.Shorter.create({short, long, type: RESERVATION_STATUS.CONFIRMED, delete_at: twoWeeksLater});
          const objTemplate = await schedulesPlaces(reservation.place.id, reservation.place.timezone);
          inProcess(reservation.place, reservation, reservation.costumer, cancelUrl, productSponsor ? true : false, menuCoca, objTemplate);
        }

        if (reservation.status == RESERVATION_STATUS.CONFIRMED || reservation.status == RESERVATION_STATUS.RE_CONFIRMED) {
          await models.Shorter.create({short, long, type: RESERVATION_STATUS.CONFIRMED, delete_at: twoWeeksLater});
          confirmed(reservation.place, reservation, reservation.costumer, cancelUrl, productSponsor ? true : false, menuCoca);
        }

        if (reservation.status == RESERVATION_STATUS.CANCELLED) {
          if (isClient) {
            cancelledCustomer(reservation.place, reservation, reservation.costumer, cancelUrl, productSponsor ? true : false, false);
          } else {
            cancelledPlace(reservation.place, reservation, reservation.costumer, cancelUrl, productSponsor ? true : false, false);
          }
        }

        // Envia notificacion de whatsApp cliente
        if ([RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.PENDING, RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED].includes(reservation.status)) {
          reservation.hour = reservation.start ? moment(reservation.date).add(reservation.start, 'minutes').format('H:mm') : '';
          moduleWhatsApp.notificationClient(reservation, reservation.status, productSponsor ? true : false, isClient);
        }
      } else {
        sendTemplatedEmail(emailFrom, reservation.costumer.email, templateName, templateData);
      }

      // *** Envio de email para el equipo del callCenter *** //
      if (reservation['partnerId'] == 1 && reservation.status == RESERVATION_STATUS.CANCELLED) {
        const emailToReservandons = reservation.partner ? reservation.partner.email : 'hola@reservandonos.com';
        sendTemplatedEmail(emailFrom, emailToReservandons, templateName, templateData);
      }
    } else {
      if (reservation['partnerId'] == 1 && [RESERVATION_STATUS.CANCELLED, RESERVATION_STATUS.PENDING, RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED].includes(reservation.status)) {
        reservation.hour = reservation.start ? moment(reservation.date).add(reservation.start, 'minutes').format('H:mm') : '';
        moduleWhatsApp.notificationClient(reservation, reservation.status, productSponsor ? true : false, isClient);
      }
    }
  }
};

emailModule.advPayCancPolEmail = async (reservationId, amount) => {
  const reservation = await models.Reservation.findByPk(reservationId, {
    include: ['costumer', 'zone', 'partner', {
      model: models.Place,
      as: 'place',
      attributes: {include: ['planId']},
    },
      {
        model: models.Table,
        as: 'table',
        include: ['tableType', 'tableHeight']
      }]
  });
  const templateName = reservation.paymentCardId ? "CancellationPolicyConfirmed2" : "AdvancePaymentConfirmed2";
  let emailData = formatEmailData(reservation);
  emailData['amount'] = amount;
  emailData['unsubscribe'] = `${env.dns}`; // 'http://bookercontrol.com'
  const emailFrom = reservation.partner ? `${reservation.partner.name}<${reservation.partner.email}>` : `${reservation.place.name}<${reservation.place.email}>`;
  const templateData = JSON.stringify(emailData);
  sendTemplatedEmail(emailFrom, reservation.costumer.email, templateName, templateData);
}

emailModule.cardInfoEmail = async (reservation, place, customer) => {
  // console.log('card info email', place)
  const urlData = encryption.encrypt(JSON.stringify({reservationId: reservation.id}));
  if (reservation.notificationSms && customer.phone) {
    smsModule.cardInfoSMS(reservation.date, place.name, customer.countryCode + customer.phone, urlData);
  }
  const templateData = {
    logoImage: reservation.partner ? reservation.partner.logoImage : place.logoImage,
    date: moment(reservation.date).locale('es').format('dddd, D [de] MMMM [de] YYYY'),
    time: reservation.start ? moment(reservation.date).add(reservation.start, 'minutes').format('H:mm') : '',
    peopleCount: reservation.peopleCount,
    name: customer.fullname,
    place: place.name,
    urlData,
    phone: reservation.partner ? reservation.partner.phone : place.phone,
    address: place.address,
    website: place.website,
    facebook: place.facebook,
    instagram: place.instagram,
    twitter: place.twitter,
    primaryColor: place.secondaryColor || '#eb3f58',
  }
  const emailFrom = reservation.partner ? `${reservation.partner.name}<${reservation.partner.email}>` : `${place.name}<${place.email}>`;
  sendTemplatedEmail(emailFrom, customer.email, 'ReservationPayment2', JSON.stringify(templateData));
}

emailModule.newReservationEmail = async (reservationId) => { // correo dirigido al restaurante
  const reservation = await models.Reservation.findByPk(reservationId, {
    include: ['costumer', 'partner', {
      model: models.Place,
      as: 'place'
    }]
  });
  const emailData = {
    reservationId: reservation.id,
    logoImage: reservation.partner ? reservation.partner.logoImage : reservation.place.logoImage,
    place: reservation.place.name,
    date: moment(reservation.date).locale('es').format('dddd, D [de] MMMM [de] YYYY'),
    time: moment(reservation.date).add(reservation.start, 'minutes').format('H:mm'),
    peopleCount: reservation.peopleCount,
    smokeArea: reservation.smokeArea === null ? 'Cualquiera' : reservation.smokeArea ? 'Fumar' : 'No Fumar',
    name: reservation.costumer.fullname,
    phone: reservation.costumer.phone ? `${reservation.costumer.countryCode} ${reservation.costumer.phone}` : '',
    email: reservation.costumer.email ? reservation.costumer.email : '',
    notes: reservation.notes ? reservation.notes : '',
  };
  let emailFrom;
  if (reservation.partner?.email && reservation.partner?.email !== '') {
    emailFrom = `${reservation.partner.name}<${reservation.partner.email}>`;
  } else if (reservation.place?.email && reservation.place?.email !== '') {
    emailFrom =  `${reservation.place.name}<${reservation.place.email}>`;
  } else {
    emailFrom =  'Nueva reserva <nueva-reserva@bookercontrol.com>';
  }
  const emailTo = 'hola@reservandonos.com';
  const templateData = JSON.stringify(emailData);
  sendTemplatedEmail(emailFrom, emailTo, 'NewReservation', templateData);
};

emailModule.reviewEmail = (send = true) => {
  const min = Math.floor(moment().minute() / 15) * 15;
  const mmt = moment().minute(min).startOf('minute');

  return new Promise((resolve, reject) => {
    models.Reservation.findAll({
      attributes: ['id', 'placeId', 'partnerId', 'date', 'start', 'peopleCount', 'smokeArea', 'notes', 'notificationSms'],
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: ['fullname', 'email', 'phone', 'countryCode'],
        where: {validEmail: 1, emailComplaints: {[Op.lt]: 3, email: {[Op.not]: null}}}
      }, {
        model: models.Zone,
        as: 'zone',
        attributes: ['name']
      }, {
        model: models.Partner,
        as: 'partner',
        attributes: ['name', 'email', 'phone', 'logoImage']
      }, {
        model: models.Place,
        as: 'place',
        attributes: ['name', 'email', 'phone', 'logoImage', 'planId', 'website', 'address'],
      }, {
        model: models.Table,
        as: 'table',
        include: [{
          model: models.TableType,
          as: 'tableType',
          attributes: ['name']
        }, {
          model: models.TableHeight,
          as: 'tableHeight',
          attributes: ['name']
        }]
      }],
      where: {
        reviewed: false,
        status: {
          [Op.or]: ['CONFIRMED', 'RE_CONFIRMED', 'ARRIVED', 'SEATED', 'GONE']
        },
        datetime: {
          [Op.or]: [{
            [Op.gt]: moment(mmt).subtract(3, 'hours').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm'),
            [Op.lte]: moment(mmt).subtract(3, 'hours').format('YYYY-MM-DD HH:mm')
          },
            {
              [Op.gt]: moment(mmt).subtract(12, 'hours').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm'),
              [Op.lte]: moment(mmt).subtract(12, 'hours').format('YYYY-MM-DD HH:mm')
            }]
        }
      }
    }).then(reservations => {
      const uniquePartners = [...new Set(reservations.map(r => r.partnerId))];
      const bulks = [];
      uniquePartners.forEach(partnerId => {
        const partnerReservations = reservations.filter(r => r.partnerId === partnerId);
        if (partnerId) {
          bulks.push(fillBulkEmail(partnerReservations, 'Review'));
        } else {
          const uniquePlaces = [...new Set(partnerReservations.map(r => r.placeId))];
          uniquePlaces.forEach(placeId => {
            const placeReservations = partnerReservations.filter(r => r.placeId === placeId);
            bulks.push(fillBulkEmail(placeReservations, 'Review'));
          });
        }
      });
      if (send) {
        bulks.forEach(params => {
          sendBulkTemplatedEmail(params);
        });
      } else {
        return resolve(bulks);
      }
    });
  });
};

emailModule.reconfirmEmail = (send = true) => {
  const min = Math.floor(moment().minute() / 15) * 15;
  const mmt = moment().minute(min).startOf('minute');
  return new Promise((resolve, reject) => {
    models.Reservation.findAll({
      attributes: ['id', 'placeId', 'partnerId', 'date', 'start', 'peopleCount', 'smokeArea', 'notes', 'notificationSms'],
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: ['fullname', 'email', 'phone', 'countryCode'],
        where: {validEmail: 1, emailComplaints: {[Op.lt]: 3, email: {[Op.not]: null}}}
      }, {
        model: models.Zone,
        as: 'zone',
        attributes: ['name']
      }, {
        model: models.Partner,
        as: 'partner',
        attributes: ['name', 'email', 'phone', 'logoImage']
      }, {
        model: models.Place,
        as: 'place',
        attributes: ['name', 'email', 'phone', 'logoImage', 'planId', 'website', 'address', 'theme'],
        where: {
          // timezone: timezone,
          planId: {
            [Op.ne]: 1
          }
        },
        required: true
      }, {
        model: models.Table,
        as: 'table',
        include: [{
          model: models.TableType,
          as: 'tableType',
          attributes: ['name']
        }, {
          model: models.TableHeight,
          as: 'tableHeight',
          attributes: ['name']
        }]
      }],
      where: {
        status: 'CONFIRMED',
        datetime: //moment(mmt).add(3, 'hours').format('YYYY-MM-DD HH:mm')
          {
            [Op.and]: {
              [Op.gte]: moment(mmt).add(3, 'hours').format('YYYY-MM-DD HH:mm'),
              [Op.lte]: moment(mmt).add(3, 'hours').add(15, 'minutes').format('YYYY-MM-DD HH:mm')
            }
          }
        // date: moment().tz(timezone).format('YYYY-MM-DD')
      }
    }).then(reservations => {
      const uniquePartners = [...new Set(reservations.map(r => r.partnerId))];
      const bulks = [];
      uniquePartners.forEach(partnerId => {
        const partnerReservations = reservations.filter(r => r.partnerId === partnerId);
        if (partnerId) {
          bulks.push(fillBulkEmail(partnerReservations, 'ReservationOnlineReconfirm2'));
        } else {
          const uniquePlaces = [...new Set(partnerReservations.map(r => r.placeId))];
          uniquePlaces.forEach(placeId => {
            const placeReservations = partnerReservations.filter(r => r.placeId === placeId);
            bulks.push(fillBulkEmail(placeReservations, 'ReservationOnlineReconfirm2'));
          });
        }
      });
      if (send) {
        bulks.forEach(params => {
          sendBulkTemplatedEmail(params);
        });
      } else {
        return resolve(bulks);
      }
      reservations.forEach(reservation => {
        if (reservation.notificationSms && reservation.costumer.phone) {
          smsModule.reconfirm(reservation);
        }
      });
    });
  });
};

emailModule.giftCardEmail = async (giftCardId) => {
  const giftCard = await models.GiftCard.findByPk(giftCardId, {
    include: ['place']
  });
  const emailData = {
    giftCardId: giftCard.id,
    logoImage: giftCard.place.logoImage,
    place: giftCard.place.name,
    name: giftCard.beneficiaryName,
    amount: giftCard.amount,
    token: giftCard.token,
    phone: giftCard.place.phone
  };
  const validatedMailFrom = giftCard.place.email ? giftCard.place.email : 'hola@reservandonos.com';
  const emailFrom = `${giftCard.place.name} <${validatedMailFrom}>`;
  const emailTo = giftCard.email;
  const templateData = JSON.stringify(emailData);
  sendTemplatedEmail(emailFrom, emailTo, 'GiftCard', templateData);
};

emailModule.passwordReset = async (user) => {
  const reservandonos = await models.Partner.findByPk(1);
  const emailData = {
    logoImage: reservandonos.logoImage,
    name: user.fullname,
    phone: reservandonos.phone,
    resetToken: encryption.encrypt(JSON.stringify({
      userId: user.id,
      dueDate: moment().add(1, 'day').utc().format()
    }))
  };
  const emailFrom = `${reservandonos.name}<${reservandonos.email}>`;
  const emailTo = user.email;
  const templateData = JSON.stringify(emailData);
  sendTemplatedEmail(emailFrom, emailTo, 'PasswordReset', templateData);
};

emailModule.verifyEmailIdentity = async (emailAddress) => {
  const SES = new AWS.SES();

  SES.verifyEmailIdentity({EmailAddress: emailAddress}, (err, data) => {
    if (err) {
      throw new AppError(err.message, AppError.TYPE_ERROR.INTERNAL);
    } else {
      console.log(data);
    }
  });
};

emailModule.subscriptionOp = async (paymentCustomerId, failed = false) => {
  const place = await models.Place.findOne({
    where: {paymentCustomerId},
    attributes: ['name', 'paymentEmail', 'monthlyPlanPrice']
  });
  if (place) {
    const emailFrom = "hola@bookercontrol.com";
    const emailTo = place.paymentEmail;
    const bookerControlLogo = "https://s3-us-west-2.amazonaws.com/sistema.reservandonos.com/assets/images/logo.png";
    const phone = "+52 1 55 7500 1302";
    let message = "";
    let templateName = "";
    if (failed) {
      templateName = "SubscriptionFailed";
      message = `Hola. Ocurri\u00F3 un problema al tratar de hacer el pago recurrente para el servicio de ${place.name}. Por favor, ponte en contacto con nosotros.`;
    } else {
      templateName = "SubscriptionPaid";
      message = `Hola. Se realiz\u00F3 el cargo mensual de $${place.monthlyPlanPrice} para el sistema Booker Control en ${place.name}. \u00A1Gracias!`;
    }
    const tags = {bookerControlLogo, message, phone};
    sendTemplatedEmail(emailFrom, emailTo, templateName, JSON.stringify(tags));
  }
};

emailModule.placeChangeStatus = async (id) => {
  try {
    const place = await models.Place.findByPk(id, {attributes: ['name', 'paymentEmail', 'isActive']});
    if (place) {
      const bookerControlLogo = "https://s3-us-west-2.amazonaws.com/sistema.reservandonos.com/assets/images/logo.png";
      let templateName = '';
      let message = '';
      if (place.isActive) {
        templateName = 'SubPlaceActivated';
        message = `Hola.\nEl servicio de Booker Control para ${place.name} ha sido activado.`;
      } else {
        templateName = 'SubPlaceDeactivated';
        message = `Hola.\nEl servicio de Booker Control para ${place.name} ha sido desactivado.`;
      }
      const phone = "+52 1 55 7500 1302";
      const emailFrom = "hola@bookercontrol.com";
      const emailTo = place.paymentEmail;
      const tags = {bookerControlLogo, message, phone};
      sendTemplatedEmail(emailFrom, emailTo, templateName, JSON.stringify(tags));
    }
  } catch (error) {
    console.error(error);
  }
}

emailModule.willBeDeactivated = async id => {
  try {
    const place = await models.Place.findByPk(id, {attributes: ['name', 'paymentEmail', 'isActive']});
    if (place) {
      const bookerControlLogo = "https://s3-us-west-2.amazonaws.com/sistema.reservandonos.com/assets/images/logo.png";
      const templateName = 'SubWillDeactivate';
      const message = `Hola. Seguimos teniendo problemas para realizar el cobro mensual de ${place.name} y el servicio se detendrá mañana. Por favor, ponte en contacto con nosotros para resolver el problema.`;
      const phone = "+52 1 55 7500 1302";
      const emailFrom = "hola@bookercontrol.com";
      const emailTo = place.paymentEmail;
      const tags = {bookerControlLogo, message, phone};
      sendTemplatedEmail(emailFrom, emailTo, templateName, JSON.stringify(tags));
    }
  } catch (error) {
    console.error(error);
  }
}

emailModule.rawWithAttachment = async id => {
  try {
    const reservation = await models.Reservation.findOne({
      where: {id},
      attributes: ['id', 'date', 'start', 'end', 'peopleCount', 'partnerId', 'status'],
      include: [{
        model: models.Costumer,
        as: 'costumer',
        attributes: ['id', 'fullname', 'email']
      },
        {
          model: models.Place,
          as: 'place',
          attributes: ['id', 'name', 'website', 'address', 'phone', 'email', 'logoImage', 'facebook', 'instagram', 'twitter', 'primaryColor', 'secondaryColor', 'timezone'],
          include: ['sponsorsPlaces']
        }]
    });
    if (!reservation.costumer.email) return;

    const mDate = moment(reservation.date);
    const time = mDate.add(reservation.start, 'minutes');
    const offset = moment.tz(reservation.place.timezone).utcOffset() / 60;
    let fixedHours = time.hour() - offset;
    let fixedDate = time.date();
    if (fixedHours > 23) {
      fixedHours = fixedHours - 24;
      fixedDate = fixedDate + 1;
    }
    const start = [time.year(), time.month() + 1, fixedDate, fixedHours, time.minutes()];
    let hours = 0;
    let minutes = reservation.end - reservation.start;
    while (minutes > 60) {
      hours++;
      minutes = minutes - 60;
    }
    const duration = {hours, minutes}

    const event = {
      start,
      startInputType: 'utc',
      duration,
      title: `Mesa para ${reservation.peopleCount}`,
      organizer: {name: reservation.costumer.fullname, email: reservation.costumer.email},
      location: reservation.place.name,
      url: reservation.place.website,
      status: 'CONFIRMED', // TODO ¿será necesario hacer esto para diferentes status?
    }

    const path = `${reservation.place.name.replace(/\s+/g, '')}.ics` // TODO buscar un path más conveniente?

    let menuCoca = undefined;
    let productSponsor = undefined;
    let banner = 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/banner.jpg';
    for (let item of reservation.place.sponsorsPlaces) {
      if (item.sponsorId == 1 && item.isActive) {
        menuCoca = `${item.urlBase}/${item.placeId}.pdf`;
      }

      if (item.sponsorId == 2 && item.isActive && ['CONFIRMED', 'RE_CONFIRMED'].includes(reservation.status)) { // Sponsor-CocaCola
        productSponsor = true;
        menuCoca = undefined;
        banner = 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/Banner-Web-Coca-Cola.png';
      }
    }

    ics.createEvent(event, (error, value) => {
      if (error) {
        console.log(error)
        return
      }
      writeFile(path, value, err => {
        if (err) {
          console.error(err)
          return;
        }
        const reservationData = {
          website: reservation.place.website,
          logoImage: reservation.place.logoImage,
          place: reservation.place.name,
          peopleCount: reservation.peopleCount,
          date: mDate.locale('es').format('dddd, D [de] MMMM [de] YYYY'),
          time: time.format('H:mm'),
          name: reservation.costumer.fullname,
          address: reservation.place.address,
          phone: reservation.place.phone,
          facebook: reservation.place.facebook,
          instagram: reservation.place.instagram,
          twitter: reservation.place.twitter,
          unsubscribeRes: `${env.dns}/#/email/${jwt.sign({reservationId: reservation.id}, env.jwtSecret, {expiresIn: '1w'})}`,
          unsubscribePlace: `${env.dns}/#/email/${jwt.sign({customerId: reservation.costumer.id}, env.jwtSecret, {expiresIn: '1w'})}`,
          secondaryColor: reservation.place.secondaryColor,
          cancelReservationUrl: `${env.dns}/#/reservations/${encryption.encrypt(JSON.stringify({reservationId: reservation.id}))}/cancel`,
          bannerLinkTo: reservation.partnerId == 1 ? 'https://alaorden.coca-cola.com.mx/cuponeras-digitales.php' : null,
          bannerImageUrl: reservation.partnerId == 1 ? banner : null,
          sponsorCocaUrl: reservation.partnerId == 1 && menuCoca ? menuCoca : null,
          productSponsor: reservation.partnerId == 1 && productSponsor ? true : null
        }
        transporter.sendMail({
          from: `${reservation.place.name} <${reservation.place.email}>`,
          to: reservation.costumer.email,
          subject: `Tu reserva en ${reservation.place.name} está confirmada.`,
          text: `Tu reserva en ${reservation.place.name} está confirmada.`,
          html: reservationConfirmedString(reservationData),
          attachments: [{
            path,
            contentType: 'text/Calendar'
          }],
        }, (err, info) => {
          if (err) {
            console.error(err)
          } else {
            console.log(info.envelope);
            console.log(info.messageId);
          }
          unlink(path, (err) => {
            if (err) {
              throw err;
            }
          })
        });

      })
    });
  } catch (error) {
    console.error('catch', error);
  }

}

emailModule.resetPasswordEmail = (placeName, emailToConfirm, id) => {
  const id_encripted = encryption.encrypt(`${id}`);
  const emailUrl = `${env.urlReservandonos}?key=` + encryption.encrypt(`${id}`);
  const body = resetPassword({placeName: placeName, emailUrl});

  transporter.sendMail({
    from: `Reservándonos <hola@reservandonos.com>`,
    to: emailToConfirm,
    subject: `¡Restablecimiento de contraseña!`,
    text: ``,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.resetPasswordSuccessEmail = (placeName, emailToConfirm) => {
  const body = resetPasswordSuccess({placeName: placeName});

  transporter.sendMail({
    from: `Reservándonos <hola@reservandonos.com>`,
    to: emailToConfirm,
    subject: `¡Contraseña actualizada!`,
    text: ``,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.confirmReservandonosReservation = (userEmail, placeName, fullName, reservation, customer) => {
  const token = encryption.encrypt(JSON.stringify({reservationId: reservation.id,}));
  const confirmUrl = `${env.dns}/#/rsv/reservation-confirmed/${token}`;
  const cancelUrl = `${env.dns}/#/rsv/reservation-cancelled/${token}`;

  let day = moment(reservation.dataValues.datetime).locale('es');
  day.set({h: 0, m: 0, s: 0});
  day.add(reservation.start, 'minutes');
  let date = day.format('dddd, D [de] MMMM [de] YYYY');
  let datetime = day.format('H:mm');
  const body = newReservation({fullName, placeName, reservation, customer, confirmUrl, cancelUrl, datetime, date});

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Confirma nueva reserva en ${placeName}`,
    text: `Entra al sistema para confirmar o cancelar. https://reservandonospartners.com`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });

}

emailModule.reservandonosConfirmedReservation = (userEmail, fullName, placeName, reservation, customer) => {
  let day = moment(reservation.dataValues.datetime).locale('es');
  day.set({h: 0, m: 0, s: 0});
  day.add(reservation.start, 'minutes');
  let date = day.format('dddd, D [de] MMMM [de] YYYY');
  let datetime = day.format('H:mm');
  const body = reservationConfirm({fullName, placeName, reservation, customer, datetime, date});

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Se ha confirmado una reserva para  ${placeName}`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.reservandonosCancelledReservation = (userEmail, fullName, placeName, reservation, customer) => {
  let day = moment(reservation.dataValues.datetime).locale('es');
  day.set({h: 0, m: 0, s: 0});
  day.add(reservation.start, 'minutes');
  let date = day.format('dddd, D [de] MMMM [de] YYYY');
  let datetime = day.format('H:mm');
  const body = reservationCancelled({fullName, placeName, reservation, customer, datetime, date});

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Se ha cancelado una reserva para ${placeName}`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.sendIntitationRsv = async (placeName, placeId, email) => {
  const encryptedEmail = encryption.encrypt(`${placeId}/${placeName}/${email}`);
  const emailUrl = `${env.dns}/#/rsv/auth/register/${placeId}/${placeName}/${email}?key=${encryptedEmail}`;
  const body = invitationRsvPartners(placeName, emailUrl);

  const info = await transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: email,
    subject: `Registrate en Reservándonos Partners`,
    text: `Confirma tu registro para disfrutar de tu perfil como partner`,
    html: body,
  });
  console.log(info.envelope);
  console.log(info.messageId);
}

emailModule.reservandonosConfirmedReservationManager = (userEmail, placeName, reservation, customer) => {
  let day = moment(reservation.dataValues.datetime).locale('es');
  day.set({h: 0, m: 0, s: 0});
  day.add(reservation.start, 'minutes');
  let date = day.format('dddd, D [de] MMMM [de] YYYY');
  let datetime = day.format('H:mm');
  const body = reservationConfirmManager({placeName, reservation, customer, datetime, date});

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Se ha confirmado una reserva para ${placeName}`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.reservandonosReConfirmedReservationFromClient = (userEmail, placeName, reservation, customer) => {
  let day = moment(reservation.dataValues.datetime).locale('es');
  day.set({h: 0, m: 0, s: 0});
  day.add(reservation.start, 'minutes');
  let date = day.format('dddd, D [de] MMMM [de] YYYY');
  let datetime = day.format('H:mm');
  const body = reservandonosReConfirmedReservationFromClient({placeName, reservation, customer, datetime, date});

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `El Cliente ha Confirmado una reserva para ${placeName}`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.reservandonosCancelledReservationManager = (userEmail, placeName, reservation, customer) => {
  let day = moment(reservation.dataValues.datetime).locale('es');
  day.set({h: 0, m: 0, s: 0});
  day.add(reservation.start, 'minutes');
  let date = day.format('dddd, D [de] MMMM [de] YYYY');
  let datetime = day.format('H:mm');
  const body = reservationCancelledManager({placeName, reservation, customer, datetime, date});

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Se ha cancelado una reserva para ${placeName}`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.reservandonosCutNotification = async (userEmail, fullName, placeName) => {
  const body = cutNotification(fullName, placeName);
  const data = {
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Nuevo Corte para ${placeName}`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  };
  return await transporter.sendMail(data);
}

emailModule.reservandonosInvoiceNotification = (userEmail, fullName, placeName) => {
  const ivoiceUrl = `${env.dns}/#/rsv/auth/`;
  const body = invoiceNotification(fullName, placeName, ivoiceUrl);

  transporter.sendMail({
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `Corte vencido en ${placeName}`,
    text: `Ya tenemos listo tu corte para procesar el pago`,
    html: body,
  }, (err, info) => {
    if (err) {
      console.error('send mail error', err)
    } else {
      console.log(info.envelope);
      console.log(info.messageId);
    }
  });
}

emailModule.reservandonosNotificationPendingPayment = async (userEmail, fullName, placeName, periodo) => {
  const body = notificationPendingPayment(fullName, placeName, periodo);
  const data = {
    from: `Reservándonos Partners <hola@reservandonos.com>`,
    to: userEmail,
    subject: `¡Pago pendiente en ${placeName}!`,
    text: `Entra al sistema para más detalles. https://reservandonospartners.com`,
    html: body,
  };
  return await transporter.sendMail(data);
}

/**
 * Send email using nodemailer
 * @param from
 * @param to
 * @param subject
 * @param text
 * @param html
 * @returns {Promise<void>}
 */
const send = async (from, to, subject, text, html) => {
  try {
    await transporter.sendMail({from, to, subject, text, html});
  } catch (e) {
    throw new AppError(e, AppError.TYPE_ERROR.INTERNAL);
  }
};

/**
 * Build template html using handlebar
 * @param filename
 * @param data
 * @returns {Promise<string>}
 */
const buildTemplate = async (filename, data = {}) => {
  const filePath = path.join(process.cwd(), 'templates/hbs/emails', `${filename}.hbs`);
  const html = await fs.readFile(filePath, 'utf-8');
  return handlebar.compile(html)(data);
};

/**
 * Build templete for link of the valet oxxo and send email
 * @param paymentId
 * @returns {Promise<void>}
 */
emailModule.dispatchLinkValetOfOxxo = async (paymentId) => {
  const payment = await models.Payments.findByPk(paymentId, {include: ['place']});
  if (payment?.statusPayment === PAYMENTS_STATUS.PROCESS && payment.paymentGateway === PAYMENTS_GATEWAY.OXXO) {
    const html = await buildTemplate('valetOxxo', {
      concept: payment.comment,
      amount: `$${payment.amount}`,
      method: payment.paymentGateway,
      link: payment.linkOxxo
    });
    const place = payment.place;
    const from = `"Reservándonos Partner" <${place.email}>`;
    const subject = 'Reservándonos Partner - Valet Oxxo'
    const to = place.emailPersonInCharge.split(';').map(item => item.trim()).filter(item => item !== '').join(', ');
    await send(from, to, subject, '', html);
  } else {
    throw new AppError('Payment not found', AppError.TYPE_ERROR.RESOURCE_NOT_FOUND);
  }
};

/**
 * Build template for payment confirmed and send email
 * @param paymentId
 * @returns {Promise<void>}
 */
emailModule.dispatchPaymentByBillingConfirmed = async (paymentId) => {
  const payment = await models.Payments.findByPk(paymentId, {include: ['place']});
  if (payment?.statusPayment === PAYMENTS_STATUS.APPROVED) {
    const html = await buildTemplate('payConfirmed', {
      concept: payment.comment,
      amount: `$${payment.amount}`,
      method: payment.paymentGateway,
      link: payment.linkOxxo,
      card: i18n.es.CARD_TYPE[payment.cardType]
    });
    const place = payment.place;
    const from = `"Reservándonos Partner" <${place.email}>`;
    const subject = 'Reservándonos Partner - Confirmación de Pago'
    const to = place.emailPersonInCharge.split(';').map(item => item.trim()).filter(item => item !== '').join(', ');
    await send(from, to, subject, '', html);
  } else {
    throw new AppError('Payment not found', AppError.TYPE_ERROR.RESOURCE_NOT_FOUND);
  }
};

/**
 * Build template for payment reject and send email
 * @param paymentId
 */
emailModule.dispatchPaymentByBillingReject = async (paymentId) => {
  const payment = await models.Payments.findByPk(paymentId, {include: ['place']});
  if (payment?.statusPayment === PAYMENTS_STATUS.DECLINED) {
    console.log({
      concept: payment.comment,
      amount: `$${payment.amount}`,
      method: payment.paymentGateway,
      link: payment.linkOxxo
    });
    const html = await buildTemplate('paymentDeclined', {
      concept: payment.comment,
      amount: `$${payment.amount}`,
      method: payment.paymentGateway,
      link: payment.linkOxxo
    });
    const place = payment.place;
    const from = `"Reservándonos Partner" <${place.email}>`;
    const subject = 'Reservándonos Partner - Pago Declinado'
    const to = place.emailPersonInCharge.split(';').map(item => item.trim()).filter(item => item !== '').join(', ');
    await send(from, to, subject, '', html);
  } else {
    throw new AppError('Payment not found', AppError.TYPE_ERROR.RESOURCE_NOT_FOUND);
  }
};


module.exports = {emailModule};
