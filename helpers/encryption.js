const crypto = require('crypto');
const env = require('../config/env');

const algorithm = 'aes-256-ctr';

module.exports.encrypt = text => {
  const cipher = crypto.createCipheriv(algorithm, env.cryptoSecret, env.cryptoIV);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

module.exports.decrypt = text => {
  const decipher = crypto.createDecipheriv(algorithm, env.cryptoSecret, env.cryptoIV);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}