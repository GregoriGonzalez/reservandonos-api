const HandleError = (requestExpress, responseExpress, error) => {
  const line = "\n***********************************************************************\n";
  if (error instanceof InternalProcessError) {
    const message = "PROCESS ERROR - CODE: " + error.type + "\n"
      + "MESSAGE: " + error.message + "\n"
      + "DATA: " + JSON.stringify(error.data) + "\n";
    console.error(line + message, error.stack, line);
  } else if (error instanceof AppError) {
    return responseExpress.status(error.statusCode).json({error: true, type: error.type, message: error.message});
  } else {
    const message = "REQUEST ERROR - MESSAGE: " + error.message + "\n"
        + "URL: " + requestExpress.originalUrl + "\n"
        + "METHOD: " + requestExpress.method + "\n"
        + "PARAMS: " + JSON.stringify(requestExpress.params) + "\n"
        + "QUERY: " + JSON.stringify(requestExpress.query) + "\n"
        + "BODY: " + JSON.stringify(requestExpress.body) + "\n"
        + "USER: " + JSON.stringify(requestExpress.user) + "\n"
        + "STACK: \n";
    console.error(line, message, error.stack, line);
    return responseExpress.status(500).json({error: true, message: error.message, stack: error});
  }
};

class AppError extends Error {
  static TYPE_ERROR = {
    TAX_INVALID: 'TAX_INVALID',
    RESOURCE_NOT_FOUND: 'RESOURCE_NOT_FOUND',
    INTERNAL: 'INTERNAL'
  };

  constructor(message, type = undefined, statusCode = 400) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, AppError)
    }
    this.name = 'AppError';
    this.type = type;
    this.statusCode = statusCode;
  }
}

class InternalProcessError extends AppError {
  constructor(appError, data) {
    super(appError.message, appError.type, appError.statusCode);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, InternalProcessError)
    }
    this.name = 'InternalProcessError';
    this.data = data;
  }
}

module.exports = {AppError, InternalProcessError, HandleError};

