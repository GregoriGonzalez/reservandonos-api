'use strict';
const {facturapi: {secretId}} = require('../config/env');
const {PAYMENTS_GATEWAY, CARD_TYPE} = require("./constants.helper");
const Facturapi = require("facturapi");
const facturapi = new Facturapi(secretId);
const Notification = require('./notifications.helper');
const {AppError} = require("./error.helper");

/**
 * determine the payment form
 * @param payment: Payment
 * @returns {string}
 */
const getPaymentForm = (payment) => {
  if ([PAYMENTS_GATEWAY.STRIPE, PAYMENTS_GATEWAY.PAYPAL].includes(payment.paymentGateway)) {
    if (payment.cardType === CARD_TYPE.CREDIT) {
      return Facturapi.PaymentForm.TARJETA_DE_CREDITO;
    } else {
      return Facturapi.PaymentForm.TARJETA_DE_DEBITO
    }
  } else if ([PAYMENTS_GATEWAY.CASH, PAYMENTS_GATEWAY.OXXO].includes(payment.paymentGateway)) {
    return Facturapi.PaymentForm.EFECTIVO;
  } else if (PAYMENTS_GATEWAY.EXTERNAL) {
    return Facturapi.PaymentForm.TRANSFERENCIA_ELECTRONICA_DE_FONDOS
  }
};

/**
 * https://docs.facturapi.io/api/#operation/createCustomer
 */
const createCustomer = async (place) => {
  if (place.businessName !== '' && place.taxRecord !== '') {
    const customerCreated = await facturapi.customers.create({
      legal_name: place.businessName,
      tax_id: place.taxRecord,
      // TODO zip y tax system vienen de base de datos
      tax_system: Facturapi.TaxSystem.GENERAL_LEY_DE_PERSONAS_MORALES,
      address: {
        zip: '48333'
      }
    });
    if (customerCreated) {
      place.facturapiCustomerId = customerCreated.id;
      await place.save();
    }
  }
};

/**
 * https://docs.facturapi.io/api/#operation/editCustomer
 */
const updateCustomer = async (place) => {
  const {registro: {is_valid: isValid}} = await fetchDataTaxRecord(place.taxRecord);
  if (isValid && place.businessName !== '') {
    await facturapi.customers.update(place.facturapiCustomerId, {
      legal_name: place.businessName,
      tax_id: place.taxRecord,
      // TODO zip y tax system vienen de base de datos
      tax_system: Facturapi.TaxSystem.GENERAL_LEY_DE_PERSONAS_MORALES,
      address: {
        zip: '48333'
      }
    });
  } else {
    throw new Error('Error de RFC: ' + place.taxRecord + ' Lugar: ' + place.id);
  }
};

/**
 * Verified RFC with provider
 * Documentation: https://docs.facturapi.io/#validar-rfc
 * @param taxRecord: String (RFC)
 * @returns {Promise<*>}
 */
const fetchDataTaxRecord = async (taxRecord) => {
  return await facturapi.tools.validateTaxId(taxRecord);
}

/**
 * generate electronic invoice
 * @param place: Place
 * @param payment: Payment
 * @returns {Promise<void>}
 */
const generate = async (place, payment) => {
  const responseTaxRecordInvalid = async () => {
    await Notification.dispatchTaxRecordInvalid(place);
    payment.pendingToInvoice = true;
    await payment.save();
    throw new AppError("Tax Record Invalid", AppError.TYPE_ERROR.TAX_INVALID, 400);
  };

  const isValidRFC = await fetchDataTaxRecord(place.taxRecord).catch(async (e) => responseTaxRecordInvalid());
  if (isValidRFC?.efos?.is_valid) {
    if (!place.facturapiCustomerId) {
      await createCustomer(place).catch();
    }
  } else {
    await responseTaxRecordInvalid();
  }

  if (place.facturapiCustomerId) {
    const createdInvoice = await facturapi.invoices.create({
      customer: place.facturapiCustomerId,
      payment_form: getPaymentForm(payment),
      payment_method: Facturapi.PaymentMethod.PAGO_EN_UNA_EXHIBICION,
      use: place.facturapiCfdi ?? Facturapi.InvoiceUse.POR_DEFINIR,
      currency: payment.currency,
      items: [{
        quantity: 1,
        product: {
          description: place.facturapiConcept ? `${payment.comment} \n\n ${place.facturapiConcept}` : payment.comment,
          product_key: '01010101',
          price: payment.amount
        }
      }]
    });
    payment.electronicInvoiceId = createdInvoice.id;
    payment.pendingToInvoice = false;
    await payment.save();
    if (place.emailPersonInCharge && payment.electronicInvoiceId) {
      const emails = place.emailPersonInCharge.split(';').map(item => item.trim()).filter(item => item !== '');
      for (const email of emails) {
        await facturapi.invoices.sendByEmail(payment.electronicInvoiceId, {email});
      }
    }
  }
};

/**
 * get file of the provider
 * @param electronicInvoiceId
 * @param format pdf or xml
 * @returns {Promise<ReadStream>}
 */
const download = (electronicInvoiceId, format) => {
  return (format === 'pdf')
    ? facturapi.invoices.downloadPdf(electronicInvoiceId)
    : facturapi.invoices.downloadXml(electronicInvoiceId);
};

const setCustomer = async (place) => {
  if (place.facturapiCustomerId) {
    await updateCustomer(place);
  } else {
    await createCustomer(place);
  }
};

module.exports = {
  fetchDataTaxRecord,
  generate,
  download,
  setCustomer
};
