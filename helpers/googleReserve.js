const models = require('../models');
const moment = require('moment');
const timezone = require('moment-timezone');
const fs = require('fs');
const SftpUpload = require('sftp-upload');
var rimraf = require("rimraf");
const path = require("path");
const {google} = require("googleapis");
const {RESERVATION_STATUS} = require("../helpers/constants.helper");
const {HandleError} = require("../helpers/error.helper");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const Op = models.Sequelize.Op;

const googleModule = {};

const loadGoogle = (options) => {
  return new Promise((resolve, reject) => {
    let sftp = new SftpUpload(options);

    sftp.on('error', (err) => {
      reject(err);
      throw err;
    })
    .on('uploading', (progress) => {
      // console.log('Uploading ', progress.file);
      console.log(progress.percent+'% completed');
    })
    .on('completed', () => {
      console.log('Upload Completed');
      resolve(true);
    }).upload();
  });
}

const loadDirectory = (availabilityPath, merchantPath, servicePath) => {
  let key = fs.readFileSync(`${process.cwd()}/config/id-rsa.ppk`);
  let availabilityOptionsSandbox = {
    host:'partnerupload.google.com',
    username:'feeds-5seyk6',
    path: availabilityPath,
    remoteDir: '/',
    port: 19321,
    privateKey: key
  }

  let merchantOptionsSandbox = {
    host:'partnerupload.google.com',
    username:'feeds-54s63z',
    path: merchantPath,
    remoteDir: '/',
    port: 19321,
    privateKey: key
  }

  let serviceOptionsSandbox = {
    host:'partnerupload.google.com',
    username:'feeds-x475a8',
    path: servicePath,
    remoteDir: '/',
    port: 19321,
    privateKey: key
  }

  let availabilityOptionsProduction = {
    host:'partnerupload.google.com',
    username:'feeds-lo0xi1',
    path: availabilityPath,
    remoteDir: '/',
    port: 19321,
    privateKey: key
  }

  let merchantOptionsProduction = {
    host:'partnerupload.google.com',
    username:'feeds-j23l94',
    path: merchantPath,
    remoteDir: '/',
    port: 19321,
    privateKey: key
  }

  let serviceOptionsProduction = {
    host:'partnerupload.google.com',
    username:'feeds-82d871',
    path: servicePath,
    remoteDir: '/',
    port: 19321,
    privateKey: key
  }

  // -------- Load Production -------- //
  loadGoogle(merchantOptionsProduction).then(() => {
    loadGoogle(serviceOptionsProduction).then(() => {
      loadGoogle(availabilityOptionsProduction).then(()=> {
      }, err => { console.log('error load Production availability', err)})
    }, err => { console.log('error load Production service', err); })
  }, err => { console.log('error load Production merchant', err); });
  // -------- End Load Production -------- //
}

const range = (start, stop, step) => {
    if (typeof stop == 'undefined') {
        stop = start;
        start = 0;
    }

    if (typeof step == 'undefined') {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }

    return result;
};

const writeFileAvailability = (name, jsonAvailibility, shard_number) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(`${name}`, JSON.stringify(jsonAvailibility), (err) => {
      if (err) {
        reject(false);
        console.log(`err availability ${shard_number}`, err);
      } else {
        console.log(shard_number);
        resolve(true);
      }
    });
  });
}

googleModule.updateReservation = async (body) => {
  return new Promise((resolve, reject) => {
    const keyPath = path.join(__dirname, '../booking-google.json');
    let keys = {};
    if (fs.existsSync(keyPath)) {
      keys = require(keyPath);
      let jwtClient = new google.auth.JWT(
        keys.client_email,
        null,
        keys.private_key,
        [ 'https://www.googleapis.com/auth/mapsbooking' ]
      );

      var oReq = new XMLHttpRequest();
      var url = `https://mapsbooking.googleapis.com/v1alpha/notification/partners/${body.partner}/bookings/${body.reservationId}?updateMask=status`;
      var params = JSON.stringify({
        name: `partners/${body.partner}/bookings/${body.reservationId}`,
        status: `${body.status}`
      });

      jwtClient.authorize( async (err, tokens) => {
        if (err) {
          reject({error: 'Error en la autorización', code: 500});
        } else {
          oReq.open("PATCH", url, true);
          oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
          oReq.setRequestHeader('Authorization', `Bearer ${tokens.access_token}`);
          oReq.addEventListener("load", async () => {
            if (oReq.readyState != 4 || oReq.status != 200) {
              reject({error: 'Error en el proceso', code: 500});
            } else {
              let response = JSON.parse(oReq.responseText);
              let status = response.status === RESERVATION_STATUS.CONFIRMED ? response.status : RESERVATION_STATUS.CANCELLED;
              await models.Reservation.update({status: status}, {
                where: {
                  id: response['merchantId'].replace('merch','')
                }
              });
              resolve(JSON.parse(oReq.responseText));
            }
          });
          oReq.send(params);
        }
      });
    }
  });
}

// ----- Search Places in booker -----
googleModule.preparedInfoBooker = () => {
  return new Promise((resolve, reject) => {
    models.Place.findAll({
      attributes: ['id', 'name', 'tablesCount', 'timezone', 'enableGoogleReserve' ,'methodReserve', 'intervalWidget', 'maxPeopleReservations'],
      include: [{
        model: models.OpeningDay,
        as: 'openingDays',
        attributes: ['weekday', 'start', 'end', 'start2', 'end2', 'smokeArea']
      }],
      where: {
        enableGoogleReserve: 1
      }
    }).then(place => {
      resolve(place);
    }, err => {
      reject(err);
    });
  });
}


// ----- Search Meta and Places in Wordpress -----
googleModule.preparedInfoWordpress = (url) => {
  var oReq = new XMLHttpRequest();
  oReq.open("GET", url, true);
  return new Promise((resolve, reject) => {
    oReq.addEventListener("load", () => {
      if (oReq.readyState != 4 || oReq.status != 200) {
        reject({error: 'Error en el proceso', code: 500});
      } else {
        let response = JSON.parse(oReq.responseText);
        resolve(response);
      }
    });
    oReq.send();
  });
};


// ----- Validate Places  -----
googleModule.validatePlaces =  async (placesBooker, placesWordpress, metaWordpress, uploadFiles) => {
  let wordpressAccepted = [], wordpressErr = [], wordpressDeclined = [];
  let jsonServiceInfo = [], jsonMerchantInfo = [];
  let sistema_id, placeUrl, placeName, placeId;
  let urlBaseFile = `${process.cwd()}/output`;

  // ----- Validate Places Wordpress -----

  for (var place of placesWordpress) {
    let street_address, locality, region, country, postal_code, name;
    sistema_id = 0;
    placeUrl = `https://reservandonos.com/lugar/${place['post_name']}/`;
    placeName = place['post_title'];
    placeId = place['ID'];

    name = place['post_title']
    name = name.replace('Restaurante y Bar ', '')
    name = name.replace('Restaurante Bar ', '')
    name = name.replace('Bar Antro ', '')
    name = name.replace('Restaurante ', '')
    name = name.replace('Bar ', '')
    name = name.replace('Antro ', '')

    for (var meta of metaWordpress) {
      if (meta['post_id'] == place['ID']) {
        if (meta['meta_key'] === '_sistema_id') {
          sistema_id = meta['meta_value'];
        }

        if (meta['meta_key'] === 'geolocation_formatted_address') {
          street_address = meta['meta_value'];
        }

        if (meta['meta_key'] === 'geolocation_city') {
          locality = meta['meta_value'];
        }

        if (meta['meta_key'] === 'geolocation_state_short') {
          region = meta['meta_value'];
        }

        if (meta['meta_key'] === 'geolocation_country_short') {
          country = meta['meta_value'];
        }

        if (meta['meta_key'] === 'geolocation_postcode') {
          postal_code = meta['meta_value'];
        }
      }
    } // ----- end-for metaWordpress -----

    let number = sistema_id;
    if (isNaN(Number(number))) {
      wordpressDeclined.push(Number(placeId));
      wordpressErr.push(`${placeName} - _sistema_id no es numero`)
    } else {
      if (sistema_id == 0) {
        wordpressDeclined.push(Number(placeId));
        wordpressErr.push(`${placeName} - _sistema_id no es numero`)
      } else {
        let empty = 0;

        if (!street_address || street_address == "") {
          wordpressErr.push(`${placeName} - geolocation_formatted_address`)
          empty = 1;
        }

        if (!locality || locality == "") {
          wordpressErr.push(`${placeName} - geolocation_city`)
          empty = 1;
        }

        if (!region || region == "") {
          wordpressErr.push(`${placeName} - geolocation_state_short`)
          empty = 1;
        }

        if (!country || country == "") {
          wordpressErr.push(`${placeName} - geolocation_country_short`)
          empty = 1;
        }

        if (!postal_code || postal_code == "") {
          wordpressErr.push(`${placeName} - geolocation_postcode`)
          empty = 1;
        }

        if (empty == 0) {
          wordpressAccepted.push(Number(sistema_id));

          jsonMerchantInfo.push({
            "merchant_id": `merch${sistema_id}`,
            "name":  name,
            "category": "restaurant",
            "geo": {
              "address": {
                "street_address": street_address,
                "locality": locality,
                "region": region,
                "country": country,
                "postal_code": postal_code
              }
            }
          })

          jsonServiceInfo.push({
            "merchant_id": `merch${sistema_id}`,
            "service_id": `${sistema_id}-dining`,
            "name":  "Reservation",
            "type":  "SERVICE_TYPE_DINING_RESERVATION",
            "action_link": [{
              "url": placeUrl
            }]
          });
        } else {
          // wordpressDeclined.push(Number(placeId));
          wordpressDeclined.push(Number(sistema_id));
        }
      }
    }
  }

  wordpressAccepted = wordpressAccepted.sort(); // list Accepted Wordpress
  wordpressDeclined = wordpressDeclined.sort(); // list Declined Wordpress

  // ----------- Days Week --------------

  let now = moment();
  let array_week_dates = [now.format('dddd')];
  for (let i = 0; i < 6; i++) {
    array_week_dates.push(now.add(1, 'days').format('dddd'));
  }

  // ----- Validations Places Booker -----

  let bookerAccepted = [], bookerErr = [], bookerDeclined = [];
  for (var place of placesBooker) {
    if (wordpressAccepted.includes(place['id'])) {
      placeId = place['id'];
      placeName = place['name'];
      len_hours = place['openingDays'].length;

      if (len_hours == 0) { // Validation hours empty
        bookerDeclined.push(Number(placeId));
        bookerErr.push(`${placeName} - no tiene horarios en el sistema`);
      } else if (len_hours > 7) { // Validation hours
        bookerDeclined.push(Number(placeId));
        bookerErr.push(`${placeName} - excede días de la semana`);
      } else {

        for (var day of place['openingDays']) {
          array_week_dates.forEach(week_date => {
            if (week_date.toLowerCase() === day['weekday'].toLowerCase()) {
              if (day['start'] && day['end']) {
                if (day['start'] > day['end']) {
                  bookerDeclined.push(Number(placeId));
                  bookerErr.push(`${placeName} - es mayor el inicio al final 1`);
                }
              }

              if (day['start2'] && day['end2']) {
                if (day['start2'] > day['end2']) {
                  bookerDeclined.push(Number(placeId));
                  bookerErr.push(`${placeName} - es mayor el inicio al final 2`);
                }
              }

              if (day['start2'] && day['start2'] != 0) {
                if (day['end'] >= day['start2']) {
                  bookerDeclined.push(Number(placeId));
                  bookerErr.push(`${placeName} - el primer fin es mayor o igual al segundo comienzo`);
                }
              }

              if ((day['start'] == 0 && day['end'] == 0) || (!day['start'] && !day['end'])) {
                if (day['start2'] && day['end2'] && day['start2'] !== 0 && day['end2'] !== 0) {
                  bookerDeclined.push(Number(placeId));
                  bookerErr.push(`${placeName} - Tiene segundo horario pero no tiene primero`);
                }
              }
            }
          });
        }
      }
    } else {
      bookerDeclined.push(Number(place['id']));
    }
  } // end-for Place Booking

  // ----- places Accepted -----

  for (var place of placesBooker) {
    if (wordpressAccepted.includes(place['id'])) {
      if (!bookerDeclined.includes(place['id'])) {
        bookerAccepted.push(place['id']);
      } else {
        console.log('declined ', place['id']);
      }
    }
  }

  bookerAccepted = bookerAccepted.sort(); // list Booker Accepted
  bookerDeclined = bookerDeclined.sort(); // list Booker Declined

  // let googlePlaces = [];
  // for (var meta of metaWordpress) {
  //   if (meta['meta_key'] === '_sistema_id') {
  //     sistema_id = meta['meta_value'];
  //     if (Number(sistema_id)) {
  //       if (bookerAccepted.includes(Number(sistema_id))) {
  //         googlePlaces.push(Number(sistema_id));
  //       }
  //     }
  //   }
  // }

  console.log('wordpressAccepted ', wordpressAccepted.length);
  console.log('wordpressDeclined ', wordpressDeclined.length);
  console.log('placesBooker ', placesBooker.length);
  console.log('bookerAccepted ', bookerAccepted.length);
  console.log('bookerDeclined', bookerDeclined.length);
  // console.log('googlePlaces ', googlePlaces.length);

// ----- Build Object Merchants and Services -----

  let jsonMerchantTmp = [], jsonServiceTmp = [];
  let jsonMerchant, jsonService;
  bookerAccepted.forEach( accept => {
    let merch = jsonMerchantInfo.find( elem => elem.merchant_id.replace('merch','') == accept);
    let service = jsonServiceInfo.find( elem => elem.merchant_id.replace('merch','') == accept);
    if (merch && service) {
      jsonMerchantTmp.push(merch)
      jsonServiceTmp.push(service)
    }
  });

  console.log('jsonMerchantTmp', jsonMerchantTmp.length);
  console.log('jsonServiceTmp', jsonServiceTmp.length);

  // ----- Verification and Creating of folders -----

  let timestamp = moment().unix();
  let c_d = moment().format('MM-DD');

  if (!fs.existsSync(urlBaseFile)) { // Folder principal output
    fs.mkdirSync(urlBaseFile);
  }

  let nameFolder = `${urlBaseFile}/${moment().format('YYYY-MM-DD')}`;
  let nameFolderOld = `${urlBaseFile}/${moment().subtract('2', 'day').format('YYYY-MM-DD')}`;
  let nameFolderAvailibity = `${urlBaseFile}/${moment().format('YYYY-MM-DD')}/availibility`;
  let nameFolderMerchant = `${urlBaseFile}/${moment().format('YYYY-MM-DD')}/merchant`;
  let nameFolderService = `${urlBaseFile}/${moment().format('YYYY-MM-DD')}/service`;

  if (fs.existsSync(nameFolder)) {
    rimraf.sync(nameFolder); // Delete Folder duplicate
  }

  if (fs.existsSync(nameFolderOld)) {
    rimraf.sync(nameFolderOld); // Delete Folder 2 days old
  }

  if (!fs.existsSync(nameFolder)) { // Folder principal resources
    fs.mkdirSync(nameFolder);
    fs.mkdirSync(nameFolderAvailibity);
    fs.mkdirSync(nameFolderMerchant);
    fs.mkdirSync(nameFolderService);
  }


  jsonMerchant = {
    "metadata": {
      "generation_timestamp": timestamp,
      "processing_instruction": "PROCESS_AS_COMPLETE",
      "shard_number": 0,
      "total_shards": 1
    },
    "merchant": jsonMerchantTmp
  }

  jsonService = {
    "metadata": {
      "generation_timestamp": timestamp,
      "processing_instruction": "PROCESS_AS_COMPLETE",
      "shard_number": 0,
      "total_shards": 1
    },
    "service": jsonServiceTmp
  }


  // ----- availibility in places -----

  let shard_number = 0;
  let total_shard = bookerAccepted.length;
  let nonce = Math.floor(Math.random() * (9999999999 - 1000000000)) + 1000000000;
  let max_person_table = 15; // count real one minus
  let time_table = 900; // time table is seconds
  let time_minus_end = Number(time_table/60);

  console.log('total_shards  ', total_shard);

  now = moment();
  array_week_dates = [now.format('YYYY-MM-DD')];

  // ----- date to 30 days -----

  for (let i = 0; i < 90; i++) {
    array_week_dates.push(now.add(1, 'days').format('YYYY-MM-DD'));
  }


  // ----- loop array placesBooker

  let jsonAvailibility, jsonAvailibilityServ, jsonAvailibilityServi, jsonAvailibilityServA;
  let local_timezone;
  let arrayPlacesInGoogle = [];
  for (var place of placesBooker) {
    const locks = await models.PlaceLock.findAll({
      where: {
        placeId: place['id'],
        [Op.or]: [
          { date: { [Op.gte]: moment().format('YYYY-MM-DD') } },
          {
            [Op.and]: [
              { date: { [Op.lte]: moment().format('YYYY-MM-DD') } },
              { dateEnd: { [Op.gte]: moment().format('YYYY-MM-DD') } }
            ]
          }
        ]
      }
    });
    if (bookerAccepted.includes(Number(place['id']))) {
      jsonAvailibility = {
        "metadata": {
            "processing_instruction": "PROCESS_AS_COMPLETE",
            "shard_number": shard_number,
            "total_shards": total_shard,
            "nonce": nonce,
            "generation_timestamp": timestamp
        }
      }

      shard_number++;

      jsonAvailibilityServ = {};
      jsonAvailibilityServi = [];
      jsonAvailibilityServA = [];

      array_week_dates.forEach( week_date => {
        let mode;
        let time_week_date = moment(week_date).tz(place['timezone']);
        max_person_table = place['maxPeopleReservations'] ? place['maxPeopleReservations'] + 1 : 15;
        for (var day of place['openingDays']) {
          if (time_week_date.format('dddd').toLowerCase() === day['weekday'].toLowerCase()) {
            mode = place['methodReserve'] == 'CONFIRMATION_MODE_SYNCHRONOUS' ? 'CONFIRMATION_MODE_SYNCHRONOUS' : 'CONFIRMATION_MODE_ASYNCHRONOUS';

            // ----- first turn -----

            if (day['start'] && day['end'] && day['start'] !== 0 && day['end'] !== 0) {
              range(day['start'], day['end'], time_minus_end * Number(place['intervalWidget'])).forEach( minutes => {
                let now = time_week_date.set({h: 0, m: 0, s: 0});
                now.add(minutes, 'minutes');
                let time = now.unix();
                let insert = true;

                for (const lock of locks) {
                  if (lock.isInterval && lock.date >= time_week_date.format('YYYY-MM-DD') && lock.dateEnd <= time_week_date.format('YYYY-MM-DD')) {
                    if (minutes >= lock.start && lock.end >= minutes) {
                      console.log(':::::::::: ', place['id'], ' - ', lock.isInterval, ' ', lock.date, ' ', lock.start, ' - ', lock.end, ' min ', minutes);
                      insert = false;
                    }
                  }

                  if (!lock.isInterval && lock.date == time_week_date.format('YYYY-MM-DD')) {
                    if (minutes >= lock.start && lock.end >= minutes) {
                      console.log(':::::::::: ', place['id'], ' - ', lock.isInterval, ' ', lock.date, ' ', lock.start, ' - ', lock.end, ' min ', minutes);
                      insert = false;
                    }
                  }
                }

                if (insert) {
                  range(1, max_person_table, 1).forEach(party_size => {
                    jsonAvailibilityServA.push({
                      "duration_sec": time_table,
                      "start_sec": time,
                      "merchant_id": `merch${place['id']}`,
                      "service_id": `${place['id']}-dining`,
                      "spots_open": 10,
                      "spots_total": 10,
                      "resources": {
                        "party_size": party_size
                      },
                      "confirmation_mode": mode
                    });
                  });
                }
              });
            } // ----- end first turn

            // ----- secundary turn -----

            if (day['start2'] && day['end2'] && day['start2'] !== 0 && day['end2'] !== 0) {
              range(day['start2'], day['end2'], time_minus_end * Number(place['intervalWidget'])).forEach( minutes => {
                let now = time_week_date.set({h: 0, m: 0, s: 0});
                now.add(minutes, 'minutes');
                let time = now.unix();

                let insert = true;

                for (const lock of locks) {
                  if (lock.isInterval == 1 && lock.date >= time_week_date.format('YYYY-MM-DD') && lock.dateEnd <= time_week_date.format('YYYY-MM-DD')) {
                    if (minutes >= lock.start && lock.end >= minutes) {
                      console.log(':::::::::: ', place['id'], ' - ' , lock.isInterval, ' ', lock.date, ' ', lock.start, ' - ', lock.end, ' min ', minutes);
                      insert = false;
                    }
                  }

                  if (lock.isInterval == 0 && lock.date == time_week_date.format('YYYY-MM-DD')) {
                    if (minutes >= lock.start && lock.end >= minutes) {
                      console.log(':::::::::: ', place['id'], ' - ', lock.isInterval, ' ', lock.date, ' ', lock.start, ' - ', lock.end, ' min ', minutes);
                      insert = false;
                    }
                  }
                }

                if (insert) {
                  range(1, max_person_table, 1).forEach(party_size => {
                    jsonAvailibilityServA.push({
                      "duration_sec": time_table,
                      "start_sec": time,
                      "merchant_id": `merch${place['id']}`,
                      "service_id": `${place['id']}-dining`,
                      "spots_open": 10,
                      "spots_total": 10,
                      "resources": {
                        "party_size": party_size
                      },
                      "confirmation_mode": mode
                    });
                  });
                }
              });
            } // ----- end secundary turn -----
          }
        }
      }); // ----- end array_week_dates

      arrayPlacesInGoogle.push(`${place['id']} - ${place['name']}`);
      jsonAvailibilityServ['availability'] = jsonAvailibilityServA;
      jsonAvailibilityServi.push(jsonAvailibilityServ);
      jsonAvailibility['service_availability'] = jsonAvailibilityServi;
      let reponse = await writeFileAvailability(`${nameFolderAvailibity}/availability${c_d}-${place['id']}-${shard_number}-${total_shard}.json`, jsonAvailibility, shard_number);
    }
  }

  // ----- Write in files  -----

  fs.writeFile(`${urlBaseFile}/wordpress-error.txt`, wordpressErr.join('\n'), (err) => {
    if (err) { console.log('err wordpress ', err); }
  });

  fs.writeFile(`${urlBaseFile}/booker-error.txt`, bookerErr.join('\n'), (err) => {
    if (err) { console.log('err booker ', err); }
  });

  fs.writeFile(`${urlBaseFile}/compare-booker-wordpress.txt`, bookerDeclined.join('\n'), (err) => {
    if (err) { console.log('err booker ', err); }
  });

  fs.writeFile(`${urlBaseFile}/places-google-reserve.txt`, arrayPlacesInGoogle.join('\n'), (err) => {
    if (err) { console.log('err arrayPlacesInGoogle ', err); }
  });

  let fileMerchant = `${nameFolderMerchant}/merchants${c_d}.json`;
  let fileService = `${nameFolderService}/services${c_d}.json`;

  fs.writeFile(fileMerchant, JSON.stringify(jsonMerchant), (err) => {
    if (err) { console.log('err merchant ', err); }
  });

  fs.writeFile(fileService, JSON.stringify(jsonService), (err) => {
    if (err) { console.log('err service ', err); }
  });

  if (uploadFiles) {
    setTimeout(()  => {
      console.log('start Upload');
      loadDirectory(`${nameFolderAvailibity}/`, `${nameFolderMerchant}/`, `${nameFolderService}/`);
    }, 60000); // ejecutamos la carga luego de 10 minutos. (600000) milisegundos
  } else {
    console.log('finish write files');
  }

  return {
    merchants: jsonMerchant.merchant.length,
    services: jsonService.service.length,
    availables: bookerAccepted.length
  }
}

module.exports = googleModule;
