const {
  RESERVATION_STATUS,
  TYPE_NOTIFICATION_BY_BILLING,
  TYPE_NOTIFICATION_BY_RESERVATION, RESERVATION_ORIGIN, TYPE_NOTIFICATION_BY_GIFT, TYPE_NOTIFICATION_BY_COUPON
} = require("./constants.helper");
const {HandleError} = require("./error.helper");
const models = require('../models');
const Op = models.Sequelize.Op;
const {NOTIFICATION_TYPE, PAYMENTS_STATUS} = require("./constants.helper");
const PushNotification = require('./pushNotifications.helper');
const Socket = require('./socket.helper');
const {emailModule: Email} = require('./email.helper');
const {InternalProcessError} = require("./error.helper");

/**
 *
 * @param {TYPE_NOTIFICATION_BY_RESERVATION} notificationType
 * @param {Sequelize.Model<Reservations>} reservation
 * @returns {Promise<string>}
 */
const getMessageByReservationStatus = async (notificationType, reservation) => {
  let message = '';

  if (notificationType === TYPE_NOTIFICATION_BY_RESERVATION.NEW_RES) {
    message = 'Nueva Reserva\n';
    if (reservation.origin === RESERVATION_ORIGIN.GOOGLE) {
      message = 'Reserva creada desde Google\n'
    } else {
      if ([RESERVATION_STATUS.WAIT_LIST_LOCAL, RESERVATION_STATUS.WAIT_LIST].includes(reservation.status)) {
        message = `Nueva reservación en espera\n`;
      } else if ([RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED].includes(reservation.status)) {
        message = "Reserva confirmada\n";
      } else if (reservation.status === RESERVATION_STATUS.SEATED) {
        message = "Se ocupó una mesa\n";
      } else if (reservation.status === RESERVATION_STATUS.PENDING) {
        message = 'Reserva pendiente\n';
      }
    }
  } else if (notificationType === TYPE_NOTIFICATION_BY_RESERVATION.CAN_RES) {
    message = 'Se canceló una reserva.\n';
  } else if (notificationType === TYPE_NOTIFICATION_BY_RESERVATION.UPD_RES) {
    if (reservation.origin === RESERVATION_ORIGIN.GOOGLE) {
      message = 'Reserva modificada desde Google\n';
    } else {
      message = 'Se actualizó una reserva.\n';
      if (reservation.status === RESERVATION_STATUS.RE_CONFIRMED) {
        message = 'Se re-confirmó una reserva.\n';
      }
    }
  }

  if (message !== '') {
    message += `${reservation.date}, `;
    if (reservation.tableId) {
      const table = await models.Table.findOne({where: {id: reservation.tableId}});
      message += `mesa: ${table.number}, `;
    }
    if (reservation.groupId) {
      const group = await models.Group.findOne({where: {id: reservation.dataValues.groupId}});
      message += `grupo: ${group.numbers}, `;
    }
    message += `${reservation.peopleCount} personas.`
  }
  return message;
}

/**
 * @param {TYPE_NOTIFICATION_BY_BILLING} notificationType
 * @param {number} billingId
 * @returns {Promise<void>}
 */
const dispatchBilling = async (notificationType, billingId) => {
  if (Object.values(TYPE_NOTIFICATION_BY_BILLING).includes(notificationType)) {
    let message;
    const billing = await models.Billings.findByPk(billingId);
    if (billing) {
      switch (notificationType) {
        case TYPE_NOTIFICATION_BY_BILLING.NEW_CUT:
          message = `Nuevo Corte ${billing.dateStart} al ${billing.dateEnd}`;
          break;
        case TYPE_NOTIFICATION_BY_BILLING.LATE_CUT:
          message = `Corte atrasado ${billing.dateStart} al ${billing.dateEnd}`;
          break;
      }
      const notification = await models.Notification.create({
        message,
        notiTypeId: notificationType,
        billingId: billing.id
      });

      const users = await models.Users.getManagers(billing.placeId);
      for (const user of users) {

        const notificationUserCreated = await models.NotiUser.create({
          seen: false,
          notificationId: notification.id,
          userId: user.id,
          placeId: billing.placeId
        });

        Socket.dispatch({
          channel: Socket.CHANNELS.USER_ID,
          object: {
            id: user.id
          }
        }, Socket.EVENT_TYPES.NEW_NOTIFICATION, {
          createdAt: billing.createdAt,
          message: message,
          seen: 0,
          billingId: billing.id,
          billingStatus: billing.status,
          billingPlaceId: billing.placeId
        })
          .catch(e => HandleError(null, null, new InternalProcessError(e, {})));

        PushNotification.dispatchBilling(notificationUserCreated.id)
          .catch(e => HandleError(null, null, InternalProcessError(e, {
            payment: payment.dataValues,
            notification: notification.dataValues,
            notificationUser: notificationUserCreated.dataValues
          })));

        const place = await billing.getPlace();
        if (place && user.usersPlaces.notificationEmail) {
          if (TYPE_NOTIFICATION_BY_BILLING.NEW_CUT === notificationType) {
            Email.reservandonosCutNotification(user.email, user.fullname, place.name)
              .catch(e => {
                HandleError(null, null, new InternalProcessError(e, {
                  user: user.toJSON(),
                  billing: billing.toJSON(),
                  place: place.toJSON(),
                }))
              });
          }
        } else if (TYPE_NOTIFICATION_BY_BILLING.LATE_CUT === notificationType) {
          Email.reservandonosNotificationPendingPayment(user.email, user.fullname, place.name, `${billing.dateStart} al ${billing.dateEnd}`)
            .catch(e => {
              HandleError(null, null, new InternalProcessError(e, {
                user: user.toJSON(),
                billing: billing.toJSON(),
                place: place.toJSON(),
              }))
            });
        }
      }
    }
  }
};

/**
 *
 * @param {TYPE_NOTIFICATION_BY_RESERVATION} notificationType
 * @param {number} reservationId
 * @returns {Promise<void>}
 */
const dispatchReservation = async (notificationType, reservationId) => {
  let message = '';
  const reservation = await models.Reservation.findByPk(reservationId);
  if (reservation) {
    message = await getMessageByReservationStatus(notificationType, reservation);
    const notification = await models.Notification.create({
      message,
      notiTypeId: notificationType,
      reservationId: reservation.id
    });
    const users = [
      ...await models.User.getAdministratorsPartners(),
      ...await models.User.getManagers(reservation.placeId)
    ];
    for (const user of users) {
      const notificationUserCreated = await models.NotiUser.create({
        seen: false,
        notificationId: notification.id,
        userId: user.id,
        placeId: reservation.placeId
      });
      Socket.dispatch({
          channel: Socket.CHANNELS.USER_ID,
          object: {
            id: user.id
          }
        },
        Socket.EVENT_TYPES.NEW_NOTIFICATION, {
          createdAt: reservation.createdAt,
          message,
          reservationStatus: reservation.status,
          reservationId: reservation.id,
          reservationType: reservation.type,
          advancePayment: reservation.advancePaymentId
        }
      )
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
      PushNotification.dispatchReservation(notificationUserCreated.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
    }
  }
};

/**
 * @param paymentId {number}
 * @returns {Promise<void>}
 */
const dispatchPayment = async (paymentId) => {
  let message;
  let notiTypeId;
  const payment = await models.Payments.findByPk(paymentId, {
    where: {
      paymentStatus: {
        [Op.in]: [PAYMENTS_STATUS.APPROVED, PAYMENTS_STATUS.DECLINED]
      }
    }
  });
  if (payment) {
    switch (payment.statusPayment) {
      case PAYMENTS_STATUS.APPROVED:
        message = `Pago Aprobado: ${payment.comment}`;
        notiTypeId = NOTIFICATION_TYPE.PAID_CUT;
        break;
      case PAYMENTS_STATUS.DECLINED:
        message = `Pago Rechazado: ${payment.comment}`;
        notiTypeId = NOTIFICATION_TYPE.REJECT_CUT;
        break;
    }
    const notification = await models.Notification.create({
      message,
      notiTypeId,
      billingId: payment.billingId
    });
    const users = [...await models.User.getAdministratorsPartners(), ...await models.User.getManagers(payment.placeId)];
    for (const user of users) {
      const notificationUserCreated = await models.NotiUser.create({
        seen: false,
        notificationId: notification.id,
        userId: user.id,
        placeId: payment.placeId
      });
      Socket.dispatch({
          channel: Socket.CHANNELS.USER_ID,
          object: {
            id: user.id
          }
        },
        Socket.EVENT_TYPES.STATUS_PAYMENT, {
          seen: false,
          notification,
          userId: user.id,
          placeId: payment.placeId
        }
      )
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
      PushNotification.dispatchPayment(notificationUserCreated.id, payment.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
    }
  }
};

/**
 * @param {TYPE_NOTIFICATION_BY_GIFT} notificationType
 * @param {number} giftCardId
 */
const dispatchGiftCard = async (notificationType, giftCardId) => {
  let message;
  const giftCard = await models.GiftCard.findByPk(giftCardId);
  if (giftCard) {
    switch (notificationType) {
      case TYPE_NOTIFICATION_BY_GIFT.NEW_GIFT:
        message = 'Se creó una nueva tarjeta de regalo';
        break;
    }

    const notification = await models.Notification.create({
      message,
      notiTypeId: notificationType,
      giftId: giftCardId.id
    });
    const users = await models.User.getManagers(giftCard.placeId);
    for (const user of users) {
      const notificationUserCreated = await models.NotiUser.create({
        seen: false,
        notificationId: notification.id,
        userId: user.id,
        placeId: payment.placeId
      });
      Socket.dispatch({
          channel: Socket.CHANNELS.USER_ID,
          object: {
            id: user.id
          }
        },
        Socket.EVENT_TYPES.NEW_NOTIFICATION, {
          seen: false,
          notification,
          userId: user.id,
          placeId: giftCard.placeId,
          giftCardId: giftCard.id,
          beneficiaryName: giftCard.beneficiaryName,
          amount: giftCard.amount
        }
      )
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
      PushNotification.dispatchGiftCard(notificationUserCreated.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
    }
  }
};

/**
 * @param {TYPE_NOTIFICATION_BY_COUPON} notificationType
 * @param couponId
 */
const dispatchCoupon = async (notificationType, couponId) => {
  let message;
  const coupon = await models.Coupons.findByPk(couponId, {
    include: [{
      model: models.Promotions,
      as: 'promotion',
      required: true
    }]
  });
  if (coupon) {
    switch (notificationType) {
      case TYPE_NOTIFICATION_BY_COUPON.NEW_COUPON:
        message = 'Nuevo Cupón Disponible';
        break;
      case TYPE_NOTIFICATION_BY_COUPON.REDEEMED_COUPON:
        message = 'Cupón Redimido Correctamente'
        break;
    }

    const notification = await models.Notification.create({
      message,
      notiTypeId: notificationType,
      couponId: coupon.id
    });
    const users = await models.User.getManagers(coupon.promotion.placeId);
    for (const user of users) {
      const notificationUserCreated = await models.NotiUser.create({
        seen: false,
        notificationId: notification.id,
        userId: user.id,
        placeId: coupon.promotion.placeId
      });
      Socket.dispatch({
          channel: Socket.CHANNELS.USER_ID,
          object: {
            id: user.id
          }
        },
        Socket.EVENT_TYPES.NEW_NOTIFICATION, {
          seen: false,
          notification,
          userId: user.id,
          placeId: coupon.promotion.placeId,
        }
      )
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
      PushNotification.dispatchCoupon(notificationUserCreated.id)
        .catch(e => HandleError(null, null, new InternalProcessError(e)));
    }
  }
};

/**
 * @param place {Sequelize.Model<Place>}
 * @returns {Promise<void>}
 */
const dispatchTaxRecordInvalid = async (place) => {
  const message = `RFC Inválido: ${place.name} - ${place.taxRecord}`;
  const notification = await models.Notification.create({
    message,
    notiTypeId: NOTIFICATION_TYPE.ERROR_DATA
  });
  const users = await models.User.getAdministratorsPartners();
  for (const user of users) {
    await models.NotiUser.create({seen: false, notificationId: notification.id, userId: user.id, placeId: place.id});
    Socket.dispatch({
      channel: Socket.CHANNELS.USER_ID,
      object: {
        id: user.id
      }
    }, 'new-notification', {})
      .catch(e => HandleError(null, null, new InternalProcessError(e)));;
  }
};

module.exports = {
  dispatchBilling,
  dispatchTaxRecordInvalid,
  dispatchPayment,
  dispatchReservation,
  dispatchGiftCard,
  dispatchCoupon
};
