const models = require('../models');
const moment = require('moment');
const Op = models.Sequelize.Op;

async function seendMessageRegisteredCustomer(reservation, message) {
  try {
    const c = await models.Costumer.findOne({ where: { id: reservation.costumerId } })
    if (c) {
      let registeredCustomer = await models.RegisteredCustomer.findOne({
        where: { email: c.email }
      });
      if (registeredCustomer) {
        //console.log({ registeredCustomerId: registeredCustomer.id, message });
        await models.CostumerNotification.create({ registeredCustomerId: registeredCustomer.id, message });
      }
    }
  } catch (error) {
  }
}

module.exports.createNotification = async function (notification) {
  return await models.CostumerNotification.create(notification);
}


module.exports.sendNotificationCustomers = async function () {
  const min = Math.floor(moment().minute() / 15) * 15;
  const mmt = moment().minute(min).startOf('minute');

  const reservationsafter = await models.Reservation.findAll({
    include: [{
      model: models.Place,
      as: 'place',
      attributes: ['name'],
      required: true
    }],
    where: {
      datetime: {
        [Op.and]: {
          [Op.gt]: moment(mmt).subtract(2, 'hours').subtract(30, 'minutes').format('YYYY-MM-DD HH:mm'),
          [Op.lte]: moment(mmt).subtract(2, 'hours').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm')
        }
      },
    },
    raw: true
  })

  const reservationsbefore = await models.Reservation.findAll({
    include: [{
      model: models.Place,
      as: 'place',
      attributes: ['name'],
      required: true
    }],
    where: {
      status: 'CONFIRMED',
      datetime:
      {
        [Op.and]: {
          [Op.gt]: moment(mmt).add(2, 'hours').format('YYYY-MM-DD HH:mm'),
          [Op.lte]: moment(mmt).add(2, 'hours').add(15, 'minutes').format('YYYY-MM-DD HH:mm')
        }
      }
    },
    raw: true
  })

  reservationsafter.forEach((r) => seendMessageRegisteredCustomer(r, "Recuerda que tienes una reserva en " + r['place.name']))
  reservationsbefore.forEach((r) => seendMessageRegisteredCustomer(r, "Califica tu experiencia en restaurante " + r['place.name']))
  /*   console.log(moment(mmt).format('YYYY-MM-DD HH:mm'));
  console.log(reservationsbefore)
  console.log(moment(mmt).add(2, 'hours').format('YYYY-MM-DD HH:mm'));
  console.log(moment(mmt).add(2, 'hours').add(15, 'minutes').format('YYYY-MM-DD HH:mm'));
  console.log(reservationsafter)
  console.log(moment(mmt).subtract(2, 'hours').subtract(30, 'minutes').format('YYYY-MM-DD HH:mm'));
  console.log(moment(mmt).subtract(2, 'hours').format('YYYY-MM-DD HH:mm')); */
}
