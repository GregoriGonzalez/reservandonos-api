'use strict';
const {paypal} = require('../config/env');
/**
 *
 * PayPal Node JS SDK dependency
 */
const checkoutPayPal = require('@paypal/checkout-server-sdk');
const {PAYMENTS_GATEWAY, PAYPAL_STATUS_ORDER, PAYMENTS_STATUS, PAYMENTS_TYPE} = require("../helpers/constants.helper");
const models = require("../models");
const {CARD_TYPE} = require("./constants.helper");

/**
 *
 * Returns PayPal HTTP client instance with environment that has access
 * credentials context. Use this instance to invoke PayPal APIs, provided the
 * credentials have access.
 */
const client = () => {
  return new checkoutPayPal.core.PayPalHttpClient(environment());
};

/**
 *
 * Set up and return PayPal JavaScript SDK environment with PayPal access credentials.
 * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
 *
 */
const environment = () => {
  let clientId = process.env.PAYPAL_CLIENT_ID ?? paypal.clientId;
  let clientSecret = process.env.PAYPAL_CLIENT_SECRET ?? paypal.secretId;

  if (process.env.NODE_ENV === 'production') {
    // return new checkoutPayPal.core.LiveEnvironment(
    //   clientId, clientSecret
    // );
  } else {
    return new checkoutPayPal.core.SandboxEnvironment(
      clientId, clientSecret
    );
  }
};

const prettyPrint = async (jsonData, pre = "") => {
  let pretty = "";

  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }

  for (let key in jsonData) {
    if (jsonData.hasOwnProperty(key)) {
      if (isNaN(key))
        pretty += pre + capitalize(key) + ": ";
      else
        pretty += pre + (parseInt(key) + 1) + ": ";
      if (typeof jsonData[key] === "object") {
        pretty += "\n";
        pretty += await prettyPrint(jsonData[key], pre + "    ");
      } else {
        pretty += jsonData[key] + "\n";
      }
    }
  }
  return pretty;
};

const getPayPalPaymentOrder = async (orderId) => {
  let request = new checkoutPayPal.orders.OrdersGetRequest(orderId);
  const order = await client().execute(request).catch(e => {
    return Promise.reject({error: true, message: e});
  });
  return Promise.resolve({error: false, data: order.result});
};

const getPayPalPaymentOrderCapture = async (orderId) => {
  let request = new checkoutPayPal.orders.OrdersCaptureRequest(orderId);
  const order = await client().execute(request).catch(e => {
    return Promise.reject({error: true, message: JSON.parse(e.message)});
  });
  return Promise.resolve({error: false, data: order.result});
};

const getPayPalPaymentCapture = async (captureId) => {
  let request = new checkoutPayPal.payments.CapturesGetRequest(captureId);
  const payment = await client().execute(request).catch(e => {
    return Promise.reject({error: true, message: e});
  });
  return Promise.resolve({error: false, data: payment.result});
};

/**
 * https://developer.paypal.com/api/orders/v2/#orders_create
 * @param body
 * @returns {Promise<{data, error: boolean}>}
 */
const createPayPalOrder = async (body) => {
  const request = new checkoutPayPal.orders.OrdersCreateRequest();
  request.body = body;
  const order = await client().execute(request).catch(e => {
    return Promise.reject({error: true, message: e});
  });
  return Promise.resolve({error: false, data: order.result});
};

const getPaypalPaymentStatus = async (paymentGateway, statusPaymentGateway) => {
  if (paymentGateway === PAYMENTS_GATEWAY.PAYPAL) {
    switch (statusPaymentGateway) {
      case PAYPAL_STATUS_ORDER.COMPLETED:
        return PAYMENTS_STATUS.APPROVED;

      case PAYPAL_STATUS_ORDER.APPROVED:
      case PAYPAL_STATUS_ORDER.CREATED:
      case PAYPAL_STATUS_ORDER.SAVED:
        return PAYMENTS_STATUS.PROCESS;

      case PAYPAL_STATUS_ORDER.VOIDED:
        return PAYMENTS_STATUS.DECLINED;
    }
  }
};

const savePayPalPayment = async (paymentType, orderId, placeId, billingId) => {
  const {data} = await getPayPalPaymentOrder(orderId);
  if (PAYMENTS_TYPE.BILLING === paymentType) {
    return await models.Payments.create({
      paymentGateway: PAYMENTS_GATEWAY.PAYPAL,
      transactionId: data.id,
      statusPaymentGateway: data.status,
      amount: data.purchase_units[0].payments.captures[0].seller_receivable_breakdown.gross_amount.value,
      fee: data.purchase_units[0].payments.captures[0].seller_receivable_breakdown.paypal_fee.value,
      net_amount: data.purchase_units[0].payments.captures[0].seller_receivable_breakdown.net_amount.value,
      paymentDate: data.purchase_units[0].payments.captures[0].create_time,
      statusPayment: await getPaypalPaymentStatus(PAYMENTS_GATEWAY.PAYPAL, data.status),
      comment: data.purchase_units[0].items[0].name,
      currency: data.purchase_units[0].amount.currency_code,
      cardType: CARD_TYPE.CREDIT,
      placeId,
      billingId
    });
  }
};

module.exports = {
  getPayPalPaymentOrder,
  getPayPalPaymentCapture,
  getPayPalPaymentOrderCapture,
  getPaypalPaymentStatus,
  savePayPalPayment,
  createPayPalOrder
};
