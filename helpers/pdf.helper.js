const fs = require('fs-extra');
const path = require("path");
const puppeteer = require('puppeteer');
const handlebar = require('handlebars');

const SERVER_LOCAL = 'http://localhost:' + (process.env.PORT || 3000);
const DEFAULT_LOGO_IMAGE = 'assets/images/default-logo.png';

handlebar.registerHelper('loadResource', (value) => {
  if (/^https:\/\//i.test(value)) {
    return value;
  } else {
    return (value === null || value === undefined)
      ? `${SERVER_LOCAL}/${DEFAULT_LOGO_IMAGE}`
      : `${SERVER_LOCAL}/${value}`;
  }
});

let _browser = null;
const instance = async () => {
  if (_browser === null) {
    _browser = await puppeteer.launch();
  }
  return _browser;
}

const buildTemplate = async (file, data = {}) => {
  const filePath = path.join(process.cwd(), 'templates/hbs/pdfs', `${file}.hbs`);
  const html = await fs.readFile(filePath, 'utf-8');
  return handlebar.compile(html)(data);
};

const downloadPdf = async(html, options = {}) => {
  const browser = await instance();
  const page = await browser.newPage();
  await page.emulateMediaType('screen');
  await page.setContent(html, {waitUntil: "networkidle0"});
  return await page.createPDFStream({
    format: 'A4',
    printBackground: true,
    ...options
  });
}

module.exports = {
  downloadPdf, buildTemplate
}
