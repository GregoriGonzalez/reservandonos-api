const models = require("../models");
const { createNotification } = require('./notificationsCustomer');
const {RESERVATION_STATUS} = require("./constants.helper");
const Op = models.sequelize.Op;

const randomCode = length => {
    let result = '';
    const characters = '!#$%&/=?1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('');
    for (let i = 0; i < length; i++) {
      result += characters[Math.floor(Math.random() * characters.length)];
    }
    return result;
}

const savePointOfMovement = async (isAdd, registeredCustomerId, points) =>{
    let intPoints = parseInt(points);
    const limitAdd = 1000;
    const limitDel = 200;

    let customerPoint = await models.CustomerPoint.findOne({
        attributes: {
            exclude: [`beforePoints`, `points`,'createdAt','updatedAt']
        },
        raw: true,
        where: { registeredCustomerId: registeredCustomerId },
        order: [['createdAt', 'desc']]
    });

    if (isAdd) {
        if (customerPoint) {
            if (customerPoint.afterPoints >= limitAdd) {
                return { CustomerPoint: customerPoint, message: 'El cliente llegó al límite de puntos.', success: false };
            } else {
                let pointsCalc = customerPoint.afterPoints + intPoints;
                if (pointsCalc > limitAdd) {
                    intPoints = limitAdd - customerPoint.afterPoints;
                    pointsCalc = limitAdd;
                }

                const customerPointNew = await models.CustomerPoint.create({
                    registeredCustomerId: customerPoint.registeredCustomerId,
                    beforePoints: customerPoint.afterPoints,
                    points: intPoints,
                    afterPoints: pointsCalc
                });

                await createNotification({ registeredCustomerId, message: "Se te han bonificado " + points + " puntos a tu cuenta." });

                return { CustomerPoint: customerPointNew, message: '', success: true };
            }
        } else {
            const customerPointNew = await models.CustomerPoint.create({
                registeredCustomerId: registeredCustomerId,
                beforePoints: 0,
                points: intPoints,
                afterPoints: intPoints
            });

            await createNotification({ registeredCustomerId, message: "Se te han bonificado " + points + " puntos a tu cuenta." });

            return { CustomerPoint: customerPointNew, message: '', success: true };
        }
    } else {
        if (customerPoint) {
            if (customerPoint.afterPoints >= limitDel) {
                let pointsCalc = customerPoint.afterPoints - intPoints;
                if (pointsCalc < 0) {
                    return { CustomerPoint: customerPoint, message: 'El cliente no cumple con los puntos para la operación.', success: false };
                } else {
                    const customerPointNew = await models.CustomerPoint.create({
                        registeredCustomerId: registeredCustomerId,
                        beforePoints: customerPoint.afterPoints,
                        points: intPoints * -1,
                        afterPoints: pointsCalc
                    });

                    return { CustomerPoint: customerPointNew, message: '', success: true };
                }
            } else {
                return { CustomerPoint: customerPoint, message: 'El cliente no cumple con los puntos minimos para la operación.', success: false };
            }
        } else {
            return { CustomerPoint: null, message: 'El cliente no tiene puntos registrados.', success: false };
        }
    }
};

const checkReservationsToRegisterCustomerByReference = async (email = '') => {
    let registerCustomer = await models.RegisteredCustomer.find({
        where: {
            email
        }
    });

    if (registerCustomer) {
        let reservations = await models.Reservation.findAndCountAll({
            attributes: ['costumerId', 'id'],
            include: [{
                model: models.Costumer,
                as: 'costumer',
                required: true,
                attributes: ['fullname', 'countryCode', 'phone', 'email', 'validEmail'],
                where: {
                    email
                }
            }],
            where: {
                status: {
                    [Op.in]: [RESERVATION_STATUS.GONE, RESERVATION_STATUS.SEATED]
                },
                createdAt: {
                    [Op.gte]: registerCustomer?.createdAt
                }
            },
            limit: 2
        });

        reservations['customer'] = registerCustomer;
        return reservations;

    }

    return false;
}


const getRegisterCustomerById = async (id) => {
    const registeredCustomers = await models.RegisteredCustomer.find({
        where: {
            id
        }
    });

    return registeredCustomers;
}

module.exports = {
    randomCode,
    savePointOfMovement,
    checkReservationsToRegisterCustomerByReference,
    getRegisterCustomerById
}
