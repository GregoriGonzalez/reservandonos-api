const firebase = require("firebase-admin");
const configurationFile = require("../resource_external/reservandonos-partners-firebase-adminsdk-pp0rb-6ac61dadd4.json");
const {
  PAYMENTS_STATUS,
  TYPE_NOTIFICATION_BY_BILLING, NOTIFICATION_TYPE
} = require("./constants.helper");
const models = require('../models');
const {propertyObjectToString} = require("./util");
const Op = models.Sequelize.Op;

firebase.initializeApp({
  credential: firebase.credential.cert(configurationFile)
});

/**
 *
 * @param tokens tokens array string to send message
 * @param title title of notification
 * @param body body of notification
 * @param data object {key: value} for payload
 */
const send = async (tokens, title, body, data) => {
  if (tokens) {
    const payload = propertyObjectToString({
      notification: {
        title,
        body
      },
      data
    });
    const options = {
      priority: "high",
      timeToLive: 60 * 60 * 24,
    };
    await firebase.messaging().sendToDevice(tokens, payload, options);
  }
};

/**
 * @param {number} notificationUserId
 * @returns {Promise<void>}
 */
const dispatchBilling = async (notificationUserId) => {
  let title;
  let body;
  let data;
  const notificationUser = await models.NotiUser.getDetails(notificationUserId);
  const {notification = null, user = null} = notificationUser;
  const {billings} = notification;
  if (user?.tokenFCM !== null) {
    const tokens = [user.tokenFCM];
    switch (notification.notiTypeId) {
      case TYPE_NOTIFICATION_BY_BILLING.NEW_CUT:
        title = 'Nuevo Corte';
        body = `${billings.dateStart} al ${billings.dateEnd}`;
        break;
      case TYPE_NOTIFICATION_BY_BILLING.LATE_CUT:
        title = 'Corte Atrasado';
        body = `${billings.dateStart} al ${billings.dateEnd}`;
        break;
    }
    data = {
      notificationId: notification.id,
      createdAt: notification.createdAt,
      message: notification.message,
      type: notification.notiTypeId,
      seen: notificationUser.seen,
      userId: notificationUser.userId,
      notificationUserId: notificationUser.id,
      billingId: billings.id,
      billingStatus: billings.status,
      billingPlaceId: billings.placeId,
      placeId: billings.placeId,
    };
    await send(tokens, title, body, data);
  }
};

/**
 *
 * @param {number} paymentId
 * @param {number} notificationUserId
 * @returns {Promise<void>}
 */
const dispatchPayment = async (notificationUserId, paymentId) => {
  let title;
  let body;
  let data;
  const payment = await models.Payments.findByPk(paymentId, {
    where: {
      paymentStatus: {
        [Op.in]: [PAYMENTS_STATUS.APPROVED, PAYMENTS_STATUS.DECLINED]
      }
    }
  });
  const notificationUser = await models.NotiUser.getDetails(notificationUserId);
  const {notification = null, user = null} = notificationUser;
  const {billings} = notification;
  if (payment && user?.tokenFCM !== null) {
    const tokens = [user.tokenFCM];
    switch (payment.statusPayment) {
      case PAYMENTS_STATUS.APPROVED:
        title = 'Su pago ha sido completado';
        body = `Pago Confirmado: ${payment.comment}`
        break;
      case PAYMENTS_STATUS.DECLINED:
        title = 'Su pago ha sido rechazado';
        body = `Pago Rechazado: ${payment.comment}`
        break;
    }
    data = {
      notificationId: notification.id,
      createdAt: notification.createdAt,
      message: notification.message,
      type: notification.notiTypeId,
      seen: notificationUser.seen,
      userId: notificationUser.userId,
      notificationUserId: notificationUser.id,
      billingId: billings.id,
      billingStatus: billings.status,
      billingPlaceId: billings.placeId,
      placeId: billings.placeId,
    };
    await send(tokens, title, body, data);
  }
};

/**
 *
 * @param {number} notificationUserId
 * @returns {Promise<void>}
 */
const dispatchReservation = async (notificationUserId) => {
  const notificationUser = await models.NotiUser.getDetails(notificationUserId);
  const {user = null, notification = null} = notificationUser;
  const {reservation = null} = notification;
  if (reservation && user?.tokenFCM !== null) {
    const title = reservation.place.name;
    const body = notification?.message;

    const tokens = [user.tokenFCM];
    const data = {
      notificationId: notification.id,
      createdAt: notification.createdAt,
      message: notification.message,
      type: notification.notiTypeId,

      seen: notificationUser.seen,
      userId: notificationUser.userId,
      notificationUserId: notificationUser.id,

      reservationStatus: reservation.status,
      reservationId: reservation.id,
      reservationType: reservation.type,
      placeId: reservation.placeId,
    };
    await send(tokens, title, body, data);

  }
};

/**
 *
 * @param {number} notificationUserId
 * @returns {Promise<void>}
 */
const dispatchGiftCard = async (notificationUserId) => {
  // const notificationUser = await models.NotiUser.getDetails(notificationUserId);
  // const {user = null, notification = null} = notificationUser;
  // const {giftCard = null} = notification;
  // if (giftCard && user?.tokenFCM !== null) {
  //   const title = notification?.message;
  //   const body = `Beneficiario: ${giftCard.beneficiaryName}`;
  //
  //   const tokens = [user.tokenFCM];
  //   const data = {
  //     notificationId: notification.id,
  //     createdAt: notification.createdAt,
  //     message: notification.message,
  //     type: notification.notiTypeId,
  //
  //     seen: notificationUser.seen,
  //     userId: notificationUser.userId,
  //     notificationUserId: notificationUser.id,
  //
  //     giftCardId: giftCard.id,
  //     beneficiaryName: giftCard.beneficiaryName,
  //     amount: giftCard.amount,
  //     placeId: giftCard.placeId,
  //   };
  //   await send(tokens, title, body, data);
  // }
};

/**
 *
 * @param {number} notificationUserId
 * @returns {Promise<void>}
 */
const dispatchCoupon = async (notificationUserId) => {
  const notificationUser = await models.NotiUser.getDetails(notificationUserId);
  const {user = null, notification = null} = notificationUser;
  const {coupon = null} = notification;
  if (coupon && user?.tokenFCM !== null) {
    const title = notification?.message;
    const body = `${coupon.promotion.title}: ${coupon.code}`;

    const tokens = [user.tokenFCM];
    const data = {
      notificationId: notification.id,
      createdAt: notification.createdAt,
      message: notification.message,
      type: notification.notiTypeId,

      seen: notificationUser.seen,
      userId: notificationUser.userId,
      notificationUserId: notificationUser.id,

      couponId: coupon.id,
      code: coupon.code,
      placeId: coupon.promotion.placeId,
    };
    await send(tokens, title, body, data);
  }
}

module.exports = {send, dispatchBilling, dispatchReservation, dispatchPayment, dispatchGiftCard, dispatchCoupon};
