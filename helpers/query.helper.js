const models = require('../models');
const Op = models.Sequelize.Op;
const {validDateFormat} = require("./utils.helper");
const moment = require("moment/moment");
const {SPONSOR_PLACE_TYPE} = require("./constants.helper");

const typeChargesToApplyInRangeDate = (dateStart, dateEnd) => {
  const start = validDateFormat(dateStart || '2018-01-01');
  const end = validDateFormat(dateEnd || moment().format('YYYY-MM-DD'));
  return {
    through: {
      where: {
        [Op.or]: [
          {
            start: {[Op.lte]: start},
            end: {
              [Op.not]: null,
              [Op.gte]: start
            }
          },
          {
            start: {[Op.lte]: end},
            end: {
              [Op.not]: null,
              [Op.gte]: end
            }
          },
          {
            start: {[Op.lte]: end},
            end: {[Op.is]: null}
          }
        ]
      }
    }
  }
}

module.exports = {typeChargesToApplyInRangeDate};
