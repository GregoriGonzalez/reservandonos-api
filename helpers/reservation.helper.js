const models = require('../models');
const moment = require("moment-timezone");
const {moduleBitacora} = require("../helpers/bitacora");
const { CANCELED_BY } = require("../helpers/constants.helper");

const statusReservation = async (id) => {
    const reservation = await models.Reservation.findByPk(id,{
        attributes: ['id', 'date', 'datetime', 'start', 'peopleCount', 'status', 'smokeArea', 'notes', 'billingId', 'placeId'],
        include: [
            {
                model: models.Place,
                as: 'place',
                attributes: ['id', 'name', 'timezone'],
            },
            {
                model: models.Costumer,
                as: 'costumer',
                attributes: ['id', 'email', 'fullname', 'countryCode', 'phone', 'validEmail', 'emailComplaints']
            }
        ]
    });

    const now = moment().tz(reservation.place.timezone);
    const date = moment.tz(reservation.date, reservation.place.timezone).startOf('days').add(reservation.start, 'minutes');

    return {oldReservation: now.isAfter(date), reservation};
}


const confirmedReservation = async (reservation, reConfirmed, user = null, emailModule) => {
    await reservation.save();
    if ( reservation.costumer && reservation.costumer.email
         && reservation.costumer.validEmail && reservation.costumer.emailComplaints < 3) {
        if (reConfirmed) {
            moduleBitacora.createBitacora('UPDATE', `[UPDATE] - Reservación ReConfirmada por el cliente`, 'Reservations', reservation.id, null, reservation.placeId);
        } else {
            moduleBitacora.createBitacora('UPDATE', `[UPDATE] - Reservación Confirmada por el Partner ${user ? user : ''}`, 'Reservations', reservation.id, null, reservation.placeId);
            await emailModule.reservationEmail(reservation.id);
        }
    }

    const placeUsers = await models.User.findAll({
        attributes: ['fullname', 'email'],
        include: [
            {
                model: models.Place,
                as: 'places',
                attributes: ['id'],
                required: true,
                through: {
                    attributes: ['placeId', 'userId', 'notificationEmail', 'notificationWhatsApp'],
                    where: {placeId: reservation.place.id}
                }
            }]
    });

    for (const user of placeUsers) {
        if (user.places.length && user.places[0].UsersPlaces.notificationEmail) {
            if (reConfirmed) {
                emailModule.reservandonosReConfirmedReservationFromClient(user.email, reservation.place.name, reservation, reservation.costumer);
            } else {
                emailModule.reservandonosConfirmedReservationManager(user.email, reservation.place.name, reservation, reservation.costumer);
            }
        }
    }
}

const cancelledReservation = async (reservation, isClient, emailModule, moduleWhatsApp) => {
    reservation.canceledBy = isClient ? CANCELED_BY.CUSTOMER : CANCELED_BY.PARTNER;
    await reservation.save();
    if  (!reservation?.hour) {
        reservation.hour = reservation?.start ? moment(reservation.date).add(reservation.start, 'minutes').format('H:mm') : '';
    }

    moduleBitacora.createBitacora('UPDATE', `[UPDATE] - Reservación Cancelada desde enlace ${isClient ? 'CLIENTE' : 'PARTNER'}`, 'Reservations', reservation.id, null, reservation.placeId);
    if (reservation.costumer && reservation.costumer.email && reservation.costumer.validEmail && reservation.costumer.emailComplaints < 3) {
        await emailModule.reservationEmail(reservation.id, false, isClient);
    }


    if (isClient) {
        const placeUsers = await models.User.findAll({
            attributes: ['fullname', 'email', 'phone'],
            include: [
                {
                    model: models.Place,
                    as: 'places',
                    attributes: ['id'],
                    required: true,
                    through: {
                        attributes: ['placeId', 'userId', 'notificationEmail', 'notificationWhatsApp'],
                        where: {
                            placeId: reservation.place.id
                        }
                    }
                },
            ]
        });

        let arrayPhones = [];
        for (const user of placeUsers) {
            if (user.places?.length && user.places[0].UsersPlaces.notificationEmail) {
                emailModule.reservandonosCancelledReservationManager(user.email, reservation.place.name, reservation, reservation.costumer);
            }
            if (user.places.length && user.places[0].UsersPlaces.notificationWhatsApp) {
                arrayPhones.push(user.phone);
            }
        }
        if (arrayPhones.length > 0) {
            moduleWhatsApp.notificationPartnerCancelled(reservation, arrayPhones);
        }
    }
}

const schedulesPlaces = async (placeId, timezone) => {
    const schedules = await models.WorkSchedule.findAll({
        where: {
            placeId: placeId
        }
    });

    if (schedules.length > 0) {
        let now = moment().tz(timezone);
        let minutes = moment.duration({
            seconds: Number(now.format('ss')),
            minutes: Number(now.format('mm')),
            hours: Number(now.format('HH'))
        }).asMinutes();
        let weekday = now.format('dddd').toLowerCase();
        let i = 0;
        let response;
        let nextDay;
        do {
            const day = schedules.find(elem => elem.weekday === now.format('dddd').toLowerCase());
            if (day) {
                console.log(day.weekday);
                if (weekday === day.weekday && day.start <= minutes && day.end >= minutes) {
                    return {template: 'standard', value: null};
                } else {
                    if ((weekday === day.weekday && day.start > minutes) || nextDay) {
                        now.locale('es');
                        return {template: 'custom', value: `${now.startOf('day').add('minutes', day.start).format('dddd HH:mm A')}`, minutes: day.start};
                    } else if (day.end < minutes && weekday === day.weekday) {
                        now.add(1, 'days');
                        nextDay = true;
                    } else {
                        now.locale('es');
                        return {template: 'custom', value: `${now.startOf('day').add('minutes', day.start).format('dddd HH:mm A')}`, minutes: day.start};
                    }
                }
                i++;
            } else {
                now.add(1, 'days');
            }
        } while(i <= schedules.length && !response);
        return response ? response : {template: 'standard', value: null};
    } else {
        return {template: 'standard', value: null};
    }
}

module.exports = {
    statusReservation,
    confirmedReservation,
    cancelledReservation,
    schedulesPlaces
}
