const Redis = require('../redis');
const models = require("../models");

const deleteServiceByPlaceId = async (placeId) => {
  const services = await models.Service.findAll({
    where: {
      placeId: placeId
    }
  });

  if (services) {
    for (const service of services) {
      Redis.deleteZoneByPlaceId(service.id, placeId);
    }
  }
}

module.exports = {deleteServiceByPlaceId};
