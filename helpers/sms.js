const AWS = require('aws-sdk');
const env = require('../config/env');
const models  = require('../models');
const moment = require('moment-timezone');
const encryption = require('./encryption');

AWS.config.accessKeyId = env.awsAccessKeyId;
AWS.config.secretAccessKey = env.awsSecretAccessKey;
AWS.config.region = env.awsRegion;

const smsModule = {};
const baseUrl = `${env.dns}/#/s/`;

smsModule.sendSMS = (phone, message) => {
  const SNS = new AWS.SNS();
  const params = {
    Message: message,
    PhoneNumber: phone,
  };

  const publishTextPromise = SNS.publish(params).promise();

  return new Promise((resolve, reject) => {
    publishTextPromise.then(data => {
      console.log("MessageID is " + data.MessageId);
      resolve('sent');
    }).catch(err => {
      resolve('failed');
      console.error(err, err.stack);
    });
  });
};

function generateRandom(length) {
  const chars = [..."ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
  return [...Array(length)].map(i=>chars[Math.random()*chars.length|0]).join``;
}

smsModule.cardInfoSMS = async (date, placeName, phone, long) => {
  try {
    console.log('card info sms')
    const dayAfter = moment(date).add(1, 'day');
    const short = generateRandom(6)
    await models.Shorter.create({ short, long, type: "ADD_CARD", delete_at: dayAfter });
    const url = `${baseUrl}${short}`;
    const message = `Confirma tu reserva en ${placeName} con una forma de pago. ${url}`;
    console.log(message);
    smsModule.sendSMS(phone, message);
  } catch (error) {
    console.error(error);
  }
}

smsModule.reconfirm = async (r) => {
  try {
    const dateTime = moment(r.date);
    const time = dateTime.add(r.start, 'minutes').format('H:mm');
    const hoursLater = moment(r.datetime).add(5, 'hours');
    const short = generateRandom(4);
    const long = encryption.encrypt(JSON.stringify({
      reservationId: r.id,
      placeId: r.placeId,
      name: r.costumer.fullname,
      email: r.costumer.email
    }));
    await models.Shorter.create({ short, long, type: "RE_CONFIRM", delete_at: hoursLater });
    const placeName = r.place.name;
    const url = `${baseUrl}${short}`;
    const message = `Re confirma tu reserva en ${placeName} a las ${time}. ${url}`;
    const phone = r.costumer.countryCode + r.costumer.phone;
    smsModule.sendSMS(phone, message);
  } catch (error) {
    console.error(error);
  }
}

smsModule.review = async r => {
  try {
    const short = generateRandom(4);
    const long = encryption.encrypt(JSON.stringify({
      reservationId: r.id,
      placeId: r.placeId,
      name: r.costumer.fullname,
      email: r.costumer.email
    }));
    const twoWeeksLater =  moment(r.date).add(15, 'days');
    await models.Shorter.create({ short, long, type: "REVIEW", delete_at: twoWeeksLater });
    const placeName = r.place.name;
    const url = `${baseUrl}${short}`;
    const message = `Danos tu opinión sobre tu experiencia en ${placeName}. ${url}`;
    const phone = r.costumer.countryCode + r.costumer.phone;
    smsModule.sendSMS(phone, message);
  } catch (error) {
    console.error(error);
  }
}

smsModule.rsvPartnersSendSMS = async (reservation, phone, user, customer, placeName) => {
  try {
    const short = generateRandom(4);
    const long = encryption.encrypt(JSON.stringify({ reservationId: reservation.id }));
    const twoWeeksLater =  moment(reservation.date).add(1, 'days');

    await models.Shorter.create({ short, long, type: "CONFIRMED", delete_at: twoWeeksLater });

    const confirmUrl = `${env.dns}/#/rsv/reservation-confirmed/${short}`;
    const cancelUrl = `${env.dns}/#/rsv/reservation-cancelled/${short}`;
    const systemUrl = `${env.dns}/#/rsv/`;

    let day = moment(reservation.date).locale('es');
    day.set({h: 0, m: 0, s: 0});
    day.add(reservation.start, 'minutes');

    const message = `Reservándonos Partners\nHola ${user.fullname}, has recibido una nueva reservación en ${placeName}.\nDetalle de la reservación:\n\nMesa para ${reservation.peopleCount}, ${day.format('dddd')}, ${day.format('DD')} de ${day.format('MMMM')} de ${day.format('YYYY')} a las ${day.format('HH:mm')} a nombre de ${customer.fullname}.\n\nConfirmar Ahora:\n${confirmUrl}\n\nEntrar al sistema\n ${systemUrl}`;
    smsModule.sendSMS(phone, message);
  } catch (error) {
    console.error(error);
  }
}

module.exports = smsModule;
