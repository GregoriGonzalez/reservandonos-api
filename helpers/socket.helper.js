const IO = require("../socket");
const {ReplaceKeyByValue} = require("./string.helper");

const EVENT_TYPES = {
  NEW_NOTIFICATION: 'new-notification',
  NEW_NOTIFICATION_PARTNER: 'new-reservation-admin',
  STATUS_PAYMENT: 'status-payment'
};

const CHANNELS = {
  USER_ID: 'user_{{id}}'
}

/**
 * @typedef {Object} ChannelKeyValue
 * @property {CHANNELS} channel
 * @property {key, value} object key value for replace
 */

/**
 *
 * @param {ChannelKeyValue} channelKeyValue
 * @param {EVENT_TYPES} event
 * @param {object} data
 */
const dispatch = async (channelKeyValue, event, data) => {
  const io = IO.getIO();
  io.to(ReplaceKeyByValue(channelKeyValue.channel, channelKeyValue.object)).emit(event, data);
};

/**
 * @param {EVENT_TYPES} event
 * @param {object} data
 */
const dispatchBroadcast = async (event, data) => {
  const io = IO.getIO();
  io.emit(event, data);
};

module.exports = {
  CHANNELS,
  EVENT_TYPES,
  dispatch,
  dispatchBroadcast
};
