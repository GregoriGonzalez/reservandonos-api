const _ = require('lodash');
const path = require("path");
const fs = require("fs");

const Capitalize = (word) => word.split(' ').map(str => _.capitalize(str)).join(' ');

const ReplaceKeyByValue = (str, keysValues) => {
  for (const key in keysValues) {
    str = str.replace('{{' + key + '}}', keysValues[key]);
  }
  return str;
};

const toBase64 = async (pathRelativeToFile) => {
  const pathFile = path.join(process.cwd(), pathRelativeToFile);
  fs.readFile(pathFile, {encoding: 'base64'}, (err, data) => {
    if (err) {
      Promise.reject(err)
    }
    console.log(data);
    Promise.resolve(data);
  });
};

module.exports = {
  Capitalize,
  ReplaceKeyByValue,
  toBase64,
}
