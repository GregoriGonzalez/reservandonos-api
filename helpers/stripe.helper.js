'use strict';
const {stripe: {secretId}} = require('../config/env');
const models = require("../models");
const {PAYMENTS_TYPE, PAYMENTS_GATEWAY, PAYPAL_STATUS_ORDER, PAYMENTS_STATUS} = require("../helpers/constants.helper");
const Stripe = require("stripe")(secretId);
const Calculate = require("./calculate.helper")
const moment = require("moment");
const {STRIPE_STATUS_ORDER} = require("./constants.helper");

const createPaymentIntent = async (params) => {
  return await Stripe.paymentIntents.create(params);
}

const getOrder = async (id) => {
  return await Stripe.paymentIntents.retrieve(id);
}

const getPaymentStatus = async (paymentGateway, statusPaymentGateway) => {
  if ([PAYMENTS_GATEWAY.STRIPE, PAYMENTS_GATEWAY.OXXO].includes(paymentGateway)) {
    switch (statusPaymentGateway.toUpperCase()) {
      case STRIPE_STATUS_ORDER.SUCCEEDED:
        return PAYMENTS_STATUS.APPROVED;

      case STRIPE_STATUS_ORDER.REQUIRES_PAYMENT_METHOD:
      case STRIPE_STATUS_ORDER.REQUIRES_ACTION:
      case STRIPE_STATUS_ORDER.REQUIRES_CONFIRMATION:
      case STRIPE_STATUS_ORDER.PROCESSING:
        return PAYMENTS_STATUS.PROCESS;

      case STRIPE_STATUS_ORDER.CANCELED:
      case STRIPE_STATUS_ORDER.REQUIRES_SOURCE:
        return PAYMENTS_STATUS.DECLINED;
    }
  }
}

/**
 * Save a payment according to the type of transaction
 */
const savePayment = async (paymentType, paymentGateway, transactionId, placeId, billingId) => {
  const data = await getOrder(transactionId);
  if (PAYMENTS_TYPE.BILLING === paymentType) {
    return await models.Payments.create({
      paymentGateway,
      transactionId: data.id,
      statusPaymentGateway: data.status,
      amount: await Calculate.divide(2, data.amount, 100),
      fee: await Calculate.divide(2, data.amount_capturable, 100),
      net_amount: await Calculate.divide(2, data.amount_received, 100),
      paymentDate: moment.unix(data.created).format('YYYY-MM-DD'),
      statusPayment: await getPaymentStatus(paymentGateway, data.status),
      comment: data.description,
      currency: data.currency?.toUpperCase(),
      cardType: data.charges?.data[0]?.payment_method_details?.card?.funding?.toUpperCase(),
      linkOxxo: data.next_action?.oxxo_display_details?.hosted_voucher_url,
      placeId,
      billingId
    });
  }
}

const getCustomer = async (placeId) => {
  const place = await models.Place.findOne({
    where: {id: placeId},
    include: [{
      model: models.City,
      as: 'cities',
      include: [{
        model: models.State,
        as: 'state',
        include: [{
          model: models.Country,
          as: 'country'
        }]
      }]
    }]
  });
  if (place.stripeCustomerId) {
    return await retrieveCustomer(place.stripeCustomerId);
  } else {
    const customer = await createCustomer(place);
    place.stripeCustomerId = customer.id;
    await place.save();
    return customer;
  }
};

/**
 * @Documentation https://stripe.com/docs/api/customers/create
 */
const createCustomer = async (place) => {
  const params = {
    name: place.name,
    description: place.name,
    email: place.email,
    phone: place.phone,
    address: {
      country: place.cities?.state?.country?.code,
      state: place.cities?.state?.name,
      city: place.cities?.name,
      line1: place.address
    },
    metadata: {
      placeId: place.id
    },
    shipping: {
      address: {
        country: place.cities?.state?.country?.code,
        state: place.cities?.state?.name,
        city: place.cities?.name,
        line1: place.address
      },
      name: place.name
    }
  };
  return await Stripe.customers.create(params);
}

/**
 * @Documentation https://stripe.com/docs/api/customers/retrieve
 */
const retrieveCustomer = async (customerId) => {
  return await Stripe.customers.retrieve(customerId);
}

/**
 * @Documentation https://stripe.com/docs/api/cards/create
 */
const attachPaymentMethodToCustomer = async (customerId, paymentMethodId) => {
  return await Stripe.paymentMethods.attach(paymentMethodId, {
    customer: customerId
  });
}

/**
 * @Documentation https://stripe.com/docs/api/cards/list
 */
const fetchCardsOfCustomer = async (customerId) => {
  return await Stripe.paymentMethods.list({type: "card", customer: customerId, limit: 100});
}

/**
 * @Documentation https://stripe.com/docs/api/customers/update
 */
const setCardByDefault = async (customerId, paymentMethodId) => {
  return await Stripe.customers.update(customerId, {
    invoice_settings: {
      default_payment_method: paymentMethodId
    }
  });
}

/**
 * @Documentation https://stripe.com/docs/api/cards/delete
 */
const deleteCardOfTheCustomer = async (paymentMethodId) => {
  return await Stripe.paymentMethods.detach(paymentMethodId);
}

const attachCustomerToPaymentIntent = async (paymentIntentId, placeId) =>{
  const customer = await getCustomer(placeId);
  return await Stripe.paymentIntents.update(paymentIntentId, {customer: customer.id});
}

module.exports = {
  attachCustomerToPaymentIntent,
  getCustomer,
  createCustomer,
  retrieveCustomer,
  createPaymentIntent,
  attachPaymentMethodToCustomer,
  deleteCardOfTheCustomer,
  getPaymentStatus,
  getOrder,
  savePayment,
  fetchCardsOfCustomer,
  setCardByDefault
};
