const env = require('../config/env');
const models  = require('../models');
const moment = require('moment-timezone');
const encryption = require('./encryption');
const { moduleDialog360 } = require('./dialog360');
const { RESERVATION_STATUS, TEMPLATES_WHATSAPP } = require('./constants.helper');
const {schedulesPlaces} = require("../helpers/reservation.helper");
const {getInfoGroup} = require('../helpers/utils.helper');
const Op = models.Sequelize.Op;

const WhatsAppModule = {}

const getParams = (reservation) => {
  return [
    { "type": "text", "text": `${reservation['place']['name']}` },
    { "type": "text", "text": `${moment(reservation['date']).format('DD-MM-YYYY')} a las ${reservation['hour']}` },
    { "type": "text", "text": `${reservation['peopleCount']}` },
    { "type": "text", "text": `${reservation['smokeArea'] == 0 || !reservation['smokeArea'] ? 'Cualquiera' : 'Fumadores'}` },
    { "type": "text", "text": `${reservation['costumer']['fullname']}` },
    { "type": "text", "text": `${reservation['costumer']['countryCode']}${reservation['costumer']['phone']}` },
    { "type": "text", "text": `${reservation['costumer']['email'] ? reservation['costumer']['email'] : ''}` },
    { "type": "text", "text": `${reservation['notes'] ? reservation['notes'] : ' '}` }
  ]
}

function generateRandom(length) {
  let chars = [..."ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
  return [...Array(length)].map(i=>chars[Math.random()*chars.length|0]).join``;
}

WhatsAppModule.sendForcePartner = async (reservation, phone, code, cancel) => {
  let params = getParams(reservation);
  params.unshift({ "type": "text", "text": `${code}` });
  await moduleDialog360.sendTemplateWhatsAppIterativoOneButton(
      cancel ? TEMPLATES_WHATSAPP.PARTNER.FORCE_CANCELLED : TEMPLATES_WHATSAPP.PARTNER.FORCE_CONFIRMED,
      params,
      phone,
      `reservation-${cancel ? RESERVATION_STATUS.CANCELLED : RESERVATION_STATUS.CONFIRMED}-${reservation.id}-${code}-${phone}-true`
  );
}

WhatsAppModule.notificationPartnerCancelled = async (reservation, phones) => {
  let arrayPhones = [];
  for (let i = 0; i < phones.length; i++) {
    let phone = phones[i].replace(/-/g,'').replace(/ /g, '');
    if (moduleDialog360.verifyNumberPhone(phone)) {
      arrayPhones.push(phone);
    }
  }

  if (arrayPhones.length > 0) {
    let responseContacts = await moduleDialog360.checkContacts(arrayPhones);
    if (responseContacts && responseContacts['contacts']) {
      for (var wa of responseContacts['contacts']) {
        if (wa.status == 'valid') {
          await moduleDialog360.sendTemplateWhatsAppDefault(TEMPLATES_WHATSAPP.PARTNER.CANCELLED, getParams(reservation), wa.wa_id);
        }
      }
    }
  }
}

WhatsAppModule.sendCoupon = async (phones, values) => {
  let arrayPhones = [];
  for (let i = 0; i < phones.length; i++) {
    let phone = phones[i].replace(/-/g,'').replace(/ /g, '');
    if (moduleDialog360.verifyNumberPhone(phone)) {
      arrayPhones.push(phone);
    }
  }

  if (arrayPhones.length > 0) {
    let responseContacts = await moduleDialog360.checkContacts(arrayPhones);
    if (responseContacts && responseContacts['contacts']) {
      for (var wa of responseContacts['contacts']) {
        if (wa.status == 'valid') {
          await moduleDialog360.sendTemplateWhatpAppOneButton(TEMPLATES_WHATSAPP.CUSTOMER.COUPON, [
            {"type": "text", "text": `${values['fullname']}`},
            {"type": "text", "text": `${values['coupon']}`},
            {"type": "text", "text": `${values['promotionName']}`},
            {"type": "text", "text": `${values['placeName']}`},
            {"type": "text", "text": `${values['start']}`},
            {"type": "text", "text": `${values['end']}`}
          ], wa.wa_id, `${values['coupon']}`);
        }
      }
    }
  }
}

WhatsAppModule.sendFrinend = async (reservation, phones) => {
  let arrayPhones = [];
  for (let i = 0; i < phones.length; i++) {
    let phone = phones[i].replace(/-/g,'').replace(/ /g, '');
    if (moduleDialog360.verifyNumberPhone(phone)) {
      arrayPhones.push(phone);
    }
  }

  if (arrayPhones.length > 0) {
    let responseContacts = await moduleDialog360.checkContacts(arrayPhones);
    if (responseContacts && responseContacts['contacts']) {
      for (var wa of responseContacts['contacts']) {
        if (wa.status == 'valid') {
          await moduleDialog360.sendTemplateWhatsAppDefault(TEMPLATES_WHATSAPP.CUSTOMER.FRIEND, [
            {"type": "text", "text": `${reservation['costumer']['fullname']}`},
            {"type": "text", "text": `${reservation['place']['name']}`},
            {"type": "text", "text": `${reservation['date']}`},
            {"type": "text", "text": `${reservation['hour']}`},
            {"type": "text", "text": `${reservation['place']['address']}`}
          ], wa.wa_id);
        }
      }
    }
  }
}

WhatsAppModule.notificationClient = async (reservation, status, sponsor, isClient = false) => {
  let send = false;
  let phone = reservation.costumer?.phone?.indexOf('+') != -1
    ? reservation.costumer.phone
    : `${reservation.costumer.countryCode}${reservation.costumer.phone}`;

  if (phone) {
    phone = phone.replace(/-/g,'').replace(/ /g, '');
    if (moduleDialog360.verifyNumberPhone(phone)) {
      let responseContacts = await moduleDialog360.checkContacts([phone]);
      let groupInvitation;
      if (responseContacts && responseContacts['contacts']) {
        for (var wa of responseContacts['contacts']) {
          if (wa.status == 'valid') {
            let template;
            let objTemplate;
            if (status == RESERVATION_STATUS.PENDING) {
              objTemplate = await schedulesPlaces(reservation.place.id, reservation.place.timezone);
              groupInvitation = getInfoGroup(reservation?.place);
              if (objTemplate.template === 'standard') {
                template = groupInvitation?.data ? TEMPLATES_WHATSAPP.CUSTOMER.PENDIENTE_TW : TEMPLATES_WHATSAPP.CUSTOMER.PENDIENTE;
              } else if (objTemplate.template === 'custom') {
                template = groupInvitation?.data ? TEMPLATES_WHATSAPP.CUSTOMER.PENDIENTE_WORK_SCHEDULES_TW : TEMPLATES_WHATSAPP.CUSTOMER.PENDIENTE_WORK_SCHEDULES;
              }
            }

            if (status == RESERVATION_STATUS.CONFIRMED || status == RESERVATION_STATUS.RE_CONFIRMED) {
              template = sponsor ? TEMPLATES_WHATSAPP.CUSTOMER.CONFIRMED_SPONSOR : TEMPLATES_WHATSAPP.CUSTOMER.CONFIRMED;
            }

            if (status == RESERVATION_STATUS.CANCELLED) {
              if (isClient) {
                // template = sponsor ? TEMPLATES_WHATSAPP.CUSTOMER.CANCELLED_FROM_CUSTOMER_SPONSOR : TEMPLATES_WHATSAPP.CUSTOMER.CANCELLED_FROM_CUSTOMER;
                template = TEMPLATES_WHATSAPP.CUSTOMER.CANCELLED_FROM_CUSTOMER;
              } else {
                // template = sponsor ? TEMPLATES_WHATSAPP.CUSTOMER.CANCELLED_SPONSOR : TEMPLATES_WHATSAPP.CUSTOMER.CANCELLED;
                template = TEMPLATES_WHATSAPP.CUSTOMER.CANCELLED;
              }
            }

            if (template) {
              let params = [
                { "type": "text", "text": `${reservation['costumer']['fullname']}` },
                { "type": "text", "text": `${reservation['place']['name']}`.toUpperCase() },
                { "type": "text", "text": `${reservation['date']}` },
                { "type": "text", "text": `${reservation['hour']}` },
                { "type": "text", "text": `${reservation['peopleCount']}` },
                { "type": "text", "text": `${reservation['place']['address']}` }
              ];
              if (status === RESERVATION_STATUS.PENDING && objTemplate?.template === 'custom') {
                params.push({ "type": "text", "text": `del ${objTemplate?.value}` });
              }

              if (groupInvitation?.data) {
                params.push({ "type": "text", "text": `${groupInvitation.info.NAME}`});
                params.push({ "type": "text", "text": `${groupInvitation.info.TELEGRAM}`});
                params.push({ "type": "text", "text": `${groupInvitation.info.WHATSAPP}`});
              }
              await moduleDialog360.sendTemplateWhatsAppDefault(template, params, wa.wa_id);
            }
          }
        }
      }
    }
  }
};

WhatsAppModule.sendWhatsAppClientRemember = async (minutes, type) => {
  const mmt = moment().tz("America/Mexico_City");
  const day = mmt.format('YYYY-MM-DD');
  mmt.add(minutes, 'minutes');
  const start = moment.duration(mmt.format('H:mm')).asMinutes();
  const end = start + 15;

  let where = {};

  if (day !== mmt.format('YYYY-MM-DD')) {
    where = {
      [Op.or]: [
        {
          date: mmt.format('YYYY-MM-DD'),
          start: {
            [Op.gt]: start,
            [Op.lte]: end
          }
        }
      ]
    }
  } else {
   where.start = {
      [Op.gt]: start,
      [Op.lte]: end
    };
    where.date = day;
  }

  where.partnerId = 1;
  if (type === 'latest') {
    where.status = { [Op.or]: [RESERVATION_STATUS.CONFIRMED, RESERVATION_STATUS.RE_CONFIRMED]};
  } else {
    where.status = RESERVATION_STATUS.CONFIRMED;
  }

  let reservations = await models.Reservation.findAll({
    attributes: ['id', 'placeId', 'date', 'start', 'peopleCount', 'partnerId'],
    include: [{
      model: models.Costumer,
      as: 'costumer',
      attributes: ['fullname', 'email', 'phone', 'countryCode']
    },{
      model: models.Place,
      as: 'place',
      attributes: ['name', 'phone'],
      include: [
        {
          model: models.SponsorPlaces,
          as: 'sponsorsPlaces',
          attributes: ['type', 'sponsorId', 'isActive']
        }
      ]
    }],
    where: where
  });

  let contacts = [];
  let clients = [];
  reservations.forEach(res => {
    if (res.costumer) {
      let phone = res.costumer.phone.indexOf('+') != -1 ? res.costumer.phone : `${res.costumer.countryCode}${res.costumer.phone}`;
      phone = phone.replace(/-/g,'').replace(/ /g, '');
      if (moduleDialog360.verifyNumberPhone(phone) && res.partnerId == 1) {
        contacts.push(phone);
        clients.push({phone: phone, data: res});
      }
    }
  });

  if (contacts.length > 0) {
    let responseContacts = await moduleDialog360.checkContacts(contacts);
    if (responseContacts && responseContacts['contacts']) {
      for (var wa of responseContacts['contacts']) {
        if (wa.status == 'valid') {
          let reservation = clients.filter(elem => elem.phone == wa.input);
          const short = generateRandom(6);
          const long = encryption.encrypt(JSON.stringify({ reservationId: reservation[0]['data']['id'] }));
          const twoWeeksLater =  moment(reservation[0]['data']['date']).add(1, 'days');
          await models.Shorter.create({ short, long, type: "CONFIRMED", delete_at: twoWeeksLater });

          let dayRsv = moment(reservation[0]['data']['date']).locale('es');
          dayRsv.set({h: 0, m: 0, s: 0});
          dayRsv.add(reservation[0]['data']['start'], 'minutes');

          let sponsor = reservation[0]['data']['place']['sponsorsPlaces']['length'] > 0 ? reservation[0]['data']['place']['sponsorsPlaces'] : [];

          let template;
          if (type === 'latest') {
            await moduleDialog360.sendTemplateWhatsAppIterativoOneButton(
                TEMPLATES_WHATSAPP.CUSTOMER.REMEMBER_LATEST, [
                  { "type": "text", "text":`${reservation[0]['data']['costumer']['fullname']}` },
                  { "type": "text", "text":`${reservation[0]['data']['peopleCount']}` },
                  { "type": "text", "text":`${reservation[0]['data']['place']['name']}` },
                  { "type": "text", "text": `el ${dayRsv.format('DD')} de ${dayRsv.format('MMMM')} del ${dayRsv.format('YYYY')} a las ${dayRsv.format('HH:mm')}` },
                  { "type": "text", "text": `${reservation[0]['data']['place']['name']}` }
                ],
                wa.wa_id,
                `reservation-${RESERVATION_STATUS.CANCELLED}-${reservation[0]['data']['id']}-${short}-${wa.wa_id}-true-customer`
            );
          } else {
            template = sponsor.filter(elem => elem.isActive == 1 && elem.sponsorId == 2).length > 0 ? TEMPLATES_WHATSAPP.CUSTOMER.REMEMBER_SPONSOR : TEMPLATES_WHATSAPP.CUSTOMER.REMEMBER;
            await moduleDialog360.sendTemplateWhatsAppIterativoTwoButtons(
                template, [
                  { "type": "text", "text":`${reservation[0]['data']['costumer']['fullname']}` },
                  { "type": "text", "text":`${reservation[0]['data']['peopleCount']}` },
                  { "type": "text", "text":`${reservation[0]['data']['place']['name'].toUpperCase()}` },
                  { "type": "text", "text": `el ${dayRsv.format('DD')} de ${dayRsv.format('MMMM')} del ${dayRsv.format('YYYY')} a las ${dayRsv.format('HH:mm')}` }
                ],
                wa.wa_id,
                `reservation-${RESERVATION_STATUS.CANCELLED}-${reservation[0]['data']['id']}-${short}-${wa.wa_id}-true-customer`,
                `reservation-${RESERVATION_STATUS.RE_CONFIRMED}-${reservation[0]['data']['id']}-${short}-${wa.wa_id}-true-customer`
            );
          }
        }
      }
    }
  }
};

WhatsAppModule.sendWhatsAppNewReservation = async (place, reservation, customer, emailModule) => {
  const placeUsers = await models.User.findAll({
    attributes: ['fullname', 'email', 'phone'],
    include: [
      {
        model: models.Place,
        as: 'places',
        attributes: ['id'],
        required: true,
        through: {
          where: {
            placeId: place.id
          }
        }
      }]
  });

  let contacts = [];
  placeUsers.forEach(user => {
    if (user.places[0].UsersPlaces.notificationEmail) {
      emailModule.confirmReservandonosReservation(user.email, place.name, user.fullname, reservation, customer);
    }
    if (user.places[0].UsersPlaces.notificationWhatsApp && moduleDialog360.verifyNumberPhone(user.phone) && reservation.partnerId == 1) {
      contacts.push(user.phone);
    }
  });

  if (contacts.length > 0) { // Checking Send WhatsApp
    let responseContacts = await moduleDialog360.checkContacts(contacts);
    if (responseContacts && responseContacts['contacts']) {

      const short = generateRandom(6);
      const long = encryption.encrypt(JSON.stringify({ reservationId: reservation.id }));
      const twoWeeksLater =  moment(reservation.date).add(1, 'days');

      await models.Shorter.create({ short, long, type: "CONFIRMED", delete_at: twoWeeksLater });

      let day = moment(reservation.date).locale('es');
      day.set({h: 0, m: 0, s: 0});
      day.add(reservation.start, 'minutes');

      for (var wa of responseContacts['contacts']) {
        if (wa.status == 'valid') {
          await moduleDialog360.sendTemplateWhatsAppIterativoTwoButtons(
              TEMPLATES_WHATSAPP.PARTNER.NEW,
              [
                { "type": "text", "text": short },
                { "type": "text", "text": `${place.name}` },
                { "type": "text", "text": `${day.format('DD')} de ${day.format('MMMM')} de ${day.format('YYYY')} a las ${day.format('HH:mm')}` },
                { "type": "text", "text": `${reservation.peopleCount}` },
                { "type": "text", "text": `${reservation.smokeArea == 0 || !reservation.smokeArea ? 'Cualquiera' : 'Fumadores'}` },
                { "type": "text", "text": `${customer.fullname}` },
                { "type": "text", "text": `${customer.countryCode}${customer.phone}` },
                { "type": "text", "text": `${customer.email ? customer.email : ''}` },
                { "type": "text", "text": `${reservation.notes ? reservation.notes : ' '}` }
              ],
              wa.wa_id,
             `reservation-${RESERVATION_STATUS.CANCELLED}-${reservation.id}-${short}-${wa.wa_id}-false-partner`,
             `reservation-${RESERVATION_STATUS.CONFIRMED}-${reservation.id}-${short}-${wa.wa_id}-false-partner`
          );
        }
      }
    }
  }
}

module.exports = {
  moduleWhatsApp: WhatsAppModule
}
