const Polyglot = require('node-polyglot');
const i18n = require('../i18n/i18n');

exports.startPolyglot = (req, res, next) => {
    // Get the locale from express-locale
    const locale = req.locale.language;

    // Start Polyglot and add it to the req
    req.polyglot = new Polyglot();

    // TODO esto debe depender del idioma del cliente, no se puede inicializar aquí
    // Decide which phrases for polyglot will be used
    // if (locale == 'es') {
        req.polyglot.extend(i18n.messages.es);
    // } else {
    //     req.polyglot.extend(i18n.messages.en);
    // }

    next();
}
