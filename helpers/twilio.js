const env = require('../config/env');

const accountSid = env.twilioAccountSid;
const authToken = env.twilioAuthToken;
const twilio = require('twilio')(accountSid, authToken);

module.exports = {
  sendWhatsappMessage: (to, message) => {
    return new Promise((resolve, reject) => {
      twilio.messages.create({
        from: `whatsapp:${env.twilioWhatsappNumber}`,
        body: message,
        to: `whatsapp:${to}`
      })
      .then(message => {
        resolve(message.status);
      })
      .catch(error => {
        reject(error);
      });
    });
  }
};