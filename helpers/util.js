const moment = require("moment-timezone");
const generateRandomCode = async (length = 8) => {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
  let code = '';
  for (let i = 0; i < length; i++) {
    const index = await generateRandomNumber(0, chars.length - 1);
    code += chars[index];
  }
  return code;
};

const generateRandomNumber = async (min = 0, max = 100) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const propertyObjectToString = (object) => {
  let obj = {};
  let keys = Object.keys(object);
  for (const key of keys) {
    if (['createdAt', 'updatedAt', 'deletedAt'].includes(key)) {
      obj[key] = moment(object[key]).format('YYYY-MM-DD HH:mm:ss');
    } else if (typeof object[key] === 'object') {
      obj[key] = propertyObjectToString(object[key]);
    } else {
      obj[key] = object[key].toString();
    }
  }
  return obj;
}

module.exports = {
  generateRandomCode,
  generateRandomNumber,
  propertyObjectToString
};
