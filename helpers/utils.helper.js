const moment = require("moment/moment");
const { GROUP_WHATSAPP_TELEGRAM } = require('./constants.helper');

const getInfoGroup = (place) => {
  if ([9].includes(Number(place?.stateId))) {
    return {data: true, info: GROUP_WHATSAPP_TELEGRAM.MEXICO_CITY};
  } else if([1817].includes(Number(place?.cityId))) {
    return {data: true, info: GROUP_WHATSAPP_TELEGRAM.TULUM};
  } else if([323, 322].includes(Number(place?.colonyId))) {
    if (Number(place?.colonyId) === 322) {
     return {data: true, info: GROUP_WHATSAPP_TELEGRAM.CANCUN};
    }
    if (Number(place?.colonyId) === 323) {
      return {data: true, info: GROUP_WHATSAPP_TELEGRAM.PCR};
    }
  }
  return {data: false, info: {}}
}

const generateRandomCode = (length = 8) => {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
  let code = '';
  for (let i = 0; i < length; i++) {
    const index = GenerateRandomNumber(0, chars.length - 1);
    code += chars[index];
  }
  return code;
};

const generateRandomNumber = (min = 0, max = 100) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const onlyMessage = (obj) => {
  const errors = {};
  for (const o in obj) {
    errors[o] = obj[o].message;
  }
  return errors;
};

const rangeByMonthsAndYear = (majorDate, minorDate) => {
  const major = validDateFormat(majorDate, 'YYYY-MM');
  const minor = validDateFormat(minorDate, 'YYYY-MM');
  const difference = major.diff(minor, 'months') + 1;
  const range = [];
  for (let i = 0; i < difference; i++) {
    range.push(`${minor.format('YYYY-MM')}`);
    minor.add('1', 'months');
  }
  return range;
};

const validDateFormat = (date, format = 'YYYY-MM-DD') => {
  const _date = moment(date, format, true);
  if (typeof date !== 'string' && !_date.isValid()) {
    throw new Error('Input format invalid. String with format YYYY-MM');
  }
  return _date;
}

module.exports = {
  generateRandomCode,
  generateRandomNumber,
  onlyMessage,
  validDateFormat,
  rangeByMonthsAndYear,
  getInfoGroup
};
