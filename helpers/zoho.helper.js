const axiosZoho = require('axios');
const axiosZohoAuth = require('axios');
const querystring = require('querystring');
const models = require('../models');
const { ZOHO_STATUS_RESPONSE } = require('./constants.helper')

const ModuleZoho = {};
const urlBaseAuth = 'https://accounts.zoho.com';
const urlBaseAPI = 'https://www.zohoapis.com/crm/v2';

zohoAuth = () => {
    const con = {
        baseURL: urlBaseAuth,
        timeout: 60000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    return axiosZohoAuth.create(con)
}

zohoCRM = (auth = {}) => {
    const con = {
        baseURL: urlBaseAPI,
        timeout: 50000,
        headers: {
            ...auth
        }
    };
    return axiosZoho.create(con)
}

requestPostZoho = (path, payload, auth = {}, raw) => {
    return new Promise((resolve, reject) => {
        if (raw) {
            zohoCRM(auth).post(path, payload)
                .then((response) => {
                    resolve(response['data']);
                })
                .catch((error) => {
                    resolve(undefined);
                });
        } else {
            zohoAuth().post(path, querystring.stringify(payload))
                .then((response) => {
                    resolve(response['data']);
                })
                .catch((error) => {
                    resolve(undefined);
                });
        }

    })
};

requestGetZoho = (path, auth = {}) => {
    return new Promise((resolve, reject) => {
        zohoCRM(auth).get(path)
            .then(function (response) {
                resolve(response['data']);
            })
            .catch(function (error) {
                console.log('Err');
                reject(error);
            })
    })
};

getDataZoho = async () => {
    return await models.AuthZoho.findByPk(1);
}

refreshToken = async () => {
    let credentials = await getDataZoho();

    const payload = {
        "refresh_token": credentials?.tokenRefresh,
        "client_id": credentials?.clientId,
        "client_secret": credentials?.clientSecret,
        "grant_type": "refresh_token"
    };

    const token = await requestPostZoho(`/oauth/v2/token`, payload, {}, false);

    if (!token?.error) {
        credentials['token'] = token?.access_token;
        await models.AuthZoho.update({token: token?.access_token}, {where: {id: 1}});
        return credentials;
    } else {
        console.error(token);
        return undefined;
    }

}

ModuleZoho.getLeads = async  () => {
    return await requestGetZoho('leads', {'Authorization': 'Bearer 1000.bfe06675e957a5ad3b5d11eae6242c5d.5f009aa624db90c5ef870ccd87d6c184'});
}

ModuleZoho.registerCustomerB2C = async (customer, place, origin) => {
    try {
        const credentials = await getDataZoho();
        let type, ticket;
        if (place?.priceMax <= 400) {
            type = `Bajo`;
            ticket = 400;
        } else if (place?.priceMax > 400 && place?.priceMax <= 800) {
            type = `Medio`;
            ticket = 800;
        } else if (place?.priceMax > 800 && place?.priceMax <= 1000) {
            type = `Alto`;
            ticket = 1000;
        } else {
            type = `Premium`;
            ticket = 1200;
        }
        let payload = {
            "data": [
                {
                    "Email": customer?.email,
                    "Tipo_de_ticket": type,
                    "Ticket_promedio_de_reserva": `${ticket}`,
                    "Name": customer?.fullname?.split(' ').slice(0, -1).join(' '),
                    "Apellido": customer?.fullname?.split(' ').slice(-1).join(' '),
                    "Estado": place?.states?.name ? place?.states?.name : '',
                    "Tel_fono": customer?.phone?.indexOf('+') != -1 ? customer?.phone : `${customer?.countryCode}${customer?.phone}`,
                    "Pa_s": "Mexico",
                    "Zona": place?.colony?.name ? place?.colony?.name : '',
                    "Ciudad": place?.cities?.name ? place?.cities?.name : '',
                    "Tipo_de_comida": place?.TypeFoods ? place?.TypeFoods?.name : 'Otro',
                    "Nombre_del_lugar": place?.name,
                    "Origen": origin
                }
            ]
        };

        let responseCustomer;
        try {
            responseCustomer = await requestPostZoho(`/Clientes_B2C/upsert`, payload, {'Authorization': `Bearer ${credentials?.token}`}, true);
        } catch (e) {
            console.log(e);
        }

        if (!responseCustomer || responseCustomer?.data?.code === ZOHO_STATUS_RESPONSE.INVALID_TOKEN) {
            const tokenRefresh = await refreshToken();
            return await requestPostZoho(`/Clientes_B2C/upsert`, payload, {'Authorization': `Bearer ${tokenRefresh?.token}`}, true);
        } else {
            return responseCustomer;
        }
    } catch (e) {
        console.error(e);
    }
}

module.exports = { ModuleZoho };
