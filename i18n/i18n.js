const availableLangs = ['es', 'en'];

const messages = {
    en: {
        'enterLocalWaitList': 'You just entered the waiting list. When a table is released we will send you a message.',
        'enterWaitList': 'You just entered the waiting list. If a table is released we will send you a message to confirm your attendance',
        'reservationCancelled': 'Your reservation has been canceled. Contact us to answer any question.',
        'reservationConfirmed': 'Your reservation has been confirmed: %{placeName} has taken your reservation for %{reservationDate} at %{hour}.',
        'reservationReConfirmed': 'Your reservation has been re-confirmed: %{placeName} is waiting for you on %{reservationDate} at %{hour}.',
        'enterOrder': 'Your order has been taken, we will inform you when it is ready. Thanks for choosing us',
        // card errors
        'emptyCard': 'The card info have some error.',
        CARD_TYPE: {
            DEBIT: 'Debit',
            CREDIT: 'Credit',
            PREPAID: 'Prepaid',
            UNKNOWN: 'Unknown',
        }
    },
    es: {
        // reservation status messages
        'enterLocalWaitList': 'Acabas de entrar en la lista de espera. En cuanto una mesa quede liberada te enviaremos otro mensaje para que te presentes con nosotros.',
        'enterWaitList': 'Acabas de entrar en la lista de espera. Si una mesa se libera te enviaremos un mensaje para que nos confirmes tu asistencia.',
        'reservationCancelled': 'Tu reserva ha sido cancelada. Ponte en contacto con nosotros para resolver cualquier duda.',
        'reservationConfirmed': 'Tu reserva está confirmada: %{placeName} ha tomado tu reserva para el %{reservationDate} a las %{hour}',
        'reservationReConfirmed': 'Tu reserva ha sido re-confirmada. ¡Te esperamos! %{placeName} para el %{reservationDate} a las %{hour}',
        'enterOrder': 'Se ha tomado su pedido, le informaremos cuando este listo. Gracias por preferirnos',
        // card errors
        'emptyCard': 'Hay problemas con los datos de la tarjeta. No se pudo hacer la reservación',
        CARD_TYPE: {
            DEBIT: 'Débito',
            CREDIT: 'Crédito',
            PREPAID: 'Prepago',
            UNKNOWN: 'Desconocida',
        }
    }
};

exports.availableLangs = availableLangs;
exports.messages = messages;
