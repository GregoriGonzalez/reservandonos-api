const jwt = require('jsonwebtoken');
const env = require('../config/env');

const auth = {
  decode: (req, res, next) => {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      jwt.verify(token, env.jwtSecret, (err, decoded) => {
        if (err) {
          const error = new Error();
          error.message = err.message;
          error.status = 450;
          return next(error);
        }
        req.user = decoded;
        const isPlatformUser = ['ADMIN', 'TELEMARKETING'].includes(req.user.role);
        if (isPlatformUser) {
          let placeId = null;
          switch (req.method) {
            case 'GET':
            case 'DELETE':
              placeId = req.query.placeId;
              break;
            case 'POST':
            case 'PUT':
              placeId = req.body.placeId;
              break;
          }
          if (placeId && !['null', 'undefined'].includes(placeId)) {
            req.user.placeId = placeId;
          }
        }
        next();
      });
    } else {
      let placeId = null;
      switch (req.method) {
        case 'GET':
        case 'DELETE':
          placeId = req.query.placeId;
          break;
        case 'POST':
        case 'PUT':
          placeId = req.body.placeId;
          break;
      }
      req.user = {placeId: placeId};
      next();
    }
  },
  isAuthenticated: (req, res, next) => {
    if (req.user) {
      next();
    } else {
      return res.status(401).json({message: 'Unauthorized'});
    }
  },
  isGuest: (req, res, next) => {
    if (!req.user) {
      next();
    } else {
      return res.status(403).json({message: 'Forbidden'});
    }
  },
  hasRole: (...allowed) => {
    const isAllowed = role => allowed.indexOf(role) > -1;
    return (req, res, next) => {
      if (req.user && isAllowed(req.user.role)) {
        next();
      } else {
        console.log(allowed);
        console.log(req.user);
        return res.status(403).json({message: 'Forbidden'});
      }
    }
  },
  isAuthenticatedGoogle: (req, res, next) => {
    if (req.headers.authorization) {
      const encoded = req.headers.authorization.split(' ')[1];
      const decoded = Buffer.from(encoded, 'base64').toString().split(':');
      const username = decoded[0];
      const password = decoded[1];
      if (env.google?.username === username && env.google?.password === password) {
        next();
      } else {
        return res.status(403).json({message: 'Forbidden'});
      }
    } else {
      return res.status(403).json({message: 'Forbidden'});
    }
  }
}

module.exports = auth;
