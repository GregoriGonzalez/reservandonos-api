const models  = require('../models');
const env = require('../config/env');

const paymentGateway = {
  privateKey: (req, res, next) => {
    if (req.user.placeId) {
      models.Place.findByPk(req.user.placeId, {
        attributes: ['paymentGatewayPrivateKey']
      }).then(place => {
        req.paymentGateway = {
          privateKey: place.paymentGatewayPrivateKey
        };
        next();
      });
    } else if(['ADMIN'].includes(req.user.role)) {
      req.paymentGateway = {
        privateKey: env.paymentGatewayPrivateKey
      };
      next();
    } else {
      next();
    }
  }
}

module.exports = paymentGateway;
