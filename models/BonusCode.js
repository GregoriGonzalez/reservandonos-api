'use strict';
module.exports = (sequelize, DataTypes) => {
    const BonusCode = sequelize.define('BonusCode', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT
        },
        registeredCustomerId: {
            type: DataTypes.BIGINT
        },
        customerPointId: {
            type: DataTypes.BIGINT
        },
        code: {
            type: DataTypes.STRING
        },
        isActive: {
            type: DataTypes.BOOLEAN
        }
    });
    BonusCode.associate = function (models) {
        BonusCode.belongsTo(models.Reservation, {
            as: 'reservation',
            foreignKey: 'reservationId'
        });
    };
    // RegisteredCustomer.associate = function (models) {
    //     RegisteredCustomer.hasMany(models.Favorite, {
    //         as: 'favorites',
    //         foreignKey: 'registeredCustomerId'
    //     });
    // };

    return BonusCode;
};