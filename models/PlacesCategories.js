'use strict';
module.exports = (sequelize, DataTypes) => {
  const PlacesCategories = sequelize.define(
    'PlacesCategories',
    {},
    {timestamps: false, paranoid: false }
  );

  PlacesCategories.associate = function(models) {};

  return PlacesCategories;
};
