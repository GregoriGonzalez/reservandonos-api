'use strict';
module.exports = (sequelize, DataTypes) => {
  const ApplicationSettings = sequelize.define('ApplicationSettings', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    enablePayCash: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    enablePayOnline: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    enablePayPal: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    enableStripe: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    enableOxxo: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    enableElectronicInvoicing: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {timestamps: false});

  ApplicationSettings.associate = function(models) {
    ApplicationSettings.belongsTo(models.Country, {
      as: 'country',
      foreignKey: {
        name: 'countryId',
        allowNull: false
      }
    });

    ApplicationSettings.addScope('defaultScope', {
      include: [{
        model: models.Country,
        as: 'country'
      }]
    }, {override: true})
  };

  return ApplicationSettings;
};
