'use strict';
module.exports = (sequelize, DataTypes) => {
    const AuthZoho = sequelize.define('AuthZoho', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT.UNSIGNED
        },
        scope: {
            allowNull: false,
            type: DataTypes.STRING
        },
        token: {
            allowNull: true,
            type: DataTypes.STRING
        },
        tokenRefresh: {
            allowNull: true,
            type: DataTypes.STRING
        },
        clientId: {
            allowNull: true,
            type: DataTypes.STRING
        },
        clientSecret: {
            allowNull: true,
            type: DataTypes.STRING
        }
    },{ paranoid: true });

    return AuthZoho;
};
