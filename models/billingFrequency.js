'use strict';
module.exports = (sequelize, DataTypes) => {
  const BillingFrequency = sequelize.define('BillingFrequency', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    description: {
      allowNull: false,
      type: DataTypes.STRING
    },
    frequency: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER
    }
  },{ paranoid: true });

  BillingFrequency.associate = function(models) {
    BillingFrequency.hasMany(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'billingFrequencyId',
        allowNull: true
      }
    })
  };

  return BillingFrequency;
};
