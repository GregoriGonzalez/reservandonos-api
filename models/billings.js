'use strict';
const {BILLINGS_LABELS} = require("../helpers/constants.helper");
module.exports = (sequelize, DataTypes) => {
  const Billings = sequelize.define('Billings', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    dateStart: {
      allowNull: false,
      type: DataTypes.DATEONLY
    },
    dateEnd: {
      allowNull: false,
      type: DataTypes.DATEONLY
    },
    status: {
      allowNull: false,
      type: DataTypes.ENUM('LATE', 'PENDING', 'CONFIRMED', 'VERIFIED')
    },
    paymentUrl: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING
    },
    appliedTax: {
      allowNull: false,
      defaultValue: 0.16,
      type: DataTypes.FLOAT
    },
    fixedChargeApplied: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.FLOAT
    },
    label: {
      type: DataTypes.ENUM(BILLINGS_LABELS.AWAITING_INVOICE, BILLINGS_LABELS.NEED_TO_UPDATE, BILLINGS_LABELS.PAY_PER_DAY, BILLINGS_LABELS.PENDING_PAYMENT),
      allowNull: true,
      defaultValue: null
    }
  }, {paranoid: true});

  Billings.associate = function (models) {
    Billings.hasMany(models.Reservation, {
      as: 'reservation',
      foreignKey: 'billingId'
    });
    Billings.belongsTo(models.Place, {
      as: 'place',
      foreignKey: 'placeId'
    });
    Billings.hasMany(models.Payments, {
      as: 'payments',
      foreignKey: 'billingId'
    });
  };

  return Billings;
};


