'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bitacora = sequelize.define('Bitacora', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    operation: {
      type: DataTypes.ENUM('CREATE', 'UPDATE', 'DELETE'),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    },
    tableName: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    },
    tableId: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    },
    userId: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    },
    placeId: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    }
  },{ paranoid: true });

  Bitacora.associate = function(models) {
    Bitacora.belongsTo(models.User, {
      as: 'users',
      foreignKey: {
        name: 'userId',
        allowNull: true
      }
    });
  };

  return Bitacora;
};
