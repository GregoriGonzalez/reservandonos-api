'use strict';
module.exports = (sequelize, DataTypes) => {
  const Categories = sequelize.define('Categories', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {timestamps: true});

  Categories.associate = function(models) {
    Categories.belongsToMany(models.Place, {
      as: 'categories',
      through: 'PlacesCategories',
      foreignKey: 'categoryId'
    });
  };

  return Categories;
};
