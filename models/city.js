'use strict';
const models = require("../models");
module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('City', {
    id: {
      primaryKey: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {paranoid: true});

  City.associate = function (models) {
    City.belongsTo(models.State, {
      as: 'state',
      foreignKey: 'stateId'
    });
    City.hasMany(models.Colony, {
      as: 'colonies',
      foreignKey: 'cityId'
    })
    City.hasMany(models.Place, {
      as: 'place',
      foreignKey: 'cityId'
    });
  }

  return City;
};
