'use strict';
const models = require("../models");
module.exports = (sequelize, DataTypes) => {
  const Colony = sequelize.define('Colony', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  },{ paranoid: true });

  Colony.associate = function(models) {
    Colony.belongsTo(models.City, {
        as: 'city',
        foreignKey: 'cityId'
    });
    Colony.hasMany(models.Place, {
      as: 'places',
      foreignKey: 'colonyId'
    })
  }

  return Colony;
};
