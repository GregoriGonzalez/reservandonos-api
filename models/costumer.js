'use strict';
module.exports = (sequelize, DataTypes) => {
  const Costumer = sequelize.define('Costumer', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    fullname: {
      allowNull: false,
      type: DataTypes.STRING
    },
    countryCode: {
      allowNull: true,
      type: DataTypes.STRING
    },
    phone: {
      allowNull: true,
      type: DataTypes.STRING
    },
    email: {
      allowNull: true,
      // unique: 'email',
      type: DataTypes.STRING
    },
    validEmail: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    emailComplaints: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED,
      defaultValue: 0
    },
    notes: {
      allowNull: true,
      type: DataTypes.STRING
    },
    userGoogleReserve: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: ''
    },
    language: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: ''
    },
    tags: DataTypes.STRING,
    paymentCustomerId: DataTypes.STRING,
    subscribedEmails: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }
  }, {paranoid: true});

  Costumer.associate = function (models) {
    Costumer.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
    Costumer.hasMany(models.Reservation, {
      as: 'reservations',
      foreignKey: {
        name: 'costumerId',
        allowNull: false
      }
    });
    Costumer.hasMany(models.Coupons, {
      as: 'coupons',
      foreignKey: {
        name: 'costumerId',
        allowNull: true,
        defaultValue: null
      }
    });
  };

  return Costumer;
};
