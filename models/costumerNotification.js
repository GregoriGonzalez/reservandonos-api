'use strict';
module.exports = (sequelize, DataTypes) => {
  const CostumerNotification = sequelize.define('CostumerNotification', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    title: {
      type: DataTypes.STRING,
      defaultValue: "",
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false
    },
    link: {
      type: DataTypes.STRING,
    },
    viewed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  }, { paranoid: true });

  CostumerNotification.associate = function (models) {
    CostumerNotification.belongsTo(models.RegisteredCustomer, {
      as: 'registeredCustomer',
      foreignKey: 'registeredCustomerId'
    });
  };

  return CostumerNotification;

};
