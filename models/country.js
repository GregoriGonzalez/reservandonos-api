'use strict';
module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    id: {
      primaryKey: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    code3: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    currency: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    }
  },{ paranoid: true });

  Country.associate = function(models) {
    Country.hasOne(models.ApplicationSettings, {
      as: 'applicationSetting',
      foreignKey: 'countryId'
    });
  };
  return Country;
};
