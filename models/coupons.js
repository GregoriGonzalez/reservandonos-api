'use strict';
const {Capitalize} = require("../helpers/string.helper");
module.exports = (sequelize, DataTypes) => {
  const Coupons = sequelize.define('Coupons', {
    id: {
      primaryKey: true, autoIncrement: true, type: DataTypes.BIGINT.UNSIGNED
    }, code: {
      type: DataTypes.STRING, allowNull: false
    }, redeemed: {
      type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false
    }, dateRedeemed: {
      type: DataTypes.DATE, allowNull: true, defaultValue: null
    },
  }, {timestamps: true, paranoid: true});

  Coupons.associate = function (models) {
    Coupons.belongsTo(models.Promotions, {
      as: 'promotion', foreignKey: {
        name: 'promotionId', allowNull: false
      }
    });
    Coupons.belongsTo(models.Costumer, {
      as: 'costumer', foreignKey: {
        name: 'costumerId', allowNull: true, defaultValue: null
      }
    })
    Coupons.belongsTo(models.RegisteredCustomer, {
      as: 'registeredCustomer', foreignKey: {
        name: 'registeredCustomerId', allowNull: true, defaultValue: null
      }
    })
  };

  /** Class Method **/

  /**
   * Get coupon by Primary Key with Promotions and place associated more model
   * customer or registeredCustomer models
   * @param couponId {number}
   * @returns {Promise<Model>}
   */
  Coupons.getDetails = async function (couponId) {
    return await Coupons.findByPk(couponId, {
      include: [{
        model: sequelize.models.Promotions,
        as: 'promotion',
        attributes: ['id', 'title', 'description', 'imageUrl'],
        include: [{
          model: sequelize.models.Place, as: 'place', attributes: ['id', 'name', 'address', 'logoImage'], required: true
        }],
        required: true,
      }, 'costumer', 'registeredCustomer']
    })
  }


  /** Instance Methods **/

  /**
   *  @return client {id: number, model: string, fullName: string, phone: string, email: string}
   */
  Coupons.prototype.getClient = function () {
    let client;
    if (this.costumer) {
      const {id, fullname, phone, email} = this.costumer.dataValues;
      client = {
        id,
        fullName: Capitalize(fullname),
        phone: `${phone}`,
        email: email?.toLowerCase(),
        model: 'Costumer'
      }
    } else if (this.registeredCustomer) {
      const {id, name, lastName, email, phone, lada} = this.registeredCustomer.dataValues;
      client = {
        id,
        fullName: Capitalize(`${name} ${lastName}`),
        phone: `${lada}${phone}`,
        email: email?.toLowerCase(),
        model: 'RegisteredCustomer'
      }
    }
    return client;
  }

  return Coupons;
};
