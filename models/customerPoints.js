'use strict';
module.exports = (sequelize, DataTypes) => {
    const CustomerPoint = sequelize.define('CustomerPoint', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT
        },
        registeredCustomerId: {
            type: DataTypes.BIGINT
        },
        beforePoints: {
            type: DataTypes.INTEGER
        },
        points: {
            type: DataTypes.INTEGER
        },
        afterPoints: {
            type: DataTypes.INTEGER
        }
    });

    // RegisteredCustomer.associate = function (models) {
    //     RegisteredCustomer.hasMany(models.Favorite, {
    //         as: 'favorites',
    //         foreignKey: 'registeredCustomerId'
    //     });
    // };

    return CustomerPoint;
};