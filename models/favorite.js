'use strict';
module.exports = (sequelize, DataTypes) => {
    const Favorite = sequelize.define('Favorite', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT
        },
    });
    Favorite.associate = function(models) {
        Favorite.belongsTo(models.RegisteredCustomer, {
            as: 'registeredCustomer',
            foreignKey: 'registeredCustomerId'
        });
        Favorite.belongsTo(models.Place, {
            as: 'place',
            foreignKey: 'placeId'
        });
    }


    return Favorite;
}
