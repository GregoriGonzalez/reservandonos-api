'use strict';
module.exports = (sequelize, DataTypes) => {
  const GiftCard = sequelize.define('GiftCard', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    beneficiaryName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    countryCode: {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    phone: {
      allowNull: false,
      type: DataTypes.STRING
    },
    amount: {
      allowNull: false,
      type: DataTypes.STRING
    },
    token: {
      allowNull: false,
      type: DataTypes.STRING
    },
    redeemed: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    paymentOrderId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    redeemedAt: DataTypes.DATE
  },{ paranoid: true });


  GiftCard.associate = function(models) {
    GiftCard.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
  };

  return GiftCard;
};
