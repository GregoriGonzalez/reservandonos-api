'use strict';
module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define('Group', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    numbers: {
      allowNull: false,
      type: DataTypes.STRING
    },
    minCapacity: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    maxCapacity: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    onlineSales: {
      allowNull: false,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    }
  },{ paranoid: true });

  Group.associate = function(models) {
    Group.belongsTo(models.Layout, {
      as: 'layout',
      foreignKey: {
        name: 'layoutId',
        allowNull: false
      }
    });

    Group.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Group.hasMany(models.Reservation, {
      as: 'reservations',
      foreignKey: 'groupId'
    })

    Group.belongsToMany(models.Table, {
      as: 'tables',
      through: 'GroupTables',
      foreignKey: 'groupId',
      otherKey: 'tableId'
    });
  };

  return Group;
};