'use strict';
module.exports = (sequelize, DataTypes) => {
  const GroupsBusiness = sequelize.define('GroupsBusiness', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
  },{ paranoid: true });

  GroupsBusiness.associate = function(models) {
    GroupsBusiness.hasMany(models.GroupsBusinessPlaces, {
      as: 'groupsBusinessPlaces',
      foreignKey: 'groupsBusinessId'
    });
  }


  return GroupsBusiness;
};
