'use strict';
module.exports = (sequelize, DataTypes) => {
  const GroupsBusinessPlaces = sequelize.define('GroupsBusinessPlaces', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    isActive: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {paranoid: true});

  GroupsBusinessPlaces.associate = function (models) {
    GroupsBusinessPlaces.belongsTo(models.Place, {
      as: 'place',
      foreignKey: 'placeId'
    });

    GroupsBusinessPlaces.belongsTo(models.GroupsBusiness, {
      as: 'groupsBusiness',
      foreignKey: 'groupsBusinessId'
    });
  }

  return GroupsBusinessPlaces;
};
