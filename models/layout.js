'use strict';
module.exports = (sequelize, DataTypes) => {
  const Layout = sequelize.define('Layout', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    tablesCount: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    maxCapacity: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    }
  },{ paranoid: true });

  Layout.associate = function(models) {
    Layout.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Layout.belongsTo(models.Zone, {
      as: 'zone',
      foreignKey: {
        name: 'zoneId',
        allowNull: false
      }
    });

    Layout.hasMany(models.Table, {
      as: 'tables',
      foreignKey: {
        name: 'layoutId',
        allowNull: false
      }
    });

    Layout.hasMany(models.Group, {
      as: 'groups',
      foreignKey: {
        name: 'layoutId',
        allowNull: false
      }
    });
  };

  return Layout;
};