'use strict';
module.exports = (sequelize, DataTypes) => {
  const Notification = sequelize.define('Notification', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false
    },
    notiTypeId: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    reservationId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    giftId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    billingId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    couponId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    }
  });

  Notification.associate = function (models) {
    Notification.belongsTo(models.Reservation, {
      as: 'reservation',
      foreignKey: 'reservationId'
    });
    Notification.belongsTo(models.GiftCard, {
      as: 'giftCard',
      foreignKey: 'giftId'
    });
    Notification.belongsTo(models.NotificationType, {
      as: 'notificationType',
      foreignKey: 'notiTypeId'
    });
    Notification.belongsTo(models.Billings, {
      as: 'billings',
      foreignKey: 'billingId'
    });
    Notification.belongsTo(models.Coupons, {
      as: 'coupon',
      foreignKey: 'couponId',
    });
  }

  return Notification;
}
