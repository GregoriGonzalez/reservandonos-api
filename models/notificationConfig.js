'use strict';
module.exports = (sequelize, DataTypes) => {
    const NotiConfig = sequelize.define('NotiConfig', {
        id: {
          primaryKey: true,
          autoIncrement: true,
          type: DataTypes.BIGINT.UNSIGNED
        },
        userId: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false
        },
        notiType: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        checked: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    });

    NotiConfig.associate = function(models) {
        NotiConfig.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        NotiConfig.belongsTo(models.NotificationType, {
            as: 'notificationType',
            foreignKey: 'notiType'
        });
    };

    return NotiConfig;
}
