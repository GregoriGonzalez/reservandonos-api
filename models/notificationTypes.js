'use strict';
module.exports = (sequelize, DataTypes) => {
    const NotificationType = sequelize.define('NotificationType', {
        id: {
            primaryKey: true,
            type: DataTypes.STRING(20)
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });
    return NotificationType;
}