'use strict';
const models = require("./index");
module.exports = (sequelize, DataTypes) => {
  const NotiUser = sequelize.define('NotiUser', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    seen: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    }
  });

  NotiUser.associate = function (models) {
    NotiUser.belongsTo(models.Notification, {
      as: 'notification',
      foreignKey: {
        name: 'notificationId'
      }
    });

    NotiUser.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'userId'
    });

    NotiUser.belongsTo(models.Place, {
      as: 'place',
      foreignKey: 'placeId'
    });
  };

  /**
   * Return model NotiUser with the  User and Notification models populate
   * @param {number} id - primaryKey
   * @returns {Promise<Model>}
   */
  NotiUser.getDetails = async function (id) {
    return await NotiUser.findByPk(id, {
      include: [
        {
          model: sequelize.models.User,
          as: 'user',
          attributes: ['tokenFCM'],
          required: true
        }, {
          model: sequelize.models.Notification,
          as: 'notification',
          attributes: ['id', 'notiTypeId', 'message', 'createdAt'],
          required: true,
          include: [
            {
              model: sequelize.models.NotificationType,
              as: 'notificationType',
              required: true,
              attributes: ['type']
            },
            {
              model: sequelize.models.Reservation,
              as: 'reservation',
              required: false,
              attributes: ['type', 'createdAt', 'peopleCount', 'date', 'id', 'status', 'advancePaymentId', 'placeId'],
              include: [
                {
                  model: sequelize.models.Place,
                  as: 'place',
                  attributes: ['id', 'name']
                }
              ]
            },
            {
              model: sequelize.models.GiftCard,
              as: 'giftCard',
              required: false,
              attributes: ['beneficiaryName', 'amount', 'createdAt', 'id', 'placeId']
            },
            {
              model: sequelize.models.Billings,
              as: 'billings',
              required: false,
            },
            {
              model: sequelize.models.Coupons,
              as: 'coupon',
              required: false,
              include: ['promotion']
            },
          ]
        }]
    });
  }

  return NotiUser;
}
