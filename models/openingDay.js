'use strict';
const moment = require("moment");

module.exports = (sequelize, DataTypes) => {

  const getNumbersOfTablesOccupied = async ({placeId, date, minutes, minutes_end}) => {
    const query =
      "SELECT T.number AS numbers\n" +
      "FROM Reservations r\n" +
      "JOIN Tables T on r.tableId = T.id\n" +
      "WHERE\n" +
      "        r.date = :date\n" +
      "  and ((:minutes between r.start and r.end) OR (:minutes_end between r.start and r.end))\n" +
      "  AND r.placeId = :placeId\n" +
      "  and r.tableId is not null\n" +
      "  and r.status NOT IN ('GONE', 'CANCELLED', 'WAIT_LIST', 'WAIT_LIST_LOCAL')\n" +
      "\n" +
      "UNION\n" +
      "\n" +
      "SELECT G.numbers as numbers\n" +
      "FROM Reservations r\n" +
      "JOIN `Groups` G on r.groupId = G.id\n" +
      "WHERE\n" +
      "        r.date = :date\n" +
      "  and ((:minutes between r.start and r.end) OR (:minutes_end between r.start and r.end))\n" +
      "  AND r.placeId = :placeId\n" +
      "  and r.groupId is not null\n" +
      "  and r.status NOT IN ('GONE', 'CANCELLED', 'WAIT_LIST', 'WAIT_LIST_LOCAL');\n"

    const rows = await sequelize.query(query, {
      replacements: {date, minutes, placeId, minutes_end},
      type: sequelize.QueryTypes.SELECT
    });

    const numbersOfTablesOccupied = [];
    for (const row of rows) {
      const numbers = row.numbers.split(',');
      for (const number of numbers) {
        if (!numbersOfTablesOccupied.includes(number)) {
          numbersOfTablesOccupied.push(number);
        }
      }
    }
    return numbersOfTablesOccupied;
  }

  const OpeningDay = sequelize.define('OpeningDay', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    weekday: {
      allowNull: false,
      type: DataTypes.STRING
    },
    start: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    end: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    start2: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    end2: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    smokeArea: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    }
  });

  OpeningDay.associate = function(models) {
    OpeningDay.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
  };

  OpeningDay.sqlBatchAvailabilityLookup = function(slot_time, placeId, hours, googleReserves = true, numberDay) {
    let sql, slot, date, day, weekday = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    if (googleReserves) {
      date = new Date(slot_time.start_sec * 1000);
      day = `${date.getFullYear()}-${date.getMonth() + 1 < 10 ? '0' : ''}${date.getMonth() + 1}-${date.getDate()}`;
      slot = slot_time.start_sec;
      // numberDay = date.getDay();
    } else {
      day = slot_time.date;
      slot = slot_time.start;
      numberDay = slot_time.dayNumber;
    }

    const sqlQuery =
      ' SELECT ( ' +
      ` SELECT IFNULL(SUM(IF(res.tableId IS NOT NULL, 1, ROUND((LENGTH(gr.numbers) - LENGTH(REPLACE(gr.numbers, ',', '')))) +1 )), 0) `+
      ` FROM Reservations res ` +
      ` LEFT JOIN Tables ta ON res.tableId = ta.id ` +
      ' LEFT JOIN `Groups` gr ON gr.id = res.groupId ' +
      ` WHERE res.placeId = ${placeId.replace('merch','')} AND res.DATE = '${day}' AND (ta.onlineSales = 1 OR gr.onlineSales) AND (res.start <= ${hours} AND res.end > ${hours}) ` +
      ` AND res.status NOT IN('GONE', 'CANCELLED', 'WAIT_LIST', 'WAIT_LIST_LOCAL') AND (res.tableId IS NOT NULL OR res.groupId IS NOT NULL)` +
      ` ) AS ocupados, ` +
      ` ( SELECT COUNT(*) FROM Layouts la ` +
      ` INNER JOIN ServiceZones AS serZon ON serZon.layoutId = la.id ` +
      ` INNER JOIN Services AS ser ON ser.id = serZon.serviceId ` +
      ` INNER JOIN Tables AS ta ON la.id = ta.layoutId ` +
      ` WHERE la.placeId = ${placeId.replace('merch','')} AND ser.deletedAt IS NULL ` +
      ` AND ta.deletedAt IS NULL AND ta.onlineSales = 1 ` +
      ` AND ser.weekdays LIKE '%${weekday[numberDay]}%' ) AS maxCapacity, '${slot}' as start_sec, ` + // maxCapacity disponible
      ` '${day}' as day` +
      ` FROM OpeningDays op ` +
      `  WHERE ` +
      `  op.placeId = ${placeId.replace('merch','')} AND op.weekday = '${weekday[numberDay]}' ` +
      ` AND ((op.start <= ${hours} AND op.end >= ${hours} ) OR (op.start2 <= ${hours} AND op.end2 >= ${hours}))`;

    return new Promise((resolve, reject) => sequelize.query(
        sqlQuery,
        { type: sequelize.QueryTypes.SELECT }
      ).then( data => {
        resolve(data);
      }, err => {
        reject(err);
      })
    );
  }

    OpeningDay.getTablesAndGroupsAvailable = async (place, timestamp, people, duration_sec, widget = false, date = '', minutes = 0) => {
    try {
      const duration_min = duration_sec / 60;
      const _datetimeOfDay = widget ? moment(date) : moment.unix(timestamp).tz(place.timezone);
      const _hoursAsMinutes = widget ? minutes : moment.duration(_datetimeOfDay.format('H:mm')).asMinutes();
      const weekdays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

      const numbersOfTablesOccupied = await getNumbersOfTablesOccupied({
        placeId: place.id,
        date: _datetimeOfDay.format('YYYY-MM-DD'),
        minutes: _hoursAsMinutes,
        minutes_end: _hoursAsMinutes + duration_min
      });

      const sqlQuery =
        "SELECT tg.*\n" +
        "FROM\n" +
        "    (SELECT " +
        "     t.placeId as placeId,  " +
        "     t.number as numbers,  " +
        "     SZ.serviceId as serviceId," +
        "     SZ.zoneId as zoneId, " +
        "     t.id as id , " +
        "     t.minCapacity as minCapacity, " +
        "     t.maxCapacity as maxCapacity," +
        "     (t.maxCapacity - :people) as freeChairs," +
        "     'table' as type\n" +
        "     FROM `Tables` t\n" +
        "              JOIN Layouts L ON L.id = t.layoutId\n" +
        "              JOIN ServiceZones SZ on L.id = SZ.layoutId\n" +
        "              JOIN Services S on SZ.serviceId = S.id\n" +
        "     WHERE\n" +
        "             t.placeId = :placeId\n" +
        "       AND onlineSales = 1\n" +
        "       AND S.weekdays LIKE :like\n" +
        "       AND :people between t.minCapacity and t.maxCapacity\n" +
        "       AND L.deletedAt is null\n" +
        "       AND S.deletedAt is null\n" +
        "       AND t.deletedAt is null\n" +
        "     UNION\n" +
        "     SELECT " +
        "     g.placeId as placeId,  " +
        "     g.numbers as numbers,  " +
        "     SZ.serviceId as serviceId," +
        "     SZ.zoneId as zoneId, " +
        "     g.id as id , " +
        "     g.minCapacity as minCapacity, " +
        "     g.maxCapacity as maxCapacity," +
        "     (g.maxCapacity - :people) as freeChairs," +
        "     'group' as type\n" +
        "     FROM `Groups` g\n" +
        "              JOIN Layouts L ON L.id = g.layoutId\n" +
        "              JOIN ServiceZones SZ on L.id = SZ.layoutId\n" +
        "              JOIN Services S on SZ.serviceId = S.id\n" +
        "     WHERE\n" +
        "             g.placeId = :placeId\n" +
        "       AND onlineSales = 1\n" +
        "       AND S.weekdays LIKE :like\n" +
        "       AND :people between g.minCapacity and g.maxCapacity\n" +
        "       AND L.deletedAt is null\n" +
        "       AND S.deletedAt is null\n" +
        "       AND g.deletedAt is null\n" +
        "\n" +
        ") as tg JOIN OpeningDays op ON op.placeId = tg.placeId\n" +
        "where op.weekday = :weekday\n" +
        "AND ((op.start <= :minutes AND op.end >= :minutes) OR (op.start2 <= :minutes AND op.end2 >= :minutes))\n" +
        "ORDER BY freeChairs";

      const dayName = weekdays[_datetimeOfDay.day()];
      const dayLike = `%${dayName}%`
      const tablesAndGroupsOnlineRows = await sequelize.query(sqlQuery, {
        replacements: {
          people: people,
          date: _datetimeOfDay.format('YYYY-MM-DD'),
          minutes: _hoursAsMinutes,
          weekday: dayName,
          placeId: place.id,
          like: dayLike
        },
        type: sequelize.QueryTypes.SELECT
      });
      const tablesAndGroupsOnlineAvailable = [];
      for (const tablesAndGroupsOnlineRow of tablesAndGroupsOnlineRows) {
        let allTablesFree = true;
        const numbers = tablesAndGroupsOnlineRow.numbers.split(',');
        for (const number of numbers) {
          if (numbersOfTablesOccupied.includes(number)) {
            allTablesFree = false;
            break;
          }
        }
        if (allTablesFree) {
          tablesAndGroupsOnlineAvailable.push(tablesAndGroupsOnlineRow);
        }
      }
      return tablesAndGroupsOnlineAvailable;
    } catch (e) {
      return [];
    }
  };

  return OpeningDay;
};
