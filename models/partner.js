'use strict';
module.exports = (sequelize, DataTypes) => {
  const Partner = sequelize.define('Partner', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    contact: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    logoImage: {
      defaultValue: '/assets/images/default-logo.png',
      type: DataTypes.STRING
    },
    notes: DataTypes.STRING,
    clabe: DataTypes.STRING,
    beneficiary: DataTypes.STRING,
    sentPersonFee: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.DECIMAL(19, 2)
    }
  },{ paranoid: true });


  Partner.associate = function(models) {
    Partner.hasMany(models.Reservation, {
      as: 'reservations',
      foreignKey: 'partnerId'
    });

    Partner.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: true
      }
    });

    Partner.hasMany(models.User, {
      as: 'users',
      foreignKey: 'partnerId'
    });
  };

  return Partner;
};
