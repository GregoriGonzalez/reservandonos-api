'use strict';
const {
  PAYPAL_STATUS_ORDER,
  PAYMENTS_STATUS,
  PAYMENTS_GATEWAY,
  STRIPE_STATUS_ORDER,
  CARD_TYPE, PAYMENTS_TYPE
} = require("../helpers/constants.helper");

module.exports = (sequelize, DataTypes) => {
  const Payments = sequelize.define('Payments', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    transactionId: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    electronicInvoiceId: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    paymentGateway: {
      allowNull: false,
      type: DataTypes.ENUM(
        PAYMENTS_GATEWAY.PAYPAL,
        PAYMENTS_GATEWAY.STRIPE,
        PAYMENTS_GATEWAY.EXTERNAL,
        PAYMENTS_GATEWAY.CASH,
        PAYMENTS_GATEWAY.OXXO,
      )
    },
    statusPaymentGateway: {
      allowNull: true,
      type: DataTypes.ENUM(
        PAYPAL_STATUS_ORDER.APPROVED,
        PAYPAL_STATUS_ORDER.COMPLETED,
        PAYPAL_STATUS_ORDER.CREATED,
        PAYPAL_STATUS_ORDER.VOIDED,
        PAYPAL_STATUS_ORDER.PAYER_ACTION_REQUIRED,
        STRIPE_STATUS_ORDER.SUCCEEDED,
        STRIPE_STATUS_ORDER.PROCESSING,
        STRIPE_STATUS_ORDER.CANCELED,
        STRIPE_STATUS_ORDER.REQUIRES_PAYMENT_METHOD,
        STRIPE_STATUS_ORDER.REQUIRES_CONFIRMATION,
        STRIPE_STATUS_ORDER.REQUIRES_ACTION,
        STRIPE_STATUS_ORDER.REQUIRES_SOURCE
      )
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    fee: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
    net_amount: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    paymentDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    statusPayment: {
      allowNull: false,
      type: DataTypes.ENUM(
        PAYMENTS_STATUS.APPROVED,
        PAYMENTS_STATUS.PROCESS,
        PAYMENTS_STATUS.DECLINED,
      ),
      defaultValue: PAYMENTS_STATUS.PROCESS
    },
    comment: {
      type: DataTypes.STRING,
      allowNull: true
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: true
    },
    backupFile: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null,
    },
    cardType: {
      allowNull: true,
      type: DataTypes.ENUM(
        ...Object.values(CARD_TYPE)
      ),
      defaultValue: null
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM(...Object.values(PAYMENTS_TYPE)),
      defaultValue: PAYMENTS_TYPE.BILLING
    },
    pendingToInvoice: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    linkOxxo: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    }
  });

  Payments.associate = function (models) {
    Payments.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
    Payments.belongsTo(models.Billings, {
      as: 'billing',
      foreignKey: {
        name: 'billingId',
        allowNull: true
      }
    });
  };

  return Payments;
}
