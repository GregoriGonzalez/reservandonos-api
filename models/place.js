'use strict';
const {InvoiceUse} = require("facturapi");
module.exports = (sequelize, DataTypes) => {
  const Place = sequelize.define('Place', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    address: {
      allowNull: true,
      type: DataTypes.STRING
    },
    phone: {
      allowNull: true,
      type: DataTypes.STRING
    },
    email: {
      allowNull: true,
      type: DataTypes.STRING
    },
    logoImage: {
      defaultValue: '/assets/images/default-logo.png',
      type: DataTypes.STRING
    },
    dueDate: {
      allowNull: false,
      type: DataTypes.DATE
    },
    payDay: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    extraTimeDate: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    monthlyPlanPrice: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    sentPersonPrice: {
      allowNull: false,
      type: DataTypes.DECIMAL(19, 2)
    },
    timeBeforeClose: {
      allowNull: true,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    cancellationFeePerPerson: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    numberPersonPolicyCancellation: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    advancePaymentPolicyEnable: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    advancePaymentPolicyFeePerPerson: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    advancePaymentPolicyNumberPeople: {
      allowNull: false,
      defaultValue: 20,
      type: DataTypes.INTEGER.UNSIGNED
    },
    numberMaxMsm: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    numberMaxWhatsapp: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    typeWidget: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    isReservationsBlocked: {
      allowNull: true,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    onlineSales: {
      allowNull: true,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    },
    hasBooking: {
      allowNull: true,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    blockedDateStart: {
      allowNull: true,
      type: DataTypes.DATEONLY
    },
    blockedHourStart: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    events: {
      allowNull: true,
      type: DataTypes.STRING
    },
    blockedDateEnd: {
      allowNull: true,
      type: DataTypes.DATEONLY
    },
    blockedHourEnd: {
      allowNull: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    timezone: {
      allowNull: true,
      defaultValue: 'America/Mexico_City',
      type: DataTypes.STRING
    },
    tablesCount: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.INTEGER.UNSIGNED
    },
    maxPeopleReservations: {
      allowNull: true,
      defaultValue: 20,
      type: DataTypes.INTEGER.UNSIGNED
    },
    cancellationPolicyEnable: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    priorityPaymentWidget: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: ''
    },
    paymentGatewayPublicKey: DataTypes.STRING,
    paymentGatewayPrivateKey: DataTypes.STRING,
    paymentCustomerId: DataTypes.STRING,
    paymentCardId: DataTypes.STRING,
    paymentSubscriptionId: DataTypes.STRING,
    paymentEmail: {
      allowNull: true,
      type: DataTypes.STRING
    },
    isActive: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    website: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING
    },
    facebook: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING
    },
    instagram: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING
    },
    twitter: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING
    },
    enableGoogleReserve: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    methodReserve: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: ''
    },
    zoom: {
      allowNull: false,
      type: DataTypes.FLOAT,
      defaultValue: 1
    },
    theme: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    primaryColor: {
      allowNull: true,
      type: DataTypes.STRING
    },
    secondaryColor: {
      allowNull: true,
      type: DataTypes.STRING
    },
    intervalWidget: {
      allowNull: false,
      defaultValue: 1,
      comment: "El valor de esta columna se multiplica por 15 que es el intervalo menor para hacer las reservas.",
      type: DataTypes.INTEGER.UNSIGNED
    },
    typeAccount: {
      allowNull: false,
      defaultValue: 'rsv',
      type: DataTypes.ENUM('rsv', 'booker')
    },
    fixedCharge: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.FLOAT
    },
    priceMin: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.DECIMAL(19, 2)
    },
    priceMax: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.DECIMAL(19, 2)
    },
    langEmail: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'es'
    },
    isActivePartner: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isSpecial: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    stripeCustomerId: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    stripeCollectionAuto: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    stripeHasDefaultCard: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    businessName: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    taxToApply: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0.16,
      comment: '16% por defecto de mexico'
    },
    taxRecord: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      comment: 'Numero de registro fiscal en el pais Mexico = RFC, Venezuela = RIF'
    },
    facturapiCustomerId: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      comment: 'Proveedor para facturacion fiscal de mexico https://www.facturapi.io/'
    },
    facturapiCfdi: {
      type: DataTypes.ENUM(...Object.values(InvoiceUse)),
      allowNull: true,
      defaultValue: null
    },
    facturapiConcept: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    hasElectronicBillingEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    personInCharge: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      comment: 'Nombre de la persona responsable a la que van dirigido los emails'
    },
    emailPersonInCharge: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      comment: 'Email de la persona encargada del lugar'
    },
    collectionNotes: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
      comment: 'Utilizado para agregar informacion de un lugar con respecto a sus cortes'
    },
    slackDays: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 15,
      comment: 'Dias de holgura utilizados para generar el cobro automatico de cortes'
    },
  }, {
    paranoid: true,
  });

  Place.associate = function (models) {

    Place.hasMany(models.SponsorPlaces, {
      as: 'sponsorsPlaces',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsTo(models.User, {
      as: 'owner',
      constraints: false,
      foreignKey: {
        name: 'ownerId',
        allowNull: false
      }
    });

    Place.hasMany(models.GroupsBusinessPlaces, {
      as: 'groupsBusinessPlaces',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.hasMany(models.PlacePayment, {
      as: 'smsPlaces',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsTo(models.Plan, {
      as: 'plan',
      foreignKey: {
        name: 'planId',
        allowNull: false
      }
    });

    Place.hasMany(models.OpeningDay, {
      as: 'openingDays',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.hasMany(models.WorkSchedule, {
      as: 'workSchedule',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.hasMany(models.StayTime, {
      as: 'stayTimes',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.hasMany(models.User, {
      as: 'users',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.Partner, {as: 'partners', foreignKey: 'placeId'});

    Place.hasMany(models.Favorite, {as: 'favorites', foreignKey: 'placeId'});

    Place.belongsTo(models.City, {
      as: 'cities',
      foreignKey: 'cityId'
    });

    Place.belongsTo(models.State, {
      as: 'states',
      foreignKey: 'stateId'
    });

    Place.belongsTo(models.Colony, {
      as: 'colony',
      foreignKey: 'colonyId'
    });

    Place.hasMany(models.Service, {
      as: 'services',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.Zone, {
      as: 'zones',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.PlaceLock, {
      as: 'placeLock',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsToMany(models.User, {
      through: 'UsersPlaces',
      as: 'usersOfPlace',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.UsersPlaces, {
      as: 'usersPlaces',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsTo(models.TypeCharge, {
      as: 'typeCharge',
      foreignKey: {
        name: 'typeChargeId',
        allowNull: true
      }
    });

    Place.belongsToMany(models.TypeCharge, {
      as: 'typeCharges',
      through: 'PlacesTypeCharge',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsTo(models.TypeFood, {
      as: 'typeFood',
      foreignKey: {
        name: 'typeFoodId',
        allowNull: true
      }
    });

    Place.belongsTo(models.BillingFrequency, {
      as: 'billingFrequency',
      foreignKey: {
        name: 'billingFrequencyId',
        allowNull: true
      }
    });

    Place.hasMany(models.Billings, {
      as: 'billings',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsTo(models.Seller, {
      as: 'sellers',
      foreignKey: 'sellerId'
    });

    Place.hasMany(models.NotiUser, {
      as: 'notiUser',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.hasMany(models.ReservationSponsors, {
      as: 'reservationSponsor',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.SponsorPlaceManual, {
      as: 'sponsorPlaceManual',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.Reservation, {
      as: 'reservation',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.PlacePayment, {
      as: 'placePayment',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.hasMany(models.Payments, {
      as: 'payments',
      foreignKey: 'placeId'
    });

    Place.hasMany(models.Promotions, {
      as: 'promotions',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Place.belongsToMany(models.Categories, {
      as: 'categories',
      through: 'PlacesCategories',
      foreignKey: 'placeId'
    });

    Place.addScope('defaultScope', {
      attributes: {
        exclude: [
          'paymentGatewayPublicKey',
          'paymentGatewayPrivateKey'
        ]
      }
    }, {override: true});

    Place.addScope('withStateAndCountry', {
      include: [{
        model: models.State,
        as: 'states',
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        },
        include: [{
          model: models.Country,
          as: 'country',
          attributes: {
            exclude: ['createdAt', 'updatedAt', 'deletedAt']
          }
        }]
      }]
    });

    Place.addScope('withApplicationSettings', {
      include: [{
        model: models.State,
        as: 'states',
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        },
        include: [{
          model: models.Country,
          as: 'country',
          attributes: {
            exclude: ['createdAt', 'updatedAt', 'deletedAt']
          },
          include: [{
            model: models.ApplicationSettings,
            as: 'applicationSetting',
            attributes: {
              exclude: ['createdAt', 'updatedAt', 'deletedAt']
            }
          }]
        }]
      }]
    });
  };

  return Place;
};
