'use strict';
module.exports = (sequelize, DataTypes) => {
  const PlaceLock = sequelize.define('PlaceLock', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    date: {
      allowNull: false,
      type: DataTypes.DATEONLY
    },
    dateEnd: {
      allowNull: true,
      type: DataTypes.DATEONLY
    },
    start: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    end: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    isInterval: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
  },{ paranoid: true });

  PlaceLock.associate = function(models) {
    PlaceLock.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
  };

  return PlaceLock;
};
