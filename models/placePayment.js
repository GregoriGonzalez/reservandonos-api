'use strict';
module.exports = (sequelize, DataTypes) => {
    const PlacePayment = sequelize.define('PlacePayment', {
        id: {
          primaryKey: true,
          autoIncrement: true,
          type: DataTypes.BIGINT.UNSIGNED
        },
        // TODO ¿será necesario tener currency y tasa de cambio?
        value: {
            allowNull: false,
            type: DataTypes.DECIMAL(10, 2)
        },
        cnktTransId: { // en conekta se guarda el resto de la información
            allowNull: false,
            type: DataTypes.STRING
        },
        pdf: { // link de descarga s3
            allowNull: true,
            type: DataTypes.STRING,
            defaultValue: null,
        },
        xml: {
            allowNull: true,
            type: DataTypes.STRING,
            defaultValue: null,
        }
    });

    PlacePayment.associate = function(models) {
        PlacePayment.belongsTo(models.Place, {
            as: 'place',
            foreignKey: 'placeId'
        });
    };

    return PlacePayment;
}