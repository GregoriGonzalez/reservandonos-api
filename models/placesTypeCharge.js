'use strict';
module.exports = (sequelize, DataTypes) => {
  const PlacesTypeCharge = sequelize.define('PlacesTypeCharge', {
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.BIGINT.UNSIGNED
      },
      start: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.fn('now')
      },
      end: {
        type: DataTypes.DATE,
        allowNull: true
      }
    },
    {
      paranoid: false,
      tableName: 'PlacesTypeCharge',
      timestamp: false
    });
  PlacesTypeCharge.associate = function (models) {
  };
  return PlacesTypeCharge;
};
