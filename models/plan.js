'use strict';
module.exports = (sequelize, DataTypes) => {
  const Plan = sequelize.define('Plan', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    monthlyPlanPrice: {
      allowNull: false,
      type: DataTypes.DECIMAL(19, 2)
    },
    sentPersonPrice: {
      allowNull: false,
      type: DataTypes.DECIMAL(19, 2)
    },
  },{ paranoid: true });

  return Plan;
};