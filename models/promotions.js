'use strict';
module.exports = (sequelize, DataTypes) => {
  const Promotions = sequelize.define('Promotions', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    imageUrl: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    start: {
      type: DataTypes.DATE,
      allowNull: false
    },
    end: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {timestamps: true, paranoid: true,});

  Promotions.associate = function (models) {
    Promotions.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
    Promotions.hasMany(models.Coupons, {
      as: 'coupons',
      foreignKey: {
        name: 'promotionId',
        allowNull: false
      }
    })
  };

  return Promotions;
};
