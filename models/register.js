'use strict';
module.exports = (sequelize, DataTypes) => {
    const Register = sequelize.define('User', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT.UNSIGNED
        },
        name: {
            type: DataTypes.STRING,
            required: true
        },
        lastName: {
            type: DataTypes.STRING,
            required: true
        },
        email: {
            type: DataTypes.STRING,
            required: true,
            unique: true
        },
        lada: {
            type: DataTypes.STRING,
            required: true
        },
        phone: {
            type: DataTypes.STRING,
            required: true,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            required: true
        },
    });
    return Register;
}