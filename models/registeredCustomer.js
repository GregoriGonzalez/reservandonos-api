'use strict';
module.exports = (sequelize, DataTypes) => {
  const RegisteredCustomer = sequelize.define('RegisteredCustomer', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT
    },
    name: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [2, 255],
          msg: "El nombre es muy corto."
        }
      },

    },
    lastName: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [2, 255],
          msg: "Apellido es muy corto."
        }
      },
    },
    email: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: "Ya existe una cuenta con este email."
      },
      allowNull: false
    },
    lada: {
      type: DataTypes.STRING,

    },
    phone: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: "Ya existe una cuenta con este teléfono."
      },
      allowNull: true,
    },
    password: {
      type: DataTypes.STRING,
      len: {
        args: [8, 255],
        msg: "El tamaño del password es incorrecto. [8 - 255]"
      },
      allowNull: true,
    },
    acceptTerms: {
      allowNull: true,
      type: DataTypes.BOOLEAN
    },
    points: {
      type: DataTypes.INTEGER
    },
    facebookUser: {
      type: DataTypes.BOOLEAN
    },
    tags: {
      defaultValue: '',
      type: DataTypes.STRING
    },
    imgUrl: {
      defaultValue: '',
      type: DataTypes.STRING
    },
    sex: {
      defaultValue: '',
      type: DataTypes.STRING
    },
    resetPasswordDatetime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    code: {
      type: DataTypes.STRING
    },
    referenceRegisteredCustomerId: {
      allowNull: true,
      type: DataTypes.BIGINT
    },
    reviewProfile: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  });

  RegisteredCustomer.associate = function (models) {
    RegisteredCustomer.hasMany(models.Favorite, {
      as: 'favorites',
      foreignKey: 'registeredCustomerId'
    });
    RegisteredCustomer.belongsTo(models.State, {
      as: 'state',
      foreignKey: 'stateId'
    });
    RegisteredCustomer.belongsTo(models.Country, {
      as: 'country',
      foreignKey: 'countryId'
    });
    RegisteredCustomer.belongsTo(models.City, {
      as: 'city',
      foreignKey: 'cityId'
    });
    RegisteredCustomer.hasMany(models.Place, {
      as: 'place',
      foreignKey: 'stateId'
    });
    RegisteredCustomer.hasMany(models.CustomerPoint, {
      as: 'CustomerPoints',
      foreignKey: 'registeredCustomerId'
    });
    RegisteredCustomer.hasMany(models.CostumerNotification, {
      as: 'CostumerNotifications',
      foreignKey: 'registeredCustomerId'
    });
    RegisteredCustomer.hasMany(models.Coupons, {
      as: 'coupons',
      foreignKey: {
        name: 'registeredCustomerId',
        allowNull: true,
        defaultValue: null
      }
    });
  };

  return RegisteredCustomer;
};
