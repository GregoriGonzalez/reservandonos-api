'use strict';
module.exports = (sequelize, DataTypes) => {
  const Reservation = sequelize.define('Reservation', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    datetime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    event: {
      allowNull: true,
      type: DataTypes.STRING
    },
    date: {
      allowNull: false,
      type: DataTypes.DATEONLY
    },
    start: {
      // allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    end: {
      type: DataTypes.INTEGER.UNSIGNED
    },
    peopleCount: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    peopleSeating: {
      type: DataTypes.INTEGER.UNSIGNED,
      defaultValue: 0,
      allowNull: true
    },
    smokeArea: DataTypes.BOOLEAN,
    duration: {
      type: DataTypes.INTEGER.UNSIGNED
    },
    isGroup: {
      type: DataTypes.BOOLEAN
    },
    status: {
      type: DataTypes.ENUM('NO_SHOW', 'CANCELLED', 'PENDING', 'CONFIRMED', 'RE_CONFIRMED', 'ARRIVED', 'SEATED', 'GONE', 'WAIT_LIST', 'WAIT_LIST_LOCAL'),
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM('FREE', 'CANCELLATION_POLICY', 'ADVANCE_PAYMENT'),
      allowNull: false
    },
    origin: {
      type: DataTypes.ENUM('WEBSITE', 'PHONE', 'WALK-IN', 'WHATSAPP', 'GOOGLE', 'APP', 'FACEBOOK', 'TWITTER', 'INSTAGRAM'),
      allowNull: false,
      defaultValue: 'PHONE'
    },
    newCostumer: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    notes: DataTypes.STRING,
    reviewed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    paymentCardId: DataTypes.STRING,
    paymentOrderId: DataTypes.STRING,
    paymentChargeId: DataTypes.STRING,
    notificationEmail: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: true
    },
    notificationSms: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: true
    },
    idempotencyToken: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ''
    },
    notificationWhatsapp: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: true
    },
    advancePaymentId: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    invoiceAmount: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0,
      comment: "Hace referencia al monto de comisiones por el tipo de cargo",
    },
    invoiceAmountTotal: {
      type: DataTypes.FLOAT,
      allowNull: true,
      comment: "Hace referencia al monto total de la factura sin descontar comisiones",
    },
    imgUrl: {
      type: DataTypes.STRING
    },
    isValidImg: {
      type: DataTypes.BOOLEAN
    },
    utmSource: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    utmMedium: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    utmCampaign: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    utmTerm: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    commentForCancellation: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    canceledBy: {
      type: DataTypes.ENUM('CUSTOMER', 'PARTNER', 'CALL-CENTER', 'NONE'),
      allowNull: false,
      defaultValue: 'NONE'
    },
    reservationIdDetour: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      defaultValue: null
    }
  },{ paranoid: true });

  Reservation.associate = function (models) {
    Reservation.belongsTo(models.Service, {
      as: 'service',
      foreignKey: {
        name: 'serviceId'
      }
    });

    Reservation.belongsTo(models.Zone, {
      as: 'zone',
      foreignKey: {
        name: 'zoneId'
      }
    });

    Reservation.belongsTo(models.Table, {
      as: 'table',
      foreignKey: {
        name: 'tableId'
      }
    });

    Reservation.belongsTo(models.Group, {
      as: 'group',
      foreignKey: {
        name: 'groupId'
      }
    });

    Reservation.belongsTo(models.Costumer, {
      as: 'costumer',
      foreignKey: {
        name: 'costumerId',
        // allowNull: false
      }
    });

    Reservation.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Reservation.belongsTo(models.Partner, {
      as: 'partner',
      foreignKey: 'partnerId'
    });

    Reservation.belongsTo(models.User, {
      as: 'creator',
      foreignKey: 'creatorUserId'
    });

    Reservation.hasOne(models.Review, {
      as: 'review',
      foreignKey: {
        name: 'reservationId',
        allowNull: false,
        unique: 'reservationId'
      }
    });

    Reservation.hasOne(models.BonusCode, {
      as: 'bonusCode',
      foreignKey: 'reservationId'
    });

    Reservation.belongsTo(models.Billings, {
      as: 'billings',
      foreignKey: {
        name: 'billingId',
        allowNull: true
      }
    });

    Reservation.hasMany(models.ReservationSponsors, {
      as: 'reservationSponsor',
      foreignKey: 'reservationId'
    });
  };

  return Reservation;
};
