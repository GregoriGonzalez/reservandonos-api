'use strict';
module.exports = (sequelize, DataTypes) => {
  const ReservationSponsors = sequelize.define('ReservationSponsors', {
    reservationId: {
      type: DataTypes.BIGINT.UNSIGNED,
      primaryKey: true
    },
    sponsorId: {
      type: DataTypes.BIGINT.UNSIGNED,
      primaryKey: true
    },
    placeId: {
      type: DataTypes.BIGINT.UNSIGNED,
      primaryKey: true
    },
    totalCommission: {
      type: DataTypes.FLOAT,
      allowNull: false,
      comment: "Total de la comision entre el valor del sponsor por la cantidad aplicada a la reservación"
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "Cantidad de producto del sponsor aplicado"
    }
  },{ paranoid: true });

  ReservationSponsors.associate = function(models) {
    ReservationSponsors.belongsTo(models.Reservation, {
      as: 'reservation',
      foreignKey: {
        as: 'reservationId',
        allowNull: false,
      }
    });
    ReservationSponsors.belongsTo(models.Sponsor, {
      as: 'sponsor',
      foreignKey: {
        as: 'sponsorId',
        allowNull: false,
      }
    });
    ReservationSponsors.belongsTo(models.Place, {
      as: 'place',
      allowNull: false
    });
  }

  return ReservationSponsors;
};
