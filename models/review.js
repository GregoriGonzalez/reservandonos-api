'use strict';
module.exports = (sequelize, DataTypes) => {
  const Review = sequelize.define('Review', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    score: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    comment: {
      type: DataTypes.STRING
    },
    imgUrl: {
      type: DataTypes.STRING
    },
    isValidImg: {
      type: DataTypes.BOOLEAN
    }
  },{ paranoid: true });

  Review.associate = function(models) {
    Review.belongsTo(models.Reservation, {
      as: 'reservation',
      foreignKey: {
        name: 'reservationId',
        allowNull: false,
        unique: 'reservationId'
      }
    });

    Review.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
  };

  return Review;
};