'use strict';
module.exports = (sequelize, DataTypes) => {
  const Seller = sequelize.define('Seller', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    name: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
    },
    phone: {
        allowNull: true,
        type: DataTypes.STRING
    },
    email: {
        allowNull: true,
        type: DataTypes.STRING
    }
},{
  timestamps: false
});

  return Seller;
};
