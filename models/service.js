'use strict';
module.exports = (sequelize, DataTypes) => {
  const Service = sequelize.define('Service', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    repetition: {
      allowNull: false,
      type: DataTypes.ENUM('NEVER', 'WEEKLY')
    },
    weekdays: {
      allowNull: false,
      defaultValue: '',
      type: DataTypes.STRING
    },
    date: {
      allowNull: false,
      type: DataTypes.DATEONLY
    },
    start: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    end: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    tablesCount: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    maxCapacity: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    }
  },{ paranoid: true });

  Service.associate = function(models) {
    Service.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Service.belongsToMany(models.Zone, {
      as: 'zones',
      foreignKey: 'serviceId',
      otherKey: 'zoneId',
      through: models.ServiceZone
    });

    // Service.belongsTo(models.ServiceZone, {
    //   as: 'serviceZones',
    //   foreignKey: 'id'
    // });

    Service.hasMany(models.Reservation, {
      as: 'reservations',
      foreignKey: 'serviceId'
    });

    Service.hasMany(models.ServiceZone, {
      as: 'serviceZones',
      foreignKey: 'serviceId'
    })

  };

  return Service;
};