'use strict';
module.exports = (sequelize, DataTypes) => {
  const ServiceZone = sequelize.define('ServiceZone', {
    smokeArea: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    }
  });

  ServiceZone.associate = function(models) {
    ServiceZone.belongsTo(models.Layout, {
      as: 'layout',
      foreignKey: 'layoutId'
    });

    ServiceZone.belongsTo(models.Service, {
      as: 'service',
      foreignKey: 'serviceId'
    });

    ServiceZone.belongsTo(models.Zone, {
      as: 'zone',
      foreignKey: 'zoneId'
    });
  };

  return ServiceZone;
};