'use strict';
module.exports = (sequelize, DataTypes) => {
  const Shorter = sequelize.define('Shorter',
    {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT.UNSIGNED
        },
        short: {
            allowNull: false,
            unique: true,
            type: DataTypes.STRING
        },
        long: {
            allowNull: false,
            type: DataTypes.STRING
        },
        type: {
          type: DataTypes.ENUM('ADD_CARD', 'RE_CONFIRM', 'REVIEW', 'CONFIRMED'),
          allowNull: false
        },
        delete_at: {
            allowNull: false,
            type: DataTypes.DATE
        }
    },
    {
        timestamps: false,
        paranoid: false
    });

  return Shorter;
};
