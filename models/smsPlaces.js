'use strict';
module.exports = (sequelize, DataTypes) => {
  const SmsPlaces = sequelize.define('SmsPlaces', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    phone: {
      allowNull: false,
      type: DataTypes.STRING
    },
    message: {
      allowNull: false,
      type: DataTypes.STRING
    },
  },{ paranoid: true });


  SmsPlaces.associate = function(models) {
    SmsPlaces.belongsTo(models.Place, {
        as: 'place',
        foreignKey: 'placeId'
    });

    SmsPlaces.belongsTo(models.User, {
      as: 'users',
      foreignKey: 'userId'
    });
  };

  return SmsPlaces;
};
