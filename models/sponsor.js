'use strict';
const {SPONSOR_KEYS} = require("../helpers/constants.helper");
module.exports = (sequelize, DataTypes) => {
  const Sponsor = sequelize.define('Sponsor', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    key: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.ENUM(Object.values(SPONSOR_KEYS))
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    title: {
      allowNull: true,
      type: DataTypes.STRING,
      comment: "Titulo que se muestra en el modal de confirmado a show"
    },
    type: {
      allowNull: true,
      type: DataTypes.STRING,
      comment: "Tipo de producto bebida, combo, etc..."
    }
  },{ paranoid: true });

  Sponsor.associate = function(models) {
    Sponsor.hasMany(models.SponsorPlaces, {
      as: 'sponsor',
      foreignKey: {
        name: 'sponsorId',
        allowNull: false
      },
    });

    Sponsor.hasMany(models.ReservationSponsors, {
      as: 'reservationSponsor',
      foreignKey: {
        name: 'sponsorId',
        allowNull: false
      }
    });
  }

  return Sponsor;
};
