'use strict';
module.exports = (sequelize, DataTypes) => {
  const SponsorPlaceManual = sequelize.define('SponsorPlaceManual', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    value: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    date: {
      allowNull: false,
      type: DataTypes.DATEONLY
    }
  },{ paranoid: true });


  SponsorPlaceManual.associate = function(models) {
    SponsorPlaceManual.belongsTo(models.Place, {
      as: 'place',
      foreignKey: 'placeId'
    });

    SponsorPlaceManual.belongsTo(models.Sponsor, {
      as: 'sponsor',
      foreignKey: 'sponsorId'
    });
  }

  return SponsorPlaceManual;
};
