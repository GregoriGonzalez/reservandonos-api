'use strict';
module.exports = (sequelize, DataTypes) => {
  const SponsorPlaces = sequelize.define('SponsorPlaces', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    isActive: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    urlBase:  {
      allowNull: true,
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.ENUM('URL_MENU', 'COMMISSION'),
      allowNull: false,
      defaultValue: 'URL_MENU'
    },
    value: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  },{ paranoid: true });


  SponsorPlaces.associate = function(models) {
    SponsorPlaces.belongsTo(models.Place, {
      as: 'place',
      foreignKey: 'placeId'
    });

    SponsorPlaces.belongsTo(models.Sponsor, {
      as: 'sponsor',
      foreignKey: 'sponsorId'
    });
  }

  return SponsorPlaces;
};
