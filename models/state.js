'use strict';
const models = require("../models");
module.exports = (sequelize, DataTypes) => {
  const State = sequelize.define('State', {
    id: {
      primaryKey: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  },{ paranoid: true });

  State.associate = function(models) {
    State.belongsTo(models.Country, {
        as: 'country',
        foreignKey: 'countryId'
    });

    State.hasMany(models.Place, {
      as: 'place',
      foreignKey: 'stateId'
    });

    State.hasMany(models.City, {
      foreignKey: 'stateId',
      as: 'Cities'
    });
  }

  return State;
};
