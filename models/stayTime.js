'use strict';
module.exports = (sequelize, DataTypes) => {
  const StayTime = sequelize.define('StayTime', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    peopleCount: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    time: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    }
  });

  StayTime.associate = function(models) {
    StayTime.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });
  };

  return StayTime;
};