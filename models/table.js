'use strict';
module.exports = (sequelize, DataTypes) => {
  const Table = sequelize.define('Table', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    number: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    isVertical: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    isInclined: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    onlineSales: {
      allowNull: false,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    },
    minCapacity: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    maxCapacity: {
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED
    },
    coordX: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER
    },
    coordY: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER
    }
  },{ paranoid: true });

  Table.associate = function(models) {
    Table.belongsTo(models.Layout, {
      as: 'layout',
      foreignKey: {
        name: 'layoutId',
        allowNull: false
      }
    });

    Table.belongsTo(models.TableHeight, {
      as: 'tableHeight',
      foreignKey: {
        name: 'tableHeightId',
        allowNull: false
      }
    });

    Table.belongsTo(models.TableType, {
      as: 'tableType',
      foreignKey: {
        name: 'tableTypeId',
        allowNull: false
      }
    });

    Table.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      }
    });

    Table.belongsToMany(models.Group, {
      as: 'groups',
      through: 'GroupTables',
      foreignKey: 'tableId',
      otherKey: 'groupId'
    });

    Table.hasMany(models.Reservation, {
      as: 'reservations',
      foreignKey: 'tableId'
    });

  };

  return Table;
};
