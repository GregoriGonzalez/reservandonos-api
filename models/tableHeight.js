'use strict';
module.exports = (sequelize, DataTypes) => {
  const TableHeight = sequelize.define('TableHeight', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  },{ paranoid: true });

  TableHeight.associate = function(models) {
    
  };

  return TableHeight;
};