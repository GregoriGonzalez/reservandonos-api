'use strict';
module.exports = (sequelize, DataTypes) => {
  const TableType = sequelize.define('TableType', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  },{ paranoid: true });

  TableType.associate = function(models) {
    
  };

  return TableType;
};