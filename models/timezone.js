'use strict';
module.exports = (sequelize, DataTypes) => {
  const Timezone = sequelize.define('Timezone', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    }
  },{ paranoid: true });

  return Timezone;
};