'use strict';
module.exports = (sequelize, DataTypes) => {
  const TypeCharge = sequelize.define('TypeCharge', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    description: {
      allowNull: false,
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.ENUM('%', '$'),
      allowNull: false
    },
    value: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER
    },
    origin: {
      type: DataTypes.ENUM('WEBSITE', 'GOOGLE'),
      allowNull: false,
      defaultValue: 'GOOGLE'
    }
  },{ paranoid: true });

  TypeCharge.associate = function(models) {
    TypeCharge.hasMany(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'typeChargeId',
        allowNull: true
      }
    });
    TypeCharge.belongsToMany(models.Place, {
      as: 'places',
      through: 'PlacesTypeCharge',
      foreignKey: {
        name: 'typeChargeId',
        allowNull: false
      }
    });
  };

  return TypeCharge;
};
