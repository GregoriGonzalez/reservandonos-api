'use strict';
module.exports = (sequelize, DataTypes) => {
  const TypeFood = sequelize.define('TypeFood', {
    id: {
      primaryKey: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  },{ paranoid: true });

  TypeFood.associate = function(models) {
    TypeFood.hasMany(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'typeFoodId',
        allowNull: true
      }
    });
  }

  return TypeFood;
};
