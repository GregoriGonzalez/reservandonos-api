'use strict';
const {ROLES} = require("../helpers/constants.helper");
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    email: {
      allowNull: false,
      // unique: 'email',
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    fullname: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address: {
      allowNull: true,
      type: DataTypes.STRING
    },
    phone: {
      allowNull: true,
      type: DataTypes.STRING
    },
    genre: {
      allowNull: true,
      defaultValue: 'MALE',
      type: DataTypes.ENUM('MALE', 'FEMALE'),
    },
    profileImage: {
      defaultValue: 'profile.jpg',
      type: DataTypes.STRING
    },
    role: {
      allowNull: false,
      type: DataTypes.ENUM,
      values: [
        'ADMIN', // reservandonos
        'OWNER', // crear lugares
        'MANAGER', // configurar lugar, ver reportes
        'HOSTESS', // recibir comensales
        'TELEMARKETING', // reservandonos > reservas por telefono,
        'RECEPCIONIST', // lugar > reservas por telefono
        'CASHIER', // gift cards
        'PARTNER', // partner
        'WAITLIST', // solo lista de espera
        'FASTFOOD' // comida rapida
      ]
    },
    adminPartner:{
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    verified: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    acceptTerms: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    unreadedNotificationsCount: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER.UNSIGNED
    },
    darkMode: {
      allowNull: false,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    },
    langCode: {
      allowNull: false,
      defaultValue: 'es',
      type: DataTypes.STRING(2)
    },
    tokenFCM: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      comment: "Token de FCM para envio de notificaciones Push",
    },
  },{ paranoid: true });

  User.associate = function(models) {
    User.belongsTo(models.Place, {
      as: 'place',
      foreignKey: 'placeId'
    });

    User.belongsTo(models.Partner, {
      as: 'partner',
      foreignKey: 'partnerId'
    });

    User.hasMany(models.Bitacora, {
      as: 'bitacora',
      foreignKey: {
        name: 'userId',
        allowNull: false
      }
    });

    User.hasMany(models.SmsPlaces, {
      as: 'smsPlaces',
      foreignKey: {
        name: 'userId',
        allowNull: false
      }
    });

    User.hasMany(models.NotiConfig, { foreignKey: 'userId' });

    User.belongsToMany(models.Place, {
      through: 'UsersPlaces',
      as: 'places',
      foreignKey: 'userId'
    });

    User.hasMany(models.UsersPlaces, {
      as: 'usersPlaces',
      foreignKey: {
        name: 'userId',
        allowNull: false
      }
    });
  };

  /**
   * Class Method
   * This method finds a user by email and looks for if it is in a place based on the placeId.
   * @param email
   * @param placeId
   * @param options
   * @returns {Promise<Model>}
   */
  User.getByEmailAndPlaceId = async function (email, placeId, options) {
    const user = await User.findOne({
      ...options,
      where: {
        email: email.toLowerCase()
      }
    });
    if (user) {
      user.usersPlaces = await user.getUsersPlaces({
        where: {
          placeId
        },
        limit: 1
      });
    }
    return user;
  }

  /**
   * @returns {Promise<Model<User[]>>}
   */
  User.getAdministratorsPartners = async function() {
    return await User.findAll({
      where: {role: ROLES.ADMIN, adminPartner: 1}
    });
  }

  /**
   * @returns {Promise<Model<User[]>>}
   */
  User.getAdministrators = async function() {
    return await User.findAll({
      where: {role: ROLES.ADMIN, adminPartner: 0}
    });
  }

  /**
   * @returns {Promise<Model<User[]>>}
   */
  User.getManagers = async function(placeId) {
    return await User.findAll({
      where: {role: ROLES.MANAGER},
      include: [{
        model: sequelize.models.UsersPlaces,
        as: 'usersPlaces',
        where: {placeId},
        required: true
      }]
    });
  }

  /**
   * @param placeId {number}
   * @param asModel {boolean}
   * @returns {Promise<Model<User[]>|string[]>}
   */
  User.getTokensFCM = async function(placeId, asModel = true) {
    const users = await User.findAll({
      attributes: ['tokenFCM'],
      where: {
        tokenFCM: {
          [sequelize.Op.not]: null
        }
      },
      include: [{
        model: sequelize.models.UsersPlaces,
        as: 'usersPlaces',
        attributes: [],
        where: {placeId},
        required: true
      }]
    });

    return asModel ? users : users.map(user => user.tokenFCM);
  }

  /**
   * Instance Method
   * If usersPlaces exist in the instance,
   * we populate notificationEmail and notificationWhatsapp from UsersPlaces table
   * to the instance of the user.
   * @returns {User}
   */
  User.prototype.populateNotification = function () {
    if (this.usersPlaces.length) {
      this.notificationEmail = this.usersPlaces[0].notificationEmail;
      this.notificationWhatsApp = this.usersPlaces[0].notificationWhatsApp;
    }
   return this;
  }

  return User;
};
