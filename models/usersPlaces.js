'use strict';
module.exports = (sequelize, DataTypes) => {
  const UsersPlaces = sequelize.define('UsersPlaces', {
    notificationEmail: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    notificationWhatsApp: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    }
  },{ paranoid: false });

  UsersPlaces.associate = function(models) {};

  return UsersPlaces;
};
