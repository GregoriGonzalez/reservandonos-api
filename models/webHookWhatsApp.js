'use strict';
module.exports = (sequelize, DataTypes) => {
    const WebHookWhatsApp = sequelize.define('WebHookWhatsApp', {
        id: {
            primaryKey: true,
            type: DataTypes.BIGINT.UNSIGNED
        },
        description: {
            allowNull: false,
            type: DataTypes.STRING
        },
        conversationId: {
            allowNull: true,
            type: DataTypes.STRING
        },
        numberPhone: {
            allowNull: true,
            type: DataTypes.STRING
        }
    },{ paranoid: true });

    return WebHookWhatsApp;
};
