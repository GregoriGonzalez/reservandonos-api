'use strict';
module.exports = (sequelize, DataTypes) => {
    const WorkSchedule = sequelize.define('WorkSchedule', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.BIGINT.UNSIGNED
        },
        weekday: {
            allowNull: false,
            type: DataTypes.STRING
        },
        start: {
            allowNull: false,
            type: DataTypes.INTEGER.UNSIGNED
        },
        end: {
            allowNull: false,
            type: DataTypes.INTEGER.UNSIGNED
        }
    }, {
        "paranoid": true,
    });

    WorkSchedule.associate = function(models) {
        WorkSchedule.belongsTo(models.Place, {
            as: 'place',
            foreignKey: {
                name: 'placeId',
                allowNull: false
            }
        });
    };

    return WorkSchedule;
};
