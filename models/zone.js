'use strict';
module.exports = (sequelize, DataTypes) => {
  const Zone = sequelize.define('Zone', {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    coordX: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    coordY: {
      allowNull: false,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    // width: {
    //   allowNull:false,
    //   type: DataTypes.INTEGER,
    //   defaultValue: 0
    // },
    // height: {
    //   allowNull: false,
    //   type: DataTypes.INTEGER,
    //   defaultValue: 0
    // }
  },{ paranoid: true });

  Zone.associate = function(models) {
    Zone.belongsTo(models.Place, {
      as: 'place',
      foreignKey: {
        name: 'placeId',
        allowNull: false
      },
      onDelete: 'CASCADE'
    });

    Zone.hasMany(models.Layout, {
      as: 'layouts',
      foreignKey: {
        name: 'zoneId',
        allowNull: false
      }
    });

    Zone.belongsToMany(models.Service, {
      as: 'services',
      foreignKey: 'zoneId',
      otherKey: 'serviceId',
      through: models.ServiceZone
    });
  };

  return Zone;
};