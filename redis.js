const {createClient} = require('redis');
const {HandleError, InternalProcessError} = require("./helpers/error.helper");

let client;


const init = async () => {
  client = createClient();
  client.on('error', (err) => {
    console.error('Redis Client Error', err);
    process.exit(0);
  });
  await client.connect();
}

const getInstance = async () => {
  if (!client) {
    await init();
  }
  return client;
}

/*
 * DELETE ZONE BY PLACE_ID AND SERVICE_ID
 */
const deleteZoneByPlaceId = async (serviceId, placeId) => {
  try {
    const client = await getInstance();
    client.del(`zone-${serviceId}-${placeId}`);
  } catch (e) {
    HandleError(null, null, new InternalProcessError(error));
  }
};

/*
* FLUSH DATA BASE REDIS
*/
const flushRedis = async () => {
  return new Promise(async (resolve, reject) => {
    const client = await getInstance();
    const response = await client.flushAll();
    return resolve({response: response});
  });
}

/*
* SET ROW BY KEY
*/
const setRegister = async (data, key, second = 10000000) => {
  try {
    const client = await getInstance();
    client.setEx(key, second, JSON.stringify(data));
  } catch (e) {
    HandleError(null, null, new InternalProcessError(error));
  }
}

/*
* GET ROW BY KEY
*/
const getRegister = async (key) => {
  try {
    const client = await getInstance();
    const data = await client.get(key);
    return JSON.parse(data);
  } catch (e) {
    HandleError(null, null, new InternalProcessError(error));
  }
}

/*
* DELETE ROW BY KEY
*/
const deleteRow = async (key) => {
  try {
    const client = await getInstance();
    return await client.del(key);
  } catch (e) {
    HandleError(null, null, new InternalProcessError(error));
  }
}

module.exports = {
  init,
  deleteZoneByPlaceId,
  flushRedis,
  deleteRow,
  setRegister,
  getRegister
}
