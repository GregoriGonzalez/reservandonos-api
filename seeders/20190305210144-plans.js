'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Plans', [{
      id: 1,
      name: 'Gratis',
      monthlyPlanPrice: 0,
      sentPersonPrice: 40
    },
    {
      id: 2,
      name: 'Plus',
      monthlyPlanPrice: 1500,
      sentPersonPrice: 30
    },
    {
      id: 3,
      name: 'Premium',
      monthlyPlanPrice: 3000,
      sentPersonPrice: 30
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Plans', null, {});
  }
};
