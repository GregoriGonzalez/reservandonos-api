'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Partners', [{
      id: 1,
      name: 'Reservandonos',
      contact: 'Hector',
      countryCode: '+521',
      phone: '551234123',
      email: 'info@reservandonos.com',
      logoImage: null,
      notes: null,
      clabe: '12345678901234567',
      beneficiary: 'Nightcode Group S. A. Promotora de Inversión de C. V.',
      sentPersonFee: 0
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Partners', null, {});
  }
};
