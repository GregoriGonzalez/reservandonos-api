'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('TableHeights', [
      {
        id: 1,
        name: 'Alta'
      },
      {
        id: 2,
        name: 'Baja'
      }
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('TableHeights', null, {});
  }
};
