'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('TableTypes', [
      {
        id: 1,
        name: 'Cuadrada'
      },
      {
        id: 2,
        name: 'Rectangular'
      },
      {
        id: 3,
        name: 'Redonda'
      }
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('TableTypes', null, {});
  }
};
