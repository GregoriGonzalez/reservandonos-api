'use strict';

// sequelize db:seed:all
// sequelize db:seed:undo
// sequelize db:seed:undo:all

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Timezones', [{
      name: 'Ciudad de México',
      code: 'America/Mexico_City'
    },
    {
      name: 'Tijuana',
      code: 'America/Tijuana'
    },
    {
      name: 'Cancún',
      code: 'America/Cancun'
    },
    {
      name: 'Mazatlán',
      code: 'America/Mazatlan'
    },
    {
      name: 'Hermosillo',
      code: 'America/Hermosillo'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Timezones', null, {});
  }
};
