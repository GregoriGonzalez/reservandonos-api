const redisAdapter = require('socket.io-redis');
const jwt = require('jsonwebtoken');
const env = require('./config/env');
const app = require('./app');
const db = require('./models');
const cron = require('./helpers/cron');
// const initClientRedis = require('./redis').init;

const port = parseInt(process.env.PORT || '3000');


/** Connect with Redis **/
// initClientRedis().then(() => { console.log('Connected With Redis....')});

db.sequelize.sync()
  .then(function () {
    const server = app.listen(port);
    if (process.env.CRON === 'true') {
      cron.init().catch(e => console.error("Error Cron", e));
    }
    const io = require('./socket').init(server);
    io.adapter(redisAdapter({host: 'localhost', port: 6379}))
    io.use(function (socket, next) {
      if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token, env.jwtSecret, function (err, decoded) {
          if (err) {
            const error = new Error();
            error.message = err.message;
            error.status = 450;
            return next(error);
          }
          socket.user = decoded;
          next();
        });
      } else {
        console.error('auth error', err)
        next(new Error('Authentication error'));
      }
    })
      .on('connection', socket => {
        socket.join('user_' + socket.user.id);
        const userPlace = socket.user.placeId;
        if (userPlace) {
          socket.join(userPlace.toString());
        }
      });
    server.on('error', onError);
    server.on('listening', onListening);
  });

const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  switch (error.code) {
    case 'EACCES':
      console.error('Port ' + port + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error('Port ' + port + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

const onListening = () => {
  console.info('Listening on port ' + port);
}
