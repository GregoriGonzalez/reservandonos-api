const env = require('../../../config/env');
const urlSite = env.dns ? `${env.dns}/#/rsv/auth/login` : 'https://sistema.reservandonos.com';
const {
  title_in_process,
  info_in_process,
  downloadMenu,
  infoCustomer,
  sendWhatsApp,
  rowCancelModidy,
  googleMapsPosition,
  socialNetwork,
  styleFonts,
  rowCalendarFriends,
  rowCalendarRestaurants,
  title_confirmed,
  info_confirmation,
  questions,
  header,
  title_cancelled_restaurant,
  title_cancelled_customer,
  groupWhatsappTelegram
} = require('./elementsEmail');
const { getInfoGroup } = require('../../../helpers/utils.helper');

const templateInProcessClient = (place, reservation, customer, cancelURL, sponsor, urlMenu, templateInProcessClient) => {
  const title = "Tu reserva esta en Proceso";
  const content = `
    <!-- estado de reserva -->
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      ${header(reservation)}

      <div style="font-weight: 500; color: #424242; text-align: center; margin-top: 2em;">
        <h3>${place.name}</h3>
        ${title_in_process(`${customer.fullname}`)}
      </div>
      <br>

      ${info_in_process(templateInProcessClient)}

      ${urlMenu ? downloadMenu(urlMenu) : ''}

      ${infoCustomer(customer, reservation)}

      ${sendWhatsApp(reservation.id)}

      ${rowCancelModidy(cancelURL, reservation.id)}
    </section>

    ${getInfoGroup(place).data ? groupWhatsappTelegram(getInfoGroup(place).info.TELEGRAM, getInfoGroup(place).info.WHATSAPP, getInfoGroup(place).info.NAME) : ''}

    ${1==2 ? googleMapsPosition(place) : ''}

    ${downloadApp()}

    ${socialNetwork()}

    <br>
  `;

  return base(title, content);
}

const templateRsvConfirmed = (place, reservation, customer, cancelURL, sponsor, urlMenu) => {
  const title = "Tu reserva ha sido confirmada";
  const content = `
    <!-- estado de reserva -->
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      ${header(reservation)}

      <div style="font-weight: 500; color: #424242; text-align: center; margin-top: 2em;">
        <h3>${place.name}</h3>
        ${title_confirmed(`${customer.fullname}`)}
      </div>
      <br>

      ${urlMenu ? downloadMenu(urlMenu) : ''}

      ${infoCustomer(customer, reservation)}

      ${sendWhatsApp(reservation.id)}

      ${rowCancelModidy(cancelURL, reservation?.id)}
    </section>

    ${getInfoGroup(place).data ? groupWhatsappTelegram(getInfoGroup(place).info.TELEGRAM, getInfoGroup(place).info.WHATSAPP, getInfoGroup(place).info.NAME) : ''}
    
    ${info_confirmation()}

    ${1==2 ? googleMapsPosition(place) : ''}

    ${downloadApp()}

    ${socialNetwork()}

    <br>
  `;

  return base(title, content);
}

const templateRsvCancelledPlace = (place, reservation, customer, cancelURL, sponsor, urlMenu) => {
  const title = "Tu reserva ha sido cancelada";
  const content = `
    <!-- estado de reserva -->
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      ${header(reservation)}

      <div style="font-weight: 500; color: #424242; text-align: center; margin-top: 2em;">
        <h3>${place.name}</h3>
        ${title_cancelled_restaurant(`${customer.fullname}`)}
      </div>
      <br>

      ${questions()}

      ${infoCustomer(customer, reservation)}
      
      ${rowCalendarRestaurants()}

    </section>

    ${getInfoGroup(place).data ? groupWhatsappTelegram(getInfoGroup(place).info.TELEGRAM, getInfoGroup(place).info.WHATSAPP, getInfoGroup(place).info.NAME) : ''}

    ${downloadApp()}

    ${socialNetwork()}
    <br>
  `;

  return base(title, content);
}


const templateRsvCancelledCustomers = (place, reservation, customer, cancelURL, sponsor, urlMenu) => {
  const title = "Tu reserva se ha cancelado";
  const content = `
    <!-- estado de reserva -->
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      ${header(reservation)}

      <div style="font-weight: 500; color: #424242; text-align: center; margin-top: 2em;">
        <h3>${place.name}</h3>
        ${title_cancelled_customer(`${customer.fullname}`)}
      </div>
      <br>

      ${infoCustomer(customer, reservation)}
      
      ${rowCalendarRestaurants()}

    </section>

    ${getInfoGroup(place).data ? groupWhatsappTelegram(getInfoGroup(place).info.TELEGRAM, getInfoGroup(place).info.WHATSAPP, getInfoGroup(place).info.NAME) : ''}

    ${questions()}

    ${downloadApp()}

    ${socialNetwork()}
    <br>
  `;

  return base(title, content);
}

const base = (title, content) => {
  return `<!DOCTYPE html>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>${title}</title>
      ${styleFonts()}
    </head>
    <body style="margin: 0; padding: 0;">
      <div style="width: 350px; margin: auto;">
        <!-- imagen header -->
        <div align="center" bgcolor="#fff" style="padding: 5px;">
            <img width='50%' src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Logo-Reserv%C3%A1ndonos.png" alt="Creating Email Magic" style="display: block;" />
        </div>

        ${content}
      </div>
    </body>
  </html>
  `;
};

module.exports = {
  templateInProcessClient,
  templateRsvConfirmed,
  templateRsvCancelledPlace,
  templateRsvCancelledCustomers
}
