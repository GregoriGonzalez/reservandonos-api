const env = require('../../../config/env');
const urlSite = env.dns ? `${env.dns}/#/rsv/auth/login` : 'https://sistema.reservandonos.com'

const coupon = (object) => {
  const title = "Código de cupón ${coupon}";
  /*
  const content = `
      <header style="
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      ">
        <div style="
            background: rgba(253, 35, 35, 0.93);
            border-radius: 10px;
            color: white;
            padding: 10px 0 40px;
            width: 100%;
            text-align: center;
        ">
          <h1 style="font-size: 24px;">CANJEA TU PROMOCIÓN</h1>
          <div style="display: flex; justify-content: center; align-items: center; gap: 30px; margin: 20px">
            <img style="height: 25px" src="reservandonos.png">
            <img style="height: 25px" src="coca-cola.png">
          </div>
        </div>
        <div>¡AQUÍ TIENES TU CÓDIGO!</div>
    </header>

    <main style="
        margin: 50px;
        text-align: center;"
    >
      <p style="font-size: 14px;">Te invitamos a revisar y aplicar las recomendaciones de las autoridades
        de salud para evitar los riesgos ante emergencia sanitaria por COVID-19</p>
    
      <div style="display: flex; gap: 20px; justify-content: flex-start; align-items: center; margin: 50px 0px;">
        <img style=" width: 140px; max-height: 140px;" src="place.logo">
        <div>
          <h3 style=" text-align: left; margin: 10px 0; font-size: 18px;">{{ place.name }}</h3>
          <h5 style="margin: 10px 0; font-weight: inherit; font-size: 14px;">Promoción válida desde el {{ promotion.start }} - {{ promotion.end }}</h5>
        </div>
      </div>
    
      <div style="display: flex; gap: 20px; justify-content: flex-start; margin: 50px 0px;">
        <div style=" width: 160px; padding: 10px; border: 2px solid red; border-radius: 4px;">
          <img style="width: 100%; max-height: 140px; border-radius: 10px;" src="{{ loadResource promotion.logo }}">
          <p style="font-weight: bold; margin: 0; text-align: center;">{{ promotion.title }}</p>
        </div>
        <div class="right">
          <div class="description">
            <h3>Cobertura</h3>
            <h5>{{ place.address }}</h5>
          </div>
          <div class="description">
            <h3>Restricciones</h3>
            <h5>Válido al solicitar el beneficio y presentar código digital o impreso. </h5>
            <h5>No aplica en entregas a domicilio. No aplica con otras promociones y/o descuentos.</h5>
          </div>
        </div>
      </div>
    
      <h2 id="code">Código o Folio: <strong>{{ coupon.code }}</strong></h2>
    </main>
  `;*/
}

const confirmEmail = ({placeName, emailUrl}) => {
  const title = "Confirma tu email";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Bienvenido!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola, ${placeName}.</p>
                <p>Completa tu registro y conviértete en usuario de Reservándonos confirmando tu correo electrónico:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${emailUrl}" style="text-decoration: none; background-color: #e43d59;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Confirmar</a>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p style="margin:0;">Gracias por crear una cuenta en Reservándonos.</p>
                <p style="margin:0;">Esperamos verte pronto.</p>
            </td>
        </tr>
        <tr>
            <td style="color:#424242;text-align: center; font-size: 13px; padding-top:40px;">
                <p style="margin:0;">Este correo electrónico forma parte del proceso de registro. Si no quieres unirte a Reservándonos Partners, ignora este correo y te eliminaremos de nuestra base de datos.</p>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const newReservation = ({fullName, placeName, reservation, customer, confirmUrl, cancelUrl, datetime, date}) => {
  const title = "Nueva reserva";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Nueva Reserva!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola ${fullName}, has recibido una nueva reserva en tu restaurante:</p>
                <p style="font-weight: bold; color:#424242; margin:0;">${placeName}</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px dashed #424242; text-align: center; font-size: 15px; padding: 10px;">
                <p style="margin:0;">Fecha: ${date}</p>
                <p style="margin:0;">Hora: ${datetime} hrs</p>
                <p style="margin:0;">Personas ${reservation.peopleCount}</p>
                <p style="margin:0;">A nombre de: ${customer.fullname}</p>
                <p style="margin:0;">Comentarios: ${reservation.notes ? reservation.notes : ''}</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-size: 15px; padding:15px 0 9px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">¿Qué quieres hacer?</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px 50px;text-align: center; font-size: 15px;">
                <a href="${confirmUrl}" style="background-color: #4a7b5d;color: white; border-radius: 1px;text-decoration:none; float:left;"><p style="padding:8px 10px; margin:0;">Confirmar</p></a>
                <a href="${cancelUrl}" style="background-color: #e43d59;color: white; border-radius: 1px;text-decoration:none; float:right;"><p style="padding:8px 10px; margin:0;">Cancelar</p></a>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-size: 15px; padding:18px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Entrar al sistema:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${urlSite}" style="text-decoration: none; background-color:#aaaaaa;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Entrar</a>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const reservationConfirm = ({fullName, placeName, reservation, customer, datetime, date}) => {
  const title = "Reserva confirmada";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Reserva Confirmada!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola ${fullName}, Gracias por reservar en ${placeName}, se ha confirmado tu reservación.</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px dashed #424242; text-align: center; font-size: 15px; padding: 10px;">
                <p style="margin:0 0 10px 0;font-weight: bold; color:#424242;">Detalles de la reservación</p>
                <p style="margin:0;">Lugar: ${placeName}</p>
                <p style="margin:0;">Fecha: ${date}</p>
                <p style="margin:0;">Hora: ${datetime} hrs</p>
                <p style="margin:0;">Personas: ${reservation.peopleCount}</p>
                <p style="margin:0;">Area: ${reservation.smokeArea == 0 ? 'No Fumar' : 'Fumar'}</p>
                <p style="margin:0;">Comentarios: ${reservation.notes} </p>
            </td>
        </tr>
        <tr>
            <td style="color:#424242;text-align: center; font-size: 13px; padding-top:40px;">
                <p style="margin:0;">Para cualquier duda o aclaración favor de comunicarte al 5575839911</p>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const reservationCancelled = ({fullName, placeName, reservation, customer, datetime, date}) => {
  const title = "Reserva cancelada";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Reserva Cancelada!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola ${fullName}, hemos intentado todo para poder encontrar un lugar en la hora que nos solicitaste, pero estamos llenos.</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px dashed #424242; text-align: center; font-size: 15px; padding: 10px;">
                <p style="margin:0 0 10px 0;font-weight: bold; color:#424242;">Detalles de la reservación</p>
                <p style="margin:0;">Lugar: ${placeName}</p>
                <p style="margin:0;">Fecha: ${date}</p>
                <p style="margin:0;">Hora: ${datetime} hrs</p>
                <p style="margin:0;">Personas: ${reservation.peopleCount}</p>
                <p style="margin:0;">Area: ${reservation.smokeArea == 0 ? 'No Fumar' : 'Fumar'}</p>
                <p style="margin:0;">Comentarios: ${reservation.notes} </p>
            </td>
        </tr>
        <tr>
            <td style="color:#424242;text-align: center; font-size: 13px; padding-top:40px;">
                <p style="margin:0;">Te recomendamos visitar nuestra pagina para buscar otro lugar cerca de la zona que deseas asistir o llamar, directamente a nuestro equipo para que tramite tu reservación al 5575839911</p>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const invitationRsvPartners = (placeName, registerUrl) => {
  const title = "Registrate en Reservandonos";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Bienvenido ${placeName}!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Te invitamos a seguir formando parte de la familia de Reservándonos.</p>
                <p>Nuestra nueva plataforma de reservaciones enfocada totalmente en nuestros socios restauranteros, en la cual podrás seguir trabajando de la misma manera, pero de forma automatizada.</p>
            </td>
        </tr>

        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${registerUrl}" style="text-decoration: none; background-color: #e43d59;color: white;padding:8px 10px; border-radius: 1px;" font-weight: bold;>Registrate ahora</a>
            </td>
        </tr>
        <tr>
            <td style="color:#424242;text-align: center; font-size: 14px; padding-top:40px;">
                <p style="font-weight: bold; margin:0;">¿Tienes Dudas?</p>
                <p style="margin:0;">Contacta a uno de nuestros colaboradores</p>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const reservationConfirmManager = ({placeName, reservation, customer, datetime, date}) => {
  const title = "Reserva confirmada";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Reserva Confirmada!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola, se ha confirmado la reservación a nombre de: ${customer.fullname}</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px dashed #424242; text-align: center; font-size: 15px; padding: 10px;">
                <p style="margin:0 0 10px 0;font-weight: bold; color:#424242;">Detalles de la reservación</p>
                <p style="margin:0;">Lugar: ${placeName}</p>
                <p style="margin:0;">Fecha: ${date}</p>
                <p style="margin:0;">Hora: ${datetime} hrs</p>
                <p style="margin:0;">Personas: ${reservation.peopleCount}</p>
                <p style="margin:0;">Area: ${reservation.smokeArea == 0 ? 'No Fumar' : 'Fumar'}</p>
                <p style="margin:0;">Comentarios: ${reservation.notes} </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-size: 15px; padding:18px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Entrar al sistema:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${urlSite}" style="text-decoration: none; background-color:#aaaaaa;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Entrar</a>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const reservandonosReConfirmedReservationFromClient = ({placeName, reservation, customer, datetime, date}) => {
  const title = "Reserva confirmada";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡En hora buena!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola, el cliente ${customer.fullname} ha confirmado su asistencia para la reserva.</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px dashed #424242; text-align: center; font-size: 15px; padding: 10px;">
                <p style="margin:0 0 10px 0;font-weight: bold; color:#424242;">Detalles de la reservación</p>
                <p style="margin:0;">Lugar: ${placeName}</p>
                <p style="margin:0;">Fecha: ${date}</p>
                <p style="margin:0;">Hora: ${datetime} hrs</p>
                <p style="margin:0;">Personas: ${reservation.peopleCount}</p>
                <p style="margin:0;">Area: ${reservation.smokeArea == 0 ? 'No Fumar' : 'Fumar'}</p>
                <p style="margin:0;">Comentarios: ${reservation.notes} </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-size: 15px; padding:18px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Entrar al sistema:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${urlSite}" style="text-decoration: none; background-color:#aaaaaa;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Entrar</a>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const reservationCancelledManager = ({placeName, reservation, customer, datetime, date}) => {
  const title = "Reserva cancelada";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Reserva Cancelada!
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                <p>Hola, la reserva a nombre de: ${customer.fullname} ha sido cancelada.</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px dashed #424242; text-align: center; font-size: 15px; padding: 10px;">
                <p style="margin:0 0 10px 0;font-weight: bold; color:#424242;">Detalles de la reservación</p>
                <p style="margin:0;">Lugar: ${placeName}</p>
                <p style="margin:0;">Fecha: ${date}</p>
                <p style="margin:0;">Hora: ${datetime} hrs</p>
                <p style="margin:0;">Personas: ${reservation.peopleCount}</p>
                <p style="margin:0;">Area: ${reservation.smokeArea == 0 ? 'No Fumar' : 'Fumar'}</p>
                <p style="margin:0;">Comentarios: ${reservation.notes} </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-size: 15px; padding:18px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Entrar al sistema:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${urlSite}" style="text-decoration: none; background-color:#aaaaaa;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Entrar</a>
            </td>
        </tr>
    </table>
    `;
  return base(title, content);
}

const cutNotification = (fullName, placeName) => {
  const title = "Notificación de corte";
  const content = `
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
              <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                  Notificacion de corte
              </td>
          </tr>
          <tr>
              <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                  <p style="margin:0;">Hola ${fullName}, ya esta listo el corte de tu establecimiento:</p>
              </td>
          </tr>
          <tr>
            <td style="font-weight: bold; padding:5px 0; color:#424242;text-align: center; font-size: 16px;">
                ${placeName}
            </td>
          </tr>
          <tr>
            <td style="color:#424242;text-align: center; font-size: 15px; padding-top:20px;">
                <p style="margin:0;">*Recuerda que tienes 5 días para poner todas las reservas en show o no show, de no ser así, al cumplirse el plazo, todas las reservas pasarán automáticamente a status show*</p>
            </td>
          </tr>
          <tr>
            <td style="text-align: center; font-size: 15px; padding:24px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Entrar al sistema:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${urlSite}" style="text-decoration: none; background-color:#aaaaaa;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Entrar</a>
            </td>
        </tr>
      </table>
      `;
  return base(title, content);
}

const invoiceNotification = (fullName, placeName, ivoiceUrl) => {
  const title = "Notificación de factura";
  const content = `
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
              <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Tu factura está lista!
              </td>
          </tr>
          <tr>
              <td style="padding: 20px 0;text-align: center; font-size: 15px;">
                  <p style="margin:0;">Hola ${fullName}, ya esta lista la factura del último corte de tu establecimiento:</p>
              </td>
          </tr>
          <tr>
            <td style="font-weight: bold; padding:5px 0; color:#424242;text-align: center; font-size: 16px;">
                ${placeName}
            </td>
          </tr>
          <tr>
            <td style="color:#424242;text-align: center; font-size: 15px; padding-top:20px;">
                <p style="margin:0;">*Te recordamos que si no actualizaste todas las reservaciones a show o no show, pasaron automáticamente a status show para su cobro*</p>
            </td>
          </tr>
          <tr>
            <td style="text-align: center; font-size: 15px; padding:24px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Facturacion:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${ivoiceUrl}" style="text-decoration: none; background-color: #e43d59;color: white;padding:8px 10px; border-radius: 1px;" font-weight: bold;>Ir a factura</a>
            </td>
        </tr>
      </table>
      `;
  return base(title, content);
}

const notificationPendingPayment = (fullName, placeName, periodo) => {
  const title = "Pago pendiente";
  const content = `
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
              <td style="font-weight: bold; color:#424242;text-align: center; font-size: 18px;">
                ¡Pago pendiente!
              </td>
          </tr>
          <tr>
              <td style="padding: 20px 0; text-align: center; font-size: 15px;">
                  <p style="margin:0;">Hola ${fullName}, recuerda que tienes un adeudo en tus reservas del periodo <b>${periodo}</b> de tu restaurante:</p>
              </td>
          </tr>
          <tr>
            <td style="font-weight: bold; padding:5px 0; color:#424242;text-align: center; font-size: 16px;">
                ${placeName}
            </td>
          </tr>
          <tr>
            <td style="color:#424242;text-align: center; font-size: 15px; padding-top:20px;">
                <p style="margin:0;">Para seguir teniendo el servicio, debes cubrir tus pagos correspondientes.</p>
            </td>
          </tr>
          <tr>
            <td style="text-align: center; font-size: 15px; padding:24px 0 10px 0;">
                <p style="font-weight: bold; color:#424242; margin:0;">Entrar al sistema:</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 9px;text-align: center; font-size: 15px;">
                <a href="${urlSite}" style="text-decoration: none; background-color:#aaaaaa;color: white;padding:8px 20px; border-radius: 1px;" font-weight: bold;>Entrar</a>
            </td>
        </tr>
      </table>
      `;
  return base(title, content);
}

const resetPassword = ({placeName, emailUrl}) => {
  const title = "¡Restablecimiento de contraseña!";
  const content = `
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td
                style="font-weight: bold; color:#424242;text-align: center; font-size: 20px; font-family: 'OmnesBold', sans-serif;">
                ¡Restablecimiento de contraseña!
            </td>
        </tr>
        <tr>
            <td style="padding: 30px 20px;text-align: center; font-size: 14px;">
                <p>Hola ${placeName}, Hemos recibido una solicitud para cambiar tu
                    contraseña. Haz clic a
                    continuación para restablecerla.</p>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 0px;text-align: center; padding-top: 20px;">
                <a href="${emailUrl}"
                    style="text-decoration: none; background-color: #e43d59;color: white;padding:8px 15px; border-radius: 1px; font-size: 15px;">
                    Restablecer contraseña</a>

            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0;text-align: center; font-size: 13px; padding-top: 10px">
                <p>Este enlace expira en 30 minutos</p>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 40px;text-align: center; font-size: 14px;">
                <p style="margin:0;">Si no solicitaste el cambio de contraseña ignora este
                    correo y tu contraseña no se modificará¡.</p>
            </td>
        </tr>
    </table>
    `;
  return baseClients(title, content);
}

const resetPasswordSuccess = ({placeName}) => {
  const title = "¡Contraseña actualizada!";
  const content = `
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td
                    style="font-weight: bold; color:#424242;text-align: center; font-size: 20px; font-family: 'OmnesBold', sans-serif;">
                    ¡Contraseña actualizada!
                </td>
            </tr>

            <tr>
                <td align="center" bgcolor="#fff" style="padding-top: 30px; padding-bottom:10px;">
                    <img width='50' src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/Check.png" alt="Creating Email Magic"
                        style="display: block;" />
                </td>
            </tr>
            <tr>
                <td
                    style="padding: 10px 10px;text-align: center; font-size: 14px; padding-bottom: 0;">
                    <p style="margin-top:10px;">Hola ${placeName}.</p>
                    <p style="margin-top:10px;">La contraseña de tu cuenta Reservándonos ha sido actualizada.</p>
                    <p style="margin-top:10px;">Gracias por ser parte de Reservándonos.</p>
                </td>
            </tr>
            <tr>
                <td style="color:#424242;text-align: center; font-size: 13px; padding-top:40px;">
                    <p style="margin:0;">Tú concierge personal</p>
                </td>
            </tr>
        </table>
    `;
  return baseClients(title, content);
}

const base = (title, content) => {
  return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${title}</title>
        <style type="text/css">
            @font-face {
                font-family: 'Omnes';
                font-style: normal;
                font-weight: 400;
                src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-regular-webfont.ttf');
            }
            body {
                font-family: 'Omnes', sans-serif;
            }
        </style>
    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" bgcolor="#faf9f7" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="370" style="border-collapse: collapse;">
                        <!-- header -->
                        <tr>
                            <td align="center" bgcolor="#fff" style="padding: 5px;">
                                <img width='50%' src="https://reservandonos-cdn.s3-us-west-2.amazonaws.com/icons/logo-red-rsv.png" alt="Creating Email Magic" style="display: block;" />

                            </td>
                        </tr>
                        <!-- content -->
                        <tr>
                            <td bgcolor="#ffffff" style="padding: 30px;">
                                ${content}
                            </td>
                        </tr>
                        <!-- footer -->
                        <tr>
                            <td style="padding: 20px 0 20px 0; background-color: #aaaaaa;">
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="370" style="border-collapse: collapse;">
                                    <tr>
                                     <td style="font-size: 10px; text-align: center;">&#169;2020 Reservándonos. Todos los derechos reservados.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    </html>
    `;
}

const baseClients = (title, content) => {
  return `<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${title}</title>
        <style type="text/css">
            * {
                margin: 0;
                padding: 0;
            }
            @font-face {
                font-family: 'Omnes';
                font-style: normal;
                font-weight: 400;
                src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-regular-webfont.ttf');
            }

            @font-face {
                font-family: 'OmnesBold';
                font-style: bold;
                font-weight: 900;
                src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-semibold-webfont.ttf');
            }

            body {
                font-family: 'Omnes', sans-serif;
            }
        </style>
    </head>
    
    <body style="margin: 0; padding: 0;">
        <table border="0" bgcolor="#faf9f7" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                        style="border-collapse: collapse; max-width: 400px;">
                        <!-- header -->
                        <tr>
                            <td align="center" bgcolor="#fff" style="padding: 0;">
                                <img width='150' src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/Logo-Reserv%C3%A1ndonos-1.png" alt="Creating Email Magic"
                                    style="display: block; max-width: 150px; width: 150px;" />
                            </td>
                        </tr>
                        <!-- content -->
                        <tr>
                            <td bgcolor="#ffffff" style="padding: 30px; padding-top: 0;">

                                ${content}
    
                            </td>
                        </tr>
                        <!-- footer -->
                        <tr>
                            <td align="center" bgcolor="#fff" >
                                <img width="400"
                                    src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/Banner-Download-app.png"
                                    alt="Creating Email Magic" style="display: block; max-width: 400px; width: 400px;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    
    </html>
    `;
}

module.exports = {
  confirmEmail,
  newReservation,
  reservationConfirm,
  reservationCancelled,
  invitationRsvPartners,
  reservationConfirmManager,
  reservationCancelledManager,
  cutNotification,
  invoiceNotification,
  notificationPendingPayment,
  reservandonosReConfirmedReservationFromClient,
  resetPassword,
  resetPasswordSuccess
}
