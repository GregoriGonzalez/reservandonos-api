


const title_in_process = (name) => {
  return `
    <p style="font-weight: 400;"> <b>${name},</b> <br> <span style="font-weight: 200;"> tu reservación está en</span> <span style="font-weight: 500;"><b>PROCESO</b></span></p>
    <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/agujas-del-reloj.png" alt="process" width="45">
  `;
};


const title_confirmed = (name) => {
  return `
    <p style="font-weight: 400;"> <b>${name},</b> <br> <span style="font-weight: 200;"> tu reservación ha sido</span> <span style="font-weight: 500;"><b>CONFIRMADA</b></span></p>
    <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/confirmacion.png" alt="process" width="45">
  `;
};

const title_cancelled_restaurant = (name) => {
  return `
  <p style="font-weight: 200; padding: 0 1em;"> <b>${name},</b> <span style="font-weight: 200;"> hemos intentado todo para poder encontrar un lugar en la hora que nos solicitaste, pero el lugar está lleno ¡No hay Disponibilidad!.</span></p>
  <p style="font-weight: 400;"> <b> Tu reserva ha sido <br>  </b><span style="font-weight: 400;"><b>CANCELADA</b></span> </p>
  <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/cancelar.png" alt="process" width="45">
  `;
};

const title_cancelled_customer = (name) => {
  return `
  <p style="font-weight: 400;"> <b>${name},</b> <br> <span style="font-weight: 200;"> tu reservación se </span> <span style="font-weight: 500;"><b>CANCELO</b> con éxito.</span></p>
  <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/cancelar.png" alt="process" width="45">
  `;
};

const info_confirmation = (id) => {
  /* <tr>
    <td><img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/whatsapp.png" alt="whatsapp" width="15"></td>
    <td style="padding-left: 0.4em; padding-bottom: 0.5em; font-size: 0.9em; font-weight: 200;"><span>Si no te hemos notificado por WhatsApp da click <a href="https://wa.me/5215551573310?text=Hola,%20Quiero%20darle%20seguimiento%20a%20mi%20reserva,%20Código:${id}" target="_blank" style="text-decoration: none; color: #e74058;"><b>aquí</b></a>.</span></td>
  </tr> */

  return `
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      <div style="padding-left: 1em; padding-top: 0.5em;">
        <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/recuerda.png" alt="recuerdo" width="12">
        <span style="padding-left: 0.5em; font-weight: 600;">Recuerda que...</span>
      </div>

      <div style="display: inline-flex; width: 100%;">
        <div style="width:70%; margin: 0.6em 0;">
          <table style="margin-left: 1.5em;">
            <tr>
              <td><img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Notificación.png" alt="recuerdo" width="15"></td>
              <td style="padding-left: 0.4em; padding-bottom: 0.5em; font-size: 0.9em; font-weight: 200;"><span>Las notificaiones sobre el estatus de tu reserva llegarán a tráves de whatsapp y correo electrónico.</span></td>
            </tr>

            <tr>
              <td><img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Reloj.png" alt="recuerdo" width="15"></td>
              <td style="padding-left: 0.4em; padding-bottom: 0.5em; font-size: 0.9em; font-weight: 200;"><span>Tienes 15 min de tolerancia.</span></td>
            </tr>

            <tr>
              <td><img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/reserva.png" alt="recuerdo" width="15"></td>
              <td style="padding-left: 0.4em; padding-bottom: 0.5em; font-size: 0.9em; font-weight: 200;"><span>No olvides revisar que los datos de tu reserva estén correctos.</span></td>
            </tr>
          </table>
        </div>

        <div style="width:30%; margin: auto; text-align: center;">
          <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Isotipo%20Reservándonos.png" alt="recuerdo" height="50">
        </div>
      </div>
    </section>
  `;
};

const info_in_process = (objTemplate) => {
  return `
    ${objTemplate.template === 'custom' ?
      '<div style="margin: 0.5em 2em 2.5em 2em; text-align: center;"> <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/advertencia_amarillo.png" alt="advertencia" width="15" height="15" style="margin-right: 1em;"> ' +
      'En este momento no hay personas en el restaurante para poder confirmar tu reservación</div>' : ''}
    <table>
      <tr>
        <td style="width: 50%;">
          <div style="font-weight: 200; border: 1px solid #e6e4e4; text-align: center; width: 90%; margin: auto; font-size: 0.9em;">
            <div style="position: relative; top: -1.5em;">
              <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Reloj.png" alt="reloj" width="35">
            </div>
            <p style="margin-top: 0; margin-bottom: 2.5em;">
              ${objTemplate.template === 'standard' ?
                'En un plazo de 15 min. te notificaremos sobre el estatus de la reserva.' :
                'El horario de confirmación empieza a partir del ' + objTemplate.value
              }
            </p>
          </div>
        </td>
        <td style="width: 50%;">
          <div style="font-weight: 200; border: 1px solid #e6e4e4; text-align: center; width: 90%; margin: auto; font-size: 0.9em;">
            <div style="position: relative; top: -1.5em;">
              <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Concierge.png" alt="concierge" width="35">
            </div>
            <p style="margin-top: 0;">
               Nuestro equipo de concierge esta trabajando para que tu reservación se confirme lo antes posible.
            </p>
          </div>
        </td>
      </tr>
    </table>

    <p style="text-align: center; font-weight: 400;">Recibirás un correo electrónico de confirmación</p>
  `;
};

const downloadMenu = (url) => {
  return `
    <div align="center">
      <img style="margin-bottom: 0.5em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Logo_Coca-Cola.png" alt="reloj" width="45">
      <div>
        <a href="${url}" target="_blank" style="font-weight: 600; background: #e74058; color: white; border: none; padding: 0.6em 0.6em; border-radius: 0.3em; text-decoration: none;" type="button" name="button">Ver menú</a>
      </div>
    </div>
  `;
};

const infoCustomer = (customer, reservation) => {
  return `
    <table style="width: 90%; margin: auto; border: 1px solid #e1dede; border-radius: 0.5em; margin-bottom: 0.5em; margin-top: 1em;">
      <tr>
        <td>
          <img style="position: relative; top: 0.5em; left: 0.5em; padding: 0 1em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Usuario.png" alt="profile" height="80">
        </td>
        <td>
          <ul style="list-style: none; padding-left: 0.4em;">
            <li style="font-weight: 500;">${customer.fullname}</li>
            <li style="font-size: 0.9em; font-weight: 200"><img style="padding-right: 0.2em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/telefono.png" alt="" width="12">${customer.phone}</li>
            <li style="font-size: 0.9em; font-weight: 200"><img style="padding-right: 0.2em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/correo-electronico.png" alt="" width="12">${customer.email}</li>
            <li style="font-size: 0.9em; font-weight: 200"><img style="padding-right: 0.2em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/calendario_reloj.png" alt="" width="12">${reservation.date}</li>
            <li style="font-size: 0.9em; font-weight: 200"><img style="padding-right: 0.2em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/reloj-de-pared.png" alt="" width="12">${reservation.hour} hrs</li>
            <li style="font-size: 0.9em; font-weight: 200"><img style="padding-right: 0.2em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/clientes_red.png" alt="" width="12">${reservation.peopleCount} personas</li>
          </ul>
        </td>
      </tr>
    </table>
  `;
};

const sendWhatsApp = (id) => {
  return `
    <div align="center" style="margin: 0 1em;">
      <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/whatsapp.png" alt="profile" height="30">
      <p style="text-align: center; font-weight: 400; margin: 0; margin-bottom: 1em;">Te hemos enviado un <span style="font-weight: 600; color: #4caf50;">WhatsApp</span> para que puedas monitorear tu reserva en tiempo real</p>
    </div>
  `;
};

const groupWhatsappTelegram = (telegram, whatsApp, name) => {
  return `<section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
        <div style="padding-left: 1em; padding-top: 0.5em;">
          <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Movil.png" alt="movil" width="10">
          <span style="padding-left: 0.5em; font-weight: 600;">Únete a nuestros grupos</span>
        </div>

        <div style="background-color: #ececec; margin: 1em 1em 0 1em; padding: 0.5em 0.5em;">
          <p style="text-align: center; margin-bottom: 0; font-weight: 400; font-size: 0.8em;">Únete a nuestros grupos de whatsapp y telegram en ${name}, y entérate de grandes promociones, descuentos y eventos</p>

          <table style="width: 100%;">
            <tr>
              <td>
                <div style="font-weight:200; text-align: center; color: #ffffff; margin: 0 1em 0.2em 2em; width: 100px;">
                  <img style="margin-top: 0.4em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/telegram.png" alt="reloj" width="30">
                  <a href="${telegram}" style="display: block; color: #ffffff; background-color: #37aee2; text-decoration: none; font-weight: 500; margin: 0; border-radius: 0.3em;" target="_blank">Unirse</a>
                </div>
              </td>
              <td>
                <div style="font-weight:200; text-align: center; color: #ffffff; margin: 0 3em 0em 2.5em; width: 100px;">
                  <img style="margin-top: 0.4em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/whatsapp.png" alt="reloj" width="30">
                  <a href="${whatsApp}" style="display: block; color: #ffffff; background-color: #4caf50; text-decoration: none; font-weight: 500; margin: 0; border-radius: 0.3em;" target="_blank">Unirse</a>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </section>`
};

const rowCancelModidy = (cancelUrl, id) => {
  return `
    <table style="width: 100%;">
      <tr>
        <td>
          <div style="font-weight:200; border: 1px solid #eb3f58; text-align: center; border-radius: 0.2em; margin: 1em 0 0.2em 4em; width: 95px;">
            <img style="margin-top: 0.4em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/cancelar.png" alt="concierge" width="20">
            <a href="${cancelUrl}" style="display: block; color: #eb3f58; text-decoration: none; font-weight: 500; margin: 0;" target="_blank">Cancelar <br> Reserva</a>
          </div>
        </td>
        <td>
          <div style="font-weight:200; border: 1px solid #36a300; text-align: center; color: #36a300; border-radius: 0.2em; margin: 0.8em 3em 0 0em; width: 95px;">
            <img style="margin-top: 0.4em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/modificar.png" alt="reloj" width="20">
            <a href="https://reservandonos.com/gracias-por-tu-reserva/?${id}" style="display: block; color: #36a300; text-decoration: none; font-weight: 500; margin: 0;" target="_blank">Modificar <br> Reserva</a>
          </div>
        </td>
      </tr>
    </table>
  `;
};

const rowCalendarFriends = () => {
  return `
    <table style="width: 100%;">
      <tr>
        <td>
          <div style="font-weight: 200; border: 1px solid #e6e4e4; text-align: center; width: 100px; border-radius: 0.2em; margin: 1em 0 0.2em 4.44em; font-size: 0.9em;">
            <img style="margin-top: -1.5em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Añadir%20a%20calendario.png" alt="agregar" width="35">
            <p style="font-weight: 500; color: #818181;">Añadir a <br> calendario</p>
          </div>
        </td>
        <td>
          <div style="font-weight: 200; border: 1px solid #e6e4e4; text-align: center; width: 100px; border-radius: 0.2em; margin: 0.8em 3em 0 0em; font-size: 0.9em;">
            <img style="margin-top: -1.5em;"  src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Acompañantes.png" alt="reloj" width="50">
            <p style="font-weight: 500; color: #818181;">Avisa a tus <br> acompañantes</p>
          </div>
        </td>
      </tr>
    </table>
  `;
};

const rowCalendarRestaurants = (urlCalendar, urlRestaurants) => {
  return `
    <table style="width: 100%;">
      <tr>
        <td>
          <div style="font-weight: 200; border: 1px solid #e6e4e4; text-align: center; width: 100px; border-radius: 0.2em; margin: 1em 0 0.2em 4.44em; font-size: 0.9em;">
            <img style="margin-top: -1.5em;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/calendario_reloj.png" alt="agregar" width="35">
            <a href="https://reservandonos.com" target="_blank" style="display: inline-block; text-decoration: none; font-weight: 500; color: #818181;">Haz una nueva <br> reserva</a>
          </div>
        </td>
        <td>
          <div style="font-weight: 200; border: 1px solid #e6e4e4; text-align: center; width: 100px; border-radius: 0.2em; margin: 0.8em 3em 0 0em; font-size: 0.9em;">
            <img style="margin-top: -1.5em;"  src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Restaurantes.png" alt="reloj" width="40">
            <a href="https://reservandonos.com/restaurantes/" target="_blank" style="display: inline-block; text-decoration: none; font-weight: 500; color: #818181;">Explora más <br> restaurantes</a>
          </div>
        </td>
      </tr>
    </table>
  `;
};

const bannerSponsorCocaCola = () => {
  return `
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      <br>
      <div align="center" style="margin: 1em;">
        <p style="font-size: 0.9em; font-weight: 600;">Recuerda que al reservar obtienes una Coca-Cola sin Azúcar para ti y tus acompañates</p>
        <a href="https://reservandonos.com" target="_blank">
          <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/Banner-Web-Coca-Cola.png" alt="sponsor" width="300">
        </a>
      </div>
    </section>
    `
};

const googleMapsPosition = (place) => {
  return `
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      <div style="padding-left: 1em; padding-top: 0.5em;">
        <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/ubicacion.png" alt="ubicacion" width="12">
        <span style="padding-left: 0.5em; font-weight: 600;">Ubicación</span>
      </div>

      <div style="display: inline-flex; width: 100%;">
        <div style="width:30%; margin-left: 0.5em; padding: 0.5em;">
            <iframe loading="lazy"
            src="https://www.google.com/maps?q=Carr.%20Tulum-Boca%20Paila,%20Tulum%20Beach,%20km%207,%20Q.R.&amp;output=embed"
            style="border: 1px solid #cdcdcd; border-radius: 0.3em;"
            allowfullscreen=""
            data-rocket-lazyload="fitvidscompatible"
            data-ll-status="loaded"
            width="100%"
            height="100%"
            frameborder="0">
          </iframe>
        </div>
        <div style="width:70%; background: #ececec; margin: 0.6em 0; margin-right: 1em;">
          <ul style="list-style: none; padding-left: 0.4em; text-align: center; margin-bottom:0;">
            <li style="font-weight: 600;">${place.name}</li>
            <br>
            <li style="font-size: 0.9em; font-weight: 200;">${place.address}</li>
            <li style="font-size: 0.9em; font-weight: 200;">${place.phone}</li>
            <!-- <li style="font-size: 0.9em; font-weight: 200;">17:00 - 23:00</li> -->
          </ul>
        </div>
      </div>
    </section>
  `;
};

downloadApp = () => {
  return `
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      <div style="padding-left: 1em; padding-top: 0.5em;">
        <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Movil.png" alt="movil" width="10">
        <span style="padding-left: 0.5em; font-weight: 600;">Descarga nuestra App</span>
      </div>

      <div style="width: 100%; background: #ececec;">
          <div style="width:100%; margin-left: 0; padding: 0.5em; display: inline-flex;">
            <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/app_1.png" alt="app" width="60" height="130">
            <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/app_2.png" alt="app2" width="60" height="130">
            <ul style="list-style: none; padding-left: 0; text-align: center; margin-bottom:0;">
              <li style="font-weight: 600;">Tú concierge personal</li>
              <br>
              <li style="font-size: 0.8em; font-weight:200;">Descubre y reserva en tus restaurantes favoritos</li>
              <table style="margin-top:-0.5em; width: 100%;">
                <tr>
                  <td style="text-align: right;">
                    <a href="https://apps.apple.com/mx/app/reserv%C3%A1ndonos/id1424467304" target="_blank">
                      <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/App-Store.png" alt="app" width="60">
                    </a>
                  </td>
                  <td style="text-align: left;">
                    <a href="https://play.google.com/store/apps/details?id=reservandonos.reservandonos&hl=es&gl=US" target="_blank">
                      <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Play-Store.png" alt="app2" width="60">
                    </a>
                  </td>
                </tr>
              </table>
            </ul>
          </div>
        </div>
    </section>
  `;
};

const socialNetwork = () => {
  return `
    <div align="center">
      <a href="https://www.facebook.com/reservandonos/" target="_blank"> <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/facebook.png" alt="fb" width="30"></a>
      <a href="https://www.instagram.com/reservandonos/?hl=es-la" target="_blank"> <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/instagram.png" alt="intagram" width="30"></a>
      <a href="https://reservandonos.com" target="_blank"> <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/red-mundial.png" alt="red" width="30"></a>
    </div>
  `;
};

const questions = () => {
  return `
    <section style="border: solid 1px #cdcdcd; border-radius: 0.5em; height: auto; margin-bottom: 1em; padding-bottom: 1em;">
      <div style="padding-left: 1em; padding-top: 0.5em;">
        <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/dudas.png" alt="dudas" width="12">
        <span style="padding-left: 0.5em; font-weight: 600;">Dudas y Aclaraciones</span>
      </div>

      <div style="display: inline-flex; width: 100%;">
        <div style="width:70%; margin: 0.6em 0;">
          <table style="margin-left: 1.5em;">
            <tr>
              <td><img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Dudas%20y%20acalaraciones.png" alt="recuerdo" width="15"></td>
              <td style="padding-left: 0.4em; padding-bottom: 0.5em; font-size: 0.9em; font-weight: 200; text-align: justify;">
                <span>
                  Queremos seguir ayudándote a encontrar un lugar ideal para ti. Escribe a nuestro equipo de Concierge y te recomendaremos lugares similares cerca de tu ubicación que si tengan disponibilidad
                  <a style="color: #818181;" href="https://reservandonos.com/comunicate-con-nuestro-equipo-de-concierge/"><b>contactar.</b></a>
                  <br>
                  Gracias por la preferencia, esperamos verte pronto en Reservándonos
                </span>
              </td>
            </tr>
          </table>
        </div>

        <div style="width:30%; margin: auto; text-align: center;">
          <img src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/Isotipo%20Reservándonos.png" alt="recuerdo" height="50">
        </div>
      </div>
    </section>
  `;
};

const styleFonts = () => {
  return `
    <style type="text/css">
      @font-face {
          font-family: 'Omnes';
          font-style: normal;
          font-weight: 600;
          src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-semibold-webfont.ttf');
      }
      @font-face {
          font-family: 'Omnes';
          font-style: normal;
          font-weight: 500;
          src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-medium-webfont.ttf');
      }
      @font-face {
          font-family: 'Omnes';
          font-style: normal;
          font-weight: 400;
          src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-regular-webfont.ttf');
      }
      @font-face {
          font-family: 'Omnes';
          font-style: normal;
          font-weight: 300;
          src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-light-webfont.ttf');
      }
      @font-face {
          font-family: 'Omnes';
          font-style: normal;
          font-weight: 200;
          src: url('https://reservandonos-cdn.s3-us-west-2.amazonaws.com/fonts/omnes-extralight-webfont.ttf');
      }
      body {
          font-family: 'Omnes', sans-serif;
      }
    </style>
  `;
}

const header = (reservation) => {
  return `
    <div style="height: 50px; text-align: center; padding: 0.5em; background: #818181; color: white; border-radius: 0.4em;">
      <p style="font-weight: 500;">Gracias por reservar con Reservándonos</p>
      <table style="position: absolute; background: white; color: black; margin-top: -0.5em; width: 330px; border-radius: 0.3em; border: solid 1px #cdcdcd; border-radius: 0.5em; font-weight: 200; font-size: 0.8em;">
        <tr>
          <td style="padding: 0.5em 0;"><img style="position: absolute;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/calendario.png" width="15px" alt="calendar"> <span style="padding-left: 1.5em"> ${reservation.date} </span></td>
          <td style="padding: 0.5em 0 ;"><img style="position: absolute;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/reloj-de-pared.png" width="15px" alt="reloj"> <span style="padding-left: 1.5em">${reservation.hour} hrs</span> </td>
          <td style="padding: 0.5em 0;"><img style="position: absolute;" src="https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/emails-v.1.0/clientes_red.png" width="15px" alt="person"> <span style="padding-left: 1.5em">${reservation.peopleCount} personas</span> </td>
        </tr>
      </table>
    </div>
  `;
}

module.exports = {
  title_in_process,
  info_in_process,
  downloadMenu,
  infoCustomer,
  sendWhatsApp,
  rowCancelModidy,
  bannerSponsorCocaCola,
  googleMapsPosition,
  socialNetwork,
  styleFonts,
  rowCalendarFriends,
  rowCalendarRestaurants,
  title_confirmed,
  info_confirmation,
  questions,
  header,
  title_cancelled_restaurant,
  title_cancelled_customer,
  groupWhatsappTelegram
}
