const fbPart = facebook => `<td>
    <a href="${facebook}" style="color: #ffffff;">
        <img src="https://reservandonos-cdn.s3-us-west-2.amazonaws.com/icons/icon-facebook.png" alt="Facebook" width="32" height="32" style="display: block;" border="0" />
    </a>
</td>
<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>`;

const instaPart =  instagram => `
    <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
        <a href="${instagram}" style="color: #ffffff;">
            <img src="https://reservandonos-cdn.s3-us-west-2.amazonaws.com/icons/icon-instagram.png" alt="Instagram" width="32" height="32" style="display: block;" border="0" />
        </a>
    </td>
    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
`;

const twitterPart =  twitter => `
    <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
        <a href="${twitter}" style="color: #ffffff;">
            <img src="https://reservandonos-cdn.s3-us-west-2.amazonaws.com/icons/icon-twitter.png" alt="Twitter" width="32" height="32" style="display: block;" border="0" />
        </a>
    </td>
`;

const toggleEmailPart = (place, unsubscribeRes, unsubscribePlace) => `
    <td bgcolor="#FFFFFF" style="padding: 30px;">
    <table style="font-family: Arial, Helvetica, sans-serif;">
        <tr>
            <td style="vertical-align: top;" valign="top">
                <span style="font-size: 10px;">¿No deseas recibir más correos relacionados con esta reserva? Haz click <a href="${unsubscribeRes}">aquí</a></span>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; line-height: 20px;">
                <span style="font-size: 10px;">Si no deseas recibir ningún correo de ${place}, sigue este <a href="${unsubscribePlace}">enlace</a></span>
            </td>
        </tr>
    </table>
    </td>`;


const restrictions = () => `
    <tr>
        <td style="padding-top: 1em; text-align: center; width: 100%;">
            <div style="font-weight:bold;margin-bottom: 10px">Restricciones para promoción de Coca Cola</div>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; width: 100%;">
            Solo aplica para restaurantes participantes
        </td>
    </tr>
    <tr>
        <td style="text-align: center; width: 100%;">
            No aplica para reservas duplicadas con otras plataformas
        </td>
    </tr>
    <tr>
        <td style="text-align: center; width: 100%;">
            Hasta agotar existencias
        </td>
    </tr>
`;

const bannerPart = (bannerLinkTo, bannerImageUrl) => `
<tr>
    <td>
        <span style="font-size: 14px;">Gracias a esta reserva con Coca Cola y Reservándonos tienes acceso a una cuponera digital con grandes promociones. Solo da clic en la imagen</span>
    </td>
</tr>
<tr>
    <td style="padding: 32px 0 32px 0;max-width: 100%;" align="center">
        <a href="${bannerLinkTo}">
            <img src="${bannerImageUrl}"
                alt="promo" style="display: block; width: 100%;">
        </a>
    </td>
</tr>
`;

const areaMenu = (sponsorCocaUrl) => `
  <div style="padding-top: 1em; text-align: center; width: 60%;">
    <img style="max-width: 100%; width: 70px;" src="https://reservandonos-cdn.s3-us-west-2.amazonaws.com/icons/Coca-Cola_logo.png" alt="logo coca cola">
    <div>
      <a href="${sponsorCocaUrl}" type="button"
         style="box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1); background-color: #e43d59;color: white; display: block; font-family: verdana; font-weight: bold; font-size: 1.2em; margin: auto; max-width: 200px; padding: 0.5em; border-radius: 0.5em; text-decoration: none;">
         Descargar Menú
      </a>
    </div>
  </div>
`;

// const themes = [
//     {className: '', name: 'Default', font: 'Omnes', primary: '#eb3f58', accent: '#eb3f58'},
//     {className: 'orange-blue', name: 'Fishers', font: 'Avenir', primary: '#264abc', accent: '#fe8300'},
//     {className: 'brown-gray', name: 'Simons', primary: '#eb3f58'},
//     {className: 'red-brown', name: 'Decrab', primary: '#eb3f58'},
//     {className: 'yellow-red', name: 'Don Capitán', primary: '#eb3f58'},
//     {className: 'black-orange', name: 'Grupo Canarios', primary: '#000000'},
//     {className: 'purple-pink', name: 'Sushiitto', primary: '#6a436d'},
//     {className: 'black-green', name: 'Sushi Roll', primary: '#000000'},
//     {className: 'black-gold', name: 'Península', primary: '#3d3835'},
//     {className: 'black-white', name: 'La Leche', primary: '#000000'},
// ];

// const primaryThemeColor = (className) => {
//     const selectedTheme = themes.find(theme => theme.className == className);
//     const primary = selectedTheme && selectedTheme.primary ? selectedTheme.primary : "#eb3f58";
//     return primary;
// }

const reservationConfirmedString =  ({website, logoImage, place, peopleCount, date, time, name, address, phone, instagram, facebook, twitter, unsubscribeRes, unsubscribePlace, secondaryColor, cancelReservationUrl, bannerLinkTo, bannerImageUrl, sponsorCocaUrl, productSponsor}) => {

        const fb = facebook ? fbPart(facebook): '';

        const insta = instagram ? instaPart(instagram) : '';

        const tw = twitter ? twitterPart(twitter) : '';

        const togglePart = toggleEmailPart(place, unsubscribeRes, unsubscribePlace);

        const color = secondaryColor;//primaryThemeColor(theme);

        const banner = bannerLinkTo ? bannerPart(bannerLinkTo, bannerImageUrl) : '';

        const menuCoca = sponsorCocaUrl ? areaMenu(sponsorCocaUrl) : '';

        const restri = productSponsor ? restrictions() : '';

        const template = `
        <!DOCTYPE html>
        <html lang="es">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Tu reserva está confirmada</title>
        </head>

        <body style="margin: 0; padding: 0;">
            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#F1F2F4">
                <tr>
                    <td>
                        <table align="center" cellpadding="0" cellspacing="0" width="600"
                            style="border-collapse: collapse; margin-bottom: 32px;">
                            <tr style="background-color:${color};">
                                <td style="padding: 32px 0 32px 0;" align="center">
                                    <a href="${website}">
                                        <img src="${logoImage}"
                                            alt="${place}" style="display: block;">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF" style="padding: 30px;">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif;">
                                        <tr>
                                            <td style="text-align: center;vertical-align: top;padding-bottom: 8px;" valign="top" align="center">
                                                <span style="line-height: 36px; text-align: center; font-size: 28px; color: rgb(45, 51, 63);">Reserva confirmada</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;vertical-align: top;padding-bottom: 16px;" valign="top" align="center">
                                                <span style="font-size: 16px;font-weight: normal;color: rgb(45, 51, 63);line-height: 24px;text-align: center;">Gracias por reservar con nosotros</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;vertical-align: top;padding-bottom: 8px;" valign="top" align="center">
                                                <a href="${website}" style="text-decoration: none;color: #DA3743;">
                                                    <span style="font-size: 18px;font-weight: normal;color: rgb(218, 55, 67);line-height: 24px;text-align: center;" >${place}</span>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;vertical-align: top;padding-bottom: 8px;" valign="top" align="center">
                                                <span style="font-size: 16px;color: rgb(45, 51, 63);line-height: 24px;text-align: center;">Mesa para ${peopleCount}, ${date} a las ${time}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: top;" valign="top" align="center">
                                                <span style="font-size: 14px;color: rgb(45, 51, 63);line-height: 24px;text-align: center;">
                                                    <span>A nombre de</span>: <span>${name}</span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;vertical-align: top;padding-bottom: 8px;" valign="top" align="center">
                                                <span style="font-size: 12px;line-height: 17px;text-align: center;">${address}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;text-align: center;vertical-align: top;" valign="top" align="center">
                                                <a href="${website}" style="text-decoration: none;color: #DA3743;">
                                                    <span style="font-size: 14px;font-weight: normal;color: rgb(218, 55, 67);line-height: 24px;text-align: center;" >Visita nuestra página</span>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;text-align: center;vertical-align: top;" valign="top" align="center">
                                                <span style="font-size: 14px;font-weight: normal;line-height: 24px;text-align: center;" >Si deseas modificar tu reserva llámanos al ${phone}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;text-align: center;vertical-align: top;" valign="top" align="center">
                                                <a href="${cancelReservationUrl}" style="text-decoration: none;color: #DA3743;">
                                                    <span style="font-size: 14px;font-weight: normal;color: rgb(218, 55, 67);line-height: 24px;text-align: center;" >Cancelar reserva</span>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            ${menuCoca}
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 32px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF" style="padding: 30px;">
                                    <table style="font-family: Arial, Helvetica, sans-serif;">
                                        <tr>
                                            <td style="vertical-align: top;" valign="top">
                                                <span style="font-size: 18px;text-align: left;color: rgb(35, 123, 152);line-height: 24px;">Información importante</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; line-height: 20px;">
                                                <span style="font-size: 14px;">Solo cuenta con 10 minutos para conservar su reserva. Si va con un mayor tiempo de atraso, por favor llámenos.<br>Es posible que lo contactemos por esta reservación, por lo que le pedimos que su correo electrónico y número de teléfono estén actualizados</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 32px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF" style="padding: 30px;" >
                                    <table style="font-family: Arial, Helvetica, sans-serif;">
                                        <tr>
                                            <td style="font-size: 14px;" width="75%">¿Dudas? Llámanos al ${phone}</td>
                                            <td align="right" width="25%">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        ${fb}
                                                        ${insta}
                                                        ${tw}
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        ${restri}
                                    </table>
                                </td>
                            </tr>
                            ${banner}
                            <tr>
                                ${togglePart}
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>

        </html>
        `;
        return template
    }

    module.exports = {
        reservationConfirmedString,
    }
